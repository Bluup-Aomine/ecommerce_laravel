<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* Routes Auth */
Route::group(['middleware' => ['web']], function () {
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
});

Route::group(['prefix' => 'posts'], function() {
    Route::post('/product/add', 'ProductController@store')->name('add.product');
    Route::delete('/product/delete/{product}', 'ProductController@destroy')->name('add.delete');
});

/* Routes Image Component */
Route::post('image-product/delete/json', 'Admin\ImageProductController@destroyJSON');
Route::post('image-product/add/json', 'Admin\ImageProductController@store');
Route::post('image-product/delete/files', 'Admin\ImageProductController@deleteFiles');
/* Routes Product Controller */
Route::post('product/delete', 'ProductController@deleteJSON');
/* Routes ColorSize Controller */
Route::post('colorSize/delete', 'Admin\ColorSizeProductController@deleteJSON');
/* Routes Collections */
Route::post('collections/modal', 'Admin\ColorSizeProductController@apiColorSize');
/* Routes orders */
Route::post('orders/payment/update', 'CheckoutController@updateAmountPaymentJSON');
