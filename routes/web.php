<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');
//Route::get('/home', 'ProductWishlistController@array');
Route::get('/my-account', 'AccountController@index')->name('my-account');
Route::post('/my-account/details', 'AccountController@postAccount')->name('account.details');
Route::post('/my-account/passwords', 'AccountController@changePassword')->name('account.passwords');

Auth::routes();
Route::post('/my-account', 'Auth\RegisterController@register')->name('register');

Route::resource('product', 'ProductController');
Route::get('collections/{category}', 'ProductController@collections')->name('collections.index');
Route::get('collections', 'CategoryController@listening')->name('collections');
Route::get('products/category/{category}', 'ProductController@listeningCategories')->name('listening.products.categories');

Route::get('/admin', 'Admin\AdminController@index')->name('admin');
Route::resource('image-product', 'Admin\ImageProductController');
Route::resource('category', 'CategoryController');

Route::get('colors-sizes-product/create/{product}', 'Admin\ColorSizeProductController@create')->name('colors-sizes.create');
Route::post('colors-sizes-product/create/{product}', 'Admin\ColorSizeProductController@store')->name('colors-sizes.store');
Route::get('colors-sizes-product/create/{product}/edit/{colorSizeProduct}', 'Admin\ColorSizeProductController@edit')->name('colors-sizes.edit');
Route::put('colors-sizes-product/create/{product}/edit/{colorSizeProduct}', 'Admin\ColorSizeProductController@update')->name('colors-sizes.update');

/*
Route::get('product-filter/{slug}', 'ProductFilterController@category')->name('product-filter.category');
Route::get('product-filter/filter/clear', 'ProductFilterController@clear')->name('product-filter.clear');
Route::get('product-filter/filter/clear/{filter}', 'ProductFilterController@clearByFilter')->name('product-filter.clear.filter');
Route::get('product-filter/filter/{index}', 'ProductFilterController@page')->name('product-filter.page');
Route::get('product-filter/filter/size/{size}', 'ProductFilterController@size')->name('product-filter.size');
Route::get('product-filter/filter/color/{color}', 'ProductFilterController@color')->name('product-filter.color');
Route::get('product-filter/filter/price/{min}/{max}', 'ProductFilterController@price')
    ->where(['min' => '[0-9]+', 'max' => '[0-9]+'])
    ->name('product-filter.price');

Route::get('product-filter/filter/selected/popularity', 'ProductFilterController@popularity')->name('product-filter.popularity');
Route::get('product-filter/filter/selected/date', 'ProductFilterController@date')->name('product-filter.date');
Route::get('product-filter/filter/selected/price-asc', 'ProductFilterController@priceAsc')->name('product-filter.price-asc');
Route::get('product-filter/filter/selected/price-desc', 'ProductFilterController@priceDesc')->name('product-filter.price-desc');
*/

/* Routes Basket And Wishlist */
Route::resource('products/wishlist', 'ProductWishlistController')->names('product.wishlist');
Route::resource('products/basket', 'BasketProductController')->names('product.basket');
Route::get('products/session/basket', 'SessionBasketController@basket')->name('product.basket.session');
Route::put('products/session/basket/update', 'SessionBasketController@update')->name('product.basket.update.session');

Route::post('product/wishlist/add', 'ProductWishlistController@add')->name('product.wishlist.add');
Route::get('product/basket/load', 'BasketProductController@loadProductsBasket')->name('product.basket.load');
Route::delete('product/basket/delete/{basketsProduct}', 'BasketProductController@delete')->name('product.basket.delete');
Route::delete('product/wishlist/delete/{wishlistsProductID}', 'ProductWishlistController@delete')->name('product.wishlist.delete');

/* Routes Checkout */
Route::get('checkout', 'BasketController@checkout')->name('checkout');
Route::post('checkout/cart', 'CheckoutController@store')->name('checkout.store');
Route::post('checkout/customer', 'CheckoutController@createCustomer')->name('checkout.customer');

/* Payment Paypal */
Route::post('payment/paypal', 'PaypalPaymentController@index')->name('paypal.payment');
Route::post('payment/paypal/sale', 'PaypalPaymentController@payment')->name('paypal.payment.sale');

Route::post('products/search', 'ProductController@search')->name('search');
Route::get('products/search/{search}', 'ProductController@searchGET')->name('search.products');
Route::get('contact', 'ContactController@contact')->name('contact');
Route::post('contact/post', 'ContactController@post')->name('contact.post');

/* Routes Products */
Route::get('admin/products', 'ProductController@listening')->name('listening.products');

/* Routes Orders And Customers */
Route::resource('/admin/orders', 'OrderController')->names('orders');
Route::resource('/admin/customers', 'CustomerController')->names('customers');

/* Routes Orders And Customers for Paypal */
Route::resource('/admin/paypal/orders', 'OrderPaypalController')->names('orders.paypal');
Route::resource('/admin/paypal/customers', 'CustomerPaypalController')->names('customers.paypal');

/* Invoice Orders */
Route::get('/invoice/{order}', 'OrderController@invoice')->name('invoice.order');
Route::get('/invoice/paypal/{order}', 'OrderPaypalController@invoice')->name('invoice.paypal.order');
/* Livraison/Retour Routes */
Route::get('/delivery', 'Pages\DeliveryController@index')->name('delivery');

/* ROUTES PAGES */
Route::get('/CGV', 'Pages\PageController@CGV')->name('CGV');

/* Concept Routes */
Route::get('/concept', 'Pages\ConceptController@index')->name('concept');
/* PAGE 404 Routes */
Route::get('/404', 'Pages\PageController@errorCode404')->name('404');
