<?php

namespace Tests\Unit\Auth;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserInitTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function initializedUsers()
    {
        factory(User::class, 10)->create();
    }
}
