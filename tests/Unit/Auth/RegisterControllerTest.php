<?php

namespace Tests\Unit\Auth;

use App\User;
use Tests\Unit\Auth\UserInitTest;

class RegisterControllerTest extends UserInitTest
{
    public function testCreateUser()
    {
        $this->initializedUsers();

        $this->post(route('register'), [
            'name' => 'Bissboss23',
            'email' => 'vincent-capek@laposte.net',
            'password' => 'testdetest',
            'password_confirm' => 'testdetest'
        ]);

        $users = User::count();

        $this->assertSame(11, (int)$users);
        $this->assertSame('Bissboss23', User::find(11)->name);
    }

    public function testNotCreateUserPasswordNotIdentical()
    {
        $this->initializedUsers();

        $this->post(route('register'), [
            'name' => 'Bissboss23',
            'email' => 'vincent-capek@laposte.net',
            'password' => 'testdetest',
            'password_confirm' => 'testdetestadadaddada'
        ]);

        $users = User::count();

        $this->assertSame(10, (int)$users);
        $this->assertNull(User::find(11));
    }

    public function testNotCreateUserPasswordConfirmNotIsset()
    {
        $this->initializedUsers();

        $this->post(route('register'), [
            'name' => 'Bissboss23',
            'email' => 'vincent-capek@laposte.net',
            'password' => 'testdetest'
        ]);

        $users = User::count();

        $this->assertSame(10, (int)$users);
        $this->assertNull(User::find(11));
    }
}
