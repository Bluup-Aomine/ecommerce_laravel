<?php

namespace Tests\src\Validator;

use App\src\Validator\RegisterValidator;
use App\User;
use Tests\Unit\Auth\UserInitTest;

class RegisterValidatorTest extends UserInitTest
{
    public function testValidateValidator()
    {
        $this->initializedUsers();

        $data = [
            'name' => 'loaddddddddddddddddddal',
            'email' => 'vincentcapek@laposte.net',
            'password' => 'damien13',
            'password_confirm' => 'damien13'
        ];

        $this->post(route('register'), $data);
        $validator = RegisterValidator::make($data);

        $users = User::count();

        $this->assertNotNull(User::find(11));
        $this->assertEquals(11, $users);
        $this->assertEmpty($validator->errors()->messages());
    }

    public function testRegisterFieldUsernameTooShort()
    {
        $this->initializedUsers();

        $data = [
            'name' => 'ff',
            'email' => 'vincentcapek@laposte.net',
            'password' => 'ldlladladldla'
        ];

        $this->post(route('register'), $data);
        $validator = RegisterValidator::make($data);

        $users = User::count();

        $this->assertNull(User::find(11));
        $this->assertEquals(10, $users);
        $this->assertNotEmpty($validator->errors()->messages());
        $this->assertSame('Le champs username doit contenir au minimum 3 caractères', current($validator->errors()->messages()['name']));
    }

    public function testRegisterFieldUsernameTooLong()
    {
        $this->initializedUsers();

        $data = [
            'name' => 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker',
            'email' => 'vincentcapek@laposte.net',
            'password' => 'ldlladladldla'
        ];

        $this->post(route('register'), $data);
        $validator = RegisterValidator::make($data);

        $users = User::count();

        $this->assertNull(User::find(11));
        $this->assertEquals(10, $users);
        $this->assertNotEmpty($validator->errors()->messages());
        $this->assertSame('Le champs username doit contenir au maximum 100 caractères', current($validator->errors()->messages()['name']));
    }

    public function testRegisterFieldUsernameEmpty()
    {
        $this->initializedUsers();

        $data = [
            'name' => '',
            'email' => 'vincentcapek@laposte.net',
            'password' => 'ldlladladldla'
        ];

        $this->post(route('register'), $data);
        $validator = RegisterValidator::make($data);

        $users = User::count();

        $this->assertNull(User::find(11));
        $this->assertEquals(10, $users);
        $this->assertNotEmpty($validator->errors()->messages());
        $this->assertSame('Le champs username doit être requis', current($validator->errors()->messages()['name']));
    }

    public function testRegisterFieldEmailEmpty()
    {
        $this->initializedUsers();

        $data = [
            'name' => 'daaaaaaaaaaaaaaaaaaaaaaa',
            'email' => '',
            'password' => 'ldlladladldla'
        ];

        $this->post(route('register'), $data);
        $validator = RegisterValidator::make($data);

        $users = User::count();

        $this->assertNull(User::find(11));
        $this->assertEquals(10, $users);
        $this->assertNotEmpty($validator->errors()->messages());
        $this->assertSame('Le champs email doit être requis', current($validator->errors()->messages()['email']));
    }

    public function testRegisterFieldPasswordEmpty()
    {
        $this->initializedUsers();

        $data = [
            'name' => 'daaaaaaaaaaaaaaaaaaaaaaa',
            'email' => 'Le champs email doit être requis',
            'password' => ''
        ];

        $this->post(route('register'), $data);
        $validator = RegisterValidator::make($data);

        $users = User::count();

        $this->assertNull(User::find(11));
        $this->assertEquals(10, $users);
        $this->assertNotEmpty($validator->errors()->messages());
        $this->assertSame('Le champs password doit être requis', current($validator->errors()->messages()['password']));
    }

    public function testRegisterFieldPasswordTooShort()
    {
        $this->initializedUsers();

        $data = [
            'name' => 'daaaaaaaaaaaaaaaaaaaaaaa',
            'email' => 'Le champs email doit être requis',
            'password' => 'ddd'
        ];

        $this->post(route('register'), $data);
        $validator = RegisterValidator::make($data);

        $users = User::count();

        $this->assertNull(User::find(11));
        $this->assertEquals(10, $users);
        $this->assertNotEmpty($validator->errors()->messages());
        $this->assertSame('Le champs password doit contenir au minimum 5 caractères', current($validator->errors()->messages()['password']));
    }

    public function testRegisterFieldPasswordConfirmEmpty()
    {
        $this->initializedUsers();

        $data = [
            'name' => 'daaaaaaaaaaaaaaaaaaaaaaa',
            'email' => 'Le champs email doit être requis',
            'password' => 'ddddddd',
            'password_confirm' => ''
        ];

        $this->post(route('register'), $data);
        $validator = RegisterValidator::make($data);

        $users = User::count();

        $this->assertNull(User::find(11));
        $this->assertEquals(10, $users);
        $this->assertNotEmpty($validator->errors()->messages());
        $this->assertSame('Le champs password confirmation doit être requis', current($validator->errors()->messages()['password_confirm']));
    }

    public function testRegisterFieldPasswordConfirmTooShort()
    {
        $this->initializedUsers();

        $data = [
            'name' => 'daaaaaaaaaaaaaaaaaaaaaaa',
            'email' => 'Le champs email doit être requis',
            'password' => 'dddddd',
            'password_confirm' => 'ddd'
        ];

        $this->post(route('register'), $data);
        $validator = RegisterValidator::make($data);

        $users = User::count();

        $this->assertNull(User::find(11));
        $this->assertEquals(10, $users);
        $this->assertNotEmpty($validator->errors()->messages());
        $this->assertSame('Le champs password confirmation doit contenir au minimum 5 caractères', current($validator->errors()->messages()['password_confirm']));
    }
}
