<?php

namespace Tests\Unit\Auth\Account;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ChangePasswordsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testInvalidDataPasswordsChange()
    {
        $this->setUser();
        $data = [
            'password' => 'mon mot de passe',
            'password_new' => '',
            'password_new_confirmation' => 'dadadead'
        ];
        $request = $this->post(route('account.passwords'), $data);
        $request->assertStatus(302);
        $request->assertSessionHas('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');

        $user = $this->getUser();
        $passwordBool = password_verify($data['password'], $user->password);
        $this->assertEquals(false, $passwordBool);
    }

    public function testInvalidPassword()
    {
        $this->setUser();
        $data = [
            'password' => 'mon mot de passe',
            'password_new' => 'addadadad',
            'password_new_confirmation' => 'dadadead'
        ];
        $request = $this->post(route('account.passwords'), $data);

        $request->assertStatus(302);
        $request->assertSessionHas('messageError', 'Votre mot de passe inscrit dans le formulaire n\'est pas identique à celui de notre base de donnée !!!');

        $user = $this->getUser();
        $passwordBool = password_verify($data['password'], $user->password);
        $this->assertEquals(false, $passwordBool);
    }

    public function testInvalidEqualNewPasswords()
    {
        $this->setUser();
        $data = [
            'password' => 'john password',
            'password_new' => 'addadadad',
            'password_new_confirmation' => 'dadadead'
        ];
        $request = $this->post(route('account.passwords'), $data);

        $request->assertStatus(302);
        $request->assertSessionHas('messageError', 'Votre nouveau mot de passe doit être égale au mot de passe confirmation !!!');

        $user = $this->getUser();
        $passwordBool = password_verify($data['password_new'], $user->password);
        $this->assertEquals(false, $passwordBool);
    }

    public function testChangePasswords()
    {
        $this->setUser();
        $data = [
            'password' => 'john password',
            'password_new' => 'new mot de passe',
            'password_new_confirmation' => 'new mot de passe'
        ];
        $request = $this->post(route('account.passwords'), $data);

        $request->assertStatus(302);
        $request->assertSessionHas('message', 'Votre mot de passe à bien était modifié !!!');

        $user = $this->getUser();
        $passwordBool = password_verify($data['password_new'], $user->password);
        $this->assertEquals(true, $passwordBool);
    }

    private function setUser()
    {
        User::create([
            'name' => 'John',
            'email' => 'john@laposte.net',
            'password' => Hash::make('john password'),
            'role' => 'user'
        ]);
        $this->actingAs($this->getUser());
    }

    /**
     * @return User
     */
    private function getUser(): User
    {
        return User::find(1);
    }
}
