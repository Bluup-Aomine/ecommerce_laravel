<?php

namespace Tests\Unit\Auth\Account;

use App\Account;
use App\src\Validator\AccountDetailsValidator;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostAccountDetailsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testPostAccountDetails()
    {
        $this->createUser();
        $data = [
            'firstname' => 'John',
            'lastname' => 'Doe',
            'username' => 'BluupA0mine',
            'email' => 'vincentcapek@laposte.net'
        ];
        $request = $this->post(route('account.details'), $data);
        $getMock = $this->getMockBuilder(AccountDetailsValidator::class)->getMock();
        $getMock->expects($this->any())
            ->method('make');

        $request->assertSessionHas('message', 'Vos information ont bien était modifié !!!');
        $request->assertStatus(302);

        $user = $this->getUser();
        $account = $user->account()->first();
        $this->assertEquals('John', $account->firstname);
        $this->assertEquals('Doe', $account->lastname);
        $this->assertEquals('BluupA0mine', $user->name);
        $this->assertEquals('vincentcapek@laposte.net', $user->email);
    }

    public function testPostAccountDetailsWithMethodUpdate()
    {
        $this->createUser();
        $this->createAccount();
        $data = [
            'firstname' => 'John',
            'lastname' => 'Doe',
            'username' => 'BluupA0mine',
            'email' => 'vincentcapek@laposte.net'
        ];
        $request = $this->post(route('account.details'), $data);
        $getMock = $this->getMockBuilder(AccountDetailsValidator::class)->getMock();
        $getMock->expects($this->any())
            ->method('make');

        $request->assertSessionHas('message', 'Vos information ont bien était modifié !!!');
        $request->assertStatus(302);

        $user = $this->getUser();
        $account = $user->account()->first();
        $this->assertEquals('John', $account->firstname);
        $this->assertEquals('Doe', $account->lastname);
        $this->assertEquals('BluupA0mine', $user->name);
        $this->assertEquals('vincentcapek@laposte.net', $user->email);
    }

    public function testPostAccountInvalidData()
    {
        $this->createUser();
        $data = [
            'firstname' => 'John',
            'lastname' => 'Doe',
            'username' => 'd',
            'email' => ''
        ];
        $request = $this->post(route('account.details'), $data);
        $getMock = $this->getMockBuilder(AccountDetailsValidator::class)->getMock();
        $getMock->expects($this->any())
            ->method('make');

        $request->assertSessionHas('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        $request->assertStatus(302);

        $user = $this->getUser();
        $account = $user->account()->first();
        $this->assertNull($account);
        $this->assertEquals('John', $user->name);
        $this->assertEquals('John@laposte.net', $user->email);
    }

    private function createUser()
    {
        User::create([
            'name' => 'John',
            'email' => 'John@laposte.net',
            'password' => 'john',
            'user' => 'user'
        ]);
        $this->actingAs($this->getUser());
    }

    private function createAccount()
    {
        Account::create([
            'firstname' => 'Johndadad',
            'lastname' => 'Doeaddad',
            'user_id' => $this->getUser()->id
        ]);
    }

    /**
     * @return User
     */
    private function getUser(): User
    {
        return User::find(1);
    }
}
