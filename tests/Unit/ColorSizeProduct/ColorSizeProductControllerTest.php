<?php

namespace Tests\Unit\ColorSizeProduct;

use App\ColorSizeProduct;
use Tests\Unit\Product\ProductInitTest;

class ColorSizeProductControllerTest extends ProductInitTest
{
    public function testSuccessStoreMethod()
    {
        $this->initializedProductsAndUserDefault('admin');
        $product = $this->initializedProductWithColorAndSize();

        $request = $this->post(route('colors-sizes.store', [$product]), [
            'colors' => 'blue',
            'sizes' => 'xl',
            'quantity' => 50
        ]);

        $builder = $this->getMockBuilder(ColorSizeProduct::class)->getMock();
        $builder->expects($this->any())
            ->method('add')
            ->with($request, $product)
            ->willReturn(true);

        $count = ColorSizeProduct::count();
        $colorSize = ColorSizeProduct::find(1);

        $this->assertEquals(1, $count);
        $this->assertEquals('blue', $colorSize->color);
        $this->assertEquals('xl', $colorSize->size);
        $this->assertNotNull($colorSize);
        $request->assertSessionHas('message', 'Vous avez ajouté une nouvelle caractéristique sur votre produit avec succès !!!');
        $request->assertStatus(302);
    }

    public function testNoSuccessStoreMethod()
    {
        $this->initializedProductsAndUserDefault('admin');
        $product = $this->initializedProductWithColorAndSize();

        $request = $this->post(route('colors-sizes.store', [$product]), [
            'colors' => '',
            'sizes' => '',
            'quantity' => null
        ]);

        $builder = $this->getMockBuilder(ColorSizeProduct::class)->getMock();
        $builder->expects($this->never())
            ->method('add')
            ->with($request, $product)
            ->willReturn(false);

        $count = ColorSizeProduct::count();
        $colorSize = ColorSizeProduct::find(1);

        $this->assertEquals(0, $count);
        $this->assertNull($colorSize);
        $request->assertSessionHas('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        $request->assertStatus(302);
    }

    public function testNoSuccessStoreMethodSizesAndColorsNotExistsInProduct()
    {
        $this->initializedProductsAndUserDefault('admin');
        $product = $this->initializedProductWithColorAndSize();

        $request = $this->post(route('colors-sizes.store', [$product]), [
            'colors' => 'addadaad',
            'sizes' => 'dadad',
            'quantity' => 50
        ]);

        $builder = $this->getMockBuilder(ColorSizeProduct::class)->getMock();
        $builder->expects($this->any())
            ->method('add')
            ->with($request, $product)
            ->willReturn(false);

        $count = ColorSizeProduct::count();
        $colorSize = ColorSizeProduct::find(1);

        $this->assertEquals(0, $count);
        $this->assertNull($colorSize);
        $request->assertSessionHas('messageError', 'Impossible de continuer la requête puisque vos caractéristiques ne correspond pas à ceux choisis dans votre produit !!!');
        $request->assertStatus(302);
    }
}
