<?php

namespace Tests\Unit\ColorSizeProduct;

use App\ColorSizeProduct;
use Tests\Unit\Product\ProductInitTest;

class ColorSizeProductValidateTest extends ProductInitTest
{
    public function testValidateDataWithAddColorSizeProductMethod()
    {
        $this->initializedProductsAndUserDefault('admin');
        $product = $this->initializedProductWithColorAndSize();

        $response = $this->post(route('colors-sizes.store', [$product]), [
            'colors' => 'blue',
            'sizes' => 'xl',
            'quantity' => 50
        ]);

        $count = ColorSizeProduct::count();
        $ColorSizeProduct = ColorSizeProduct::find(1);

        $this->assertStringContainsString('blue', $product->colors);
        $this->assertStringContainsString('xl', $product->sizes);
        $this->assertEquals(1, $count);
        $this->assertNotNull($ColorSizeProduct);
        $response->assertSessionHas('message', 'Vous avez ajouté une nouvelle caractéristique sur votre produit avec succès !!!');
        $response->assertStatus(302);
    }

    public function testNoValidateFieldColorsNoRequired()
    {
        $this->initializedProductsAndUserDefault('admin');
        $product = $this->initializedProductWithColorAndSize();

        $response = $this->post(route('colors-sizes.store', [$product]), [
            'colors' => '',
            'sizes' => 'xl',
            'quantity' => 50
        ]);

        $count = ColorSizeProduct::count();
        $ColorSizeProduct = ColorSizeProduct::find(1);

        $errors = session('errors');
        $this->assertStringContainsString('', $product->colors);
        $this->assertStringContainsString('xl', $product->sizes);
        $this->assertEquals(0, $count);
        $this->assertNull($ColorSizeProduct);
        $response->assertSessionHas('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        $this->assertEquals('Le champs couleur doit être requis', $errors->get('colors')[0]);
        $response->assertStatus(302);
    }

    public function testNoValidateFieldSizesNoRequired()
    {
        $this->initializedProductsAndUserDefault('admin');
        $product = $this->initializedProductWithColorAndSize();

        $response = $this->post(route('colors-sizes.store', [$product]), [
            'colors' => 'blue',
            'sizes' => '',
            'quantity' => 50
        ]);

        $count = ColorSizeProduct::count();
        $ColorSizeProduct = ColorSizeProduct::find(1);

        $errors = session('errors');
        $this->assertStringContainsString('blue', $product->colors);
        $this->assertStringContainsString('', $product->sizes);
        $this->assertEquals(0, $count);
        $this->assertNull($ColorSizeProduct);
        $response->assertSessionHas('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        $this->assertEquals('Le champs taille doit être requis', $errors->get('sizes')[0]);
        $response->assertStatus(302);
    }

    public function testNoValidateFieldQuantityNoRequired()
    {
        $this->initializedProductsAndUserDefault('admin');
        $product = $this->initializedProductWithColorAndSize();

        $response = $this->post(route('colors-sizes.store', [$product]), [
            'colors' => 'blue',
            'sizes' => 'xl',
            'quantity' => ''
        ]);

        $count = ColorSizeProduct::count();
        $ColorSizeProduct = ColorSizeProduct::find(1);

        $errors = session('errors');
        $this->assertStringContainsString('blue', $product->colors);
        $this->assertStringContainsString('xl', $product->sizes);
        $this->assertEquals(0, $count);
        $this->assertNull($ColorSizeProduct);
        $response->assertSessionHas('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        $this->assertEquals('Le champs quantité doit être requis', $errors->get('quantity')[0]);
        $response->assertStatus(302);
    }

    public function testNoValidateFieldQuantityNoInteger()
    {
        $this->initializedProductsAndUserDefault('admin');
        $product = $this->initializedProductWithColorAndSize();

        $response = $this->post(route('colors-sizes.store', [$product]), [
            'colors' => 'blue',
            'sizes' => 'xl',
            'quantity' => false
        ]);

        $count = ColorSizeProduct::count();
        $ColorSizeProduct = ColorSizeProduct::find(1);

        $errors = session('errors');
        $this->assertStringContainsString('blue', $product->colors);
        $this->assertStringContainsString('xl', $product->sizes);
        $this->assertEquals(0, $count);
        $this->assertNull($ColorSizeProduct);
        $response->assertSessionHas('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        $this->assertEquals('Le champs quantité doit être un nombre', $errors->get('quantity')[0]);
        $response->assertStatus(302);
    }
}

