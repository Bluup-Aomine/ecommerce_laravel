<?php

namespace Tests\Unit\Checkout;

use App\Basket;
use App\Order;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Tests\TestCase;

class CheckoutValidateRouteCustomerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testSuccessValidate()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => 'Bissboss',
            'email' => 'vincent-capek@laposte.net',
            'phone' => '0122334455',
            'address' => '5 places du Moulin',
            'country' => 'FR',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());

        $this->assertTrue($response->success);
    }

    public function testErrorValidateNameNotRequired()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => '',
            'email' => 'vincent-capek@laposte.net',
            'phone' => '0122334455',
            'address' => '5 places du Moulin',
            'country' => 'FR',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());
        $messages = $response->messages;

        $this->assertFalse($response->success);
        $this->assertEquals('Le champs name doit être requis', $messages->name[0]);
    }

    public function testErrorValidateToShortName()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => 'ddd',
            'email' => 'vincent-capek@laposte.net',
            'phone' => '0122334455',
            'address' => '5 places du Moulin',
            'country' => 'FR',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());
        $messages = $response->messages;

        $this->assertFalse($response->success);
        $this->assertEquals('Le champs name doit contenir au minimum 5 caractères', $messages->name[0]);
    }

    public function testErrorValidateToLongName()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.',
            'email' => 'vincent-capek@laposte.net',
            'phone' => '0122334455',
            'address' => '5 places du Moulin',
            'country' => 'FR',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());
        $messages = $response->messages;

        $this->assertFalse($response->success);
        $this->assertEquals('Le champs name doit contenir au maximum 60 caractères', $messages->name[0]);
    }

    public function testErrorNotRequiredEmail()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => 'Bissboss',
            'email' => '',
            'phone' => '0122334455',
            'address' => '5 places du Moulin',
            'country' => 'FR',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());
        $messages = $response->messages;

        $this->assertFalse($response->success);
        $this->assertEquals('Le champs email doit être requis', $messages->email[0]);
    }

    public function testErrorToShortEmail()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => 'Bissboss',
            'email' => 'dad',
            'phone' => '0122334455',
            'address' => '5 places du Moulin',
            'country' => 'FR',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());
        $messages = $response->messages;

        $this->assertFalse($response->success);
        $this->assertEquals('Le champs email doit contenir au minimum 5 caractères', $messages->email[0]);
    }

    public function testErrorNotTypeEmail()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => 'Bissboss',
            'email' => 'daddadadadadda',
            'phone' => '0122334455',
            'address' => '5 places du Moulin',
            'country' => 'FR',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());
        $messages = $response->messages;

        $this->assertFalse($response->success);
        $this->assertEquals('Le champs email doit être une addresse email', $messages->email[0]);
    }

    public function testPhoneNotRequired()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => 'Bissboss',
            'email' => 'vincent-capek@laposte.net',
            'phone' => '',
            'address' => '5 places du Moulin',
            'country' => 'FR',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());
        $messages = $response->messages;

        $this->assertFalse($response->success);
        $this->assertEquals('Le champs phone doit être requis', $messages->phone[0]);
    }

    public function testPhoneNotTypeTel()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => 'Bissboss',
            'email' => 'vincent-capek@laposte.net',
            'phone' => 0544666666666666,
            'address' => '5 places du Moulin',
            'country' => 'FR',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());
        $messages = $response->messages;

        $this->assertFalse($response->success);
        $this->assertEquals('Le champs phone doit être un numéro de téléphonique', $messages->phone[0]);
    }

    public function testAddressNotRequired()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => 'Bissboss',
            'email' => 'vincent-capek@laposte.net',
            'phone' => 0544666666666666,
            'address' => '',
            'country' => 'FR',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());
        $messages = $response->messages;

        $this->assertFalse($response->success);
        $this->assertEquals('Le champs address doit être requis', $messages->address[0]);
    }

    public function testAddressTooShort()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => 'Bissboss',
            'email' => 'vincent-capek@laposte.net',
            'phone' => 0544666666666666,
            'address' => 'dada',
            'country' => 'FR',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());
        $messages = $response->messages;

        $this->assertFalse($response->success);
        $this->assertEquals('Le champs address doit contenir au minimun 5 caractères', $messages->address[0]);
    }

    public function testCountryNotRequired()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => 'Bissboss',
            'email' => 'vincent-capek@laposte.net',
            'phone' => 0544666666666666,
            'address' => 'dada',
            'country' => '',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());
        $messages = $response->messages;

        $this->assertFalse($response->success);
        $this->assertEquals('Le champs country doit être requis', $messages->country[0]);
    }

    public function testCountryNotGoodFormat()
    {
        $this->createUser();
        $paymentIntent = $this->setPaymentIntent();
        $this->createOrder($paymentIntent);
        $data = [
            'name' => 'Bissboss',
            'email' => 'vincent-capek@laposte.net',
            'phone' => 0544666666666666,
            'address' => 'dada',
            'country' => 'DADADAD',
            'order_notes' => 'Je suis content de vos différents produits'
        ];
        $request = $this->post(route('checkout.customer'), $data);
        $response = \GuzzleHttp\json_decode($request->getContent());
        $messages = $response->messages;

        $this->assertFalse($response->success);
        $this->assertEquals('Le champs country doit comporter un code ISO3166-A2', $messages->country[0]);
    }

    /**
     * @param $paymentIntent
     */
    private function createOrder($paymentIntent)
    {
        $order = new Order();
        $order->payment_indent_id = $paymentIntent->id;
        $order->amount = 1000;
        $order->payment_method = 'visa';
        $order->payment_indent_created = date('Y-m-d H:i:s');
        $order->order_notes = '';
        $order->products = '';
        $order->user_id = 1;
        $order->save();
    }

    private function createUser()
    {
        $user = User::create([
            'name' => 'Jean',
            'email' => 'jean@live.com',
            'password' => 'jean',
            'role' => 'user'
        ]);
        $this->actingAs($user);
    }

    private function setPaymentIntent()
    {
        Stripe::setApiKey('sk_test_BJKdV08hRhmEPEXqwVS6CK2x00DuIb1Cu4');
        return PaymentIntent::create([
            'amount' => round(50) . '00',
            'currency' => 'eur',
            // Verify your integration in this guide by including this parameter
            'metadata' => ['integration_check' => 'accept_a_payment'],
        ]);
    }
}
