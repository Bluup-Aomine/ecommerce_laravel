<?php

namespace Tests\Unit\Contact;

use App\src\Validator\ContactValidator;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class PostContactTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
    }

    public function testContact()
    {
        $this->setAuthUser();

        $request = $this->get(route('contact'));
        $request->assertViewIs('other.contact');
        $request->assertStatus(200);
    }

    public function testPostContactInvalidData()
    {
        $this->setAuthUser();
        $data = [
            'name' => 'd',
            'email' => 'jean@laposte.net',
            'content' => 'Je suis un contenu !!!'
        ];

        $request = $this->post(route('contact.post'), $data);

        $getMock = $this->getMockBuilder(ContactValidator::class)->getMock();
        $getMock->expects($this->any())
                ->method('make');

        $request->assertStatus(302);
        $request->assertSessionHas('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
    }

    public function testPostContact()
    {
        $this->setAuthUser();
        $data = [
            'name' => 'Jean',
            'email' => 'jean@laposte.net',
            'content' => 'Je suis un contenu !!!'
        ];
        $request = $this->post(route('contact.post'), $data);
        $request->assertStatus(302);
        $request->assertSessionHas('message', 'Votre message à bien était envoyé !!!');
    }

    private function setAuthUser()
    {
        $user = User::create([
            'name' => 'Jean',
            'email' => 'jean@live.com',
            'password' => 'jean'
        ]);
        $user->role = 'user';
        $user->save();

        $this->actingAs($user);
    }
}
