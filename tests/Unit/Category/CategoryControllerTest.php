<?php

namespace Tests\Unit\Category;

use App\Category;
use App\src\Validator\CategoryValidator;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Str;

class CategoryControllerTest extends CategoryInitTest
{
    public function testCreateCategory()
    {
        $this->initializedCategoriesAndUserDefault();

        $request = $this->post(route('category.store'), [
            'name' => 'Chaussure',
            'content' => 'Je suis une belle catégorie'
        ]);

        $mockerValidator = $this->getMockBuilder(Validator::class)->getMock();
        $mocker = $this->getMockBuilder(CategoryValidator::class)->getMock();
        $mocker->expects($this->any())
            ->method('make')
            ->with($request)
            ->willReturn($mockerValidator);

        $mockerValidator
            ->expects($this->any())
            ->method('fails')
            ->willReturn(false);

        $category = Category::find(1);
        $categoryCount = Category::count();

        $this->assertNotNull($category);
        $this->assertEquals(1, $categoryCount);
        $this->assertEquals('Chaussure', $category->name);
    }

    public function testFailCreateCategory()
    {
        $this->initializedCategoriesAndUserDefault();

        $request = $this->post(route('category.store'), [
            'name' => '',
            'content' => 'df'
        ]);

        $mockerValidator = $this->getMockBuilder(Validator::class)->getMock();
        $mocker = $this->getMockBuilder(CategoryValidator::class)->getMock();
        $mocker->expects($this->any())
            ->method('make')
            ->with($request)
            ->willReturn($mockerValidator);

        $mockerValidator
            ->expects($this->any())
            ->method('fails')
            ->willReturn(true);

        $category = Category::find(1);
        $categoryCount = Category::count();

        $request->assertStatus(302);
        $this->assertNull($category);
        $this->assertStringContainsString('Redirecting to http://localhost/category/create', $request->content());
        $this->assertEquals(0, $categoryCount);
    }

    public function testNoAuthenticatedUser()
    {
        $this->initializedCategoriesAndUserDefault(false);

        $request = $this->post(route('category.store'), [
            'name' => 'Chaussure',
            'content' => 'Je suis une catégorie'
        ]);

        $request->assertRedirect('http://localhost/my-account');
    }

    public function testUpdateCategory()
    {
        $this->initializedCategoriesAndUserDefault();

        Category::create([
            'name' => 'Chaussure',
            'slug' => Str::slug('Chaussure', '-'),
            'content' => 'Trop cool ma catégorie'
        ]);

        $category = Category::find(1);
        $request = $this->put(route('category.update', [$category]), [
            'name' => 'Chaussure Update',
            'content' => 'Je suis une catégorie'
        ]);
        $newCategory = Category::find(1);

        $mockerValidator = $this->getMockBuilder(Validator::class)->getMock();
        $mocker = $this->getMockBuilder(CategoryValidator::class)->getMock();
        $mocker->expects($this->any())
            ->method('make')
            ->with($request)
            ->willReturn($mockerValidator);

        $mockerValidator
            ->expects($this->any())
            ->method('fails')
            ->willReturn(false);

        $categoryCount = Category::count();

        $this->assertNotNull($category);
        $this->assertEquals(1, $categoryCount);
        $this->assertEquals('Chaussure Update', $newCategory->name);
    }

    public function testUpdateFailCategory()
    {
        $this->initializedCategoriesAndUserDefault();

        $category = Category::create([
            'name' => 'Chaussure',
            'slug' => Str::slug('Chaussure', '-'),
            'content' => 'Je suis une catégorie'
        ]);

        $request = $this->put(route('category.update', [$category]), [
            'name' => '',
            'content' => ''
        ]);
        $category = Category::find(1);

        $mockerValidator = $this->getMockBuilder(Validator::class)->getMock();
        $mocker = $this->getMockBuilder(CategoryValidator::class)->getMock();
        $mocker->expects($this->any())
            ->method('make')
            ->with($request)
            ->willReturn($mockerValidator);

        $mockerValidator
            ->expects($this->any())
            ->method('fails')
            ->willReturn(true);

        $categoryCount = Category::count();

        $request->assertStatus(302);
        $this->assertStringContainsString('Redirecting to http://localhost/category/1/edit', $request->content());
        $this->assertEquals(1, $categoryCount);
        $this->assertEquals('Chaussure', $category->name);
    }

    public function testSlugValid()
    {
        $this->initializedCategoriesAndUserDefault();

        $request = $this->post(route('category.store'), [
            'name' => 'Chaussure de bois',
            'content' => 'Je suis une catégorie'
        ]);

        $mockerValidator = $this->getMockBuilder(Validator::class)->getMock();
        $mocker = $this->getMockBuilder(CategoryValidator::class)->getMock();
        $mocker->expects($this->any())
            ->method('make')
            ->with($request)
            ->willReturn($mockerValidator);

        $mockerValidator
            ->expects($this->any())
            ->method('fails')
            ->willReturn(false);

        $category = Category::find(1);
        $categoryCount = Category::count();

        $this->assertNotNull($category);
        $this->assertEquals(1, $categoryCount);
        $this->assertEquals('Chaussure de bois', $category->name);
        $this->assertEquals('chaussure-de-bois', $category->slug);
    }
}
