<?php

namespace Tests\Unit\Category;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryInitTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @param bool $auth
     * @return User
     */
    public function initializedCategoriesAndUserDefault(bool $auth = true): User
    {
        factory(User::class, 1)->create();

        $user = User::find(1);
        if ($auth) {
            $this->actingAs($user);
        }

        return $user;
    }
}
