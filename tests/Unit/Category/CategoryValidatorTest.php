<?php

namespace Tests\Unit\Category;

use App\Category;
use App\Product;
use Tests\Unit\Category\CategoryInitTest;

class CategoryValidatorTest extends CategoryInitTest
{
    public function testValidateName()
    {
        $this->initializedCategoriesAndUserDefault();

        $response = $this->post(route('category.store'), [
            'name' => 'Chaussure',
            'content' => 'Je suis une catégorie'
        ])->assertStatus(302);

        $categories = Category::count();
        $this->assertEquals(1, $categories);
        $this->assertNotNull(Category::find(1));

        $result = $response->getContent();
        $this->assertStringContainsString('http://localhost/category', $result);
        $response->assertSessionHas('message', 'Vous avez ajouté une nouvelle catégorie avec succès !!!');
    }

    public function testValidateNoRequiredName()
    {
        $this->initializedCategoriesAndUserDefault();

        $response = $this->post(route('category.store'), [
            'name' => '',
            'content' => 'Je suis une catégorie'
        ])->assertStatus(302);

        $categories = Category::count();
        $this->assertEquals(0, $categories);
        $this->assertNull(Category::find(1));

        $errors = session('errors');
        $result = $response->getContent();
        $this->assertStringContainsString('http://localhost/category', $result);
        $response->assertSessionHas('message', 'Certaines informations sont incorrects !!!');
        $this->assertEquals('Le champs name doit être requis', $errors->get('name')[0]);
    }

    public function testValidateNameToShort()
    {
        $this->initializedCategoriesAndUserDefault();

        $response = $this->post(route('category.store'), [
            'name' => 'dd',
            'content' => 'Je suis une catégorie'
        ])->assertStatus(302);

        $categories = Category::count();
        $this->assertEquals(0, $categories);
        $this->assertNull(Category::find(1));

        $errors = session('errors');
        $result = $response->getContent();
        $this->assertStringContainsString('http://localhost/category', $result);
        $response->assertSessionHas('message', 'Certaines informations sont incorrects !!!');
        $this->assertEquals('Le champs name doit contenir au minimum 3 caractères', $errors->get('name')[0]);
    }

    public function testValidateNameToLong()
    {
        $this->initializedCategoriesAndUserDefault();

        $response = $this->post(route('category.store'), [
            'name' => 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.',
            'content' => 'Je suis une catégorie'
        ])->assertStatus(302);

        $categories = Category::count();
        $this->assertEquals(0, $categories);
        $this->assertNull(Category::find(1));

        $errors = session('errors');
        $result = $response->getContent();
        $this->assertStringContainsString('http://localhost/category', $result);
        $response->assertSessionHas('message', 'Certaines informations sont incorrects !!!');
        $this->assertEquals('Le champs name doit contenir au maximum 100 caractères', $errors->get('name')[0]);
    }

    public function testValidateContent()
    {
        $this->initializedCategoriesAndUserDefault();

        $response = $this->post(route('category.store'), [
            'name' => 'Chaussure',
            'content' => 'Je suis une catégorie'
        ])->assertStatus(302);

        $categories = Category::count();
        $this->assertEquals(1, $categories);
        $this->assertNotNull(Category::find(1));

        $result = $response->getContent();
        $this->assertStringContainsString('http://localhost/category', $result);
        $response->assertSessionHas('message', 'Vous avez ajouté une nouvelle catégorie avec succès !!!');
    }

    public function testValidateContentTooShort()
    {
        $this->initializedCategoriesAndUserDefault();

        $response = $this->post(route('category.store'), [
            'name' => 'dadaddada',
            'content' => 'df'
        ])->assertStatus(302);

        $categories = Category::count();
        $this->assertEquals(0, $categories);
        $this->assertNull(Category::find(1));

        $errors = session('errors');
        $result = $response->getContent();
        $this->assertStringContainsString('http://localhost/category', $result);
        $response->assertSessionHas('message', 'Certaines informations sont incorrects !!!');
        $this->assertEquals('Le champs content doit contenir au minimum 5 caractères', $errors->get('content')[0]);
    }

    public function testValidateContentTooLong()
    {
        $this->initializedCategoriesAndUserDefault();

        $response = $this->post(route('category.store'), [
            'name' => 'ddadaddad',
            'content' => 'Contrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire. Il trouve ses racines dans une oeuvre de la littérature latine classique datant de 45 av. J.-C., le rendant vieux de 2000 ans. Un professeur du Hampden-Sydney College, en Virginie, s\'est intéressé à un des mots latins les plus obscurs, consectetur, extrait d\'un passage du Lorem Ipsum, et en étudiant tous les usages de ce mot dans la littérature classique, découvrit la source incontestable du Lorem Ipsum. Il provient en fait des sections 1.10.32 et 1.10.33 du "De Finibus Bonorum et Malorum" (Des Suprêmes Biens et des Suprêmes Maux) de Cicéron. Cet ouvrage, très populaire pendant la Renaissance, est un traité sur la théorie de l\'éthique. Les premières lignes du Lorem Ipsum, "Lorem ipsum dolor sit amet...", proviennent de la section 1.10.32.'
        ])->assertStatus(302);

        $categories = Category::count();
        $this->assertEquals(0, $categories);
        $this->assertNull(Category::find(1));

        $errors = session('errors');
        $result = $response->getContent();
        $this->assertStringContainsString('http://localhost/category', $result);
        $response->assertSessionHas('message', 'Certaines informations sont incorrects !!!');
        $this->assertEquals('Le champs content doit contenir au maximum 500 caractères', $errors->get('content')[0]);
    }

    public function testValidateContentRequired()
    {
        $this->initializedCategoriesAndUserDefault();

        $response = $this->post(route('category.store'), [
            'name' => 'ddadaddad',
            'content' => ''
        ])->assertStatus(302);

        $categories = Category::count();
        $this->assertEquals(0, $categories);
        $this->assertNull(Category::find(1));

        $errors = session('errors');
        $result = $response->getContent();
        $this->assertStringContainsString('http://localhost/category', $result);
        $response->assertSessionHas('message', 'Certaines informations sont incorrects !!!');
        $this->assertEquals('Le champs content doit être requis', $errors->get('content')[0]);
    }
}
