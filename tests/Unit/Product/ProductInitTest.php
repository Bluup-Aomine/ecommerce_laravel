<?php

namespace Tests\Unit\Product;

use App\Category;
use App\ColorSizeProduct;
use App\Product;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductInitTest extends TestCase
{
    /**
     * @var Product[]
     */
    private $products = [];
    /**
     * @var User[]
     */
    private $users = [];

    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @param string $role
     * @param bool $noAuth
     * @return User
     */
    public function initializedProductsAndUserDefault(string $role, bool $noAuth = true): User
    {
        $this->products = factory(Product::class, 10)->create();
        factory(Category::class, 5)->create();

        $user = User::create([
            'name' => 'Jean',
            'email' => 'jean@live.com',
            'password' => 'jean'
        ]);
        $user->role = $role;
        $user->save();

        $this->relationUserPost($this->products, $user);
        if ($noAuth)  {
            $this->actingAs($user);
        }

        return $user;
    }

    /**
     * @return Product
     */
    public function initializedProductWithColorAndSize(): Product
    {
        $product = Product::findOrFail(10);
        $product->colors = 'blue, red, green';
        $product->sizes = 'xl, m, s';
        $product->save();

        return $product;
    }

    public function initializedSizesAndColorsProducts()
    {
        $product = Product::findOrFail(10);
        for ($i = 0; $i < 3; $i++) {
            $colorSize = new ColorSizeProduct();
            $colorSize->color = 'red';
            $colorSize->size = 'xl';
            $colorSize->quantity = 5;
            $colorSize->product_id = (int)$product->id;
            $colorSize->save();
        }
    }

    /**
     * @param string $name
     */
    public function setProductsName(string $name)
    {
        $product = Product::all();
        for ($i = 0; $i < 3; $i++) {
            $product[$i]->name = $name . ' ' . $i;
            $product[$i]->save();
        }
    }

    public function initializedRelationsProductsToCategory()
    {
        $category = Category::find(1);
        $category->slug = 'anime';
        $category->save();

        for ($i = 0; $i < 3; $i++) {
            $this->products[$i]->category_id = (int)$category->id;
            $this->products[$i]->save();
        }

        for ($i = 0; $i >= 3 && $i <= 9; $i++) {
            $this->products[$i]->category_id = 2;
            $this->products[$i]->save();
        }

        return $category;
    }

    /**
     * @param int $price
     */
    public function initializedProductWithPrice(int $price)
    {
        $product = Product::findOrFail(1);
        $product->price = $price;
        $product->save();
    }


    /**
     * @param Collection $products
     * @param User $user
     */
    private function relationUserPost(Collection $products, User $user)
    {
        foreach ($products as $product) {
            $product->user_id = (int)$user->id;
            $product->save();
        }
    }
}
