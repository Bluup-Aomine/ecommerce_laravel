<?php

namespace Tests\Unit\Product;

use App\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductSearchTest extends ProductInitTest
{
    use RefreshDatabase;

    public function testSearchProduct()
    {
        $request = $this->post(route('search'), [
            'search' => 'veste'
        ]);
        $content = $request->getContent() ?: '';

        $request->assertStatus(302);
        $this->assertStringContainsString('http://localhost/products/search/veste', $content);
    }

    public function testSearchProductWithEmptyValue()
    {
        $request = $this->post(route('search'), [
            'search' => ''
        ]);
        $content = $request->getContent() ?: '';

        $request->assertStatus(302);
        $request->assertSessionHas('error', 'Le champs search ne doit pas être vide si vous voulez trouver un produit !!!');
        $this->assertStringContainsString('http://localhost', $content);
    }

    public function testSearchProductWithoutValue()
    {
        $request = $this->post(route('search'));
        $content = $request->getContent() ?: '';

        $request->assertStatus(302);
        $request->assertSessionHas('error', 'Le champs search ne doit pas être vide si vous voulez trouver un produit !!!');
        $this->assertStringContainsString('http://localhost', $content);
    }

    public function testSearchProductGETPage()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->setProductsName('Veste numero');

        $search = 'veste';
        $request = $this->get(route('search.products', [$search]));

        $products = Product::SearchProduct($search)->count();
        $request->assertStatus(200);
        $request->assertViewIs('products.search');
        $this->assertEquals(3, $products);
    }
}
