<?php

namespace Tests\Unit\Product;

use App\Product;

class ProductTest extends ProductInitTest
{
    public function testAuthorizedAddProduct()
    {
       $this->initializedProductsAndUserDefault('admin');

       $response = $this->post(route('add.product'), [
            'name' => 'lolsasasasas',
            'slug' => 'lolsasasasas',
            'content' => 'contentsaaaaaaaaaaaaaaaa',
            'price' => 80,
            'promo' => false,
            'colors' => ['blue', 'red'],
            'sizes' => ['s', 'm'],
            'category_id' => 1
        ])->assertStatus(302);

        $products = Product::count();
        $product = Product::find(11);

        $this->assertEquals(11, $products);
        $this->assertNotNull($product);
        $this->assertEquals('lolsasasasas', $product->name);
        $this->assertEquals('lolsasasasas', $product->slug);
        $this->assertEquals('blue, red', $product->colors);
        $this->assertEquals('s, m', $product->sizes);

        $result = $response->content();
        $this->assertStringContainsString('Redirecting to http://localhost/product/create', $result);
        $response->assertSessionHas('message', 'Ajout du produit effectué, vous pouvez maintenant ajouter des images à votre produit !!!');
    }

    public function testNoAuthorizedAddProduct()
    {
        $this->initializedProductsAndUserDefault('user');

        $response = $this->post(route('add.product'), [
            'name' => 'lolsasasasas',
            'slug' => 'lolsasasasas',
            'content' => 'contentsaaaaaaaaaaaaaaaa',
            'price' => 80,
            'promo' => false,
            'colors' => ['blue', 'red'],
            'sizes' => ['s', 'm'],
            'category_id' => 1
        ])->assertStatus(302);

        $products = Product::count();
        $product = Product::find(11);

        $this->assertEquals(10, $products);
        $this->assertNull($product);

        $result = $response->content();
        $this->assertStringContainsString('Redirecting to http://localhost/product/create', $result);
        $response->assertSessionHas('messageError', 'Vous n\'avez pas le droit d\'effectuer cette action');
    }

    public function testAuthorizedRemoveProduct()
    {
        $this->initializedProductsAndUserDefault('admin');
        $product = Product::find(10);

        $response = $this->delete(route('product.destroy', [$product]));

        $products = Product::count();
        $this->assertEquals(9, $products);
        $this->assertNull(Product::find(10));

        $result = $response->content();
        $this->assertStringContainsString('Redirecting to http://localhost/product', $result);
        $response->assertSessionHas('message', 'Le produit a était supprimé avec succès !!!');
        $response->assertStatus(302);
    }

    public function testNoAuthorizedRemoveProduct()
    {
        $this->initializedProductsAndUserDefault('user');
        $product = Product::find(10);

        $response = $this->delete(route('product.destroy', [$product]))
            ->assertStatus(302);

        $products = Product::count();
        $this->assertEquals(10, $products);
        $this->assertNotNull(Product::find(10));

        $result = $response->content();
        $this->assertStringContainsString('Redirecting to http://localhost/product', $result);
        $response->assertSessionHas('messageError', 'Vous n\'avez pas le droit d\'effectuer cette action');
    }

    public function testAuthorizedEditProduct()
    {
        $this->initializedProductsAndUserDefault('admin');

        $product = Product::find(10);
        $response = $this->put(route('product.update', [$product]), [
            'name' => 'lolsasasasas',
            'slug' => 'lolsasasasas',
            'content' => 'contentsaaaaaaaaaaaaaaaa',
            'price' => 80,
            'promo' => false,
            'colors' => ['blue', 'red'],
            'sizes' => ['s', 'm'],
            'category_id' => 1
        ]);
        $product = Product::find(10);

        $products = Product::count();

        $this->assertEquals(10, $products);
        $this->assertNotNull($product);
        $this->assertEquals('lolsasasasas', $product->name);
        $this->assertEquals('lolsasasasas', $product->slug);
        $this->assertEquals('blue, red', $product->colors);
        $this->assertEquals('s, m', $product->sizes);

        $result = $response->content();
        $this->assertStringContainsString('Redirecting to http://localhost/product/10/edit', $result);
        $response->assertSessionHas('message', 'Modification du produit effectué, vous pouvez maintenant ajouter des images à votre produit !!!');
        $response->assertStatus(302);
    }

    public function testNoAuthorizedEditProduct()
    {
        $this->initializedProductsAndUserDefault('user');

        $product = Product::find(10);
        $response = $this->put(route('product.update', [$product]), [
            'name' => 'lolsasasasas',
            'slug' => 'lolsasasasas',
            'content' => 'contentsaaaaaaaaaaaaaaaa',
            'price' => 80,
            'promo' => false,
            'colors' => ['blue', 'red'],
            'sizes' => ['s', 'm'],
            'category_id' => 1
        ])->assertStatus(302);

        $products = Product::count();

        $this->assertEquals(10, $products);
        $this->assertNotNull($product);
        $this->assertNotEquals('lolsasasasas', $product->name);
        $this->assertNotEquals('lolsasasasas', $product->slug);
        $this->assertNotEquals('blue, red', $product->colors);
        $this->assertNotEquals('s, m', $product->sizes);

        $result = $response->content();
        $this->assertStringContainsString('Redirecting to http://localhost/product/10/edit', $result);
        $response->assertSessionHas('messageError', 'Vous n\'avez pas le droit d\'effectuer cette action');
        $response->assertStatus(302);
    }
}
