<?php

namespace Tests\Unit\Product;

use App\Product;

class ProductValidateTest extends ProductInitTest
{
    public function testValidateDataWithAddProductMethod()
    {
        $user = $this->initializedProductsAndUserDefault('admin');

        $response = $this->post(route('add.product'), [
            'name' => 'Je suis un produit',
            'content' => 'je suis un content de produit',
            'price' => 800,
            'promo' => false,
            'userID' => $user->id
        ])->assertStatus(302);

        $products = Product::count();
        $this->assertEquals(11, $products);
        $this->assertNotNull(Product::find(11));

        $result = (array)\GuzzleHttp\json_decode($response->content());
        $this->assertEquals(true, $result['success']);
    }

    public function testNoValidateDataWithAddProductMethod()
    {
        $user = $this->initializedProductsAndUserDefault('admin');

        $response = $this->post(route('add.product'), [
            'name' => 'ad',
            'content' => 'ddd',
            'price' => 800,
            'promo' => false,
            'userID' => $user->id
        ])->assertStatus(500);

        $products = Product::count();
        $this->assertEquals(10, $products);
        $this->assertNull(Product::find(11));

        $result = (array)\GuzzleHttp\json_decode($response->content());
        $this->assertEquals(false, $result['success']);
    }

    public function testNoValidateDataWithAddProductMethodFieldNameTooShort()
    {
        $user = $this->initializedProductsAndUserDefault('admin');

        $response = $this->post(route('add.product'), [
            'name' => 'ad',
            'content' => 'Je suis un content',
            'price' => 800,
            'promo' => false,
            'userID' => $user->id
        ])->assertStatus(500);

        $products = Product::count();
        $this->assertEquals(10, $products);
        $this->assertNull(Product::find(11));

        $result = (array)\GuzzleHttp\json_decode($response->content());
        $this->assertEquals(false, $result['success']);
        $this->assertSame('Le nom du produit doit contenir au minimum 5 caractères', $result['message']->{'name'}[0]);
    }

    public function testNoValidateDataWithAddProductMethodFieldNameTooLong()
    {
        $user = $this->initializedProductsAndUserDefault('admin');

        $response = $this->post(route('add.product'), [
            'name' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500',
            'content' => 'Je suis un content',
            'price' => 800,
            'promo' => false,
            'userID' => $user->id
        ])->assertStatus(500);

        $products = Product::count();
        $this->assertEquals(10, $products);
        $this->assertNull(Product::find(11));

        $result = (array)\GuzzleHttp\json_decode($response->content());
        $this->assertEquals(false, $result['success']);
        $this->assertSame('Le nom du produit doit contenir au maximum 100 caractères', $result['message']->{'name'}[0]);
    }

    public function testNoValidateDataWithAddProductMethodFieldNameRequired()
    {
        $user = $this->initializedProductsAndUserDefault('admin');

        $response = $this->post(route('add.product'), [
            'name' => '',
            'content' => 'Je suis un content',
            'price' => 800,
            'promo' => false,
            'userID' => $user->id
        ])->assertStatus(500);

        $products = Product::count();
        $this->assertEquals(10, $products);
        $this->assertNull(Product::find(11));

        $result = (array)\GuzzleHttp\json_decode($response->content());
        $this->assertEquals(false, $result['success']);
        $this->assertSame('Le champs name doit être requis', $result['message']->{'name'}[0]);
    }

    public function testNoValidateDataWithAddProductMethodFieldContentRequired()
    {
        $user = $this->initializedProductsAndUserDefault('admin');

        $response = $this->post(route('add.product'), [
            'name' => 'Je suis un produit',
            'content' => '',
            'price' => 800,
            'promo' => false,
            'userID' => $user->id
        ])->assertStatus(500);

        $products = Product::count();
        $this->assertEquals(10, $products);
        $this->assertNull(Product::find(11));

        $result = (array)\GuzzleHttp\json_decode($response->content());
        $this->assertEquals(false, $result['success']);
        $this->assertSame('Le champs contenu doit être requis', $result['message']->{'content'}[0]);
    }

    public function testNoValidateDataWithAddProductMethodFieldContentTooShort()
    {
        $user = $this->initializedProductsAndUserDefault('admin');

        $response = $this->post(route('add.product'), [
            'name' => 'Je suis un produit',
            'content' => 'Je suis',
            'price' => 800,
            'promo' => false,
            'userID' => $user->id
        ])->assertStatus(500);

        $products = Product::count();
        $this->assertEquals(10, $products);
        $this->assertNull(Product::find(11));

        $result = (array)\GuzzleHttp\json_decode($response->content());
        $this->assertEquals(false, $result['success']);
        $this->assertSame('Le champs contenu doit contenir  au minimum 10 caractères', $result['message']->{'content'}[0]);
    }

    public function testNoValidateDataWithAddProductMethodFieldPriceRequired()
    {
        $user = $this->initializedProductsAndUserDefault('admin');

        $response = $this->post(route('add.product'), [
            'name' => 'Je suis un produit',
            'content' => 'Je suis addadaddad',
            'price' => '',
            'promo' => false,
            'userID' => $user->id
        ])->assertStatus(500);

        $products = Product::count();
        $this->assertEquals(10, $products);
        $this->assertNull(Product::find(11));

        $result = (array)\GuzzleHttp\json_decode($response->content());
        $this->assertEquals(false, $result['success']);
        $this->assertSame('Le champs prix doit être requis', $result['message']->{'price'}[0]);
    }

    public function testNoValidateDataWithAddProductMethodFieldPriceBetween()
    {
        $user = $this->initializedProductsAndUserDefault('admin');

        $response = $this->post(route('add.product'), [
            'name' => 'Je suis un produit',
            'content' => 'Je suis addadaddad',
            'price' => 15,
            'promo' => false,
            'userID' => $user->id
        ])->assertStatus(500);

        $products = Product::count();
        $this->assertEquals(10, $products);
        $this->assertNull(Product::find(11));

        $result = (array)\GuzzleHttp\json_decode($response->content());
        $this->assertEquals(false, $result['success']);
        $this->assertSame('Le prix doit être entre 20 € et 1000 €', $result['message']->{'price'}[0]);
    }
}
