<?php

namespace Tests\Unit\Product;

use App\Category;
use App\Product;
use Carbon\Carbon;

class ProductFilterControllerTest extends ProductInitTest
{
    public function testFilterByCategory()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initializedProductWithColorAndSize();
        $category = $this->initializedRelationsProductsToCategory();

        $request = $this->get(route('product-filter.category', $category->slug));
        $response = \GuzzleHttp\json_decode($request->content());

        $products = Product::findByCategory($category->id)->get();

        $request->assertStatus(302);
        $this->assertEquals(true, $response->success);
        $this->assertEquals(3, count($products));
        $this->assertEquals(3, count($response->data->products));
    }

    public function testFilterByCategoryWithNotExistCategory()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initializedProductWithColorAndSize();
        $this->initializedRelationsProductsToCategory();

        $request = $this->get(route('product-filter.category','slug-de-merde'));
        $response = \GuzzleHttp\json_decode($request->content());

        $request->assertStatus(302);
        $this->assertEquals(false, $response->success);
        $this->assertEquals('Cette catégorie n\'est pas existante dans notre base de donnée !!!', $response->message);
    }

    public function testFilterClear()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initializedProductWithColorAndSize();

        $request = $this->get(route('product-filter.clear'));
        $response = \GuzzleHttp\json_decode($request->content());

        $products = Product::paginate(12);

        $request->assertStatus(302);
        $this->assertEquals(true, $response->success);
        $this->assertEquals(10, count($products));
        $this->assertEquals(10, count($response->data->products));
    }

    public function testFilterProductIndexPage()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initializedProductWithColorAndSize();

        $request = $this->get(route('product-filter.page', 9));
        $response = \GuzzleHttp\json_decode($request->content());

        $products = Product::paginate(9);

        $request->assertStatus(302);
        $this->assertEquals(true, $response->success);
        $this->assertEquals(9, count($products));
        $this->assertEquals(9, count($response->data->products));
    }

    public function testFilterProductSize()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initializedProductWithColorAndSize();
        $this->initializedSizesAndColorsProducts();

        $request = $this->get(route('product-filter.size', 'xl'));
        $response = \GuzzleHttp\json_decode($request->content());

        $products = Product::paginate(3);

        $request->assertStatus(302);
        $this->assertEquals(true, $response->success);
        $this->assertEquals(3, count($products));
        $this->assertEquals(3, count($response->data->products));
    }

    public function testFilterProductSizeNotFound()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initializedProductWithColorAndSize();
        $this->initializedSizesAndColorsProducts();

        $request = $this->get(route('product-filter.size', 'daddadda'));
        $response = \GuzzleHttp\json_decode($request->content());

        $products = [];

        $request->assertStatus(302);
        $this->assertEquals(true, $response->success);
        $this->assertEquals(0, count($products));
        $this->assertEquals(0, count($response->data->products));
    }

    public function testFilterProductColor()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initializedProductWithColorAndSize();
        $this->initializedSizesAndColorsProducts();

        $request = $this->get(route('product-filter.color', 'red'));
        $response = \GuzzleHttp\json_decode($request->content());

        $products = Product::paginate(3);

        $request->assertStatus(302);
        $this->assertEquals(true, $response->success);
        $this->assertEquals(3, count($products));
        $this->assertEquals(3, count($response->data->products));
    }

    public function testFilterProductColorNotFound()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initializedProductWithColorAndSize();
        $this->initializedSizesAndColorsProducts();

        $request = $this->get(route('product-filter.color', 'dadadadad'));
        $response = \GuzzleHttp\json_decode($request->content());

        $products = [];

        $request->assertStatus(302);
        $this->assertEquals(true, $response->success);
        $this->assertEquals(0, count($products));
        $this->assertEquals(0, count($response->data->products));
    }

    public function testFilterProductPrice()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initializedProductWithColorAndSize();
        $this->initializedSizesAndColorsProducts();
        $this->initializedProductWithPrice(350);

        $request = $this->get(route('product-filter.price', [
           'min' => 25,
           'max' => 650
        ]));
        $response = \GuzzleHttp\json_decode($request->content());

        $products = Product::paginate(1);

        $request->assertStatus(302);
        $this->assertEquals(true, $response->success);
        $this->assertEquals(1, count($products));
        $this->assertEquals(1, count($response->data->products));
        $this->assertEquals(2, count($response->data->itemsFiltersPrice));
    }

    public function testFilterProductByDate()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initializedProductWithColorAndSize();
        $this->initializedSizesAndColorsProducts();
        $this->initializedProductWithPrice(350);

        $product = Product::find(1);
        $product->created_at = (new Carbon())->addMinutes(5);
        $product->save();

        $request = $this->get(route('product-filter.date'));
        $response = \GuzzleHttp\json_decode($request->content());

        $products = Product::paginate(12);
        $request->assertStatus(302);
        $this->assertEquals(true, $response->success);
        $this->assertEquals(10, count($products));
        $this->assertEquals(10, count($response->data->products));

        $products = Product::GetDateLast()->paginate(12);
        $this->assertEquals(1, $products[0]->id);
    }

    public function testFilterProductByPriceAsc()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initializedProductWithColorAndSize();
        $this->initializedSizesAndColorsProducts();
        $this->initializedProductWithPrice(350);

        $product = Product::find(1);
        $product->price = 15;
        $product->save();

        $request = $this->get(route('product-filter.price-asc'));
        $response = \GuzzleHttp\json_decode($request->content());

        $products = Product::paginate(12);
        $request->assertStatus(302);
        $this->assertEquals(true, $response->success);
        $this->assertEquals(10, count($products));
        $this->assertEquals(10, count($response->data->products));

        $products = Product::OrderByAsc('price')->paginate(12);
        $this->assertEquals(15, $products[0]->price);
    }

    public function testFilterProductByPriceDesc()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initializedProductWithColorAndSize();
        $this->initializedSizesAndColorsProducts();
        $this->initializedProductWithPrice(350);

        $product = Product::find(1);
        $product->price = 800000000;
        $product->save();

        $request = $this->get(route('product-filter.price-desc'));
        $response = \GuzzleHttp\json_decode($request->content());

        $products = Product::paginate(12);
        $request->assertStatus(302);
        $this->assertEquals(true, $response->success);
        $this->assertEquals(10, count($products));
        $this->assertEquals(10, count($response->data->products));

        $products = Product::OrderByDesc('price')->paginate(12);
        $this->assertEquals(800000000, $products[0]->price);
    }
}
