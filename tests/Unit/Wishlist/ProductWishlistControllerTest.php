<?php

namespace Tests\Unit\Wishlist;

use App\Product;
use App\src\Validator\ProductWishlistValidator;
use App\Wishlist;
use App\WishlistsProduct;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Unit\Product\ProductInitTest;

class ProductWishlistControllerTest extends ProductInitTest
{
    use RefreshDatabase;

    public function testAddTitleWishlist()
    {
        $this->initializedProductsAndUserDefault('admin');

        $wishList = new Wishlist();
        $wishList->add(1);

        $request = $this->put(route('product.wishlist.update', [Wishlist::find(1)]), [
            'title' => 'Je suis un title !!!'
        ]);

        $getMock = $this->getMockBuilder(ProductWishlistValidator::class)->getMock();
        $getMock->expects($this->any())
            ->method('make')
            ->with($request);

        $product = Product::find(1);
        $wishList = Wishlist::find(1);

        $request->assertStatus(302);
        $request->assertSessionHas('message', 'Vous avez modifié le titre de votre wishlist avec succés');
        $this->assertNotNull($wishList);
        $this->assertEquals('Je suis un title !!!', $wishList->title);
    }

    public function testAddTitleWishlistErrorValidator()
    {
        $this->initializedProductsAndUserDefault('admin');

        $wishList = new Wishlist();
        $wishList->add(1);

        $request = $this->put(route('product.wishlist.update', [Wishlist::find(1)]), [
            'title' => ''
        ]);

        $getMock = $this->getMockBuilder(ProductWishlistValidator::class)->getMock();
        $getMock->expects($this->any())
            ->method('make')
            ->with($request);

        $wishList = Wishlist::find(1);
        $product = Product::find(1);

        $request->assertStatus(302);
        $request->assertSessionHas('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        $this->assertNotNull($wishList);
        $this->assertNotEquals('', $wishList->title);
    }

    public function testAddProductInTheWishlist()
    {
        $this->initializedProductsAndUserDefault('admin');

        $product = Product::find(1);
        $request = $this->post(route('product.wishlist.add'), [
            'productID' => (integer)$product->id
        ]);
        $getMock = $this->getMockBuilder(Wishlist::class)->getMock();
        $getMock->expects($this->any())
            ->method('add')
            ->with($request);

        $response = \GuzzleHttp\json_decode($request->getContent());

        $wishList = Wishlist::find(1);
        $request->assertStatus(302);
        $this->assertNotNull($wishList);
        $this->assertNotEquals('', $wishList->title);
        $this->assertEquals(true, $response->success);
        $this->assertEquals('Vous avez ajouté le produit ' . $product->title . 'dans votre wishlist !!!', $response->data->message);
    }

    public function testAddProductInTheWishlistIfExist()
    {
        $user = $this->initializedProductsAndUserDefault('admin');

        $product = Product::find(1);
        $wishList = new Wishlist();
        $wishList->add((integer)$user->id);

        $wishListProduct = new WishlistsProduct();
        $wishListProduct->product_id = (integer)$product->id;
        $wishListProduct->wishlist_id = (integer)$wishList->id;
        $wishListProduct->save();

        $request = $this->post(route('product.wishlist.add'), [
            'productID' => (integer)$product->id
        ]);
        $getMock = $this->getMockBuilder(Wishlist::class)->getMock();
        $getMock->expects($this->never())
            ->method('add')
            ->with($request);

        $response = \GuzzleHttp\json_decode($request->getContent());

        $wishList = Wishlist::find(1);
        $count = Wishlist::count();

        $this->assertEquals(1, $count);
        $request->assertStatus(302);
        $this->assertNotNull($wishList);
        $this->assertNotEquals('', $wishList->title);
        $this->assertEquals(false, $response->success);
        $this->assertEquals('Vous avez déja ajouter ce produit à votre wishlist !!!', $response->message);
    }

    public function testNotAddProductInTheWishlistBecauseUserNotConnected()
    {
        $this->initializedProductsAndUserDefault('admin', false);

        $wishList = new Wishlist();
        $wishList->add(1);

        $request = $this->post(route('product.wishlist.add'), [
            'productID' => 1
        ]);
        $getMock = $this->getMockBuilder(Wishlist::class)->getMock();
        $getMock->expects($this->never())
            ->method('add')
            ->with($request);

        $response = \GuzzleHttp\json_decode($request->getContent());

        $wishList = Wishlist::find(1);
        $product = Product::find(1);

        $request->assertStatus(302);
        $this->assertNotNull($wishList);
        $this->assertNotEquals('', $wishList->title);
        $this->assertEquals(false, $response->success);
        $this->assertEquals('Vous devez vous connecté pour ajouter un produit à votre wishlist !!!', $response->message);
    }

    public function testDeleteWishlistProduct()
    {
        $this->initializedProductsAndUserDefault('admin', false);

        $wishList = new Wishlist();
        $wishList->add(1);

        $product = Product::find(1);
        $wishlistProduct = new WishlistsProduct();
        $wishlistProduct->add($product, $wishList);

        $request = $this->call('DELETE', route('product.wishlist.delete', [$wishlistProduct->id]));
        $response = \GuzzleHttp\json_decode($request->getContent());

        $wishlistProduct = WishlistsProduct::find(1);

        $request->assertStatus(302);
        $this->assertNull($wishlistProduct);
        $this->assertEquals(true, $response->success);
        $this->assertEquals('Vous avez supprimer le produit "' . $product->name . '" de votre wishlist !!!', $response->data->message);
    }

    public function testFailedDeleteBecauseWishlistProductNotFound()
    {
        $this->initializedProductsAndUserDefault('admin', false);

        $wishList = new Wishlist();
        $wishList->add(1);

        $product = Product::find(1);
        $wishlistProduct = new WishlistsProduct();
        $wishlistProduct->add($product, $wishList);

        $request = $this->call('DELETE', route('product.wishlist.delete', [5]));
        $response = \GuzzleHttp\json_decode($request->getContent());

        $wishlistProduct = WishlistsProduct::find(1);

        $request->assertStatus(302);
        $this->assertNotNull($wishlistProduct);
        $this->assertEquals(false, $response->success);
        $this->assertEquals('Ce produit n\'est pas existant dans votre wishlist !!!', $response->message);
    }
}
