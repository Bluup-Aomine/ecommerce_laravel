<?php

namespace Tests\Unit\BasketProduct;

use App\Basket;
use App\BasketsProduct;
use App\Product;
use App\Wishlist;
use App\WishlistsProduct;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Unit\Product\ProductInitTest;

class AddBasketTest extends ProductInitTest
{
    use RefreshDatabase;

    /**
     * @var Wishlist
     */
    private Wishlist $wishlist;
    /**
     * @var WishlistsProduct
     */
    private WishlistsProduct $wishlistProduct;

    /**
     * AddBasketTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->wishlist = new Wishlist();
        $this->wishlistProduct = new WishlistsProduct();
    }

    public function testAddBasketWithAuth()
    {
        $this->initializedProductsAndUserDefault('admin');

        $request = $this->post(route('product.basket.store'), [
            'productID' => 2,
            'quantity' => 5
        ]);

        $mockBasket = $this->getMockBuilder(Basket::class)->getMock();
        $mockBasketProduct = $this->getMockBuilder(BasketsProduct::class)->getMock();
        $mockBasket
            ->expects($this->any())
            ->method('add');
        $mockBasketProduct
            ->expects($this->any())
            ->method('add')
            ->with(2, 1, 5);

        $response = \GuzzleHttp\json_decode($request->getContent());

        $basket = Basket::find(1);
        $basketProduct = BasketsProduct::find(1);
        $product = Product::find(2);

        $request->assertStatus(302);
        $this->assertEquals(1, $basket->user_id);
        $this->assertEquals(2, $basketProduct->product_id);
        $this->assertEquals(1, $basketProduct->basket_id);
        $this->assertEquals(5, $basketProduct->quantity);

        $this->assertEquals(true, $response->success);
        $this->assertEquals('Vous avez ajouté ce produit à votre panier !!!', $response->data->message);
        $this->assertStringContainsString($product->name, $response->data->products[0]);
        $this->assertStringContainsString($product->price, $response->data->products[0]);
        $this->assertCount(1, $response->data->products);
    }

    public function testAddBasketWithAuthAndWishlist()
    {
        $this->initializedProductsAndUserDefault('admin');

        $this->addWishlist();
        $request = $this->post(route('product.basket.store'), [
            'productID' => 2,
            'quantity' => 5,
            'wishlist' => true
        ]);

        $mockBasket = $this->getMockBuilder(Basket::class)->getMock();
        $mockBasketProduct = $this->getMockBuilder(BasketsProduct::class)->getMock();
        $mockBasket
            ->expects($this->any())
            ->method('add');
        $mockBasketProduct
            ->expects($this->any())
            ->method('add')
            ->with(2, 1, 5);

        $response = \GuzzleHttp\json_decode($request->getContent());

        $basket = Basket::find(1);
        $basketProduct = BasketsProduct::find(1);
        $product = Product::find(2);
        $wishlistProduct = $this->getWishlistProduct();

        $request->assertStatus(302);
        $this->assertEquals(1, $basket->user_id);
        $this->assertEquals(2, $basketProduct->product_id);
        $this->assertEquals(1, $basketProduct->basket_id);
        $this->assertEquals(5, $basketProduct->quantity);

        $this->assertNull($wishlistProduct);
        $this->assertEquals(true, $response->success);
        $this->assertEquals('Vous avez ajouté ce produit à votre panier !!!', $response->data->message);
        $this->assertStringContainsString($product->name, $response->data->products[0]);
        $this->assertStringContainsString($product->price, $response->data->products[0]);
        $this->assertCount(1, $response->data->products);
    }

    /**
     * @return void
     */
    private function addWishlist(): void
    {
        $wishlist = $this->wishlist->add(1);
        $product = Product::find(2);
        $this->wishlistProduct->add($product, $wishlist);
    }

    /**
     * @return null|WishlistsProduct
     */
    private function getWishlistProduct(): ?WishlistsProduct
    {
        return WishlistsProduct::find(1);
    }
}
