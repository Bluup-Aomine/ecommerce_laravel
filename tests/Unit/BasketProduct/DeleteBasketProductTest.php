<?php

namespace Tests\Unit\BasketProduct;

use App\Basket;
use App\BasketsProduct;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Unit\Product\ProductInitTest;

class DeleteBasketProductTest extends ProductInitTest
{
    use RefreshDatabase;

    /**
     * @return Basket
     */
    private function initBasketAndBasketProducts(): Basket
    {
        $basket = new Basket();
        $basket->user_id = 1;
        $basket->save();

        for ($i = 0; $i < 2; $i++) {
            $basketProduct = new BasketsProduct();
            $basketProduct->basket_id = 1;
            $basketProduct->product_id = $i === 0 ? 1 : 2;
            $basketProduct->quantity = $i === 0 ? 1 : 3;
            $basketProduct->save();
        }

        return $basket;
    }

    public function testDeleteProduct()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initBasketAndBasketProducts();

        $basketProduct = BasketsProduct::find(1);
        $route = route('product.basket.delete', [$basketProduct->id]);
        $request = $this->call('DELETE', $route, ['_token' => csrf_token()]);
        $response = \GuzzleHttp\json_decode($request->getContent());

        $this->assertNull(BasketsProduct::find(1));
        $this->assertEquals(true, $response->success);
        $this->assertEquals('Vous avez supprimé ce produit à votre panier !!!', $response->data->message);
        $request->assertStatus(302);
    }

    public function testDeleteProductWithBasketProductNotExist()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initBasketAndBasketProducts();

        $route = route('product.basket.delete', [5]);
        $request = $this->call('DELETE', $route, ['_token' => csrf_token()]);
        $response = \GuzzleHttp\json_decode($request->getContent());

        $this->assertNull(BasketsProduct::find(5));
        $this->assertNotNull(BasketsProduct::find(1));
        $this->assertEquals(false, $response->success);
        $this->assertEquals('Ce produit n\'est pas existant dans votre panier !!!', $response->message);
        $request->assertStatus(302);
    }

    public function testDeleteProductWithBasketNotExist()
    {
        $this->initializedProductsAndUserDefault('admin');
        $this->initBasketAndBasketProducts();

        $basketProduct = BasketsProduct::find(1);
        $basketProduct->basket_id = 4144;
        $basketProduct->save();

        $route = route('product.basket.delete', [1]);
        $request = $this->call('DELETE', $route, ['_token' => csrf_token()]);
        $response = \GuzzleHttp\json_decode($request->getContent());

        $this->assertNull(BasketsProduct::find(5));
        $this->assertNotNull(BasketsProduct::find(1));
        $this->assertEquals(false, $response->success);
        $this->assertEquals('Ce produit n\'est pas existant dans votre panier !!!', $response->message);
        $request->assertStatus(302);
    }
}
