<?php

namespace Tests\Unit\BasketProduct;

use App\Basket;
use App\BasketsProduct;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Unit\Product\ProductInitTest;

class UpdateBasketTest extends ProductInitTest
{
    use RefreshDatabase;

    /**
     * @return Basket
     */
    private function initBasketAndBasketProducts(): Basket
    {
        $basket = new Basket();
        $basket->user_id = 1;
        $basket->save();

        for ($i = 0; $i < 2; $i++) {
            $basketProduct = new BasketsProduct();
            $basketProduct->basket_id = 1;
            $basketProduct->product_id = $i === 0 ? 1 : 2;
            $basketProduct->quantity = $i === 0 ? 1 : 3;
            $basketProduct->save();
        }

        return $basket;
    }

    public function testUpdateBasket()
    {
        $this->initializedProductsAndUserDefault('admin');

        $quantities = ['2', '6'];
        $basket = $this->initBasketAndBasketProducts();
        $request = $this->put(route('product.basket.update', [$basket]), [
            'quantity' => $quantities
        ]);

        $basketProduct = BasketsProduct::find(1);
        $basketProduct2 = BasketsProduct::find(2);

        $this->assertEquals('2', $basketProduct->quantity);
        $this->assertEquals('6', $basketProduct2->quantity);
        $request->assertSessionHas('message', 'Votre panier à était modifié avec succés !!!');
        $request->assertStatus(302);
    }

    public function testUpdateBasketFailed()
    {
        $this->initializedProductsAndUserDefault('admin');

        $quantities = '5';
        $basket = $this->initBasketAndBasketProducts();
        $request = $this->put(route('product.basket.update', [$basket]), [
            'quantity' => $quantities
        ]);

        $basketProduct = BasketsProduct::find(1);
        $basketProduct2 = BasketsProduct::find(2);

        $this->assertEquals('1', $basketProduct->quantity);
        $this->assertEquals('3', $basketProduct2->quantity);
        $request->assertSessionHas('messageError', 'Impossible de modifié votre panier, veuillez recommencer cette action !!!');
        $request->assertStatus(302);
    }
}
