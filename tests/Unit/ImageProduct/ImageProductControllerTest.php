<?php
namespace Tests\Unit\ImageProduct;

use App\ImageProduct;
use App\Product;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;

class ImageProductControllerTest extends \Tests\TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testAddNewImageToProduct()
    {
        $this->post(route('image-product.store'), [
            'file' => UploadedFile::fake()->image('file.png', 1980, 1080)
        ]);

        $directoryImage = public_path() . DIRECTORY_SEPARATOR . 'img/productImages/';

        $currentProduct = Product::find(1);
        $this->assertEmpty($currentProduct->name);
        $this->assertEmpty($currentProduct->content);
        $this->assertEquals(1, Product::count());

        $this->assertTrue(file_exists($directoryImage . 'file.png'));
        $this->assertTrue(file_exists($directoryImage . 'file-450x250.png'));
        $this->assertTrue(file_exists($directoryImage . 'file-200x200.png'));

        $imgMedium = getimagesize($directoryImage . 'file-450x250.png');
        $imgThumb = getimagesize($directoryImage . 'file-200x200.png');
        $this->assertContains(450, $imgMedium);
        $this->assertContains(200, $imgThumb);
    }

    public function testAddNewImageToProductInTheDatabase()
    {
        $this->post(route('image-product.store'), [
            'file' => UploadedFile::fake()->image('file.png', 1980, 1080)
        ]);

        $currentProduct = Product::find(1);
        $this->assertEmpty($currentProduct->name);
        $this->assertEmpty($currentProduct->content);
        $this->assertEquals(1, Product::count());

        $currentImageProduct = ImageProduct::find(1);
        $this->assertEquals(1, ImageProduct::count());
        $this->assertEquals(1, $currentImageProduct->product_id);
        $this->assertEquals('file.png', $currentImageProduct->name);
    }

    public function testInputFileNotFound()
    {
        $request = $this->post(route('image-product.store'), [
            'fileFake' => UploadedFile::fake()->image('file.png', 1980, 1080)
        ]);

        $request->assertStatus(302);
        $this->assertStringContainsString('Redirecting to http://localhost/product/create', $request->content());
    }
}
