<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="column-xl-3 column-md-4 column-sidebar">
                <section class="widget widget-categories">
                    @include('products.Widget.widget-categories', [$categories])
                </section>
                <section class="widget widget-filter-price">
                    @include('products.Widget.widget-filter-price')
                </section>
                <section class="widget widget-filter-size">
                    @include('products.Widget.widget-filter-size')
                </section>
                <section class="widget widget-filter-color">
                    @include('products.Widget.widget-filter-color')
                </section>
            </div>
            <div class="column-xl-9 column-md-8">
                <div class="products-filter-container">
                    <header class="products-filter-header"></header>
                    <div class="products-sorting">
                        <div class="products-show-per-page">
                            <span>Show</span>
                            <a href="{{ route('product-filter.page', 9) }}" title="9" class="filter-page">
                                <span>9</span>
                            </a>
                            <a href="{{ route('product-filter.page', 12) }}" title="12" class="filter-page">
                                <span>12</span>
                            </a>
                            <a href="{{ route('product-filter.page', 18) }}" title="18" class="filter-page">
                                <span>18</span>
                            </a>
                            <a href="{{ route('product-filter.page', 24) }}" title="24" class="filter-page">
                                <span>24</span>
                            </a>
                        </div>
                        <div class="products-display-mode">
                            <a href="#" title="List" class="top-btn">
                                        <span>
                                            <svg class="active" style="fill:  #CCC;" version="1.1" id="list-view"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="18"
                                                 height="18" viewBox="0 0 18 18" enable-background="new 0 0 18 18"
                                                 xml:space="preserve"> <rect width="18" height="2"></rect> <rect y="16"
                                                                                                                 width="18"
                                                                                                                 height="2"></rect> <rect
                                                    y="8" width="18" height="2"></rect> </svg>
                                        </span>
                            </a>
                            <a href="#" title="List" class="top-btn">
                                        <span>
                                            <svg style="fill:  #CCC;" version="1.1" id="Layer_1"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px"
                                                 height="19px" viewBox="0 0 19 19" enable-background="new 0 0 19 19"
                                                 xml:space="preserve"> <path d="M7,2v5H2V2H7 M9,0H0v9h9V0L9,0z"></path> <path
                                                    d="M17,2v5h-5V2H17 M19,0h-9v9h9V0L19,0z"></path> <path
                                                    d="M7,12v5H2v-5H7 M9,10H0v9h9V10L9,10z"></path> <path
                                                    d="M17,12v5h-5v-5H17 M19,10h-9v9h9V10L19,10z"></path> </svg>
                                        </span>
                            </a>
                            <a href="#" title="List" class="top-btn">
                                        <span>
                                            <svg style="fill:  #CCC;" version="1.1" id="Layer_1"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px"
                                                 height="19px" viewBox="0 0 19 19" enable-background="new 0 0 19 19"
                                                 xml:space="preserve"> <rect width="5" height="5"></rect> <rect x="7"
                                                                                                                width="5"
                                                                                                                height="5"></rect> <rect
                                                    x="14" width="5" height="5"></rect> <rect y="7" width="5"
                                                                                              height="5"></rect> <rect
                                                    x="7" y="7" width="5" height="5"></rect> <rect x="14" y="7"
                                                                                                   width="5"
                                                                                                   height="5"></rect> <rect
                                                    y="14" width="5" height="5"></rect> <rect x="7" y="14" width="5"
                                                                                              height="5"></rect> <rect
                                                    x="14" y="14" width="5" height="5"></rect> </svg>
                                        </span>
                            </a>
                            <a href="#" title="List" class="top-btn">
                                        <span>
                                            <svg style="fill:  #CCC;" version="1.1" id="Layer_1"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px"
                                                 height="19px" viewBox="0 0 19 19" enable-background="new 0 0 19 19"
                                                 xml:space="preserve"> <rect width="4" height="4"></rect> <rect x="5"
                                                                                                                width="4"
                                                                                                                height="4"></rect> <rect
                                                    x="10" width="4" height="4"></rect> <rect x="15" width="4"
                                                                                              height="4"></rect> <rect
                                                    y="5" width="4" height="4"></rect> <rect x="5" y="5" width="4"
                                                                                             height="4"></rect> <rect
                                                    x="10" y="5" width="4" height="4"></rect> <rect x="15" y="5"
                                                                                                    width="4"
                                                                                                    height="4"></rect> <rect
                                                    y="15" width="4" height="4"></rect> <rect x="5" y="15" width="4"
                                                                                              height="4"></rect> <rect
                                                    x="10" y="15" width="4" height="4"></rect> <rect x="15" y="15"
                                                                                                     width="4"
                                                                                                     height="4"></rect> <rect
                                                    y="10" width="4" height="4"></rect> <rect x="5" y="10" width="4"
                                                                                              height="4"></rect> <rect
                                                    x="10" y="10" width="4" height="4"></rect> <rect x="15" y="10"
                                                                                                     width="4"
                                                                                                     height="4"></rect> </svg>
                                        </span>
                            </a>
                        </div>
                        <form action="{{ route('product-filter.clear') }}" method="GET" class="products-ordering">
                            <select name="orderby" class="orderby" aria-label="Shop order">
                                <option value="clear" selected="selected">Default sorting</option>
                                <option value="popularity">Sort by popularity</option>
                                <option value="date">Sort by latest</option>
                                <option value="price-asc">Sort by price: low to high</option>
                                <option value="price-desc">Sort by price: high to low</option>
                            </select>
                        </form>
                    </div>
                    <div class="products-selected-filters">
                        <a href="{{ route('product-filter.clear') }}" class="products-clear-filters" id="clear-filter">
                            Clear filters
                        </a>
                        <div class="widget widget-nav-filters">
                            <h2 class="widget-title-nav-filters">Active filters</h2>
                            <ul id="filters-list">
                                <li class="chosen"></li>
                            </ul>
                        </div>
                    </div>
                    <ul class="products columns-4" id="block-products">
                        @foreach($products as $product)
                            <li class="product">
                                <div class="product-contain">
                                    <div class="product-content-top">
                                        <div class="product-image">
                                            <a href="#">
                                                <img
                                                    src="{{ isset($product->images[0]) ? $product->images[0]->img_medium : '' }}"
                                                    class="product-img" width="420" height="510" alt="">
                                            </a>
                                            <div class="product-active-buttons">
                                                <div class="button-cart top-btn" aria-label="Add To Cart">
                                                    <a href="#" class="btn btn-add-cart">
                                                        <i class="fa fa-shopping-cart icon icon-bag-cart"></i>
                                                    </a>
                                                </div>
                                                <div class="button-cart top-btn" aria-label="Quick View">
                                                    <a href="{{ route('product.show', [$product]) }}" class="btn btn-add-cart">
                                                        <i class="fa fa-shopping-bag icon icon-bag-cart"></i>
                                                    </a>
                                                </div>
                                                <div class="button-cart top-btn" aria-label="Quick View">
                                                    <a href="#" class="btn btn-add-cart">
                                                        <i class="fa fa-heart-o icon icon-bag-cart"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="padding-left: 25px;">
                                        <h3 class="product-title">
                                            <a href="{{ route('product.show', [$product]) }}">{{ $product->name }}</a>
                                        </h3>
                                        <div class="star-rating">
                                                        <span style="width: 100%">
                                                            Rated
                                                            <strong class="rating">5.00</strong>
                                                             out of 5
                                                        </span>
                                        </div>
                                        <span class="price price-filter-products">
                                                        <span class="price-amount">
                                                            <span class="price-symbol">$</span>{{ $product->price . '.00' }}
                                                        </span>
                                            </span>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <div class="products-sorting">
                        <p class="products-result-count"> Showing 1–12 of 16 results</p>
                        <nav class="products-pagination">
                            <ul class="page-numbers">
                                <li>
                                    <span aria-current="page" class="current">1</span>
                                </li>
                                <li>
                                    <a href="#">2</a>
                                </li>
                                <li>
                                    <a href="#" class="next">→</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
