@foreach($products as $product)
    <div class="modal fade modal-product-quantity-{{ $product->id }}" tabindex="-1" role="dialog" aria-label="ModalQuantityProducts"
         aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0 font-h5-size font-weight-light-bold">Caractéristiques de votre produit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="mb-4">
                        Bonjour cher Admin, sur ce modal vous pouvez modifier ou ajouter les nouvelles caractéristiques de votre produit choisis sur ce dernier par couleurs mais également par tailles
                    </p>
                    <div class="text-sm-left">
                        <a href="{{ route('colors-sizes.create', [$product]) }}" class="btn btn-submit-success btn-success btn-rounded effect-click-btn mb-2 mr-2">
                            <i class="fa fa-plus mr-1"></i>
                            Ajouter de nouvelles caractéristiques
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-centered table-nowrap">
                            <thead class="thead-light">
                            <tr>
                                <th>ID</th>
                                <th>Couleur</th>
                                <th>Size</th>
                                <th>Quantité</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($product->ColorSize as $key => $colorSize)
                                <tr id="colorSize-{{ $colorSize->id }}">
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $colorSize->color }}</td>
                                    <td>{{ $colorSize->size }}</td>
                                    <td>{{ $colorSize->quantity }}</td>
                                    <td>
                                        <a href="{{ route('colors-sizes.edit', [$product, $colorSize->id]) }}"
                                           class="mr-3 text-primary">
                                            <i class="fa fa-pencil font-size-14"></i>
                                        </a>
                                        <a href="#" class="text-danger" id="delete-colorSize"
                                           data-colorSize-id="{{ $colorSize->id }}"
                                           data-user-auth="{{ Auth::id() }}">
                                            <i class="fa fa-close font-size-14"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
