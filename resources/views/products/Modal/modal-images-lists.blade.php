<div class="modal fade modal-delete-images" tabindex="-1" role="dialog" aria-label="ModalDeleteImage"
     aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0 font-h5-size font-weight-light-bold">Supprimer une de vos images</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="mb-4">
                    Bonjour cher Admin, sur ce modal vous pouvez supprimer une de vos images sur votre produit associée
                    à ce dernier !!!
                </p>
                <div class="table-responsive">
                    <form action="#" method="POST" id="delete-files">
                        <input type="hidden" value="{{ $product->id }}" id="product-value">
                        <table class="table table-nowrap table-centered mb-0">
                            <tbody>
                            @foreach($images as $image)
                                <tr id="image-list-{{ $image->id }}">
                                    <td style="width: 60px">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input"
                                                   value="{{ $image->id }}"
                                                   id="custom-{{$image->name}}">
                                            <label for="custom-{{$image->name}}" class="custom-control-label"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <h5 class="text-truncate font-size-14 mr-0">{{ $image->name }}</h5>
                                    </td>
                                    <td>
                                        <img class="rounded mr-2" alt="50x50" width="50" src="{{ $image->img_thumb }}" data-holder-rendered="true">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if (!empty($images->toArray()))
                            <button class="btn btn-sm btn-primary btn-submit-primary">Envoyer</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
