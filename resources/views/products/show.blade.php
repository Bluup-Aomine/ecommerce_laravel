@extends('layouts.app')

@section('title')
    {{ $product->name }} vegan - POE&TIK
@endsection

@section('content')
    <div class="content-title-head-page color-bg-black">
        <div class="container">
            <div class="title-contain">
                <h2>Produit "{{ $product->name }}"</h2>
            </div>
            <span>
                <a href="{{ route('home') }}" class="home">
                    <span>Accueil</span>
                </a>
            </span>
            <span class="fa fa-angle-right"></span>
            <span>
                <a href="{{ route('product.show', [$product]) }}" class="home">
                    <span>Produit "{{ $product->name }}"</span>
                </a>
            </span>
        </div>
    </div>
    <div id="shop-product">
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="site-main" id="container-main">
                            <div id="message-json"></div>
                            <div class="auth-account-notices"></div>
                            <div class="single-product product">
                                <div class="contain-single-product">
                                    <div class="row">
                                        <div class="col-6 col-xl-6 col-lg-6 col-xs-6">
                                            <div class="product-gallery product-gallery-with-image">
                                                <!-- Slider main container -->
                                                <div class="swiper-container swiper-container-horizontal"
                                                     id="slider-image"
                                                     data-images-count="{{ count($product->images) }}">
                                                    <!-- Additional required wrapper -->
                                                    <div class="swiper-wrapper">
                                                        <!-- Slides -->
                                                        @if (!empty($product->images->toArray()))
                                                            @foreach($product->images as $image)
                                                                <div class="swiper-slide">
                                                                    <div>
                                                                        <a href="{{ $image->img_thumb }}">
                                                                            <img class="image-product" width="800"
                                                                                 height="973"
                                                                                 src="{{ $image->img_original }}"
                                                                                 alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="swiper-slide">
                                                                <div>
                                                                    <a href="#">
                                                                        <svg class="bd-placeholder-img card-img-top"
                                                                             width="100%" height="225"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             preserveAspectRatio="xMidYMid slice"
                                                                             focusable="false" role="img"
                                                                             aria-label="Placeholder: Thumbnail"><title>
                                                                                Placeholder</title>
                                                                            <rect width="100%" height="100%"
                                                                                  fill="#55595c"></rect>
                                                                            <text x="50%" y="50%" fill="#eceeef"
                                                                                  dy=".3em">Image non valide
                                                                            </text>
                                                                        </svg>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <!-- If we need pagination -->
                                                    <div
                                                        class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"></div>

                                                    <!-- If we need navigation buttons -->
                                                    <div class="swiper-button-prev"></div>
                                                    <div class="swiper-button-next"></div>

                                                    <!-- If we need scrollbar -->
                                                </div>

                                                <!-- Slider main pagination images -->
                                                <div
                                                    class="swiper-pagination-images swiper-container swiper-container-horizontal swipper-gallery"
                                                    style="padding-top: 15px;">
                                                    <div class="swiper-wrapper">
                                                        <!-- Slides -->
                                                        @if (!empty($product->images->toArray()))
                                                            @foreach($product->images as $image)
                                                                <div class="swiper-slide"
                                                                     style="width: 106px; margin-right: 10px;">
                                                                    <img class="image-product"
                                                                         width="100" height="100"
                                                                         src="{{ $image->img_original }}"
                                                                         alt="">
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="swiper-slide">
                                                                <div>
                                                                    <a href="#">
                                                                        <svg class="bd-placeholder-img card-img-top"
                                                                             style="font-size: 11px!important;"
                                                                             width="100" height="100"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             preserveAspectRatio="xMidYMid slice"
                                                                             focusable="false" role="img"
                                                                             aria-label="Placeholder: Thumbnail"><title>
                                                                                Placeholder</title>
                                                                            <rect width="100%" height="100%"
                                                                                  fill="#55595c"></rect>
                                                                            <text x="50%" y="50%" fill="#eceeef"
                                                                                  dy=".3em">Image non valide
                                                                            </text>
                                                                        </svg>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 col-xl-6 col-lg-6 col-xs-6">
                                            <div class="summary entry-summary">
                                                <div class="summary-contain">
                                                    <h1 class="product-title">{{ $product->name }}</h1>
                                                    <div class="product-short-description">
                                                        <p>{{ $product->category->content }}</p>
                                                    </div>
                                                    <p class="price">
                                                        @if ($product->promo)
                                                            <del>
                                                                <span class="price-amount">
                                                                    <span class="price-symbol">€</span>{{ $product->price }}
                                                                </span>
                                                            </del>
                                                            <ins>
                                                                <span class="price-amount">
                                                                    <span class="price-symbol">€</span>{{ $product->price_promo }}.00
                                                                </span>
                                                            </ins>
                                                        @else
                                                            <span class="price-amount">
                                                                <span class="price-symbol">€</span>{{ $product->price }}
                                                            </span
                                                        @endif
                                                    </p>
                                                    <form action="{{ route('product.basket.store') }}" class="cart"
                                                          method="POST" id="add-to-cart">
                                                        <input type="hidden" data-product-id="{{ $product->id }}"
                                                               id="add-card-product-id">
                                                        @include('products.Form.sizes-colors')
                                                        <div class="quantity-cart">
                                                            <input type="button" value="-"
                                                                   class="min-quantity-products">
                                                            <div class="quantity">
                                                                <label for="quantity-product"
                                                                       class="reader-product-text">Arizona Sandals
                                                                    quantity</label>
                                                                <input type="number" id="quantity-product"
                                                                       class="quantity-product" step="1" min="1" max=""
                                                                       name="quantity" value="1" title="quantity"
                                                                       inputmode="numeric" size="4">
                                                            </div>
                                                            <input type="button" value="+"
                                                                   class="plus-quantity-products">
                                                        </div>
                                                        <button type="submit" name="add-to-cart"
                                                                class="btn-add-car-product">
                                                            <i class="fa fa-shopping-cart cart-btn"></i>
                                                            Ajouter au panier
                                                        </button>
                                                    </form>
                                                    <div class="add-to-wishlist-btn">
                                                        <div class="wishlist-add-block">
                                                            <form action="{{ route('product.wishlist.add') }}"
                                                                  method="POST">
                                                                @csrf
                                                                <a href="" type="button"
                                                                   class="add-wishlist-action {{ $wishlist ? 'd-none' : '' }}"
                                                                   data-product-id="{{ $product->id }}">
                                                                    <i class="fa fa-heart-o"></i>
                                                                    <span
                                                                        class="span-wishlist">Ajouter à votre Wishlist</span>
                                                                </a>
                                                                <a href="{{ route('product.wishlist.index') }}"
                                                                   class="add-wishlist-browse {{ $wishlist ? '' : 'd-none' }}">
                                                                    <i class="fa fa-heart-o"></i>
                                                                    <span
                                                                        class="span-wishlist">Browse Wishlist	</span>
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="product-meta">
                                                        <span class="posted">
                                                            Collections:
                                                            <a href="{{ route('collections.index', [$product->category]) }}">{{ $product->category->name }}</a>
                                                        </span>
                                                        <span class="posted">
                                                            Couleurs:
                                                            <a href="#">{{ $product->colors }}</a>
                                                        </span>
                                                        <span class="posted">
                                                            Tailles:
                                                            <a href="#" class="text-uppercase">{{ $product->sizes }}</a>
                                                        </span>
                                                    </div>
                                                    <div class="product-info">
                                                        <div class="product-info-desc">
                                                            <ul>
                                                                <li>Livraison en france métropolitaine gratuite pour 50€ d'achat</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-info-security">
                                                            <h5>Paiement sécurisé garanti</h5>
                                                            <img
                                                                src="http://demo2.themelexus.com/zoli/wp-content/uploads/2018/07/payment.png"
                                                                class="image-responsive" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-tabs products-accordions">
                                                <h3 class="title-single-product-ui-header ui-header-active">
                                                    <span class="arrow-product-ui-header"></span>
                                                    <a href="#" class="no-arrow-icon">
                                                        Description
                                                    </a>
                                                </h3>
                                                <div class="products-accordions-panel panel">
                                                    <h5>La description de ce produit vous permet de mieux comprendre les
                                                        caractéristiques de ce dernier</h5>
                                                    <p>{!! nl2br($product->content) !!}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <section class="related-products">
                                        <h2>Produits similaires</h2>
                                        <ul class="products">
                                            @include('products.Widget.related-products')
                                        </ul>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('extra-script')
    <script src="{{ asset('js/component/QuantityValueProduct.js') }}" type="module" defer></script>
@endsection
