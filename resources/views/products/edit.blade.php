@extends('template.admin.layout.app')

@section('content')
    <div class="container-fluid">
        <!-- Block Message Flash (error ou success) -->
    @include('template.admin.message')
    @include('template.admin.message')

    <!-- Block Title Content -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-content d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Editer votre produit</h4>
                </div>
            </div>
        </div>
        <!-- Block Form Content -->
        <div class="row">
            <div class="col-12">
                <!-- Start Tab Ul -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a href="#informations" class="nav-link active" data-toggle="tab" role="tab"
                           aria-selected="true">
                            <span class="d-sm-block">Informations</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#images" class="nav-link" data-toggle="tab" role="tab" aria-selected="false">
                            <span class="d-sm-block">Images</span>
                        </a>
                    </li>
                </ul>
                <!-- Start Tab panes -->
                <div class="tab-content pt-2 text-muted">
                    <div class="tab-pane active" id="informations" role="tabpanel">
                        @include('products.session.form-informations-session')
                    </div>
                    <div class="tab-pane" id="images" role="tabpanel">
                        @include('products.session.form-dropzone-session')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-script')
<script src="{{ asset('js/admin/Action/FieldToggleState.js') }}" type="module" defer></script>
@endsection
