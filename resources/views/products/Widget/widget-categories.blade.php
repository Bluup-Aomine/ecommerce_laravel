<h2 class="widget-title">categories</h2>
<ul class="product-categories">
    @foreach($categories as $category)
        <li class="category-item">
            <a href="{{ route('product-filter.category', $category['slug']) }}" class="url-filter">
                {{ $category['name'] }}
            </a>
            <span class="count">({{ $category['count'] }})</span>
        </li>
    @endforeach
</ul>
