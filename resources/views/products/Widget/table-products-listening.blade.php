<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="mb-2">
                <div class="text-sm-left">
                    <a href="{{ route('product.create') }}"
                       class="btn btn-submit-success btn-success btn-rounded effect-click-btn mb-2 mr-2">
                        <i class="fa fa-plus mr-1"></i>
                        Ajouter un nouveau produit
                    </a>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-centered table-nowrap">
                    <thead class="thead-light">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>Description</th>
                        <th>Prix</th>
                        <th>Promo</th>
                        <th>Catégorie</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $key => $product)
                        <tr id="product-{{ $product->id }}">
                            <th>{{ $key + 1 }}</th>
                            <th>{{ $product->name }}</th>
                            <th>{{ $product->slug }}</th>
                            <th>{{ $product->getContent() }}</th>
                            <th>{{ $product->price . '€' }}</th>
                            <th>{{ $product->promo ? 'OUI' : 'NON' }}</th>
                            <th>
                                <a href="{{ route('listening.products.categories', [$product->category]) }}">{{ $product->category->name }}</a>
                            </th>
                            <th>{{ $product->created_at }}</th>
                            <th>
                                <a href="{{ route('product.edit', [$product]) }}" class="mr-3 text-primary">
                                    <i class="fa fa-pencil font-size-14"></i>
                                </a>
                                <a href="" class="mr-3 text-success" data-toggle="modal"
                                   data-target=".modal-product-quantity-{{ $product->id }}">
                                    <i class="fa fa-suitcase font-size-14"></i>
                                </a>
                                <a href="#" class="text-danger" id="delete-product" data-product-id="{{ $product->id }}"
                                   data-user-auth="{{ Auth::id() }}">
                                    <i class="fa fa-close font-size-14"></i>
                                </a>
                            </th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('products.Modal.modal-products-quantity', [$products])

