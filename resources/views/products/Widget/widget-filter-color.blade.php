<h2 class="widget-title">Filter by color</h2>
<ul class="product-color">
    @foreach($colors as $color)
        <li class="category-item">
            <span class="filter-color" style="background-color: {{ $color['color'] }};"></span>
            <a href="{{ route('product-filter.color', $color['item']) }}" class="url-filter">
                {{ $color['item'] }}
            </a>
            <span class="count">({{ $color['count'] }})</span>
        </li>
    @endforeach
</ul>
