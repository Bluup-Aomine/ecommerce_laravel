<div class="card">
    <div class="card-body">

        <h4 class="card-title">
            Vos Images
            <p class="text-right">
                <button class="btn btn-danger btn-submit-danger" data-toggle="modal" data-target=".modal-delete-images">Supprimer une de vos images</button>
            </p>
        </h4>
        <p class="card-title-desc">Zoom effect works only with images.</p>

        <div class="zoom-gallery">
            @foreach($images as $image)
                <a class="float-left pl-3" id="image-list-{{ $image->id }}" href="{{ $image->img_original }}" target="_blank" title="Project 1">
                    <img src="{{ $image->img_original }}" alt="" width="275" height="200">
                </a>
            @endforeach
        </div>
    </div>
</div>
@include('products.Modal.modal-images-lists', [$images])
