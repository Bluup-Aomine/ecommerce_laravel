<h3 class="title-single-product-ui-header ui-header-active">
    <span class="arrow-product-ui-header"></span>
    <a href="#">
        Reviews (1)
    </a>
</h3>
<div class="products-accordions-panel panel">
    <div id="reviews">
        <div id="comments">
            <h2 class="reviews-title">
                1 review for<span>Arizona Sandals</span>
            </h2>
            <ol class="comments-list">
                <li class="comment comment-item">
                    <div class="comment-container">
                        <img src="http://0.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028?s=60&d=mm&r=g" alt=""
                             class="avatar" height="60" width="60">
                        <div class="comment-text">
                            <div class="star-rating" role="img">
                                 <span style="width:100%">
                                      Rated
                                      <strong class="rating">5</strong>
                                     out of 5
                                 </span>
                            </div>
                            <p class="comment-meta">
                                <strong>admin </strong>
                                <em class="verified">(verified owner)</em>
                                <time>January 31, 2019</time>
                            </p>
                            <div class="comment-description">
                                <p>
                                    Awesome design. Congratulations! GLWS
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
            </ol>
        </div>
        <div id="review-form">
            <div id="review-form-container">
                <div id="respond-comment">
                    <form action="#" method="POST" class="comment-form">
                        <div class="comment-rating-form">
                            <label for="rating">Your rating</label>
                            <p class="stars">
                                                                                <span>
                                                                                    <a class="star-1" href="#">1</a>
                                                                                </span>
                                <span>
                                                                                    <a class="star-2" href="#">2</a>
                                                                                </span>
                                <span>
                                                                                    <a class="star-3" href="#">3</a>
                                                                                </span>
                                <span>
                                                                                    <a class="star-4" href="#">4</a>
                                                                                </span>
                                <span>
                                                                                    <a class="star-5" href="#">5</a>
                                                                                </span>
                            </p>
                            <select name="rating" id="rating" required style="display: none!important;">
                                <option value="">Rate…</option>
                                <option value="5">Perfect</option>
                                <option value="4">Good</option>
                                <option value="3">Average</option>
                                <option value="2">Not that bad</option>
                                <option value="1">Very poor</option>
                            </select>
                        </div>
                        <p class="comment-form-comment">
                            <label for="comment">
                                Your review
                                <span class="required">*</span>
                            </label>
                            <textarea id="comment" name="comment" cols="45" rows="8" required=""></textarea>
                        </p>
                        <p class="form-submit">
                            <input name="submit" type="submit" id="submit" class="btn btn-primary btn-comment-submit"
                                   value="Submit">
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
