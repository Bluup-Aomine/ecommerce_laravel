<div class="container">
    <div class="title-contain">
        <h2>Shop sur la collection "{{ $category->name }}"</h2>
    </div>
    <span>
        <a href="{{ route('home') }}" class="home">
            <span>Accueil</span>
        </a>
    </span>
    <span class="fa fa-angle-right"></span>
    <span>
        <a href="{{ route('collections.index', [$category]) }}" class="home">
            <span>Collections "{{ $category->name }}"</span>
        </a>
    </span>
</div>
