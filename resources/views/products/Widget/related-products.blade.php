@foreach($productsRelated as $productRelated)
    <li class="product">
        <div class="product-contain">
            <div class="product">
                <div class="product-content-top">
                    <div class="product-image">
                        <a href="{{ route('product.show', [$productRelated]) }}">
                            @if (isset($productRelated->images[0]))
                                <img src="{{ $productRelated->images[0]->img_medium }}"
                                     class="product-img" width="420"
                                     height="510"
                                     alt="">
                            @else
                                <svg class="bd-placeholder-img card-img-top"
                                     width="100%" height="225"
                                     xmlns="http://www.w3.org/2000/svg"
                                     preserveAspectRatio="xMidYMid slice"
                                     focusable="false" role="img"
                                     aria-label="Placeholder: Thumbnail"><title>
                                        Placeholder</title>
                                    <rect width="100%" height="100%"
                                          fill="#55595c"></rect>
                                    <text x="50%" y="50%" fill="#eceeef"
                                          dy=".3em">Image non valide
                                    </text>
                                </svg>
                            @endif
                        </a>
                        <div class="product-active-buttons">
                            <div class="button-cart top-btn"
                                 aria-label="Add To Cart">
                                <a href="{{ route('product.basket.store') }}" class="btn btn-add-cart"
                                   data-product-id="{{ $productRelated->id }}" id="btnAddCartIcon">
                                    <i class="fa fa-shopping-cart icon icon-bag-cart"></i>
                                </a>
                            </div>
                            <div class="button-cart top-btn"
                                 aria-label="Quick View">
                                <a href="{{ route('product.show', [$productRelated]) }}"
                                   class="btn btn-add-cart">
                                    <i class="fa fa-shopping-bag icon icon-bag-cart"></i>
                                </a>
                            </div>
                            <div class="button-cart top-btn"
                                 aria-label="Quick View">
                                <a href="{{ route('product.wishlist.add') }}"
                                   class="btn btn-add-cart {{ isset($productRelated->wishlistProducts[0]) ? 'd-none' : '' }}"
                                   data-product-id="{{ $product->id }}"
                                   id="btn-add-wishlist">
                                    <i class="fa fa-heart-o icon icon-bag-cart"></i>
                                </a>
                                <a href="{{ route('product.wishlist.index') }}"
                                   class="btn btn-add-cart {{ !isset($productRelated->wishlistProducts[0]) ? 'd-none' : '' }}"
                                   id="wishlist-index-{{ $product->id }}">
                                    <i class="fa fa-heart-o icon icon-bag-cart color-gold"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 class="product-title pb-5px">
                    <a href="{{ route('product.show', [$productRelated]) }}">{{ $productRelated->name }}</a>
                </h3>
                <span class="price price-filter-products">
                    <span class="price-amount">
                        <span class="price-symbol">€</span>{{ $productRelated->price }}
                    </span>
                </span>
            </div>
        </div>
    </li>
@endforeach
