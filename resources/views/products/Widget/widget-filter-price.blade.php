<h2 class="widget-title">Filter by price</h2>
<form action="{{ route('product-filter.price', ['min' => 10, 'max' => 15000]) }}" method="get" id="price-form-filter">
    <div id="ui-slider"></div>
    <div class="price-slider-amount">
        <div class="price-label" style="">
            Price: <span class="from-price">$20</span> — <span class="to-price">$670</span>
        </div>
    </div>
</form>
