<h2 class="widget-title">Filter by size</h2>
<ul class="product-sizes">
    @foreach($sizes as $size)
        <li class="category-item">
            <a href="{{ route('product-filter.size', $size['item']) }}" class="url-filter">
                {{ $size['item'] }}
            </a>
            <span class="count">({{ $size['count'] }})</span>
        </li>
    @endforeach
</ul>
