@if (isset($images) && !is_null($images))
    @include('products.Widget.images-lightbox', [$images])
@endif
<div class="card font-poppins card-dropzone">
    <div class="card-body">
        <h4 class="card-title">Product Image</h4>
        {!! form_start($formDropzone) !!}
        {!! form_end($formDropzone) !!}
    </div>
</div>
<button class="btn btn-primary btn-block btn-lg btn-submit-primary" id="dropzone-submit">Envoyer</button>
