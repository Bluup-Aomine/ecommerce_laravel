<div class="card font-poppins">
    <div class="card-body">
        <h4 class="card-title">Vos Informations</h4>
        <p class="card-title-desc">Fill all information below</p>
        {!! form_start($form) !!}
        <div class="row">
            <div class="col-sm-6">
                {!! form_row($form->name) !!}
                {!! form_row($form->price) !!}
                {!! form_row($form->colors) !!}
                {!! form_row($form->sizes) !!}
            </div>
            <div class="col-sm-6">
                {!! form_row($form->category_id) !!}
                {!! form_row($form->content) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                {!! form_row($form->promo) !!}
            </div>
            <div class="col-sm-6">
                {!! form_row($form->price_promo) !!}
            </div>
        </div>
    </div>
</div>
{!! form_row($form->submit) !!}
{!! form_end($form, false) !!}
