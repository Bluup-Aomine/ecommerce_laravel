<table class="variations" cellspacing="0">
    <tbody>
        <tr>
            <td class="label">
                <label for="sizes">Tailles</label>
            </td>
            <td class="value select-elements-product">
                <select name="sizes" id="sizes" class="select-element-product-raw">
                    <option value="">Choose an option</option>
                    @foreach($sizes as $size)
                        <option value="{{ $size }}" class="attached enabled">{{ mb_strtoupper($size) }}</option>
                    @endforeach
                </select>
                <ul class="items-product-wrapper">
                    @foreach($sizes as $size)
                        <li class="variable-item button-variable-item" data-title="{{ $size }}">
                            <span class="variable-item-span">{{ mb_strtoupper($size) }}</span>
                        </li>
                    @endforeach
                </ul>
            </td>
            <td class="label">
                <label for="colors">Couleurs</label>
            </td>
            <td class="value select-elements-product">
                <select name="colors" id="colors" class="select-element-product-raw">
                    <option value="">Choose an option</option>
                    @foreach($colors as $color)
                        <option value="{{ $color['name'] }}" class="attached enabled">{{ mb_strtoupper($color['name']) }}</option>
                    @endforeach
                </select>
                <ul class="items-product-wrapper">
                    @foreach($colors as $color)
                        <li class="variable-item button-variable-item" data-title="{{ $color['name'] }}">
                            <span class="variable-item-span variable-item-span-color" style="background-color: {{ $color['color'] }}">
                                {{ mb_strtoupper($color['name']) }}
                            </span>
                        </li>
                    @endforeach
                </ul>
            </td>
        </tr>
    </tbody>
</table>

@section('extra-js')
    <script lang="js" defer>
        const btnsItems = document.querySelectorAll('.button-variable-item')
        btnsItems.forEach((element) => {
            element.addEventListener('click', function (e) {
                e.preventDefault()
                const target = e.target instanceof HTMLSpanElement ? e.target.parentElement : e.target
                target.classList.add('selected')
                $(target).siblings().removeClass('selected')
            })
        })
    </script>
@endsection
