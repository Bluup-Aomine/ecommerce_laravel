@extends('layouts.app')

@section('title')
    Votre recherche {{ $search }} - POE&TIK
@endsection

@section('content')
    <div class="content-title-head-page color-bg-black" id="products-listening-collections">
        <div class="container">
            <div class="title-contain">
                <h2>Produits sur la recherche "{{ $search }}" </h2>
            </div>
            <span>
                <a href="#" class="home">
                    <span>Home</span>
                </a>
            </span>
            <span class="fa fa-angle-right"></span>
            <span>
                <a href="#" class="home">
                    <span>Products</span>
                </a>
            </span>
        </div>
    </div>
    <div id="listening-products">
        <div class="container-collections">
            @include('layouts.component.message.json')
            <div class="collection-products">
                <div class="row-products">
                    @foreach($productsHead as $product)
                        <div class="col-1">
                            <div class="cell-1">
                                <div class="products-collection image-row-collections">
                                    <div class="products-collection-visual">
                                        @if (isset($product->images[0]))
                                            <div class="products-collection-img image-collection-img no-contain"
                                                 style="z-index:1; background-image: url('{{ $product->images[0]->img_original }}');"></div>
                                            <a href="{{ route('product.show', [$product]) }}"
                                               class="products-collection-link"></a>
                                        @else
                                            <svg class="bd-placeholder-img card-img-top"
                                                 width="100%" height="225"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 preserveAspectRatio="xMidYMid slice"
                                                 focusable="false" role="img"
                                                 aria-label="Placeholder: Thumbnail"><title>
                                                    Placeholder</title>
                                                <rect width="100%" height="100%"
                                                      fill="#55595c"></rect>
                                                <text x="50%" y="50%" fill="#eceeef"
                                                      dy=".3em">Image non valide
                                                </text>
                                            </svg>
                                            <a href="{{ route('product.show', [$product]) }}"
                                               class="products-collection-link"></a>
                                        @endif
                                        <div class="products-collection-details">
                                            <h3 class="products-collection-details-title">{{ $product->name }}</h3>
                                            <span>
                                            <p class="products-collection-details-price">{{ $product->price }}€</p>
                                        </span>
                                            <p class="products-collection-details-text">
                                                <a href="#" class="products-collection-details-link"
                                                   id="btn-visual-modal" data-product-id="{{ $product->id }}">Quick
                                                    Shop</a>
                                                <svg id="ico-cart" viewBox="0 0 21 25">
                                                    <path
                                                        d="M1202.97,49.414l-1.88-19.249a0.472,0.472,0,0,0-.49-0.426h-3.68V29.207A4.3,4.3,0,0,0,1192.56,25h-0.09a4.3,4.3,0,0,0-4.36,4.207v0.532h-3.76a0.5,0.5,0,0,0-.5.426L1182,49.494a0.4,0.4,0,0,0,.14.346,0.622,0.622,0,0,0,.36.16h20a0.487,0.487,0,0,0,.5-0.479A0.146,0.146,0,0,0,1202.97,49.414Zm-13.9-20.207a3.337,3.337,0,0,1,3.4-3.275h0.09a3.335,3.335,0,0,1,3.39,3.275v0.532h-6.88V29.207Zm-6.02,19.861,1.74-18.371h3.32v2.716a0.5,0.5,0,0,0,.99,0V30.671h6.88v2.742a0.5,0.5,0,0,0,1,0V30.671h3.26l1.79,18.37h-18.98v0.027Z"
                                                        transform="translate(-1182 -25)"></path>
                                                </svg>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="products-collection-text">
                                        <p class="products-collection-text-info">
                                            <a href="{{ route('product.show', [$product]) }}"
                                               class="products-collection-text-info-link">
                                                <strong class="products-collection-name">{{ $product->name }}</strong>
                                                <span>— {{ $product->price }}€</span>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('collections.modal-background', [$product])
                    @endforeach
                </div>
                <div class="row-products col-33 pt-2">
                    @foreach($productsBody as $product)
                        <div class="col-1">
                            <div class="cell-1">
                                <div class="products-collection image-row-collections">
                                    <div class="products-collection-visual">
                                        @if (isset($product->images[0]))
                                            <div class="products-collection-img image-collection-img no-contain"
                                                 style="z-index:1; background-image: url('{{ $product->images[0]->img_original }}');"></div>
                                            <a href="{{ route('product.show', [$product]) }}"
                                               class="products-collection-link"></a>
                                        @else
                                            <svg class="bd-placeholder-img card-img-top"
                                                 width="100%" height="225"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 preserveAspectRatio="xMidYMid slice"
                                                 focusable="false" role="img"
                                                 aria-label="Placeholder: Thumbnail"><title>
                                                    Placeholder</title>
                                                <rect width="100%" height="100%"
                                                      fill="#55595c"></rect>
                                                <text x="50%" y="50%" fill="#eceeef"
                                                      dy=".3em">Image non valide
                                                </text>
                                            </svg>
                                            <a href="{{ route('product.show', [$product]) }}"
                                               class="products-collection-link"></a>
                                        @endif
                                        <div class="products-collection-details">
                                            <h3 class="products-collection-details-title">{{ $product->name }}</h3>
                                            <span>
                                            <p class="products-collection-details-price">{{ $product->price }}€</p>
                                        </span>
                                            <p class="products-collection-details-text">
                                                <a href="#" class="products-collection-details-link">Quick Shop</a>
                                                <svg id="ico-cart" viewBox="0 0 21 25">
                                                    <path
                                                        d="M1202.97,49.414l-1.88-19.249a0.472,0.472,0,0,0-.49-0.426h-3.68V29.207A4.3,4.3,0,0,0,1192.56,25h-0.09a4.3,4.3,0,0,0-4.36,4.207v0.532h-3.76a0.5,0.5,0,0,0-.5.426L1182,49.494a0.4,0.4,0,0,0,.14.346,0.622,0.622,0,0,0,.36.16h20a0.487,0.487,0,0,0,.5-0.479A0.146,0.146,0,0,0,1202.97,49.414Zm-13.9-20.207a3.337,3.337,0,0,1,3.4-3.275h0.09a3.335,3.335,0,0,1,3.39,3.275v0.532h-6.88V29.207Zm-6.02,19.861,1.74-18.371h3.32v2.716a0.5,0.5,0,0,0,.99,0V30.671h6.88v2.742a0.5,0.5,0,0,0,1,0V30.671h3.26l1.79,18.37h-18.98v0.027Z"
                                                        transform="translate(-1182 -25)"></path>
                                                </svg>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="products-collection-text">
                                        <p class="products-collection-text-info">
                                            <a href="#" class="products-collection-text-info-link">
                                                <strong class="products-collection-name">{{ $product->name }}</strong>
                                                <span>— {{ $product->price }}€</span>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('collections.modal-background', [$product])
                    @endforeach
                </div>
                <div class="row-products pt-2">
                    @foreach($productsFooter as $product)
                        <div class="col-1">
                            <div class="cell-1">
                                <div class="products-collection image-row-collections">
                                    <div class="products-collection-visual">
                                        @if (isset($product->images[0]))
                                            <div class="products-collection-img image-collection-img no-contain"
                                                 style="z-index:1; background-image: url('{{ $product->images[0]->img_original }}');"></div>
                                            <a href="{{ route('product.show', [$product]) }}"
                                               class="products-collection-link"></a>
                                        @else
                                            <svg class="bd-placeholder-img card-img-top"
                                                 width="100%" height="225"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 preserveAspectRatio="xMidYMid slice"
                                                 focusable="false" role="img"
                                                 aria-label="Placeholder: Thumbnail"><title>
                                                    Placeholder</title>
                                                <rect width="100%" height="100%"
                                                      fill="#55595c"></rect>
                                                <text x="50%" y="50%" fill="#eceeef"
                                                      dy=".3em">Image non valide
                                                </text>
                                            </svg>
                                            <a href="{{ route('product.show', [$product]) }}"
                                               class="products-collection-link"></a>
                                        @endif
                                        <div class="products-collection-details">
                                            <h3 class="products-collection-details-title">{{ $product->name }}</h3>
                                            <span>
                                            <p class="products-collection-details-price">{{ $product->price }}€</p>
                                        </span>
                                            <p class="products-collection-details-text">
                                                <a href="#" class="products-collection-details-link">Quick Shop</a>
                                                <svg id="ico-cart" viewBox="0 0 21 25">
                                                    <path
                                                        d="M1202.97,49.414l-1.88-19.249a0.472,0.472,0,0,0-.49-0.426h-3.68V29.207A4.3,4.3,0,0,0,1192.56,25h-0.09a4.3,4.3,0,0,0-4.36,4.207v0.532h-3.76a0.5,0.5,0,0,0-.5.426L1182,49.494a0.4,0.4,0,0,0,.14.346,0.622,0.622,0,0,0,.36.16h20a0.487,0.487,0,0,0,.5-0.479A0.146,0.146,0,0,0,1202.97,49.414Zm-13.9-20.207a3.337,3.337,0,0,1,3.4-3.275h0.09a3.335,3.335,0,0,1,3.39,3.275v0.532h-6.88V29.207Zm-6.02,19.861,1.74-18.371h3.32v2.716a0.5,0.5,0,0,0,.99,0V30.671h6.88v2.742a0.5,0.5,0,0,0,1,0V30.671h3.26l1.79,18.37h-18.98v0.027Z"
                                                        transform="translate(-1182 -25)"></path>
                                                </svg>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="products-collection-text">
                                        <p class="products-collection-text-info">
                                            <a href="#" class="products-collection-text-info-link">
                                                <strong class="products-collection-name">{{ $product->name }}</strong>
                                                <span>— {{ $product->price }}€</span>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('collections.modal-background', [$product])
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
