@extends('template.admin.layout.app')

@section('content')
    <div class="container-fluid">
        <!-- Block Message Flash (error ou success) -->
        @include('template.admin.message')

        <!-- Block Title Content -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-content d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Ajouter un produit</h4>
                </div>
            </div>
        </div>
        <!-- Block Form Content -->
        <div class="row">
            <div class="col-12">
                @include('products.Form.form-informations')
                @include('products.Form.form-dropzone')
            </div>
        </div>
    </div>
@endsection

@section('extra-script')
<script src="{{ asset('js/admin/Action/FieldToggleState.js') }}" type="module" defer></script>
@endsection
