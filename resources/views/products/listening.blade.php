@extends('template.admin.layout.app')

@section('content')
    <div class="container-fluid">
        <!-- Message Flash -->
    @include('template.admin.message')

    <!-- Block Title Content -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-content d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Vos Produits</h4>
                </div>
            </div>
        </div>

        <!-- Block Table Content -->
        <div class="row">
            @include('products.Widget.table-products-listening')
        </div>
    </div>
@endsection

@section('extra-script')
    <script src="{{ asset('js/admin/Action/DeleteProduct.js') }}" type="module" defer></script>
    <script src="{{ asset('js/admin/Action/DeleteColorSizeProduct.js') }}" type="module" defer></script>
@endsection
