@if ($page !== 'admin')
    @foreach($orders as $order)
        <div class="modal fade show order-products-{{ $order['id'] }}" tabindex="-1" role="dialog" aria-modal="true"
             aria-labelledby="order-products">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-h5-size mt-0 font-weight-light-bold">Order Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="mb-2">
                            Product id:
                            <span class="text-primary">#SK{{ $order['id'] }}</span>
                        </p>
                        <p class="mb-4">
                            Billing Name:
                            @if ($order instanceof \App\OrderPaypal)
                                <span class="text-primary">{{ \App\OrderPaypal::getName($order) }}</span>
                            @else
                                <span class="text-primary">{{ \App\Order::getName($order) }}</span>
                            @endif
                        </p>
                        <p class="mb-4">
                            Other Notes:
                            <span class="text-primary">{{ $order['order_notes'] }}</span>
                        </p>
                        <div class="table-responsive">
                            <table class="table table-centered table-nowrap">
                                <thead>
                                <tr>
                                    <th scope="col">Product</th>
                                    <th scope="col">Product Name</th>
                                    <th scope="col">Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order['products'] as $product)
                                    <tr>
                                        <th scope="row">
                                            <div>
                                                <img src="{{ $product['image'] }}" class="avatar-sm" alt="">
                                            </div>
                                        </th>
                                        <td>
                                            <div>
                                                <h5 class="text-truncate font-size-14">{{ $product['name'] }}</h5>
                                                <p class="text-muted mb-0"> {{ $product['price'] }} €
                                                    x {{ $product['quantity'] }}</p>
                                            </div>
                                        </td>
                                        <td>{{ $product['total'] }} €</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="2">
                                        <h6 class="mb-0 text-right font-h6-size">Sub Total:</h6>
                                    </td>
                                    <td>{{ \App\src\helpers\Price::addTotal($order['products']) }} €</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <h6 class="mb-0 text-right font-h6-size">Shipping:</h6>
                                    </td>
                                    <td>{{ \App\src\helpers\Price::addTotal($order['products']) > 50 ? 'GRATUIT' : \App\src\helpers\Price::shipping($order['products']) }} €</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <h6 class="mb-0 text-right font-h6-size">TVA:</h6>
                                    </td>
                                    <td>{{ $order->tva ? 'AVEC' : 'SANS' }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <h6 class="mb-0 text-right font-h6-size">Total:</h6>
                                    </td>
                                    <td>{{ $order['amount'] }} €</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-submit-primary" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
