@extends('layouts.app')

@section('title')
    Collection T-Shirt vegan - POE&TIK
@endsection

@section('content')
    <div class="container-collections">
        <div class="collections">
            @if (isset($categories[0]))
                <div class="row" style="margin-bottom: 22rem">
                    <div class="col-1">
                        <div class="cell-1 mb-collection-cell">
                            <a href="{{ route('collections.index', $categories[0]) }}"
                               class="image-collection image-collection-light">
                                <div class="image-collection-visual">
                                    <div class="image-collection-panel image-collection-panel-middle">
                                        <h3 class="image-collection-title">{{ $categories[0]->name }}</h3>
                                        <span class="btn btn--light">Découvrir</span>
                                    </div>
                                    <div class="image-collection-img"
                                         style="background-image: url('{{ asset('img/t-shirt.jpg') }}')"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row-collections-title">
                <div class="col-1">
                    <div class="cell-1">
                        <div class="collections-title">
                            <h3 class="collections-title-title">A Savoir</h3>
                            <p class="collections-title-text" style="font-size: 1.3rem">LA LIVRAISON EST OFFERTE DÈS 50€ DE COMMANDE ET LES
                                RETOURS SONT À NOTRE CHARGE.<br>
                                <br>
                                Pour chaque article acheté, 1€ du prix du vêtement sera reversé à une ONG qui œuvre pour
                                le respect de l’environnement et du bien être animal.<br>
                                <br>
                                VOUS POUVEZ PLANTER TOUTES LES ÉTIQUETTES DE NOS VÊTEMENTS ET LES REGARDER POUSSER
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            @if (isset($categories[1]))
                <div class="row" id="footer-collection">
                    <div class="col-1">
                        <div class="cell-1">
                            <a href="{{ route('collections.index', $categories[1]) }}"
                               class="image-collection image-collection-light">
                                <div class="image-collection-visual">
                                    <div class="image-collection-panel image-collection-panel-middle">
                                        <h3 class="image-collection-title">{{ $categories[1]->name }}</h3>
                                        <span class="btn btn--light">Découvrir</span>
                                    </div>
                                    <div class="image-collection-img" style="background-image: url('{{ asset('img/t-shirt.jpg') }}')"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
