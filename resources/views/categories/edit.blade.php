@extends('template.admin.layout.app')

@section('content')
    <div class="container-fluid">
        <!-- Block Title Content -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-content d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Editer une catégorie</h4>
                </div>
            </div>
        </div>
        <!-- Block Form Content -->
        <div class="row">
            <div class="col-12">
                <div class="card font-poppins">
                    <div class="card-body">
                        <h4 class="card-title">Vos Informations</h4>
                        <p class="card-title-desc">Fill all information below</p>
                        {!! form_start($form) !!}
                            {!! form_row($form->name, ['value' => $category->name]) !!}
                            {!! form_row($form->content, ['value' => $category->content]) !!}
                            {!! form_row($form->submit) !!}
                        {!! form_end($form) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
