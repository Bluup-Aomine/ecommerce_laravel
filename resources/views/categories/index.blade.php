@extends('template.admin.layout.app')

@section('content')
    <div class="container-fluid">
        <!-- Message Flash -->
        @include('template.admin.message')

        <!-- Block Title Content -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-content d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Vos Catégories</h4>
                </div>
            </div>
        </div>

        <!-- Block Table Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="mb-2">
                            <div class="text-sm-left">
                                <a href="{{ route('category.create') }}" class="btn btn-submit-success btn-success btn-rounded effect-click-btn mb-2 mr-2">
                                    <i class="fa fa-plus mr-1"></i>
                                    Ajouter une nouvelle catégorie
                                </a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-centered table-nowrap">
                                <thead class="thead-light">
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Content</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($categories as $key => $category)
                                        <tr>
                                            <th>{{ $key + 1 }}</th>
                                            <th>{{ $category->name }}</th>
                                            <th>{{ $category->slug }}</th>
                                            <th>{{ $category->getContent() }}</th>
                                            <th>{{ $category->created_at }}</th>
                                            <th>
                                                <a href="{{ route('category.edit', [$category]) }}" class="mr-3 text-primary">
                                                    <i class="fa fa-pencil font-size-14"></i>
                                                </a>
                                                <a href="#" class="text-danger">
                                                    <i class="fa fa-close font-size-14"></i>
                                                </a>
                                            </th>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
