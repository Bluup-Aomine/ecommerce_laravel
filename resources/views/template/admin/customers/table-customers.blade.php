<table class="table table-centered table-nowrap mb-0">
    <thead class="thead-light">
    <tr>
        <th>Customer ID</th>
        <th>Username</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Total Achat</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    @foreach($customers as $customer)
        <tr>
            <td>
                <a href="#" class="text-body font-weight-bold">#{{ $customer->id  }}</a>
            </td>
            <td>{{ $stripe->getCustomer($customer->customer_id)->name }}</td>
            <td>{{ $stripe->getCustomer($customer->customer_id)->email }}</td>
            <td>{{ $stripe->getCustomer($customer->customer_id)->phone }}</td>
            <td>{{ $stripe->getCustomer($customer->customer_id)->address->line1 }}</td>
            <td>{{ $customer->total_purchase }} €</td>
            <td>{{ $customer->created_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
