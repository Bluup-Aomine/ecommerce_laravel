@extends('template.admin.layout.app')

@section('content')
<div class="container-fluid">
    <!-- Message Flash -->
@include('template.admin.message')

<!-- Block Title Content -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-content d-flex align-items-center justify-content-between">
                <h4 class="mb-0">Les clients</h4>
            </div>
        </div>
    </div>

    <!-- Block Table Content -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        @if (!isset($stripe))
                            @include('template.admin.customers.table-customers-paypal', [$customers])
                        @else
                            @include('template.admin.customers.table-customers', [$customers])
                        @endif
                    </div>
                    {{ $customers->links('template.admin.layout.widget.pagination') }}
                </div>
            </div>
        </div>
    </div>
@endsection
