@extends('template.admin.layout.app')

@section('content')
    <div class="container-fluid">
        <!-- Message Flash -->
    @include('template.admin.message')

    <!-- Block Title Content -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-content d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Ajouter de nouvelle caractéristique sur un produit "{{ $product->name }}"</h4>
                </div>
            </div>
        </div>

        <!-- Block Form Content -->
        <div class="row">
            <div class="col-12">
                <div class="card font-poppins">
                    <div class="card-body">
                        <h4 class="card-title">Vos Informations</h4>
                        <p class="card-title-desc">Fill all information below</p>
                        {!! form_start($form) !!}
                        <div class="row">
                            <div class="col-sm-12">
                                {!! form_row($form->colors) !!}
                                {!! form_row($form->sizes) !!}
                                {!! form_row($form->quantity) !!}
                            </div>
                        </div>
                    </div>
                </div>
                {!! form_row($form->submit) !!}
                {!! form_end($form, false) !!}
            </div>
        </div>
    </div>
@endSection
