<div class="navbar-header">
    <div class="d-flex">
        <div class="navbar-box">
            <a href="{{ asset('admin') }}" class="logo logo-block" title="logo">
                <span id="logo-head">
                    <img src="{{ asset('img/icons/logo-text.jpg') }}" alt="" height="30">
               </span>
            </a>
        </div>
    </div>
    <div class="d-flex">
        <div class="dropdown d-inline-block">
            <button type="button" class="btn header-item effect-click-btn dropdown-header-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="rounded-circle header-profile-user" src="{{ asset('img/icons/logo-user.png') }}" alt="">
                <span class="d-none d-xl-inline-block ml-1 user-head-text">
                    {{ Auth::user()->name }}
                </span>
                <i class="fa fa-chevron-down d-none d-xl-inline-block"></i>
            </button>
            <div class="dropdown-menu item-font">
                <a href="{{ route('home') }}" class="dropdown-item">
                    <i class="fa fa-home font-size-icon-head align-middle mr-1"></i>
                    Accéder au site
                </a>
                <a href="#" class="dropdown-item">
                    <i class="fa fa-wrench font-size-icon-head align-middle mr-1"></i>
                    Settings
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('logout') }}" id="logout-action-link" class="dropdown-item text-danger">
                    <i class="fa fa-power-off font-size-icon-head align-middle mr-1 text-danger"></i>
                    Logout
                </a>
            </div>
        </div>
    </div>
</div>
