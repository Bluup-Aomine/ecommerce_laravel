<div class="container-fluid">
    <nav class="navbar navbar-expand-lg topnav-menu">
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav">
                @include('template.admin.layout.widget.items-menu')
            </ul>
        </div>
    </nav>
</div>
