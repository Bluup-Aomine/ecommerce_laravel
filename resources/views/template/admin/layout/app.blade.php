<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex,follow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ADMIN - POE&TIK</title>

    <!-- Scripts -->
    <script lang="js" src="{{ asset('js/library/jquery.min.js') }}" defer></script>
    <script lang="js" src="{{ asset('js/library/bootstrap.min.js') }}" defer></script>
    <script lang="js" src="{{ asset('js/library/dropzone.min.js') }}" defer></script>
    <script lang="js" src="{{ asset('js/admin/app.js') }}" type="module" defer></script>
    @yield('extra-script')

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Avenir" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/admin/app.scss') }}"/>
    <link rel="stylesheet" href="{{ asset('css/library/dropzone.min.css') }}"/>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
    <div class="layout-wrapper">
        <header id="topbar-header">
            @include('template.admin.layout.header')
        </header>
        <div class="topnav">
            @include('template.admin.layout.topbar')
        </div>
        <div class="main-content">
            <div class="page-content">
                @yield('content')
            </div>
        </div>
    </div>
</body>
</html>
