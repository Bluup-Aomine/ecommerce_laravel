@if ($paginator->hasPages())
    <ul class="pagination pagination-rounded justify-content-end mb-2 mt-4">

        <!-- Previous Page Link !-->
        @if ($paginator->onFirstPage())
            <li class="page-item disabled">
                <a href="#" class="page-link" aria-label="Previous">
                    <i class="fa fa-chevron-left"></i>
                </a>
            </li>
        @else
            <li class="page-item disabled">
                <a href="{{ $paginator->previousPageUrl() }}" class="page-link" aria-label="Previous">
                    <i class="fa fa-chevron-left"></i>
                </a>
            </li>
        @endif

        <!-- Block Link Page Number !-->
        @foreach($elements as $element)
            @if (is_string($element))
                <li class="page-item disabled">
                    <a href="#" class="page-link">{{ $element }}</a>
                </li>
            @endif

            @if (is_array($element))
                @foreach($element as $page => $url)
                    @if ($page === $paginator->currentPage())
                        <li class="page-item active">
                            <a href="#" class="page-link">{{ $page }}</a>
                        </li>
                        @else
                            <li class="page-item">
                                <a href="{{ $url }}" class="page-link">{{ $page }}</a>
                            </li>
                    @endif
                @endforeach
            @endif
        @endforeach

        <!-- Next Page Link !-->
        @if (!$paginator->hasMorePages())
            <li class="page-item disabled">
                <a class="page-link" href="#" aria-label="Next">
                    <i class="fa fa-chevron-right"></i>
                </a>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" aria-label="Next">
                    <i class="fa fa-chevron-right"></i>
                </a>
            </li>
        @endif
    </ul>
@endif
