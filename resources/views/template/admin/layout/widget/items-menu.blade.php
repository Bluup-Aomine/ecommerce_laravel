<li class="nav-item dropdown">
    <a href="#" class="nav-link dropdown-header-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-shopping-bag mr-2"></i>
        Products
        <div class="arrow-down"></div>
    </a>
    <div class="dropdown-menu">
        <a href="{{ route('listening.products') }}" class="dropdown-item">Voir produits</a>
        <a href="{{ route('product.create') }}" class="dropdown-item">Créer un produit</a>
    </div>
</li>
<li class="nav-item dropdown">
    <a href="#" class="nav-link dropdown-header-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-th-large mr-2"></i>
        Catégories
        <div class="arrow-down"></div>
    </a>
    <div class="dropdown-menu">
        <a href="{{ route('category.index') }}" class="dropdown-item">Voir catégories</a>
        <a href="{{ route('category.create') }}" class="dropdown-item">Créer une catégorie</a>
    </div>
</li>
<li class="nav-item dropdown">
    <a href="#" class="nav-link dropdown-header-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-tasks mr-2"></i>
        Orders
        <div class="arrow-down"></div>
    </a>
    <div class="dropdown-menu">
        <a href="{{ route('orders.index') }}" class="dropdown-item">Commandes par Carte</a>
        <a href="{{ route('orders.paypal.index') }}" class="dropdown-item">Commandes par Paypal</a>
    </div>
</li>
<li class="nav-item dropdown">
    <a href="#" class="nav-link dropdown-header-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-tasks mr-2"></i>
        Acheteurs
        <div class="arrow-down"></div>
    </a>
    <div class="dropdown-menu">
        <a href="{{ route('customers.index') }}" class="dropdown-item">Acheteurs par Carte</a>
        <a href="{{ route('customers.paypal.index') }}" class="dropdown-item">Acheteurs par Paypal</a>
    </div>
</li>
