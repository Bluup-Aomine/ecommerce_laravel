@extends('template.admin.layout.app')

@section('content')
    <div class="container-fluid">
        <!-- Block Message Flash (error ou success) -->
    @include('template.admin.message')

    <!-- Start Block Title Content -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-content d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Dashboard</h4>
                </div>
            </div>
        </div>
    <!-- End Block Title Content -->

    <!-- Start Block Content -->
        <div class="row">
            <div class="col-xl-4">
                <div class="card overflow-hidden">
                    <div class="bg-soft-primary">
                        <div class="row">
                            <div class="col-7">
                                <div class="text-primary p-3">
                                    <h5 class="text-primary font-h5-size">Welcome Back !</h5>
                                    <p>Skote Dashboard</p>
                                </div>
                            </div>
                            <div class="col-5 align-self-end">
                                <img src="{{ asset('img/dashboard-img.png') }}" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="avatar-md avatar-position mb-4">
                                    <img src="{{ asset('img/icons/logo-user.png') }}" alt="" class="img-thumbnail rounded-circle">
                                </div>
                                <h5 class="font-size-15 text-truncate">{{ $user->name }}</h5>
                                <p class="text-muted mb-0 text-truncate">Admin</p>
                            </div>
                            <div class="col-sm-8">
                                <div class="pt-4">
                                    <div class="row">
                                        <div class="col-6">
                                            <h5 class="font-size-15">{{ $countProducts }}</h5>
                                            <p class="text-muted mb-0">Produits</p>
                                        </div>
                                        <div class="col-6">
                                            <h5 class="font-size-15">€{{ is_float($sumOrders) ? $sumOrders : $sumOrders . '.00' }}</h5>
                                            <p class="text-muted mb-0">Revenue</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted font-weight-light-bold">Orders</p>
                                        <h4 class="mb-0 font-h4-size">{{ $countOrders }}</h4>
                                    </div>
                                    <div class="avatar-icon avatar-sm rounded-circle bg-primary align-self-center">
                                        <span class="avatar-title">
                                            <i class="fa fa-copy avatar-icon-dashboard"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted font-weight-light-bold">Revenue</p>
                                        <h4 class="mb-0 font-h4-size">€{{ is_float($sumOrders) ? $sumOrders : $sumOrders . '.00' }}</h4>
                                    </div>
                                    <div class="avatar-icon avatar-sm rounded-circle bg-primary align-self-center">
                                        <span class="avatar-title">
                                            <i class="fa fa-archive avatar-icon-dashboard"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted font-weight-light-bold">Average Price</p>
                                        <h4 class="mb-0 font-h4-size">€{{ $averageOrders }}</h4>
                                    </div>
                                    <div class="avatar-icon avatar-sm rounded-circle bg-primary align-self-center">
                                        <span class="avatar-title">
                                            <i class="fa fa-tags avatar-icon-dashboard"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted font-weight-light-bold">Products</p>
                                        <h4 class="mb-0 font-h4-size">{{ $countProducts }}</h4>
                                    </div>
                                    <div class="avatar-icon avatar-sm rounded-circle bg-primary align-self-center">
                                        <span class="avatar-title">
                                            <i class="fa fa-shopping-bag avatar-icon-dashboard"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted font-weight-light-bold">Categories</p>
                                        <h4 class="mb-0 font-h4-size">{{ $countCategories }}</h4>
                                    </div>
                                    <div class="avatar-icon avatar-sm rounded-circle bg-primary align-self-center">
                                        <span class="avatar-title">
                                            <i class="fa fa-th-large avatar-icon-dashboard"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted font-weight-light-bold">Total Products Bought</p>
                                        <h4 class="mb-0 font-h4-size">{{ $totalProducts }}</h4>
                                    </div>
                                    <div class="avatar-icon avatar-sm rounded-circle bg-primary align-self-center">
                                        <span class="avatar-title">
                                            <i class="fa fa-shopping-cart avatar-icon-dashboard"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Latest Products</h4>
                        <div class="table-responsive">
                            @include('template.admin.products.table-products')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Latest Transaction</h4>
                        <div class="table-responsive">
                            @include('template.admin.orders.table-orders')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Latest Transaction Paypal</h4>
                        <div class="table-responsive">
                            @include('template.admin.orders.table-orders-paypal')
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <!-- End Block Content -->
    </div>
@endsection
