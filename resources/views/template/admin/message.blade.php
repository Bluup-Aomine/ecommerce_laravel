@if(Session::has('message'))
    <div class="alert alert-success alert-dismissible fade show">
        <i class="fa fa-check mr-2"></i>
        {{ Session::get('message') }}
        <button class="close" type="button" aria-label="close" data-dismiss="alert">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@endif

@if(Session::has('messageError'))
    <div class="alert alert-danger alert-dismissible fade show">
        <i class="fa fa-check mr-2"></i>
        {{ Session::get('messageError') }}
        <button class="close" type="button" aria-label="close" data-dismiss="alert">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@endif

<div id="message-json">

</div>
