<table class="table table-centered table-nowrap mb-0">
    <thead class="thead-light">
    <tr>
        <th>Order ID</th>
        <th>Billing Name</th>
        <th>Date</th>
        <th>Total</th>
        <th>Payment Status</th>
        <th>Tracking Order</th>
        <th>Payment Method</th>
        @if ($page !== 'admin')
            <th>View Details</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($ordersPaypal as $order)
        <tr>
            <td>
                <a href="#" class="text-body font-weight-bold">#SK{{ $order['id'] }}</a>
            </td>
            <td>{{ \App\OrderPaypal::getName($order) }}</td>
            <td>{{ $order->created_at }}</td>
            <td>{{ $order->amount }}€</td>
            <td>
                <span class="badge badge-pill badge-soft-success font-size-12">Paid</span>
            </td>
            <td>
                @if ($order->tracking_order)
                    @if ($order->tracking_order === 'en cours de traitement')
                        <span
                            class="badge badge-pill badge-warning font-size-12">{{ ucfirst($order->tracking_order) }}</span>
                    @elseif($order->tracking_order === 'envoyer')
                        <span
                            class="badge badge-pill badge-primary font-size-12">{{ ucfirst($order->tracking_order) }}</span>
                    @elseif($order->tracking_order === 'reçu')
                        <span
                            class="badge badge-pill badge-soft-success font-size-12">{{ ucfirst($order->tracking_order) }}</span>
                    @endif
                @else
                    <span
                        class="badge badge-pill badge-danger font-size-12">Echec de l'envoi</span>
                @endif
            </td>
            <td>
                <i class="fa fa-cc-paypal mr-1 font-size-14"></i>
                {{ ucfirst('paypal') }}
            </td>
            @if ($page !== 'admin')
                <td>
                    <button type="button" class="btn btn-sm btn-rounded effect-click-btn btn-primary btn-submit-primary"
                            data-toggle="modal" data-target=".order-products-{{ $order['id'] }}">View Details
                    </button>
                    <a href="{{ route('orders.paypal.edit', [$order->id]) }}"
                       class="btn btn-sm btn-rounded effect-click-btn btn-success btn-submit-success">Update
                        tracking</a>
                </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
@include('orders.modal', ['orders' => $ordersPaypal])
