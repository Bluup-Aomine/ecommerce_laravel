<table class="table table-centered table-nowrap mb-0">
    <thead class="thead-light">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Slug</th>
        <th>Description</th>
        <th>Prix</th>
        <th>Promo</th>
        <th>Catégorie</th>
        <th>Date</th>
        @if ($page === 'admin/products')
            <th>Actions</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($products as $key =>  $product)
        <tr>
            <th>{{ $key + 1 }}</th>
            <th>{{ $product->name }}</th>
            <th>{{ $product->slug }}</th>
            <th>{{ $product->getContent() }}</th>
            <th>{{ $product->price . '€' }}</th>
            <th>{{ $product->promo ? 'OUI' : 'NON' }}</th>
            <th>
                <a href="{{ route('listening.products.categories', [$product->category]) }}">{{ $product->category->name }}</a>
            </th>
            <th>{{ $product->created_at }}</th>
            @if ($page === 'admin/products')
                <th>
                    <a href="{{ route('product.edit', [$product]) }}" class="mr-3 text-primary">
                        <i class="fa fa-pencil font-size-14"></i>
                    </a>
                    <a href="#" class="text-danger">
                        <i class="fa fa-close font-size-14"></i>
                    </a>
                </th>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
