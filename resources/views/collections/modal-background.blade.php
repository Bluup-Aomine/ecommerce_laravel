<div id="visual-product-{{ $product->id }}" class="d-none">
    <div class="background-shop-product" id="modal-back-product"></div>
    <div class="popup popup-shop popup-position">
        <div class="popup-main">
            <div class="popup-main-img popup-main-shop">
                @if (isset($product->images[0]))
                    <div class="popup-img-visual popup-main-shop no-cover" style="background-image: url('{{ $product->images[0]->img_medium }}')"></div>
                @else
                    <svg class="bd-placeholder-img card-img-top"
                         width="100%" height="225"
                         xmlns="http://www.w3.org/2000/svg"
                         preserveAspectRatio="xMidYMid slice"
                         focusable="false" role="img"
                         aria-label="Placeholder: Thumbnail"><title>
                            Placeholder</title>
                        <rect width="100%" height="100%"
                              fill="#55595c"></rect>
                        <text x="50%" y="50%" fill="#eceeef"
                              dy=".3em">Image non valide
                        </text>
                    </svg>
                @endif
                <div class="popup-shop⁻infos popup-main-shop">
                    <h3 class="popup-main-shop popup-shop-title">{{ $product->name }}</h3>
                    <div class="popup-main-shop popup-main-price">{{ $product->price }}$</div>
                    <div class="popup-main-product">
                        <div class="product-color-type mt18 middle-item">
                            <ul class="colors-list-products" id="sizes-list-collections"></ul>
                        </div>
                        <div class="product-color-type mt18 middle-item">
                            <ul class="colors-list-products" id="colors-list-collections"></ul>
                        </div>
                    </div>
                    <a href="{{ route('product.basket.store') }}" class="btn btn--dark mt17" id="add-to-cart">Ajouter au panier</a>
                    <div class="d-none" id="add-card-product-id" data-product-id="{{ $product->id }}"></div>
                    <div class="text-center mt22">
                        <a href="{{ route('product.show', [$product]) }}" class="popup-shop⁻link">Voir la fiche complète</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
