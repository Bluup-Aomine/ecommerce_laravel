@extends('template.admin.layout.app')

@section('content')
    <div class="container-fluid">
        <!-- Block Message Flash (error ou success) -->
    @include('template.admin.message')
    @include('template.admin.message')

    <!-- Block Title Content -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-content d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Edit tracking order</h4>
                </div>
            </div>
        </div>
        <!-- Block Form Content -->
        <div class="row">
            <div class="col-12">
                <div class="card font-poppins">
                    <div class="card-body">
                        <h4 class="card-title">Tracking Order</h4>
                        {!! form_start($form) !!}
                            {!! form_row($form->tracking_order) !!}
                            {!! form_row($form->submit) !!}
                        {!! form_end($form, false) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
