@extends('template.admin.layout.app')

@section('content')
    <div class="container-fluid">
        <!-- Message Flash -->
    @include('template.admin.message')

    <!-- Block Title Content -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-content d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Les commandes sur "Paypal"</h4>
                </div>
            </div>
        </div>

        <!-- Block Table Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            @include('template.admin.orders.table-orders-paypal', [$ordersPaypal])
                        </div>
                        {{ $ordersPaypal->links('template.admin.layout.widget.pagination') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
