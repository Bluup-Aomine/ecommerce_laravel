@extends('layouts.app')

@section('content')
    <div class="content-title-head-page color-bg-black">
        <div class="container">
            <div class="title-contain">
                <h2>Error 404</h2>
            </div>
            <span>
                <a href="{{ route('404') }}" class="home">
                    <span>Error 404</span>
                </a>
            </span>
        </div>
    </div>
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="site-main col-12">
                    <article class="type-code-status hentry">
                        <div class="entry-content">
                            <div class="element">
                                <div class="element-container">
                                    <div class="element-section-wrap">
                                        <section class="element element-section element-section-boxed">
                                            <div class="element-container">
                                                <div class="element-row">
                                                    <div class="element element-col-100 element-column">
                                                        <div class="element-column-wrap">
                                                            <div class="element-widget-wrap">
                                                                <div
                                                                    class="element element-widget element-widget-image">
                                                                    <div class="element-widget-contain">
                                                                        <div class="element-image">
                                                                            <img width="728" height="348"
                                                                                 src="{{ asset('img/404.jpg') }}"
                                                                                 alt="Error 404" class="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section
                                            class="element element-section element-section-boxed margin-bottom-5rem">
                                            <div class="element-container">
                                                <div class="element-row">
                                                    <div class="element element-column element-col-25">
                                                        <div class="element-column-wrap">
                                                            <div class="element-widget-wrap"></div>
                                                        </div>
                                                    </div>
                                                    <div class="element element-column element-col-50 margin-top-5rem">
                                                        <div class="element-column-wrap">
                                                            <div class="element-widget-wrap">
                                                                <div
                                                                    class="element element-widget element-widget-error-code element-widget-header">
                                                                    <div
                                                                        class="element-widget-contain element-code-error">
                                                                        <h2 class="element-header-title element-size-error-code">
                                                                            whoops!</h2>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="element element-widget element-widget-error-code element-widget-header">
                                                                    <div
                                                                        class="element-widget-contain element-code-error-under-title">
                                                                        <h2 class="element-header-title element-size-error-code-under-title">
                                                                            Cette page n'éxiste pas sur notre site
                                                                            :D</h2>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="element element-widget element-position-r element-section-width-100 element-widget-error-code margin-top-5rem">
                                                                    <div class="element-widget-contain">
                                                                        <div
                                                                            class="element-button-link-back element-button-outline-primary element-widget-opal-btn">
                                                                            <a href="{{ route('home') }}"
                                                                               class="element-button-link">
                                                                                <span class="element-button-content">
                                                                                    <span class="element-button-text">Go Home</span>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
