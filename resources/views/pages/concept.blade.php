@extends('layouts.app')

@section('title')
    Notre Concept - POE&TIK
@endsection

@section('content')
    <div class="content-title-head-page color-bg-black">
        <div class="container">
            <div class="title-contain">
                <h2>Notre Concept</h2>
            </div>
            <span>
                <a href="{{ route('home') }}" class="home">
                    <span>Home</span>
                </a>
            </span>
            <span class="fa fa-angle-right"></span>
            <span>
                <a href="{{ route('concept') }}" class="home">
                    <span>Notre Concept</span>
                </a>
            </span>
        </div>
    </div>
    <div class="wrapper">
        <div class="container">
            <div class="main-content">
                <div class="concept-grid">
                    <div class="row">
                        <p class="text-element-concept">
                            Né d’une volonté de consommer mieux mais moins, Poe &amp; Tik
                            propose des vêtements respectueux de l’environnement, de
                            l’homme et des animaux.
                            Notre marque s’attache rigoureusement, à l’origine des matières
                            premières et à toute la chaine de production de nos vêtements pour
                            respecter nos valeurs et nos engagements.
                            Pour nous, les mots environnement, engagement et qualité
                            constituent le socle même de notre structure familiale et “made in
                            France&quot; loin des simples arguments marketing.
                        </p>
                        <article class="concept-grid-item img-concept-center">
                            <a href="#">
                                <img src="{{ asset('img/concept/éléphant_V1.jpg') }}" alt=""
                                     class="img-concept-item">
                            </a>
                        </article>
                    </div>
                    <div class="row">
                        <p class="text-element-concept">
                            Notre existence et nos ambitions de changer les choses et
                            bousculer les codes de l’industrie font partir de l’adn de Poe &amp; Tik
                            qui propose une gamme de vêtements intemporels en accord avec
                            vos principes.
                            Notre démarche s’accompagne d’un soutien à différentes ONG qui
                            œuvrent pour le respect de l’environnement et le bien-être animal.
                            En achetant Poe &amp; Tik vous faites le choix de vous habiller
                            responsable et de protéger notre belle planète.
                        </p>
                        <article class="concept-grid-item img-concept-center">
                            <a href="#">
                                <img src="{{ asset('img/concept/tortue_V1.jpg') }}" alt="" class="img-concept-item">
                            </a>
                        </article>
                    </div>
                    <div class="row">
                        <p class="text-element-concept">
                            Simon le créateur de Poe &amp; Tik, rêvait d’un monde meilleur, bercé
                            par les dessins animés de son enfance et les différents livres qu’il a
                            pu lire. En grandissant, il a découvert son appétence, pour la mode
                            et la couture.
                        </p>

                        <p class="text-element-concept">
                            De là, est né, Poe &amp; Tik, une marque modeste mais qui lui permet d’utiliser sa
                            créativité dans des pièces uniques, intemporelles, sans aucune
                            matière animale et rémunérant correctement les acteurs de la vie
                            de ce produit.
                            C&#39;est une histoire qui vient de commencer, et va s’écrire avec vous.
                        </p>

                        <p class="text-element-concept">
                            De voyages en voyages, il a pris conscience des
                            enjeux environnementaux, et des différents rapports qu’il peut y
                            avoir avec la nature et la culture. Ayant vécu à Vancouver quelque
                            temps dans un cadre conciliant nature et urbanisme, dans une
                            certaine harmonie, dès son retour en France, il eut le déclic, qui
                            concilie toutes ses passions : la mode et l’environnement.
                        </p>
                        <article class="concept-grid-item img-concept-center">
                            <a href="#">
                                <img src="{{ asset('img/concept/panda_V1.jpg') }}" alt="" class="img-concept-item">
                            </a>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
