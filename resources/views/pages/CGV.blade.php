@extends('layouts.app')

@section('content')
    <div class="content-title-head-page color-bg-black">
        <div class="container">
            <div class="title-contain">
                <h2>Nos Conditions Générales de Vente</h2>
            </div>
            <span>
                <a href="{{ route('home') }}" class="home">
                    <span>Accueil</span>
                </a>
            </span>
            <span class="fa fa-angle-right"></span>
            <span>
                <a href="{{ route('CGV') }}" class="home">
                    <span> Conditions Générales de Vente</span>
                </a>
            </span>
        </div>
    </div>
    <div id="shop-product">
        <div class="wrapper">
            <div class="container">
                <p class="text-cgv">
                    CONDITIONS GÉNÉRALES

                    DE VENTES



                    Poe & Tik


                    www.poeandtik.com est édité par la Société Poe & Tik, dont le siège social est 104 bld des dames 13002 Marseille, immatriculée au Registre des indépendants sous le numéro de SIREN suivant 853 383 404, dont le numéro de TVA intra-communautaire FR27 853 383 404. Le gérant est Simon Tomi et la micro entreprise porte son nom.


                    I. APPLICATION DES CONDITIONS GENERALES DE VENTE

                    Les conditions générales de vente (les "CGV") détaillées ci-dessous s'appliquent à toutes les commandes de produits et services passés via le Site auprès de Poe & Tik par toute personne (le "Client").


                    Le Client doit prendre connaissance des CGV préalablement à toute commande (la "Commande"), les CGV étant disponibles sur le Site.


                    Poe & Tik se réserve le droit d’adapter ou de modifier à tout moment les présentes CGV. La version des CGV applicable à toute vente étant celle figurant en ligne sur le site www.poeandtik.com au moment de la Commande. En conséquence, le fait de passer une Commande requiert l'entière adhésion préalable et sans réserve, aux CGV par le Client en cliquant sur le bouton "J'ai lu et j'accepte les conditions générales de vente".


                    II. INFORMATIONS SUR LE SITE ET ACCESSIBILITE DU SITE

                    www.poeandtik.com est un site de commerce électronique qui appartient et est géré par Simon Tomi.


                    Le Site est accessible à tous les utilisateurs du réseau internet par principe 24/24h, 7/7j, sauf interruption, programmée ou non, par Simon Tomi ou ses prestataires, pour les besoins de sa maintenance et/ou de sécurité ou cas de force majeure (tel que défini ci-après). Simon Tomi ne saurait être tenu responsable de tout dommage, quelle qu'en soit la nature, résultant d'une indisponibilité du Site.


                    Simon Tomi ne garantit pas que le Site sera exempt d’anomalies, erreurs ou bugs, ni que le Site fonctionnera sans panne ni interruption. Il peut à cet égard déterminer librement et à son entière discrétion toute période d’indisponibilité du Site ou de son contenu. Simon Tomi ne peut non plus être tenu responsable de problèmes de transmission de données, de connexion ou d’indisponibilité du réseau.


                    Simon Tomi se réserve le droit de faire évoluer le Site, pour des raisons techniques ou commerciales. Lorsque ces modifications n’altèrent pas les conditions de la fourniture des services, de manière substantielle et négative, le Client peut être informé des modifications intervenues, mais son acceptation n’est pas sollicitée.



                    III. PRODUITS

                    Les Produits proposés à la vente sont ceux décrits sur le Site au jour de la consultation du Site par le Client, dans la limite des stocks disponibles. Ces indications sont mises à jour automatiquement en temps réel. Toutefois, une erreur dans la mise à jour, quelle qu'en soit l'origine, n'engage pas la responsabilité de Simon Tomi. A ce titre, Simon Tomi ne saurait être tenu responsable de l’annulation d’une Commande d’un Produit du fait de l’épuisement des stocks.


                    Simon Tomi apporte le plus grand soin dans la présentation et la description de ses Produits pour satisfaire au mieux l’information du Client. Il est toutefois possible que des erreurs puissent figurer sur le Site, ce que le Client reconnaît et accepte.


                    Simon Tomi ne garantit ni la précision ni la sécurité des informations transmises ou obtenues au moyen du Site.


                    Il est possible que le Client reçoive à la suite d'une Commande une pièce précédemment retournée par une autre personne. Il est précisé que Simon Tomi n'accepte que le retour de Produits intacts et non portés, ces deux conditions étant contrôlées avant la remise en stock des Produits retournés.


                    IV. COMMANDES

                    La prise de Commande est soumise au respect de la procédure mise en place par Simon Tomi sur le Site comprenant des étapes successives aboutissant à la validation de la Commande.


                    Le Client peut sélectionner autant de Produits qu'il le souhaite qui s'ajouteront au panier (le "Panier"). Le Panier récapitule les Produits choisis par le Client ainsi que les prix et les frais y afférents. Le Client pourra librement modifier le Panier avant validation de sa Commande. La validation de la Commande vaut confirmation de l'acceptation par le Client des CGV, des Produits achetés, de leur prix ainsi que des frais associés.


                    Un email de confirmation récapitulant la Commande (Produit(s), prix, disponibilité du(es) Produit(s), quantité…) sera adressé au Client par Poe & Tik. A cet effet, le Client accepte formellement l’usage du courrier électronique pour la confirmation par Poe & Tik du contenu de sa Commande. Les factures sont disponibles dans la rubrique "mon compte" du Site.


                    V. REFUS DE TRAITER UNE COMMANDE

                    Simon Tomi se réserve le droit de retirer à tout moment tout Produit affiché sur le Site et de remplacer ou modifier tout contenu ou information figurant sur ce dernier. Malgré les meilleurs efforts de Poe & Tik pour satisfaire les attentes de ses clients, il se peut que cette dernière soit amenée à refuser de traiter une Commande après avoir adressé au Client l’email de confirmation récapitulant la Commande.


                    Simon Tomi ne saurait être tenu responsable envers le Client ou un tiers des conséquences dommageables du retrait d’un Produit du Site, ou du remplacement ou de la modification de tout contenu ou information figurant sur ce Site, ou encore du refus de traiter une Commande après l’envoi de l’email de confirmation récapitulant la Commande.


                    Simon Tomi se réserve également le droit de refuser ou d’annuler une Commande provenant d’un Client avec lequel il a un litige sur le paiement d’une précédente commande ou une suspicion objective de fraude.


                    VI. PRIX ET MODALITÉS DE PAIEMENT

                    Les prix des produits sont indiqués sur le Site en euros.


                    Tous les prix affichés sont calculés et incluent la taxe sur la valeur ajoutée (TVA) applicable en France ou celle applicable dans le pays de livraison situé en Union Européenne.


                    Simon Tomi se réserve le droit de modifier ses prix à tout moment mais les Produits seront facturés sur la base des tarifs en vigueur au moment de l’enregistrement et du paiement de la Commande, sous réserve de disponibilité.


                    Les Produits sont payables comptant lors de la Commande effective.


                    Le règlement des achats s’effectue soit via Paypal, soit via la plateforme sécurisée


                    Paypal (Europe) S.à.r.l. et Cie, est une société en commandite par actions luxembourgeoise, immatriculée au R.C.S de Luxembourg sous le numéro B118349, dont le siège social est sis 22-24, Boulevard Royal – L-2449 Luxembourg. Pour toute information, le Client peut consulter le site internet suivant : https://www.paypal.com.


                    Le Client reconnaît expressément que la communication de son numéro de carte bancaire à Poe & Tik vaut autorisation de débit de son Compte à concurrence du prix des Produits commandés. Le cas échéant, une notification d’annulation de Commande pour défaut de paiement est envoyée au Client par Simon Tomi sur l’adresse email communiquée par le Client lors de son inscription sur le Site.


                    Les données enregistrées et conservées par Poe & Tik constituent la preuve de la Commande et de l’ensemble des ventes passées. Les données enregistrées par Paypal ou Stripe constituent la preuve de toute transaction financière intervenue entre le Client et Simon Tomi.



                    VII. LIVRAISON

                    Les frais de Livraison applicables à la Commande sont ceux mentionnés sur le Site au moment de la Commande dans la rubrique « Livraison et retours ».


                    Lorsque Simon Tomi se charge de l’acheminement du Produit, le risque de perte ou de détérioration du Produit est transféré au Client au moment de la Livraison.


                    Par exception, le risque est transféré au Client lors de la remise du Produit au transporteur lorsque celui-ci est chargé du transport par le Client et non par Simon Tomi.


                    La Livraison est effectuée à l’adresse de livraison indiquée par le Client, étant précisé que celle-ci doit être l’adresse de résidence du Client, d'une personne physique de son choix ou d'une personne morale (livraison à son entreprise). La Livraison ne peut être effectuée ni dans des hôtels, ni à des boîtes postales.


                    En cas d'impossibilité d’effectuer la Livraison, due à une adresse de livraison erronée ou à l'absence de retrait par le Client de sa Commande auprès du point de retrait sélectionné ou de Chronopost, aucune réexpédition ne pourra être réalisée et le Client sera remboursé dans un délai de cinq (5) jours à compter de la réception de la Commande par Simon Tomi.


                    Simon Tomi livre les Commandes dans un délai maximum de principe de douze (12) jours ouvrés pour une Livraison en France Métropolitaine et de vingt (20) jours ouvrés pour une Livraison internationale, ce délai étant décompté à compter du premier jour ouvré après la validation de la Commande.


                    Afin que ces délais puissent être respectés, le Client doit s’assurer d’avoir communiqué des informations exactes et complètes concernant l’adresse de Livraison (tels que, notamment : n° de rue, de bâtiment, d’escalier, codes d’accès, noms et/ou numéros d’interphone, etc.).


                    Simon Tomi ne pourra être tenu responsable du retard d’acheminement n’étant pas de son fait ou justifié par un cas de force majeure (tel que défini ci-après).


                    En cas de dépassement du délai de Livraison, le Client pourra demander l'annulation de la vente et obtenir dans un délai maximum de quatorze (14) jours de sa demande en ce sens le remboursement des sommes versées à l'occasion de la Commande. Nonobstant ce qui précède, Simon Tomi ne pourra être tenu responsable des conséquences dommageables consécutives à un retard d’acheminement, seul le remboursement du Produit par Simon Tomi étant possible à l’exclusion de toute autre forme de dédommagement.


                    VIII. DROIT DE RETRACTATION - REMBOURSEMENTS ET RETOURS

                    9.1. Délai et modalités d’exercice du droit de rétractation


                    Conformément à l’article L.221-18 du Code de la Consommation, le Client non-professionnel dispose d'un délai de quatorze (14) jours à compter de la réception de la Commande pour exercer son droit de rétractation auprès de Simon Tomi, sans avoir à motiver sa décision.


                    L’exercice du droit de rétractation peut s’effectuer en utilisant et envoyant le modèle de formulaire de rétractation figurant en annexe des Conditions Générales de Vente, par courrier, à l’adresse suivante : 104 bld des dames ou par email à Simontomi.s@gmail.com ou poetiklamarque@gmail.com.


                    9.2. Modalités de retour de la Commande dans le cadre du droit de rétractation


                    Le droit de rétractation s’exerce sans pénalité.


                    Le Client renvoie la Commande avec le bon de retour prépayé fourni par Simon Tomi, sans retard excessif et, au plus tard, dans les quatorze (14) jours suivant la communication de sa décision de se rétracter conformément à l'article L. 221-21 du Code de la Consommation.


                    Au-delà de ce délai de quatorze (14) jours, la vente est ferme et définitive. Le Produit doit être retourné dans son emballage d’origine, dans son état d’origine, neuf, non porté, non lavé. Pour effectuer un retour, le Client doit suivre la procédure indiquée sur le bon de retour reçu avec sa Commande. Le retour des Produits est pris en charge par Simon Tomi si le lieu de Livraison s’effectue e France métropolitaine.


                    Le retour des Produits est à la charge du Client et se fait à ses risques si le lieu de Livraison n’est pas en France.


                    9.3. Remboursement des Produits retournés dans le cadre du droit de rétractation


                    Le remboursement de la Commande par Simon Tomi s’effectue au plus tard dans les quatorze (14) jours à compter de la date à laquelle elle est informée de la décision du Client de se rétracter.


                    Cependant, le remboursement intervient sous réserve que Simon Tomi ait pu récupérer les Produits objet du retour et de la demande de remboursement.


                    Simon Tomi effectue le remboursement en utilisant le même moyen de paiement que celui qui aura été utilisé pour le paiement de la Commande, sauf accord exprès du Client pour qu’elle utilise un autre. Simon Tomi ne pourra être responsable de remboursement sur un moyen de paiement expiré.


                    A défaut de respect par le Client des présentes CGV, Simon Tomi ne pourra procéder au remboursement des Produits concernés. Dans tous les cas, les frais de retour sont à la charge de Simon Tomi si le Produit livré au Client est différent du Produit commandé ou s’il est livré endommagé.


                    IX. GARANTIES - LIMITATION DE RESPONSABILITE

                    10.1 Limitation de responsabilités

                    La responsabilité de Simon Tomi à l'égard de tout Produit acheté sur le Site est strictement limitée au prix d'achat de ce dernier. Simon Tomi ne sera en aucun cas responsable des pertes suivantes, indépendamment de leur origine :


                    - perte de revenus ou de ventes

                    - perte d'exploitation

                    - perte de profits ou de contrats

                    - perte d'économies prévues

                    - perte de données

                    - perte de temps de travail ou de gestion

                    - préjudice d’image

                    - perte de chance, et notamment de commander un Produit,

                    - préjudice moral.


                    Les documents, descriptions et informations relatifs aux Produits figurant sur le Site ne sont couverts par aucune garantie, explicite ou implicite, à l'exception des garanties prévues par la loi.


                    Simon Tomi ne fournit aucune garantie concernant tout préjudice qui pourrait être causé par la transmission d’un virus informatique, d’un ver, d’une bombe temporelle, d’un cheval de Troie, d’un cancelbot, d’une bombe logique ou de toute autre forme de routine de programmation conçue pour endommager, détruire ou détériorer de toute autre manière une fonctionnalité d’un ordinateur ou d’entraver le bon fonctionnement de celui-ci, en ce compris toute transmission découlant d’un téléchargement de tout contenu effectué par le Client, des logiciels utilisés par celui-ci pour télécharger le contenu, du Site ou du serveur qui permet d’y accéder. A cet égard, le Client reconnaît qu’il est de sa responsabilité d’installer des anti-virus et des logiciels de sécurité appropriés sur son matériel informatique et tout autre dispositif afin de les protéger contre tout bogue, virus ou autre routine de programmation de cet ordre s’avérant nuisible.


                    Le Client reconnaît assumer l’ensemble des risques liés à tout contenu téléchargé ou obtenu de toute autre manière par le biais de l’utilisation du Site et convient qu’il est seul responsable de tout dommage causé à son système informatique ou de toute perte de données résultant du téléchargement de ce contenu.


                    Simon Tomi est uniquement tenue de livrer des Produits conformes aux dispositions contractuelles. Les Produits sont considérés comme étant conformes aux dispositions contractuelles si les conditions suivantes sont réunies : 1 ils doivent être conformes à la description et posséder les caractéristiques exposées sur le Site ; 2 ils doivent être adaptés aux fins pour lesquelles des produits de ce genre sont généralement conçus ; 3 ils doivent répondre aux critères de qualité et de résistance qui sont généralement admis pour des produits du même genre et auxquels on peut raisonnablement s'attendre.


                    En outre, Simon Tomi garantit les consommateurs des défauts de conformité et des vices cachés pour les Produits en vente sur le Site dans les conditions suivantes :


                    10. 2 Garanties légales


                    Tous les produits en vente sur le Site bénéficient de la garantie légale de conformité (telle que définie aux articles L217-4 et suivants du Code de la Consommation) et de la garantie contre les vices cachés (telle que définie aux articles 1641 et suivants du Code Civil), permettant au Client de renvoyer sans frais les Produits livrés défectueux ou non conformes.


                    Garantie légale de conformité



                    Article L217-4 du Code de la Consommation : « Le vendeur est tenu de livrer un bien conforme au contrat et répond des défauts de conformité existant lors de la délivrance. Il répond également des défauts de conformité résultant de l'emballage, des instructions de montage ou de l'installation lorsque celle-ci a été mise à sa charge par le contrat ou a été réalisée sous sa responsabilité. »



                    Article L211-5 du Code de la Consommation : « Pour être conforme au contrat, le bien doit :

                    1) Être propre à l'usage habituellement attendu d'un bien semblable et, le cas échéant :

                    correspondre à la description donnée par le vendeur et posséder les qualités que celui-ci a présentées à l'acheteur sous forme d'échantillon ou de modèle ;

                    présenter les qualités qu'un acheteur peut légitimement attendre eu égard aux déclarations publiques faites par le vendeur, par le producteur ou par son représentant, notamment dans la publicité ou l'étiquetage ;

                    2) Ou présenter les caractéristiques définies d'un commun accord par les parties ou être propre à tout usage spécial recherché par l'acheteur, porté à la connaissance du vendeur et que ce dernier a accepté. »


                    Article L211-12 du Code de la Consommation : « L'action résultant du défaut de conformité se prescrit par deux ans à compter de la délivrance du bien. »


                    Garantie contre les vices cachés



                    Article 1641 du Code Civil : « Le vendeur est tenu de la garantie à raison des défauts cachés de la chose vendue qui la rendent impropre à l'usage auquel on la destine, ou qui diminuent tellement cet usage, que l'acheteur ne l'aurait pas acquise, ou n'en aurait donné qu'un moindre prix, s'il les avait connus. »



                    Article 1648 du Code Civil : « L'action résultant des vices rédhibitoires doit être intentée par l'acquéreur dans un délai de deux ans à compter de la découverte du vice. Dans le cas prévu par l'article 1642-1, l'action doit être introduite, à peine de forclusion, dans l'année qui suit la date à laquelle le vendeur peut être déchargé des vices ou des défauts de conformité apparents. »



                    Dans le cadre de la garantie légale des vices cachés, Simon Tomi, au choix du Client, s'engage, après évaluation du vice :

                    - Soit à rembourser la totalité du prix du Produit retourné,

                    - Soit à lui rembourser une partie du prix du produit si le Client décide de conserver le Produit.


                    Exclusion de garanties


                    Sont exclus de garantie les Produits modifiés, réparés, intégrés ou ajoutés par le Client. La garantie ne jouera pas pour les vices apparents. La garantie ne prendra pas en charge les Produits endommagés lors du transport après la Livraison ou du fait d’une mauvaise utilisation.


                    10.3 Modalités de mise en oeuvre des garanties


                    Dans le cadre de la garantie légale de conformité, le Client :

                    (I) bénéficie d’un délai de deux (2) années à compter de la délivrance du bien pour agir ;

                    (II) peut choisir entre la réparation ou le remplacement du bien, sous réserve des conditions de coûts prévues par l’article L217-9 du Code de la Consommation ;

                    (III) est dispensé de rapporter la preuve de l’existence du défaut de conformité du bien durant les deux années.


                    La garantie légale de conformité s’applique indépendamment de la garantie commerciale éventuellement consentie.



                    Le Client peut décider de mettre en œuvre la garantie contre les défauts cachés de la chose vendue au sens de l’article 1641 du Code Civil. Dans cette hypothèse, il peut choisir entre la résolution de la vente ou une réduction du prix de vente conformément à l’article 1644 du Code Civil.




                    Ces dispositions ne sont pas exclusives du droit de rétraction défini à l’Article 9 ci-dessus.


                    10.4 Conséquences de la mise en oeuvre des garanties légales


                    Dans le cadre de la garantie légale de conformité, Simon Tomi, s’engage au choix du Client:

                    - soit à remplacer le Produit par un produit identique en fonction des stocks disponibles,

                    - soit à rembourser le prix du Produit si le remplacement d’un Produit s’avérait impossible.


                    Dans le cadre de la garantie légale des vices cachés, Simon Tomi, selon le choix du Client, s’engage, après évaluation du vice :

                    - soit à lui rembourser la totalité du prix du Produit retourné,soit à lui rembourser une partie du prix du Produit si le Client décide de conserver le Produit.


                    10.5 Force Majeures


                    En cas de survenance d’un événement de force majeure empêchant l’exécution des présentes CGV, Simon Tomi en informe le Client dans un délai de quinze (15) jours à compter de la survenance de cet événement, par mail ou par lettre recommandée avec accusé de réception. De façon expresse, sont considérés comme cas de force majeure ou cas fortuit, outre ceux habituellement retenus par la jurisprudence des cours et tribunaux français, les grèves totales ou partielles, lock-out, émeute, les boycottages ou autres actions à caractère industriel ou litiges commerciaux, trouble civil, insurrection, guerre, acte de terrorisme, intempérie, épidémie, blocage des moyens de transport ou d’approvisionnement pour quelque raison que ce soit, tremblement de terre, incendie, tempête, inondation, dégâts des eaux, restrictions gouvernementales ou légales, modifications légales ou réglementaires des formes de commercialisation, panne d’ordinateur, blocage des télécommunications, y compris des réseaux de télécommunications filaires ou hertziens, et tout autre cas indépendant de la volonté des parties empêchant l’exécution normale de la relation contractuelle. L’ensemble des obligations des parties sont suspendues pendant toute la durée de l’événement de force majeure, sans indemnité. Si l’événement de force majeure se prolonge pendant plus de trois (3) mois, la transaction concernée pourra être résiliée à la demande de Simon Tomi ou du Client sans indemnité de part et d’autre. Le défaut de paiement par le Client ne peut être justifié par un cas de force majeure.



                    X. RÉSERVE DE PROPRIETÉ

                    Simon Tomi conserve la propriété pleine et entière des Produits vendus jusqu'au parfait encaissement du prix intégral, en principal, frais, taxes et contributions obligatoires compris.


                    XI. INVALIDITE PARTIELLE

                    Si une ou plusieurs stipulations des présentes CGV sont tenues pour non valides ou déclarées comme telles en application d'une loi, d'un règlement ou à la suite d'une décision définitive d'une juridiction compétente, les autres stipulations garderont toute leur force et leur portée.


                    XII. NON-RENONCIATION

                    Aucune tolérance, inaction ou inertie de Simon Tomi ne pourra être interprétée comme renonciation à ses droits aux termes des CGV.



                    ANNEXE 1 : MODÈLE DE FORMULAIRE DE RÉTRACTATION

                    (Compléter et renvoyer le présent formulaire ainsi que le numéro de commande uniquement si vous souhaitez vous rétracter du contrat. Nous vous conseillons de préciser également votre numéro de commande)


                    A L’attention de Simon Tomi Poe & Tik – 104 bld des dames 13002 Marseille.


                    Courrier électronique : simontomi.s@gmail.com / poetiklamarque@gmail.com


                    Je vous notifie par la présence ma rétractation du contrat portant sur la vente du(es) bien(s) ci-dessous:

                    Commandé le ……………..(date de la commande) et/ou reçu le ……………..(date de la livraison)


                    Nom du Client consommateur :


                    Adresse du Client consommateur :


                    Signature du Client consommateur (uniquement en cas de notification du présent formulaire sur papier)


                    Date :



                    INFORMATIONS LÉGALES

                    Il est rappelé que le secret des correspondances n’est pas garanti sur le réseau Internet et qu’il appartient à chaque utilisateur d’Internet de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination d’éventuels virus circulant sur Internet.


                    I. EDITEUR

                    Simon Tomi, micro entreprise, immatriculée au Registre des indépendant sous le numéro de SIREN suivant 853 383 404, dont le siège social est 104 bld des dames - 13002 Marseille, France.



                    Contact : simontomi.s@gmail.com / poetiklamarque@gmail.com



                    II. HÉBERGEUR

                    O2switch



                    III. DONNÉES PERSONNELLES ET COOKIES

                    Conformément aux dispositions de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, le site a fait l’objet d’une déclaration à la CNIL (Commission Nationale Informatique et Libertés) sous le numéro 2039445.

                    Toutes les informations de votre Compte ne sont utilisées que dans le cadre de votre relation commerciale avec www.poeandtik.com. Ces informations ne sont jamais partagées avec des tiers ou revendues. Enfin, vos informations bancaires ne sont jamais en notre possession. Les transactions sont entièrement traitées par Paypal ou par le module de paiement sécurisé Stripe


                    Le Site utilise des cookies (témoins de connexion) ce dont l’utilisateur est informé en arrivant sur le site internet qui permettent d’enregistrer des informations relatives à la navigation de l’ordinateur sur le site internet. Ces cookies ne sont installés qu’après acceptation par l’utilisateur, la poursuite de la navigation sur le site internet valant acceptation. L’utilisateur peut s’opposer à l’utilisation de ces cookies en paramétrant son navigateur, sachant que l’accès à certains services peut nécessiter l’acceptation préalable par l’utilisateur des cookies.
                </p>
            </div>
        </div>
    </div>
@endsection
