@extends('layouts.app')

@section('content')
    <div class="content-title-head-page color-bg-black">
        <div class="container">
            <div class="title-contain">
                <h2>Livraison/Retour</h2>
            </div>
            <span>
                <a href="{{ route('home') }}" class="home">
                    <span>Home</span>
                </a>
            </span>
            <span class="fa fa-angle-right"></span>
            <span>
                <a href="{{ route('delivery') }}" class="home">
                    <span>Livraison/Retour</span>
                </a>
            </span>
        </div>
    </div>
    <div class="wrapper">
        <div class="container">
            <div class="row-collections-title">
                <div class="col-1">
                    <div class="cell-1">
                        <div class="collections-title">
                            <h3 class="collections-title-title">A Savoir</h3>
                            <p class="collections-title-text">LA LIVRAISON EST OFFERTE DÈS 50€ DE COMMANDE ET LES
                                RETOURS SONT À NOTRE CHARGE.<br>
                                <br>
                                Pour chaque article acheté, 1€ du prix du vêtement sera reversé à une ONG qui œuvre pour
                                le respect de l’environnement et du bien être animal.<br>
                                <br>
                                Vous pouvez planter toutes les étiquettes de nos vêtements et les regarder pousser
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
