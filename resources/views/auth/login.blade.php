<div class="u-column1" style="display: block">
    <form action="{{ route('login') }}" method="POST" class="form-login">
        @csrf
        <p class="form-row form-wide">
            <label for="email">
                Email
                <span class="required">*</span>
            </label>
            <input class="input-text" type="email" name="email" id="email" autocomplete="email">
        </p>
        <p class="form-row form-wide">
            <label for="password">
                Mot de passe&nbsp;
                <span class="required">*</span>
            </label>
            <input class="input-text" type="password" name="password" id="password" autocomplete="password">
        </p>
        <p class="form-row">
            <label for="remember-me" class="form-label-checkbox">
                <input class="input-form-checkbox" type="checkbox" value="forever" name="remember" id="remember">
                <span>Se souvenir de moi</span>
            </label>
            <button type="submit" name="login" value="Log in">Se connecter</button>
        </p>
        <p class="forgot-password">
            <a href="{{ route('password.request') }}">Avez-vous perdu votre mot de passe ?</a>
        </p>
    </form>
</div>
