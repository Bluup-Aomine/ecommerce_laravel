<div class="u-column2" style="display: none">
    <form action="{{ route('register') }}" method="POST" class="form-login register">
        @csrf
        <p class="form-row form-wide">
            <label for="name">
                Pseudo
                <span class="required">*</span>
            </label>
            <input class="input-text" type="text" name="name" id="name" autocomplete="name">
        </p>
        <p class="form-row form-wide">
            <label for="email">
                Email
                <span class="required">*</span>
            </label>
            <input class="input-text" type="email" name="email" id="email" autocomplete="email">
        </p>
        <p class="form-row form-wide">
            <label for="password">
                Mot de passe
                <span class="required">*</span>
            </label>
            <input class="input-text" type="password" name="password" id="password" autocomplete="password">
        </p>
        <p class="form-row form-wide">
            <label for="password_confirm">
                Confirmation de votre mot de passe
                <span class="required">*</span>
            </label>
            <input class="input-text" type="password" name="password_confirm" id="password_confirm"
                   autocomplete="password_confirm">
        </p>
        <p>A password will be sent to your email address.</p>
        <p class="form-row">
            <button type="submit" name="register" value="Register">S'inscrire</button>
        </p>
    </form>
</div>
