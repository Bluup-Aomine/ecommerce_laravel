@include('layouts.component.message.primary')
@include('layouts.component.message.danger')
<form action="{{ route('account.details') }}" method="POST" class="account-form-details">
    @csrf
    <p class="form-row form-row-first">
        <label for="account_first_name">
            First name
            <span class="required">*</span>
        </label>
        <input type="text" name="firstname" id="account_first_name" value="{{ $account !== null ? $account->firstname : '' }}">
    </p>
    <p class="form-row form-row-first">
        <label for="account_last_name">
            Last name
            <span class="required">*</span>
        </label>
        <input type="text" name="lastname" id="account_last_name" value="{{ $account !== null ? $account->lastname : '' }}">
    </p>
    <p class="form-row form-row-first">
        <label for="account_username">
            Username
            <span class="required">*</span>
        </label>
        <input type="text" name="username" id="account_username" value="{{ $user->name }}">
    </p>
    <p class="form-row form-row-first">
        <label for="account_email">
            Email
            <span class="required">*</span>
        </label>
        <input type="email" name="email" id="account_email" value="{{ $user->email }}">
    </p>
    <p>
        <button type="submit" value="Save changes" class="btn btn-float-left">Enregistrer</button>
    </p>
</form>
