<div id="shop-product">
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('layouts.component.message.primary')
                    @include('layouts.component.message.danger')
                    @include('layouts.component.message.json')
                    <article class="entry-post-cart">
                        <div class="entry-content">
                            <div class="cart-products">
                                <div class="table-responsive">
                                    <table class="cart shop-products">
                                        <thead>
                                        <tr>
                                            <th class="product-name">Order ID</th>
                                            <th class="product-price">Date</th>
                                            <th class="product-size">Total</th>
                                            <th class="product-color">Etat</th>
                                            <th class="product-color">Suivi de commande</th>
                                            <th class="product-quantity">Méthodee de paiment</th>
                                            <th class="product-total">View Détails</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @include('auth.account.component.table-orders', [$orders, $ordersPaypal])
                                        </tbody>
                                    </table>
                                </div>
                                @include('auth.account.component.modal-orders', [$orders, $ordersPaypal])
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>
