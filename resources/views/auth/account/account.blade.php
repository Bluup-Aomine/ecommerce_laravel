@extends('layouts.app')

@section('title')
    @guest()
        Authentification - POE&TIK
    @else
        Mon Compte - POE&TIK
    @endguest
@endsection

@section('content')
    <div class="content-title-account">
        <div class="container">
            <div class="title-contain">
                <h1>Mon Compte</h1>
            </div>
            <span>
                <a href="{{ route('home') }}" class="home">
                    <span>Accueil</span>
                </a>
            </span>
            <span class="fa fa-angle-right"></span>
            <span>
                <a href="{{ route('my-account') }}" class="home">
                    <span>Mon Compte</span>
                </a>
            </span>
        </div>
    </div>
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <article class="block-content">
                        <div class="content-account">
                            <div class="auth-account">
                                @guest()
                                    <div class="auth-account-notices">
                                        @include('layouts.errors')
                                    </div>
                                    <div class="auth-account-form">
                                        <div class="auth-account-heading text-center">
                                            <a class="item-auth active" id="u-column1">Se connecter</a>
                                            <a class="item-auth" id="u-column2">S'inscrire</a>
                                        </div>
                                        <div class="col-set" id="customer-auth">
                                            @include('auth.login')
                                            @include('auth.register')
                                        </div>
                                    </div>
                                @else
                                    <nav class="account-navigation">
                                        <ul>
                                            <li class="is-active">
                                                <a href="#" title="Dashboard" id="link-tab-account">
                                                    Tableau de bord
                                                </a>
                                            </li>
                                            <li class="">
                                                <a href="#" title="Orders" id="link-tab-account">
                                                    Commandes
                                                </a>
                                            </li>
                                            <li class="">
                                                <a href="#" title="Change-Password" id="link-tab-account">
                                                    Changer votre mot de passe
                                                </a>
                                            </li>
                                            <li class="">
                                                <a href="#" title="Account-Details" id="link-tab-account">
                                                    Détaille de votre compte
                                                </a>
                                            </li>
                                            <li class="">
                                                <a href="#" type="button" id="logout-action-link">
                                                    Se déconnecter
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                    <div class="account-content" id="Dashboard">
                                        <p>
                                            Bonjour <strong>{{ $user->name }}</strong> (tu n'es pas
                                            <strong>{{ $user->name }}</strong>? <a
                                                href="#" type="button" id="logout-action-link">Se Déconnecter</a>)
                                        </p>
                                        <p>
                                            À partir du tableau de bord de votre compte, vous pouvez afficher vos
                                            adresses de livraison et de facturation, et modifier votre mot de passe et
                                            les détails de votre compte.
                                        </p>
                                    </div>
                                    <div class="account-content mb-4 d-none" id="Account-Details">
                                        @include('auth.account.form-details')
                                    </div>
                                    <div class="account-content mb-4 d-none" id="Change-Password">
                                        @include('auth.account.change-password')
                                    </div>
                                    <div class="account-content mb-4 d-none" id="Orders">
                                        @include('auth.account.orders', [$orders])
                                    </div>
                                @endAuth
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script lang="js" type="module" src="{{ asset('js/auth/component/OrderModal.js') }}"></script>
    <script lang="js" type="module" src="{{ asset('js/auth/AuthAccountTab.js') }}"></script>
@endsection
