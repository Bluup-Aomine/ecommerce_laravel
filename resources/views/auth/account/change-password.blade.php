@include('layouts.component.message.primary')
@include('layouts.component.message.danger')
<form action="{{ route('account.passwords') }}" method="POST" class="account-form-details">
    @csrf
    <p class="form-row form-row-first">
        <label for="password">
            Mon mot de passe actuel
            <span class="required">*</span>
        </label>
        <input type="text" name="password" id="password">
    </p>
    <p class="form-row form-row-first">
        <label for="password_new">
            Nouveau mot de passe
            <span class="required">*</span>
        </label>
        <input type="password" name="password_new" id="password_new">
    </p>
    <p class="form-row form-row-first">
        <label for="password_new_confirmation">
            Confirmer votre nouveau mot de passe
            <span class="required">*</span>
        </label>
        <input type="password" name="password_new_confirmation" id="password_new_confirmation">
    </p>
    <p>
        <button type="submit" value="Save changes" class="btn btn-float-left">Enregistrer</button>
    </p>
</form>
