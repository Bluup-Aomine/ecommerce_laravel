@foreach($orders as $key => $order)
    <tr class="cart-products-item" id="basket-product-{{ $order->id }}">
        <td class="order-id">
            <span>{{ $order->id }}</span>
        </td>
        <td class="order-date">
            <span>{{ $order->payment_indent_created	 }}</span>
        </td>
        <td class="order-total">
            <span>{{ $order->amount . '€' }}</span>
        </td>
        <td class="order-etat">
            <span class="badge badge-pill badge-soft-success font-size-12">Paid</span>
        </td>
        <td class="order-tracking">
            @if ($order->tracking_order)
                @if ($order->tracking_order === 'en cours de traitement')
                    <span
                        class="badge badge-pill badge-warning font-size-12">{{ ucfirst($order->tracking_order) }}</span>
                @elseif($order->tracking_order === 'envoyer')
                    <span
                        class="badge badge-pill badge-primary font-size-12">{{ ucfirst($order->tracking_order) }}</span>
                @elseif($order->tracking_order === 'reçu')
                    <span
                        class="badge badge-pill badge-soft-success font-size-12">{{ ucfirst($order->tracking_order) }}</span>
                @endif
            @endif
        </td>
        <td class="order-payment-method">
            <i class="fa fa-cc-visa mr-1 font-size-14"></i>
            Visa
        </td>
        <td>
            <a href="#" class="btn-table btn-sm btn-table-primary btn-rounded btn-view-order"
               data-order-id="{{ $order->id }}">Plus de détails</a>
        </td>
    </tr>
@endforeach
@foreach($ordersPaypal as $key => $order)
    <tr class="cart-products-item" id="basket-product-{{ (count($orders) + $order->id) }}">
        <td class="order-id">
            <span>{{ (count($orders) + $order->id) }}</span>
        </td>
        <td class="order-date">
            <span>{{ $order->created_at	 }}</span>
        </td>
        <td class="order-total">
            <span>{{ $order->amount . '€' }}</span>
        </td>
        <td class="order-etat">
            <span class="badge badge-pill badge-soft-success font-size-12">Paid</span>
        </td>
        <td class="order-tracking">
            @if ($order->tracking_order)
                @if ($order->tracking_order === 'en cours de traitement')
                    <span
                        class="badge badge-pill badge-warning font-size-12">{{ ucfirst($order->tracking_order) }}</span>
                @elseif($order->tracking_order === 'envoyer')
                    <span
                        class="badge badge-pill badge-primary font-size-12">{{ ucfirst($order->tracking_order) }}</span>
                @elseif($order->tracking_order === 'reçu')
                    <span
                        class="badge badge-pill badge-soft-success font-size-12">{{ ucfirst($order->tracking_order) }}</span>
                @endif
            @else
                <span
                    class="badge badge-pill badge-danger font-size-12">Echec de la commande</span>
            @endif
        </td>
        <td class="order-payment-method">
            <i class="fa fa-cc-paypal mr-1 font-size-14"></i>
            Paypal
        </td>
        <td>
            <a href="#" class="btn-table btn-sm btn-table-primary btn-rounded btn-view-order"
               data-order-id="{{ (count($orders) + $order->id) }}">Plus de détails</a>
        </td>
    </tr>
@endforeach
