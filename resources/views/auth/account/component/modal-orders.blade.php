@foreach($orders as $key => $order)
    <div class="modal modal-open fade show d-none" role="dialog" id="modal-order-front-{{ $order->id}}">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold">Order Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            data-order-id="{{ $order->id }}">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="mb-2">
                        Product id:
                        <span class="text-primary">#SK{{ $order->id }}</span>
                    </p>
                    <p class="mb-4">
                        Billing Name:
                        <span class="text-primary">{{ $order->name }}</span>
                    </p>
                    <p class="mb-4">
                        Other Notes
                        <span class="text-primary">{{ $order->order_notes }}</span>
                    </p>
                    <div class="table-responsive">
                        <table class="table table-centered table-nowrap">
                            <thead>
                            <tr>
                                <th scope="col">Product</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order['products'] as $product)
                                <tr>
                                    <th scope="row">
                                        <div>
                                            <img src="{{ $product['image'] }}" class="avatar-sm" alt="">
                                        </div>
                                    </th>
                                    <td>
                                        <div>
                                            <h5 class="text-truncate font-size-14">{{ $product['name'] }}</h5>
                                            <p class="text-muted mb-0"> {{ $product['price'] }} €
                                                x {{ $product['quantity'] }}</p>
                                        </div>
                                    </td>
                                    <td>{{ $product['total'] }} €</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="2">
                                    <h6 class="mb-0 text-right font-h6-size">Prix commande:</h6>
                                </td>
                                <td>{{ \App\src\helpers\Price::addTotal($order['products']) }} €</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h6 class="mb-0 text-right font-h6-size">Livraison:</h6>
                                </td>
                                <td>{{ \App\src\helpers\Price::addTotal($order['products']) > 50 ? 'FREE' : \App\src\helpers\Price::shipping($order['products']) . '€' }}</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h6 class="mb-0 text-right font-h6-size">TVA:</h6>
                                </td>
                                <td>{{ $order->tva ? 'AVEC' : 'SANS' }}</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h6 class="mb-0 text-right font-h6-size">Total:</h6>
                                </td>
                                <td>{{ $order->amount . '€' }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <form action="#" method="GET">
                        {{ csrf_field() }}
                        <a href="{{ route('invoice.order', [$order]) }}" type="button"
                           class="btn btn-primary btn-submit-primary">Télécharger la facture</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-backdrop fade show d-none" id="modal-order-show-{{ $order->id}}"></div>
@endforeach
@foreach($ordersPaypal as $key => $order)
    <div class="modal modal-open fade show d-none" role="dialog" id="modal-order-front-{{ (count($orders) + $order->id) }}">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold">Order Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            data-order-id="{{ (count($orders) + $order->id) }}">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="mb-2">
                        Product id:
                        <span class="text-primary">#SK{{ (count($orders) + $order->id) }}</span>
                    </p>
                    <p class="mb-4">
                        Billing Name:
                        <span class="text-primary">{{ $order->name }}</span>
                    </p>
                    <p class="mb-4">
                        Other Notes
                        <span class="text-primary">{{ $order->order_notes }}</span>
                    </p>
                    <div class="table-responsive">
                        <table class="table table-centered table-nowrap">
                            <thead>
                            <tr>
                                <th scope="col">Product</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order['products'] as $product)
                                <tr>
                                    <th scope="row">
                                        <div>
                                            <img src="{{ $product['image'] }}" class="avatar-sm" alt="">
                                        </div>
                                    </th>
                                    <td>
                                        <div>
                                            <h5 class="text-truncate font-size-14">{{ $product['name'] }}</h5>
                                            <p class="text-muted mb-0"> {{ $product['price'] }} €
                                                x {{ $product['quantity'] }}</p>
                                        </div>
                                    </td>
                                    <td>{{ $product['price'] }} €</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="2">
                                    <h6 class="mb-0 text-right font-h6-size">Prix commande:</h6>
                                </td>
                                <td>{{ \App\src\helpers\Price::addTotal($order['products']) }} €</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h6 class="mb-0 text-right font-h6-size">Livraison:</h6>
                                </td>
                                <td>{{ \App\src\helpers\Price::addTotal($order['products']) > 50 ? 'FREE' : \App\src\helpers\Price::shipping($order['products']) . '€' }}</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h6 class="mb-0 text-right font-h6-size">TVA:</h6>
                                </td>
                                <td>{{ $order->tva ? 'AVEC' : 'SANS' }}</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h6 class="mb-0 text-right font-h6-size">Total:</h6>
                                </td>
                                <td>{{ $order->amount . '€' }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <form action="#" method="GET">
                        {{ csrf_field() }}
                        <a href="{{ route('invoice.paypal.order', [$order]) }}" type="button"
                           class="btn btn-primary btn-submit-primary">Télécharger la facture</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-backdrop fade show d-none" id="modal-order-show-{{ (count($orders) + $order->id) }}"></div>
@endforeach
