@extends('layouts.app')

@section('title')
    Changement De Mot De Passe - POE&TIK
@endsection

@section('content')
    <div class="content-title-account">
        <div class="container">
            <div class="title-contain">
                <h1>Changement de votre mot de passe ?</h1>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('layouts.component.message.primary')
                    @include('layouts.component.message.danger')
                    <form action="{{ route('password.update') }}" method="POST" class="form-login">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <p class="form-row form-wide">
                            <label for="password">
                                Nouveau mot de passe
                                <span class="required">*</span>
                            </label>
                            <input class="input-text" type="password" name="password" id="password">
                        </p>
                        <p class="form-row form-wide">
                            <label for="password_confirmation">
                                Confirmation de votre nouveau mot de passe
                                <span class="required">*</span>
                            </label>
                            <input class="input-text" type="password" name="password_confirmation" id="password_confirmation">
                        </p>
                        <p class="form-row">
                            <button type="submit" name="send" value="Envoyer">Envoyer</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
