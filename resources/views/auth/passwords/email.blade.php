@extends('layouts.app')

@section('title')
    Mot de passe oublié ? - POE&TIK
@endsection

@section('content')
    <div class="content-title-account">
        <div class="container">
            <div class="title-contain">
                <h1>Mot de passe oublié ?</h1>
            </div>
            <span>
                <a href="{{ route('my-account') }}" class="home">
                    <span>Login</span>
                </a>
            </span>
            <span class="fa fa-angle-right"></span>
            <span>
                <a href="#" class="home">
                    <span>Mot de passe oublié ?</span>
                </a>
            </span>
        </div>
    </div>
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form action="{{ route('password.email') }}" method="POST" class="form-login">
                        @csrf
                        <p class="form-row form-wide">
                            <label for="email">
                                Email
                                <span class="required">*</span>
                            </label>
                            <input class="input-text" type="email" name="email" id="email" autocomplete="email">
                        </p>
                        <p class="form-row">
                            <button type="submit" name="login" value="Log in">Envoyer</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
