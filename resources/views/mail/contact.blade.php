<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.scss') }}" rel="stylesheet">
    <title>Contact</title>
</head>
<body>
<div class="body">
    <div class="page-site">
        <header class="header-page">
            <div class="container-header">
                <div class="element-heading">
                    @include('layouts.Widget.LogoHeaderMail')
                </div>
            </div>
        </header>
        <div class="content-title-head-page color-bg-black container-mail">
            <div class="container">
                <p>
                    {{ $content }}
                </p>
                <p>{{ $name }}, Merci</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
