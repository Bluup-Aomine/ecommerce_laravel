<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.scss') }}" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <title>Contact</title>
</head>
<body>
<div class="body">
    <div class="page-site">
        <header class="header-page">
            <div class="container-header">
                <div class="element-heading">
                    @include('layouts.Widget.LogoHeaderMail')
                </div>
            </div>
        </header>
        <div class="content-title-head-page color-bg-black container-mail">
            <div class="container">
                <h2 class="text-center font-strong font-size-16">Commande n°{{ $order->id }}</h2>
                <p>
                    Merci d’avoir acheté ce vêtement, en nous choisissant, vous
                    participer au respect de l’environnement, de l’animal et de l’humain.
                </p>
                <p>
                    Un Euro du prix de ce vêtement, sera reversé à une ONG qui
                    œuvre pour le respect de l’environnement et pour le bien-être
                    animal
                </p>
                <h4 class="font-strong">Produit</h4>
                <p class="font-strong">{{ count($order->products) }} Articles</p>
                <div class="products-mail">
                    @foreach($order->products as $product)
                        <div class="product-item-mail">
                            <div class="product-item-head-mail">
                                <img src="{{ $product['image'] }}" alt="">
                                <div class="product-item-details-mail">
                                    <p class="first-element-no-mb">XL</p>
                                    <p class="first-element-no-mb">Rouge</p>
                                    <p>{{ $product['name'] }}</p>
                                </div>
                                <div class="product-item-price-mail">
                                    <p class="first-element-no-mb">Unité {{ $product['quantity'] }}</p>
                                    <p>{{ $product['price'] }} EUR</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="product-item-payment-mail">
                        <h4 class="font-strong">Paiement</h4>
                        <p class="first-element-no-mb">{{ \App\Order::getName($order) }}</p>
                        <p>Carte Bancaire : 4242 4242 4242 4242</p>
                    </div>
                    <div class="table-purshase-products">
                        <table class="table table-centered table-nowrap">
                            <tbody>
                            <tr>
                                <td colspan="1">
                                    <h6 class="mb-0 text-right font-h6-size">Total des articles:</h6>
                                </td>
                                <td>{{ $order->amount }} €</td>
                            </tr>
                            <tr>
                                <td colspan="1">
                                    <h6 class="mb-0 text-right font-h6-size">Frais de livraison:</h6>
                                </td>
                                <td>0 €</td>
                            </tr>
                            <tr>
                                <td colspan="1">
                                    <h6 class="mb-0 text-right font-h6-size">Total:</h6>
                                </td>
                                <td>{{ $order->amount }} €</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="footer-products-mail">
                        <h4 class="font-strong">Condition général d'achat</h4>
                        <div class="social-follow">
                            <div class="flex-social mail-footer">
                                <div class="facebook">
                                    <a href="#" target="_blank" title="Facebook">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </div>
                                <div class="twitter">
                                    <a href="#" target="_blank" title="Twitter">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </div>
                                <div class="instagram-icon">
                                    <a href="#" target="_blank" title="Instagram">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
