<div class="element-dropdown-menu-header-login">
    <span class="element-title-login">Se Connecter</span>
    <span class="pull-right">
          <a href="{{ route('my-account') }}" class="register-link"
             title="Register">Créer un compte</a>
    </span>
</div>
<form action="{{ route('login') }}" method="POST" class="form-login-menu">
    @csrf
    <p>
        <label for="email">
            Email
            <span class="required">*</span>
        </label>
        <input type="text" id="email" name="email"
               required placeholder="Email">
    </p>
    <p>
        <label for="password">
            Mot de passe
            <span class="required">*</span>
        </label>
        <input type="password" id="password" name="password"
               required placeholder="Mot de passe...">
    </p>
    <button type="submit"
            class="btn btn-primary w-100 mt-1 mb-15">Se Connecter
    </button>
</form>
<div class="login-forgot-password">
    <a href="#" title="Lost your password?">Avez-vous perdu votre mot de passe ?</a>
</div>
