<ul class="account-items">
    <li class="li-account">
        <a href="{{ route('my-account') . '#Dashboard' }}">
            Tableau de bord
        </a>
    </li>
    <li class="li-account">
        <a href="{{ route('my-account') . '#Orders' }}">
            Commandes
        </a>
    </li>
    <li class="li-account">
        <a href="{{ route('my-account') . '#Change-Password' }}">
            Changer votre mot de passe
        </a>
    </li>
    <li class="li-account">
        <a href="{{ route('my-account') . '#Account-Details' }}">
            Détaille de votre compte
        </a>
    </li>
    <li class="li-account">
        <a href="{{ route('logout') }}" id="logout-action-link">
            Se déconnecter
        </a>
    </li>
</ul>
