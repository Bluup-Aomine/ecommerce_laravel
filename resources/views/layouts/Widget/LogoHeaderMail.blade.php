<div class="element-column">
    <div class="element-column-wrap element-menu-show">
        <div class="element-widget-menu">
            <div class="element element-widget element-widget-image">
                <div class="element-widget-contain">
                    <div class="element-image">
                        <a href="#">
                            @include('layouts.icons.logo')
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
