<section class="element element-section element-footer element-content">
    <div class="element-container">
        <div class="element-row">
            <div class="element element-column element-col-100">
                <div class="element-column-wrap footer-element">
                    <div class="element-widget-menu">
                        <section class="element element-section footer">
                            <div class="element-container">
                                <div class="element-row">
                                    <div class="row lg-align-center w-100">
{{--                                        <div class="column-lg-4 column-md-6">--}}
{{--                                            <div class="email-footer-inscription">--}}
{{--                                                <p class="h2">Newsletter</p>--}}
{{--                                                @guest()--}}
{{--                                                    <form action="#" method="POST">--}}
{{--                                                        <div class="newsletter">--}}
{{--                                                            <input type="text" name="email" placeholder="Votre e-mail">--}}
{{--                                                            <input type="submit" value="Inscrivez-vous"--}}
{{--                                                                   name="submitNewsletter">--}}
{{--                                                        </div>--}}
{{--                                                    </form>--}}
{{--                                                    <p class="small padding-footer-after-form">Conformément à l’article--}}
{{--                                                        4(11) du RGPD, j’autorise la société SAS K INVEST à stocker les--}}
{{--                                                        données collectées via ce formulaire dans le cadre de la fourniture--}}
{{--                                                        de son service. Je comprends que ces données peuvent être utilisées--}}
{{--                                                        dans vos traitements informatiques. Sur simple demande par courriel--}}
{{--                                                        à contact@kulte.fr , je dispose du droit de modification et--}}
{{--                                                        d’effacement des données de mon profil.</p>--}}
{{--                                                @else--}}
{{--                                                    <p class="small">Conformément à l’article--}}
{{--                                                        4(11) du RGPD, j’autorise la société SAS K INVEST à stocker les--}}
{{--                                                        données collectées via ce formulaire dans le cadre de la fourniture--}}
{{--                                                        de son service. Je comprends que ces données peuvent être utilisées--}}
{{--                                                        dans vos traitements informatiques. Sur simple demande par courriel--}}
{{--                                                        à contact@kulte.fr , je dispose du droit de modification et--}}
{{--                                                        d’effacement des données de mon profil.</p>--}}
{{--                                                @endguest--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="column-lg-4 column-lg-2">
                                            <div class="links-block">
                                                <h3>À propos</h3>
                                                <ul class="bullet">
                                                    <li>
                                                        <a href="{{ route('delivery')  }}">Livraison/Retour</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('contact') }}">Contactez-nous</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('CGV') }}">Conditions générales de vente</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="column-lg-4 column-lg-2">
                                            <div class="social-follow">
                                                <div class="flex-social">
                                                    <div class="facebook">
                                                        <a href="https://www.facebook.com/PoeTik-104736678061400" target="_blank" title="Facebook">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </div>
                                                    <div class="twitter">
                                                        <a href="#" target="_blank" title="Twitter">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </div>
                                                    <div class="instagram-icon">
                                                        <a href="https://www.instagram.com/poe_tik" target="_blank" title="Instagram">
                                                            <i class="fa fa-instagram"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
