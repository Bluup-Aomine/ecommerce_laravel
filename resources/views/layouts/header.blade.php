<header class="header-page">
    <div class="container-header">
        <div class="element-heading">
            @include('layouts.component.responsive.headerDesktop')
            @include('layouts.component.responsive.headerMobile')
        </div>
    </div>
</header>
