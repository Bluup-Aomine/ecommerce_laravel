<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="{{ isset($robots) ? $robots : 'follow,index' }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app/app.js') }}" type="module" defer></script>
    <script src="{{ asset('js/library/slick.min.js') }}" type="module" defer></script>
    <script src="{{ asset('js/library/nouislider.min.js') }}" type="module" defer></script>
    <script src="{{ asset('js/library/swipper.min.js') }}" type="module" defer></script>
    <script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
    <script src="https://code.iconify.design/1/1.0.6/iconify.min.js" defer></script>
    <script src="{{ asset('js/use-library.js') }}"></script>
@yield('extra-script')

<!-- RGPD -->
    <script type="text/javascript" src="//www.cookieconsent.com/releases/3.1.0/cookie-consent.js"></script>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
            cookieconsent.run({
                "notice_banner_type": "simple",
                "consent_type": "express",
                "palette": "light",
                "language": "fr",
                "change_preferences_selector": "",
                "cookies_policy_url": "https://www.poeandtik.com/politique-de-cookies",
                "website_name": "PoeAndTik"
            });
        });
    </script>

    <!-- ICON WEB -->
    <link rel="icon" type="image/jpeg" href="{{ asset('img/icons/logo.jpg') }}"/>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Avenir" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/library/nouislider.min.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('css/library/swipper.min.css') }}" rel="stylesheet"> -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
<div class="body">
    <div class="page-site">
        @include('layouts.header')
            @include('layouts.aside')
            <div class="site-content">
                @yield('content')
            </div>
            @include('layouts.footer')
            @yield('extra-js')
        </div>
    </div>
</body>
</html>
