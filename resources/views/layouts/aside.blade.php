<aside aria-hidden="true" class="canvas canvas-transition canvas-overlay canvas-left is-closed d-none">
    <div class="canvas-container">
        <div class="canvas-logo">
            <a href="#" class="logo-img">
                @include('layouts.icons.logo')
            </a>
        </div>
        <button class="canvas-btn-close">
            <span class="button-text">
                <i class="fa fa-close" aria-hidden="true"></i>
            </span>
        </button>
        <div class="canvas-top">
            <form method='POST' action="{{ route('search') }}" role="search" id="searchForm">
                @csrf
                <label for="search" class="sr-only">Rechercher</label>
                <div class="input-group">
                    <input class="form-control" type="text" id="search" name="search" placeholder="Rechercher...">
                    <span class="input-group-append">
                        <button class="btn" name="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
        </div>
        <div class="canvas-content">
            <ul class="nav-menu">
                <li class="menu-item">
                    <a href="{{ route('home') }}" class="nav-aside-item">
                        Accueil
                    </a>
                </li>
                <li class="menu-item">
                    <a href="{{ route('collections') }}" class="nav-aside-item">
                        Collections
                    </a>
                </li>
                <li class="menu-item">
                    <a href="{{ route('concept') }}" class="nav-aside-item">
                        Notre Concept
                    </a>
                </li>
                <li class="menu-item">
                    <a href="{{ route('contact') }}" class="nav-aside-item">
                        Nous Contacter
                    </a>
                </li>
            </ul>
        </div>
        <div class="canvas-footer">
            <a href="{{ route('my-account') }}">
                <span class="iconify" data-icon="bx:bx-user" data-inline="false"></span>
            </a>
            <a href="{{ route('product.basket.edit', [\App\Basket::findByUser(Auth::id())->first() ? \App\Basket::findByUser(Auth::id())->first() : 1 ]) }}">
                <span class="iconify" data-icon="ant-design:shopping-cart-outlined" data-inline="false"></span>
            </a>
        </div>
    </div>
</aside>
<div class="canvas-bg is-closed"></div>
