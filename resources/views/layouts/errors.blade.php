@if($errors->any())
    <ul class="error-message">
        @foreach($errors->all() as $error)
            <li>
                <strong>Error:</strong>
                {{ $error }}
            </li>
        @endforeach
    </ul>
@endif
