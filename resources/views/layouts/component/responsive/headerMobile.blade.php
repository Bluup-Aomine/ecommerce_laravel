@php
    use App\Basket;
    use Illuminate\Support\Facades\Auth;
    $basketUri = Auth::user() ? route('product.basket.edit', [Basket::findByUser(Auth::id())->first() ? Basket::findByUser(Auth::id())->first() : null ]): route('product.basket.session')
@endphp

<section class="element element-menu-mobile element-desktop-hidden element-section element-section-boxed">
    <div class="element-container">
        <div class="element-row">
            <div class="element element-column element-col-33 element-column-mobile">
                <div class="element-column-wrap">
                    <div class="element-widget-menu">
                        <div class="element element-widget element-widget-l">
                            <div class="element-widget-contain">
                                <div class="element-image element-image-responsible">
                                    <a href="{{ route('home') }}">
                                        @include('layouts.icons.logo')
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="element element-column element-col-66 element-menu-mobile-widget element-column-mobile">
                <div class="element-column-wrap">
                    <div class="element-widget-menu">
                        <div class="element element-widget">
                            <div class="element-widget-contain">
                                <div class="element-button-wrap element-widget-element">
                                    <span class="element-float-right">
                                            <a href="#" class="element-button no-radius element-size-sm">
                                                <span class="button-menu button-menu-mobile">
                                                    <span class="iconify icon-iconify" data-icon="uil:bars"
                                                          data-inline="false"></span>
                                                </span>
                                            </a>
                                    </span>
                                </div>
                                <div class="element-button-wrap element-widget-element element-left-item">
                                    <a href="{{ route('my-account') }}" class="element-button-mobile">
                                       <span class="iconify element-widget-header"
                                             data-icon="ant-design:user-outlined"
                                             data-inline="false"></span>
                                    </a>
                                    <a href="{{ route('product.wishlist.index') }}"
                                       class="element-header-button element-button-mobile widget-element-mobile">
                                        <span class="iconify element-widget-header"
                                              data-icon="ant-design:heart-outlined"
                                              data-inline="false"></span> <span class="title-badge"></span>
                                    </a>
                                    <a href="{{ $basketUri }}"
                                       class="element-header-button element-button-mobile widget-element-mobile">
                                        <span class="iconify element-widget-header"
                                              data-icon="ant-design:shopping-cart-outlined"
                                              data-inline="false"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
