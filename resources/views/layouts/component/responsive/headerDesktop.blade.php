@php
    use App\Basket;
    use Illuminate\Support\Facades\Auth;
    $basketUri = Auth::user() ? route('product.basket.edit', [Basket::findByUser(Auth::id())->first() ? Basket::findByUser(Auth::id())->first() : null ]): route('product.basket.session')
@endphp

<section class="element-position element-header element-section header-section element-desktop-head">
    <div class="element-container">
        <div class="element-row">
            <div class="element-column element-col-16">
                <div class="element-column-wrap element-menu-show">
                    <div class="element-widget-menu">
                        <div class="element element-widget">
                            <div class="element-widget-contain">
                                <div class="element-widget-element">
                                    <span>
                                        <a href="#" class="element-button no-radius element-size-sm">
                                            <span class="button-menu">
                                                <span class="iconify icon-iconify" data-icon="uil:bars"
                                                      data-inline="false"></span>
                                            </span>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="element-column element-col-66">
                <div class="element-column-wrap element-menu-show">
                    <div class="element-widget-menu">
                        <div class="element element-widget element-widget-image">
                            <div class="element-widget-contain">
                                <div class="element-image">
                                    <a href="{{ route('home') }}">
                                        @include('layouts.icons.logo')
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="element-column element-col-16 element-responsive">
                <div class="element-column-wrap element-menu-show">
                    <div class="element-widget-menu">
                        <div class="element element-widget element-search element-full-screen">
                            <div class="element-widget-contain">
                                <form action="{{ route('search') }}" class="element-search-form" method="POST">
                                    @csrf
                                    <div class="element-search-form-toggle">
                                                    <span class="iconify element-widget-header"
                                                          data-icon="ant-design:search-outlined"
                                                          data-inline="false"></span>
                                    </div>
                                    <div class="element-search-container element-d-none d-none">
                                        <input type="search" class="element-search-input"
                                               placeholder="Rechercher..." name="search" title="Search">
                                        <div class="btn-dialog close-btn-dialog" id="close-search">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div
                            class="element element-widget element-search element-full-screen element-account element-widget-mb-0">
                            <div class="element-widget-contain">
                                <div class="element-header-account">
                                    <div class="element-dropdown">
                                        <a href="{{ route('my-account') }}">
                                                        <span class="iconify element-widget-header"
                                                              data-icon="ant-design:user-outlined"
                                                              data-inline="false"></span>
                                        </a>
                                    </div>
                                    <div class="element-dropdown-menu element-d-none d-none">
                                        <div class="account-bloc">
                                            <div class="account-contain">
                                                @guest()
                                                    @include('layouts.Widget.AccountWidget')
                                                @else
                                                    @include('layouts.Widget.AuthAccountWidget')
                                                @endauth
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="element element-widget element-search element-full-screen element-widget-wishlist">
                            <div class="element-widget-contain">
                                <div class="element-dropdown">
                                    <a href="{{ route('product.wishlist.index') }}" class="element-header-button">
                                        <span class="iconify element-widget-header"
                                              data-icon="ant-design:heart-outlined"
                                              data-inline="false"></span> <span class="title-badge"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="element element-widget element-search element-full-screen element-widget-cart">
                            <div class="element-widget-contain">
                                <div class="element-dropdown">
                                    <a href="{{ $basketUri }}"
                                       class="element-header-button">
                                        <div id="url-load-products" style="display: none"
                                             value="{{ route('product.basket.load') }}"></div>
                                        <span class="iconify element-widget-header"
                                              data-icon="ant-design:shopping-cart-outlined"
                                              data-inline="false"></span>
                                        <span class="title-badge"></span>
                                        <span class="count-badge" id="count-quantity-products-header">0 </span>
                                    </a>
                                    <div class="element-dropdown-menu element-d-none d-none">
                                        <div class="widget widget-basket">
                                            <div class="widget-commerce-cart">
                                                <ul class="cart-list products-list-cart"></ul>
                                                <p class="price-products-price" id="total-products-cart"></p>
                                                <p class="products-buttons">
                                                    <a href="{{ $basketUri }}" class="btn btn-bg-black">Mon panier</a>
                                                    <a href="{{ route('checkout') }}"
                                                       class="btn btn-bg-black">Passer commande</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
