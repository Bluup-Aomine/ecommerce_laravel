<div class="notices-wrapper d-none" id="message-danger-json-list">
    <div class="notices-message notice-danger" role="alert">
        Les informations suivantes sont invalides :
        <br>
        <ul id="list-errors-forms"></ul>
    </div>
</div>
