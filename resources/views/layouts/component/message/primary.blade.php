@if(Session::has('message'))
    <div class="notices-wrapper">
        <div class="notices-message" role="alert">
            {{ Session::get('message') }}
        </div>
    </div>
@endif
