@extends('layouts.app')

@section('title')
    Votre Wishlist - POE&TIK
@endsection

@section('content')
    <div class="content-title-head-page color-bg-black">
        <div class="container">
            <div class="title-contain">
                <h2>Wishlist</h2>
            </div>
            <span>
                <a href="{{ route('home')  }}" class="home">
                    <span>Accueil</span>
                </a>
            </span>
            <span class="fa fa-angle-right"></span>
            <span>
                <a href="#" class="home">
                    <span>Wishlist</span>
                </a>
            </span>
        </div>
    </div>
    <div id="wishlist-products">
        <div class="wrapper" id="page-wrapper">
            <div class="container">
                <div class="row">
                    <div class="main col-12">
                        <article class="wishlist-post">
                            <div class="entry-post-wishlist">
                                @if($wishlist)
                                    <form action="{{ route('product.wishlist.update', [$wishlist]) }}" method="POST" class="form-wishlist">
                                        @csrf
                                        @method('put')
                                        <div class="wishlist-title">
                                            <h2>Ma Wishlist</h2>
                                            @include('layouts.component.message.json')
                                            <a class="btn button btn-float-left" id="edit-form-wishlist">
                                                <i class="fa fa-pencil"></i>
                                                Editer votre titre
                                            </a>
                                        </div>
                                        <div class="hidden-form-title">
                                            <input type="text" name="title" value="{{ $wishlist->title }}">
                                            <button type="submit" class="btn-float-left">Sauvegarder</button>
                                            <a class="btn button btn-float- ml-2" id="click-hidden-form">
                                                <i class="fa fa-remove"></i>
                                                Annuler
                                            </a>
                                        </div>

                                        <!-- Wishlist Table Listing -->
                                        <div class="table-responsive table-responsive-mt-wishlist">
                                            @include('wishlists.table', [$products])
                                        </div>
                                    </form>
                                @endif
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
