<table class="wishlist-table cart">
    <thead>
    <tr>
        <th class="product-remove"></th>
        <th class="product-thumbnail"></th>
        <th class="product-name">
            <span>Nom du produit</span>
        </th>
        <th class="product-price">
            <span>Prix du produit</span>
        </th>
        <th class="product-stock">
            <span>Stock status</span>
        </th>
        <th class="product-add-to-cart"></th>
    </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
            <tr id="tr-wishlist-{{ $product['id'] }}">
                <td class="product-remove">
                    <a href="{{ route('product.wishlist.delete', [$product['id'] ?: null]) }}" class="remove" aria-label="Remove this item" id="remove-wishlist-product">×</a>
                </td>
                <td class="product-thumbnail">
                    @if (isset($product['image']) && !empty($product['image']))
                        <a href="{{ $product['image']  }}">
                            <img width="420" height="510" src="{{ $product['image'] ?: null }}" alt="" sizes="(max-width: 420px) 100vw, 420px">
                        </a>
                    @else
                        <svg class="bd-placeholder-img card-img-top"
                             width="300" height="250"
                             xmlns="http://www.w3.org/2000/svg"
                             preserveAspectRatio="xMidYMid slice"
                             focusable="false" role="img"
                             aria-label="Placeholder: Thumbnail"><title>
                                Placeholder</title>
                            <rect width="100%" height="100%"
                                  fill="#55595c"></rect>
                            <text x="50%" y="50%" fill="#eceeef"
                                  dy=".3em">Image non valide
                            </text>
                        </svg>
                    @endif
                </td>
                <td class="product-name" data-title="Product">
                    <a href="{{ route('product.show', [\App\Product::find((int)$product['productID'])]) }}">{{ $product['product'] ?: '' }}</a>
                </td>
                <td class="product-price" data-title="Price">
                    <span class="price-amount">
                        <span class="woocommerce-Price-currencySymbol">$</span>{{ $product['price'] ?: 0 }}
                    </span>
                </td>
                <td colspan="product-stock">
                    <span>In Stock</span>
                </td>
                <td class="product-add-to-cart">
                    <input type="hidden" id="add-card-product-id" data-product-id="{{ $product['productID'] }}">
                    <a href="{{ route('product.basket.store') }}" class="btn button" id="add-to-cart">
                        <i class="fa fa-shopping-cart"></i>
                        Add to Cart
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
