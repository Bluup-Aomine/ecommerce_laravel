@extends('layouts.app')

@php
    use App\Basket;
    use Illuminate\Support\Facades\Auth;
    $basketUri = Auth::user() ? route('product.basket.edit', [Basket::findByUser(Auth::id())->first() ? Basket::findByUser(Auth::id())->first() : null ]): route('product.basket.session')
@endphp

@section('title')
    Panier - POE&TIK
@endsection

@section('content')
    <div class="content-title-head-page color-bg-black">
        <div class="container">
            <div class="title-contain">
                <h2>Panier</h2>
            </div>
            <span>
                <a href="{{ route('home') }}" class="home">
                    <span>Accueil</span>
                </a>
            </span>
            <span class="fa fa-angle-right"></span>
            <span>
                <a href="{{ $basketUri }}" class="home">
                    <span>Panier</span>
                </a>
            </span>
        </div>
    </div>
    <div id="shop-product">
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        @include('layouts.component.message.primary')
                        @include('layouts.component.message.danger')
                        @include('layouts.component.message.json')
                        <article class="entry-post-cart">
                            <div class="entry-content">
                                <div class="cart-products">
                                    <form
                                        action="{{ $basket ? route('product.basket.update', [$basket]) : route('product.basket.update.session') }}"
                                        class="cart-products-form" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}
                                        <div class="table-responsive">
                                            <table class="cart shop-products">
                                                <thead>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th>&nbsp;</th>
                                                    <th scope="col" class="product-name">Produit</th>
                                                    <th scope="col" class="product-price">Prix</th>
                                                    <th scope="col" class="product-size">Taille</th>
                                                    <th scope="col" class="product-color">Couleur</th>
                                                    <th scope="col" class="product-quantity">Quantité</th>
                                                    <th scope="col" class="product-total">Total</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @include('baskets.basketProducts', [$basketProducts])
                                                @if (count($basketProducts) !== 0)
                                                    <tr>
                                                        <td colspan="6" class="actions">
                                                            <button type="submit" class="button" name="update-cart">Modifier
                                                                panier
                                                            </button>
                                                        </td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </form>
                                    <div class="cart-collaterals">
                                        <div class="cart-totals">
                                            <h2>Panier Total</h2>
                                            <table class="shop-products cart">
                                                <tbody>
                                                <tr class="shipping">
                                                    <th>Livraison</th>
                                                    <td data-title="Shipping" class="shipping-text">
                                                        Achat disponibles dans toutes les régions du monde
                                                    </td>
                                                </tr>
                                                <tr class="shipping">
                                                    <th>Frais de port</th>
                                                    <td data-title="Shipping" class="shipping-text">
                                                        €{{ $total <= 50 ? \App\src\helpers\Price::shipping($basketProducts) : 0 }}
                                                    </td>
                                                </tr>
                                                <tr class="order-total">
                                                    <th>Total</th>
                                                    <td data-title="Total">
                                                        <strong>
                                                                <span class="price-amount">
                                                                    <span>€</span>{{ ($total + \App\src\helpers\Price::shipping($basketProducts)) }}
                                                                </span>
                                                        </strong>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="btn-checkout">
                                                <a href="{{ route('checkout') }}" class="btn button">Proceed to
                                                    checkout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
