<table class="table table-centered mb-0 table-nowrap">
    <thead class="thead-light">
    <tr>
        <th scope="col">Produit</th>
        <th scope="col">Produit Description</th>
        <th scope="col">Produit Couleur</th>
        <th scope="col">Produit Taille</th>
        <th scope="col">Prix</th>
    </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
            <tr>
                <th scope="row">
                    <img src="{{ $product['image'] }}" alt="product-img" title="product-img" class="avatar-md">
                </th>
                <td>
                    <h5 class="font-size-14 text-truncate">
                        <a href="#" class="text-dark">{{ $product['product'] }} </a>
                    </h5>
                    <p class="text-muted mb-0">{{ $product['price'] }} x {{ $product['quantity'] }}</p>
                </td>
                <td>{{ mb_strtoupper($product['color']) }}</td>
                <td>{{ mb_strtoupper($product['size']) }}</td>
                <td>{{ mb_strtoupper($product['price']) }} €</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="2">
                <h6 class="mb-0">Total:</h6>
            </td>
            <td>
                {{ is_int($total) ? $total . '.00 €' : $total }}
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <div class="bg-soft-primary p-3 rounded">
                    <h5 class="font-size-14 text-primary mb-0">
                        <i class="fa fa-truck"></i>
                        Livraison
                        <span
                            class="float-right">{{ $total > 50 ? 'Gratuit' : (\App\src\helpers\Price::shipping($products)) . '€' }}</span>
                    </h5>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <h6 class="mb-0 text-right">Total:</h6>
            </td>
            <td>
                {{ $total > 50 ? 'Gratuit' : ($total + \App\src\helpers\Price::shipping($products)) . '€' }}
            </td>
        </tr>
    </tbody>
</table>
