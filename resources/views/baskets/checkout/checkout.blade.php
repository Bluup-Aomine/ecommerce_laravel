@extends('layouts.app')

@section('extra-script')
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
@endSection

@php
    use App\Basket;
    use Illuminate\Support\Facades\Auth;
    $basketUri = Auth::user() ? route('product.basket.edit', [Basket::findByUser(Auth::id())->first() ? Basket::findByUser(Auth::id())->first() : 1 ]): route('product.basket.session')
@endphp

@section('title')
    Paiement - POE&TIK
@endsection

@section('content')
    <div class="content-title-head-page color-bg-black">
        <div class="container">
            <div class="title-contain">
                <h2>Votre Commande</h2>
            </div>
            <span>
                <a href="{{ route('home') }}" class="home">
                    <span>Accueil</span>
                </a>
            </span>
            <span class="fa fa-angle-right"></span>
            <span>
                <a href="{{ route('checkout') }}" class="home">
                    <span>Panier</span>
                </a>
            </span>
        </div>
    </div>
    <div class="content">
        <div class="container">
            @include('layouts.component.message.json')
            @include('layouts.component.message.jsonForm')
            <div class="checkout-tabs">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="nav nav-pills flex-column">
                            <a href="#" class="nav-link active checkout-link" id="shipping">
                                <i class="fa fa-truck nav-icon d-block mb-2 mt-4"></i>
                                <p class="font-weight-bold mb-4">Livraison Info</p>
                            </a>
                            <a href="#" class="nav-link checkout-link" id="payment">
                                <i class="fa fa-money nav-icon d-block mb-2 mt-4"></i>
                                <p class="font-weight-bold mb-4">Paiement Info</p>
                            </a>
                            <a href="#" class="nav-link checkout-link" id="confirmation">
                                <i class="fa fa-check-circle-o nav-icon d-block mb-2 mt-4"></i>
                                <p class="font-weight-bold mb-4">Confirmation</p>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-9" id="loading-element">
                        <div class="card card-gray">
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="tab-checkout-shipping">
                                        <div>
                                            <h2 class="card-title">Livraison information</h2>
                                            <p class="card-title-desc">Remplissez toutes les informations ci-dessous</p>
                                            <form action="{{ route('checkout.customer') }}" method="POST"
                                                  id="payment-form-customer">
                                                <div class="form-group row mb-4">
                                                    <label for="name" class="col-md-2 col-form-label">Nom</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control card-form-control"
                                                               name="name" id="name" placeholder="Entrer votre nom...">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-4">
                                                    <label for="email" class="col-md-2 col-form-label">Email</label>
                                                    <div class="col-md-10">
                                                        <input type="email" name="email"
                                                               class="form-control card-form-control" id="email"
                                                               placeholder="Entrer votre email...">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-4">
                                                    <label for="phone" class="col-md-2 col-form-label">Tel No.</label>
                                                    <div class="col-md-10">
                                                        <input type="tel" name="phone"
                                                               class="form-control card-form-control" id="phone"
                                                               placeholder="Entrer votre numéro de téléphone...">
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-4">
                                                    <label for="address"
                                                           class="col-md-2 col-form-label">Adresse</label>
                                                    <div class="col-md-10">
                                                        <textarea name="address" class="form-control card-form-control"
                                                                  id="address"
                                                                  placeholder="Entrer votre adresse..."></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-4">
                                                    <label for="country" class="col-md-2 col-form-label">Pays</label>
                                                    <div class="col-md-10">
                                                        <select name="country" id="country"
                                                                class="form-control card-form-control"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-4">
                                                    <label for="order_notes" class="col-md-2 col-form-label">Autres
                                                        Notes:</label>
                                                    <div class="col-md-10">
                                                        <textarea name="order_notes"
                                                                  class="form-control card-form-control"
                                                                  id="order_notes"
                                                                  placeholder="Ecrirer d'autres notes.."></textarea>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-checkout-payment">
                                        <div>
                                            <h4 class="card-title">Paiement information</h4>
                                            <p class="card-title-desc">Remplissez toutes les informations ci-dessous</p>
                                            <div>
                                                <form action="#">
                                                    <div class="custom-control custom-radio custom-control-inline mr-4">
                                                        <input type="radio" name="payment" id="input-cart-ratio"
                                                               class="custom-control-input" checked>
                                                        <label for="input-cart-ratio" class="custom-control-label">
                                                            <i class="fa fa-credit-card mr-1 align-top font-size-20"></i>
                                                            Carte bancaire
                                                        </label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline mr-4">
                                                        <input type="radio" name="payment" id="input-paypal-ratio"
                                                               class="custom-control-input">
                                                        <label for="input-paypal-ratio" class="custom-control-label">
                                                            <i class="fa fa-cc-paypal mr-1 align-top font-size-20"></i>
                                                            Paypal
                                                        </label>
                                                    </div>
                                                </form>
                                            </div>
                                            <h5 class="mt-5 mb-3 font-size-15">Pour le paiement par carte</h5>
                                            <div class="p-4 border" id="cart-content">
                                                <form action="{{ route('checkout.store') }}" id="payment-form-cart"
                                                      method="POST">
                                                    @csrf
                                                    <div id="card-element"></div>
                                                    <div id="card-errors" role="alert"></div>
                                                </form>
                                            </div>
                                            <div class="p-4 d-none" id="paypal-content">
                                                <div id="payment-form-paypal"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab-checkout-confirmation">
                                        <div class="card shadow-none border mb-0">
                                            <div class="card-body">
                                                <h4 class="card-title mb-4">Récapitulatif de la commande</h4>
                                                <div class="table-responsive">
                                                    @include('baskets.checkout.table')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-4">
                            <div class="col-sm-6">
                                <a href="{{ $basketUri }}"
                                   class="btn btn-float-left text-muted d-none d-sm-inline-block btn-link">
                                    <i class="fa fa-arrow-left mr-1"></i>
                                    Retour au panier
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <div class="text-sm-right">
                                    <button type="button" class="btn btn-sm-radius btn-success" id="submit-checkout">
                                        <i class="fa fa-truck mr-1"></i>
                                        Procéder à l'achat
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script src="{{ asset('js/library/countries.js') }}"></script>
    <script src="{{ asset('js/component/checkout/ChangeMethodPayment.js') }}" type="module" defer></script>
    <script lang="js">populateCountries('country')</script>
    <script lang="js" type="module">
        import Card from '{{ asset('js/component/checkout/Card.js') }}';

        let stripe = Stripe("{{ \App\src\concern\CacheToken::getTokenStripe() }}");
        let elements = stripe.elements();

        let card = elements.create('card')
        card.mount("#card-element")
        card.on('change', ({error}) => {
            const displayError = document.getElementById('card-errors');
            if (error) {
                displayError.classList.add('color-cart-red')
                displayError.textContent = error.message;
            } else {
                displayError.classList.remove('color-cart-red')
                displayError.textContent = '';
            }
        });
        let submitCheckout = document.getElementById('submit-checkout');

        const cardAction = new Card()
        submitCheckout.addEventListener('click', async function (ev) {
            ev.preventDefault();
            cardAction.sendLoading()
            submitCheckout.disabled = true

            stripe.createPaymentMethod({
                type: 'card',
                card
            }).then(async function (result) {
                if (result) {
                    let request = await cardAction.UpdateAmountPayment("{{ $payment_intent_id }}", result)
                    if (request) {
                        stripe.confirmCardPayment("{{ $clientSecret }}", {
                            payment_method: {card}
                        }).then(async function (result) {
                            if (result.error) {
                                // Show error to your customer (e.g., insufficient funds)
                                submitCheckout.disabled = true
                                cardAction.errorMessage()
                            } else {
                                // The payment has been processed!
                                if (result.paymentIntent.status === 'succeeded') {
                                    let response = await cardAction.responseCardSuccess(result)
                                    if (response.success) {
                                        let success = await cardAction.responseCustomerSuccess(result.paymentIntent.id)
                                        if (success) {
                                            setInterval(() => window.location.href = '{{ route('home') }}', 2000)
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
            })
        });
    </script>
    <script lang="js" type="module">
        import NotificationElement from '{{ asset('js/component/element/NotificationElement.js') }}'
        import MessageError from '{{ asset('js/component/checkout/MessageError.js') }}'

        paypal.Button.render({
            env: '{{ app()->environment('production') ? 'production' : 'sandbox' }}',
            commit: true,
            locale: 'fr_FR',
            style: {
                size: 'medium',
                color: 'blue'
            },
            payment: function () {
                const formCheckout = document.getElementById('payment-form-customer')
                const json = {
                    name: formCheckout.querySelector('#name').value,
                    email: formCheckout.querySelector('#email').value,
                    phone: formCheckout.querySelector('#phone').value,
                    address: formCheckout.querySelector('#address').value,
                    country: formCheckout.querySelector('#country').value,
                }

                return paypal.request({
                    method: 'POST',
                    url: '{{ route('paypal.payment') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    json
                }).then((data) => {
                    if (data.success !== true) {
                        const error = new MessageError()
                        error.renderMessage(data.messages)
                    }
                    if (data.success) {
                        return data.id
                    }
                })
            },
            onAuthorize: function (data, actions) {
                const formCheckout = document.getElementById('payment-form-customer')

                return paypal.request({
                    method: 'POST',
                    url: '{{ route('paypal.payment.sale') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    json: {
                        paymentID: data.paymentID,
                        payerID: data.payerID,
                        name: formCheckout.querySelector('#name').value,
                        email: formCheckout.querySelector('#email').value,
                        phone: formCheckout.querySelector('#phone').value,
                        address: formCheckout.querySelector('#address').value,
                        country: formCheckout.querySelector('#country'),
                        other_notes: formCheckout.querySelector('#order_notes').value,
                    }
                }).then(function (data) {
                    if (data.success) {
                        (new NotificationElement).notification(data.message)
                        setInterval(() => window.location.href = '{{ route('home') }}', 2000)
                    }
                }).catch(function (err) {
                    console.log('erreur', err)
                });
            }
        }, '#payment-form-paypal');
    </script>
@endsection
