@foreach($basketProducts as $key => $basketProduct)
    <tr class="cart-products-item" id="basket-product-{{ $basketProduct['id'] }}">
        <td class="product-remove">
            <a href="{{ route('product.basket.delete', [$basketProduct['id']]) }}" class="remove" id="remove-basket-product">×</a>
        </td>
        <td class="product-thumbnail">
            <a href="#">
                <img src="{{ $basketProduct['image'] }}"
                    width="420" height="510" alt="">
            </a>
        </td>
        <td class="product-name" data-title="product">
            <a href="#">{{ isset($basketProduct['product']) ? $basketProduct['product'] : $basketProduct['name'] }}</a>
        </td>
        <td class="product-price" data-title="price">
            <span class="price-amount">
                  <span>€</span>{{ $basketProduct['price'] }}
             </span>
        </td>
        <td class="product-size" data-title="size">
            {{ mb_strtoupper($basketProduct['size']) }}
        </td>
        <td class="product-color" data-title="color">
            {{ mb_strtoupper($basketProduct['color']) }}
        </td>
        <td class="product-quantity" data-title="quantity">
            <div class="quantity">
                <label for="quantity" class="off-screen-text"></label>
                <input type="number" id="quantity" class="quantity-cart"
                       step="1" min="0" name="quantity[]" value="{{ $basketProduct['quantity'] }}" size="4">
                @if (is_null($basket))
                    <input type="hidden" name='session-basket' value="{{ $key }}">
                @endif
            </div>
        </td>
        <td class="product-total" data-title="total">
              <span class="price-amount">
                    <span>€</span>{{ $basketProduct['total'] }}
              </span>
        </td>
    </tr>
@endforeach
