@extends('layouts.app')

@section('title')
    Boutique vêtements vegan éthique - POE&TIK
@endsection

@section('content')
    <div class="element-content">
        <div class="element-content-contain">
            <div class="element-section-wrap">
                @include('home.content')
                @include('home.categories')
                @include('home.products-new')
                @include('home.instagram-products')
            </div>
        </div>
    </div>
@endsection

@section('extra-script')
<script src="{{ asset('js/component/instagram/ApiFetch.js') }}" type="module" defer></script>
@endsection
