<section class="element-section element-news-products w-100 fadeIn animated">
    <div class="element-container instagram-width ">
        <div class="element-row">
            <div class="element element-column element-col-100">
                <div class="element-column-wrap">
                    <div class="element-widget-menu">
                        <div class="element element-widget element-widget-header element-widget-title-news ta">
                            <div class="element-widget-contain">
                                <h2 class="element-widget-title-heading">Instagram POE_TIK</h2>
                            </div>
                        </div>
                        <div class="element element-widget element-products-lists pt-2">
                            <div class="element-widget-contain">
                                <div class="posts-instagram row lg-align-center">
                                    <div class="col-12 flex-wrap posts-row column-flex" id="instagram-elements"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
