<section class="element-section w-100 fadeIn animated">
    <div class="element-container">
        <div class="element-row">
            <div class="element-column element-col-100">
                <div class="element-column-wrap">
                    <div class="element-widget-menu">
                        <div class="element element-widget">
                            <div class="element-widget-contain">
                                <div class="element-content-img-container">
                                    <div class="element-img-wrapper">
                                        <div class="element-img-bg element-transition-img-bg" style="background-image: url({{ asset('img/head.jpeg') }}); "></div>
                                        <div class="element-img-bg-overlay"></div>
                                    </div>
                                    <div class="element-img-bg-content">
                                        <h2 class="element-img-bg-content-title content-title">
                                            POE & TIK
                                            <!--
                                                <div class="content-after-title">
                                                    <a href="#" class="shop-link-content link-btn element-size-sm"> Shop </a>
                                                </div>
                                            -->
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
