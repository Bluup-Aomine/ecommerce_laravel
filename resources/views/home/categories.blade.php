<section class="element-section element-categories w-100 fadeIn animated">
    <div class="element-container">
        <div class="element-row flex-wrap-element-category">
            <div class="element-column element-col-66 pr-4">
                <div class="element-column-wrap">
                    <div class="element-widget-menu">
                        <div class="element element-widget element-transform-move">
                            <div class="element-widget-contain">
                                <div class="element-content-img-container">
                                    <div class="element-img-wrapper">
                                        <div class="element-img-bg-categories element-transition-img-bg element-transition-scale" style="background-image: url({{ asset('img/concept.jpeg') }}); "></div>
                                        <div class="element-img-bg-overlay"></div>
                                    </div>
                                    <div class="element-img-bg-content-categories">
                                        <h2 class="element-img-bg-content-title content-title">
                                            POE & TIK
                                            <div class="content-after-title">
                                                <a href="{{ route('concept') }}" class="shop-link-content link-btn element-size-sm"> Notre Concept </a>
                                            </div>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="element-column element-col-66">
                <div class="element-column-wrap">
                    <div class="element-widget-menu">
                        <div class="element element-widget element-transform-move">
                            <div class="element-widget-contain">
                                <div class="element-content-img-container">
                                    <div class="element-img-wrapper">
                                        <div class="element-img-bg-categories element-transition-img-bg element-transition-scale" style="background-image: url({{ asset('img/collection.jpeg') }}); "></div>
                                        <div class="element-img-bg-overlay"></div>
                                    </div>
                                    <div class="element-img-bg-content-categories">
                                        <h2 class="element-img-bg-content-title content-title text-white">
                                            POE & TIK
                                            <div class="content-after-title">
                                                <a href="{{ route('collections') }}" class="shop-link-content link-btn element-size-sm text-white"> Collection </a>
                                            </div>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
