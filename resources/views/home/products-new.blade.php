<section class="element-section element-news-products w-100 fadeIn animated">
    <div class="element-container element-products-width">
        <div class="element-row">
            <div class="element element-column element-col-100">
                <div class="element-column-wrap">
                    <div class="element-widget-menu">
                        <div class="element element-widget element-widget-header element-widget-title-news ta">
                            <div class="element-widget-contain">
                                <h2 class="element-widget-title-heading">Nouveaux Produits</h2>
                            </div>
                        </div>
                        <div
                            class="element element-widget element-widget-header element-widget-title-news element-under-title">
                            <div class="element-widget-contain">
                                <div class="ta">
                                    <p>Ajoutez nos nouveautés à votre programmation hebdomadaire</p>
                                </div>
                            </div>
                        </div>
                        <div class="element element-widget element-products-lists mb--4">
                            @include('layouts.component.message.json')
                            <div class="element-widget-contain">
                                <div class="commerce-products">
                                    @include('layouts.component.message.json')
                                    <ul class="{{ count($products) > 4 ? 'products carousel-news-product' : 'products' }}">
                                        @foreach($products as $key => $product)
                                            <li class="product">
                                                <div class="product-contain">
                                                    <div class="product">
                                                        <div class="product-content-top">
                                                            <div class="product-image">
                                                                <a href="{{ route('product.show', [$product]) }}">
                                                                    @if (isset($product->images[0]))
                                                                        <img src="{{ $product->images[0]->img_medium }}"
                                                                             class="product-img" width="420"
                                                                             height="510"
                                                                             alt="">
                                                                    @else
                                                                        <svg class="bd-placeholder-img card-img-top"
                                                                             width="100%" height="225"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             preserveAspectRatio="xMidYMid slice"
                                                                             focusable="false" role="img"
                                                                             aria-label="Placeholder: Thumbnail"><title>
                                                                                Placeholder</title>
                                                                            <rect width="100%" height="100%"
                                                                                  fill="#55595c"></rect>
                                                                            <text x="50%" y="50%" fill="#eceeef"
                                                                                  dy=".3em">Image non valide
                                                                            </text>
                                                                        </svg>
                                                                    @endif
                                                                </a>
                                                                <div class="product-active-buttons">
                                                                    <div class="button-cart top-btn"
                                                                         aria-label="Add To Cart">
                                                                        <a href="{{ route('product.basket.store') }}"
                                                                           class="btn btn-add-cart"
                                                                           data-product-id="{{ $product->id }}"
                                                                           id="btnAddCartIcon">
                                                                            <i class="fa fa-shopping-cart icon icon-bag-cart"></i>
                                                                        </a>
                                                                    </div>


                                                                    <div class="button-cart top-btn"
                                                                         aria-label="Quick View">
                                                                        <a href="{{ route('product.show', [$product]) }}"
                                                                           class="btn btn-add-cart">
                                                                            <i class="fa fa-shopping-bag icon icon-bag-cart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="button-cart top-btn"
                                                                         aria-label="Quick View">
                                                                        @if (!is_null($wishlist) && isset($product->wishlistProducts[0]) && $product->wishlistProducts[0]->wishlist_id === $wishlist->id)
                                                                            <a href="{{ route('product.wishlist.index') }}"
                                                                               class="btn btn-add-cart"
                                                                               id="wishlist-index-{{ $product->id }}">
                                                                                <i class="fa fa-heart-o icon icon-bag-cart color-gold"></i>
                                                                            </a>
                                                                        @elseif(is_null($wishlist) || !isset($product->wishlistProducts[0]) || $product->wishlistProducts[0]->wishlist_id !== $wishlist->id)
                                                                            <a href="{{ route('product.wishlist.add') }}"
                                                                               class="btn btn-add-cart"
                                                                               data-product-id="{{ $product->id }}"
                                                                               id="btn-add-wishlist">
                                                                                <i class="fa fa-heart-o icon icon-bag-cart"></i>
                                                                            </a>
                                                                            <a href="{{ route('product.wishlist.index') }}"
                                                                               class="btn btn-add-cart d-none"
                                                                               id="wishlist-index-{{ $product->id }}">
                                                                                <i class="fa fa-heart-o icon icon-bag-cart color-gold"></i>
                                                                            </a>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h3 class="product-title pb-5px">
                                                            <a href="{{ route('product.show', [$product]) }}">{{ $product->name }}</a>
                                                        </h3>
                                                        <!--
                                                            <div class="star-rating">
                                                                <span style="width: 100%">
                                                                    Rated
                                                                    <strong class="rating">5.00</strong>
                                                                     out of 5
                                                                </span>
                                                            </div>
                                                        -->
                                                        <span class="price price-filter-products">
                                                            @if ($product->promo)
                                                                <del>
                                                                    <span class="price-amount">
                                                                        <span class="price-symbol">€</span>{{ $product->price }}
                                                                    </span>
                                                                </del>
                                                                <ins>
                                                                    <span class="price-amount">
                                                                        <span class="price-symbol">€</span>{{ $product->price_promo }}.00
                                                                    </span>
                                                                </ins>
                                                            @else
                                                                <span class="price-amount">
                                                                    <span class="price-symbol">€</span>{{ $product->price }}
                                                                </span>
                                                            @endif
                                                         </span>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="element element-widget element-more-products ta margin-top-5rem">
                            <div class="element-widget-contain">
                                <div class="element-button-wrap">
                                    <a href="{{ route('collections') }}"
                                       class="element-button element-size-sm button-more-products" role="button">
                                        <span class="element-button-content">
                                            <span class="element-button-text">Voir Plus</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
