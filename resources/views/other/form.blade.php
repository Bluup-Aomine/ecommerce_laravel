<form action="{{ route('contact.post') }}" method="POST" class="form-contact">
    @csrf
    <div class="form-group">
        <p>Votre nom</p>
        <p>
             <span class="form-control-wrap">
                       <input type="text" name="name" size="40" aria-required="true" aria-invalid="false"
                              placeholder="Ex: John Doe">
             </span>
        </p>
    </div>
    <div class="form-group">
        <p>Votre Email</p>
        <p>
               <span class="form-control-wrap">
                    <input type="email" name="email" size="40" aria-required="true" aria-invalid="false"
                           placeholder="Ex: John_Doe@domain.com">
                </span>
        </p>
    </div>
    <div class="form-group">
        <p>Votre contenu</p>
        <p>
             <span class="form-control-wrap">
                 <textarea cols="40" rows="4" name="content" aria-invalid="false"
                           placeholder="Votre contenu..."></textarea>
             </span>
        </p>
    </div>
    <div class="form-group">
        <input type="submit" value="Envoyer le message" class="btn btn-float-left">
    </div>
</form>
