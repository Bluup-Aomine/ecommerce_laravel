@php
    $robots = 'nofollow,noindex'
@endphp

@extends('layouts.app', [$robots])

@section('title')
    Nous Contacter - POE&TIK
@endsection

@section('content')
    <div class="content-title-head-page color-bg-black">
        <div class="container">
            <div class="title-contain">
                <h2>Nous Contacter</h2>
            </div>
            <span>
                <a href="#" class="home">
                    <span>Home</span>
                </a>
            </span>
            <span class="fa fa-angle-right"></span>
            <span>
                <a href="#" class="home">
                    <span>Nous Contacter</span>
                </a>
            </span>
        </div>
    </div>
    <div id="contact-content">
        <div class="element">
            <div class="element-container">
                <div class="element-section-wrap">
                    <section class="element-section element-section-boxed element-section-width-100 animated">
                        <div class="element-container">
                            <div class="element-row">
                                <div class="element element-column element-col-100">
                                    @include('layouts.component.message.primary')
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="element-section element-section-boxed element-section-width-100 animated animated">
                        <div class="element-container">
                            <div class="element-row">
                                <div class="element element-column element-col-33">
                                    <div class="element-column-wrap">
                                        <div class="element-widget-wrap">
                                            <div class="element element-widget element-widget-header element-widget-left">
                                                <div class="element-widget-contain">
                                                    <h2 class="element-header-title">Écrivez-nous</h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="element element-column element-col-66 mb-4">
                                    <div class="element-column-wrap">
                                        <div class="element-widget-wrap">
                                            <div class="element element-widget">
                                                <div class="element-widget-contain">
                                                    <div role="form" class="block-form-contact">
                                                        @include('other.form')
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
