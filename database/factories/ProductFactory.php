<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'slug' => $faker->slug,
        'content' => $faker->text(250),
        'price' => $faker->numberBetween(650, 15000),
        'promo' => $faker->boolean,
        'category_id' => (int)$faker->numberBetween(2, 5),
        'colors' => 'blue, red, black',
        'sizes' => 'xl, s, m, xxl, xxxl'
    ];
});
