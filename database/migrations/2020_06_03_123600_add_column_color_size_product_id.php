<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnColorSizeProductId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('baskets_products', function (Blueprint $table) {
            $table->unsignedBigInteger('color_size_product_id')->nullable();
            $table->foreign('color_size_product_id')->references('id')->on('color_size_products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('baskets_products', function (Blueprint $table) {
            //
        });
    }
}
