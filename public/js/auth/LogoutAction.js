import Ajax from "../app/src/Ajax.js";

/**
 * @property {HTMLLinkElement} logoutBtn
 */
export default class LogoutAction
{
    constructor() {
        this.logoutBtn = document.querySelectorAll('#logout-action-link')
        this.request = new Ajax()
    }

    /**
     * @param {Event} e
     * @returns {Promise<void>}
     */
    async eventLogout(e)
    {
        e.preventDefault()

        const request = await this.request.ajaxURL('/api/logout')
        if (request.status === 200 || request.status === 204) {
            window.location.reload()
        }
    }

    init()
    {
        this.logoutBtn.forEach((btn) => {
            btn.addEventListener('click', this.eventLogout.bind(this))
        })
    }

}

const logoutAction = new LogoutAction()
logoutAction.init()
