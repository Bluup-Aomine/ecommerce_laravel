/**
 * @property {HTMLLinkElement} btns
 * @property {HTMLButtonElement} closeBtn
 */
export default class OrderModal {
    constructor() {
        this.btns = document.querySelectorAll('.btn-view-order')
        this.closeBtn = document.querySelectorAll('.close')
    }

    /**
     * @param {Event} e
     */
    close(e)
    {
        e.preventDefault()
        const target = e.target
        const orderID = target.getAttribute('data-order-id') === null ? target.parentNode.getAttribute('data-order-id') : target.getAttribute('data-order-id')

        if (orderID) {
            const element = document.getElementById('modal-order-front-' + orderID)
            const elementShow = document.getElementById('modal-order-show-' + orderID)

            element.classList.add('d-none')
            element.classList.remove('d-block')
            elementShow.classList.add('d-none')
        }
    }

    /**
     * @param {Event} e
     */
    modal(e)
    {
        e.preventDefault()

        const target = e.target
        const orderID = target.getAttribute('data-order-id')
        if (orderID) {
            const element = document.getElementById('modal-order-front-' + orderID)
            const elementShow = document.getElementById('modal-order-show-' + orderID)

            element.classList.add('d-block')
            element.classList.remove('d-none')
            elementShow.classList.remove('d-none')
        }
    }

    init()
    {
        this.btns.forEach((link) => {
            link.addEventListener('click', this.modal.bind(this))
        })
        this.closeBtn.forEach((button) => {
            button.addEventListener('click', this.close.bind(this))
        })
    }
}

const orderModal = new OrderModal()
orderModal.init()
