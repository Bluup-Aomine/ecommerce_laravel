export default class AuthAccountTab {
    constructor() {
        this.linkTab = document.querySelectorAll('#link-tab-account')
    }

    /**
     * @param {Event} e
     */
    showBlock(e)
    {
        e.preventDefault()

        const target = e.target
        const title = target.getAttribute('title')

        window.location.hash = '';
        this.activeElement(title)
        return this.activeLink(title)
    }

    /**
     * @param {string} search
     */
    activeElement(search)
    {
        const element = document.getElementById(search)
        if (element) {
            $(element).siblings('div').addClass('d-none')
            $(element).removeClass('d-none')
        }
    }
    /**
     *
     * @param {string} search
     */
    activeLink(search)
    {
        return this.linkTab.forEach((link) => {
            if (link.getAttribute('title') === search) {
                const li = link.parentElement
                $(li).siblings('li').removeClass('is-active')
                $(li).addClass('is-active')
            }
        })
    }

    /**
     * @return {boolean|void}
     */
    hrefActive()
    {
        const locationHash = window.location.hash
        if (locationHash === '') {
            return false
        }
        const href = locationHash.split('#')[1]
        this.activeElement(href)
        return this.activeLink(href)
    }

    init()
    {
        this.linkTab.forEach((link) => {
            link.addEventListener('click', this.showBlock.bind(this))
        })
        this.hrefActive()
    }
}

const accountTab = new AuthAccountTab()
accountTab.init()
