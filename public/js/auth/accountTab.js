/**
 * @property {HTMLElement} loginColumn
 * @property {HTMLElement} registerColumn
 */
export default class AccountTab {
    constructor() {
        this.loginColumn = document.querySelector('#u-column1')
        this.registerColumn = document.querySelector('#u-column2')
    }

    init() {
        this.tabClick(this.registerColumn, this.loginColumn)
        this.tabClick(this.loginColumn, this.registerColumn)
    }

    /**
     * @param {HTMLElement} elementFirst
     * @param {HTMLElement} elementSecond
     */
    tabClick(elementFirst, elementSecond)
    {
        if (elementFirst !== null) {
            elementFirst.addEventListener('click', function (e) {
                e.preventDefault()

                const FirstID = elementFirst.getAttribute('id')
                const secondID =  elementSecond.getAttribute('id')

                const TabFirst = document.querySelector('.' + FirstID)
                const TabSecond = document.querySelector('.' + secondID)

                if (elementFirst.classList.value.indexOf('active') === -1) {
                    elementSecond.classList.remove('active')
                    TabSecond.style.display = 'none'

                    elementFirst.classList.add('active')
                    TabFirst.style.display = 'block'
                }
            })
        }
    }
}

const account = new AccountTab()
account.init()
