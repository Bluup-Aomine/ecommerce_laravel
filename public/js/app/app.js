export {default as HeaderComponent} from '../headerComponent/header.js'
export {default as SearchWidget} from '../headerComponent/searchWidget.js'
export {default as WidgetHeader} from '../headerComponent/WidgetHeader.js'
export {default as LoadProductsBasket} from '../headerComponent/LoadProductsBasket.js'

export {default as AccountTab} from '../auth/accountTab.js'

export {default as SwiperComponent} from '../component/SwiperComponent.js'
export {default as FilterComponent} from '../component/FilterComponent.js'
export {default as Slider} from '../component/Slider.js'
export {default as AddWishlist} from '../component/wishlist/AddWishlist.js'
export {default as AddWishlistIcon} from '../component/wishlist/AddWishlistIcon.js'
export {default as Wishlist} from '../component/wishlist/Wishlist.js'
export {default as AddCartProduct} from '../component/AddCartProduct.js'
export {default as AddCartProductIcon} from '../component/AddCartProductIcon.js'
export {default as AddCartProductWishlist} from '../component/wishlist/AddCartProductWishlist.js'

export {default as DeleteBasketProduct} from '../component/cart/DeleteBasketProduct.js'
export {default as HoverElement} from '../component/collections/HoverElement.js'
export {default as tabs} from '../component/checkout/tabs.js'

// Auth Modules
export {default as LogoutAction} from '../auth/LogoutAction.js'
