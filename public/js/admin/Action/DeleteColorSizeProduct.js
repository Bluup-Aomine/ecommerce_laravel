import Ajax from "../../app/src/Ajax.js";
import NotificationElement from "../../component/element/NotificationElement.js";
/**
 * @property {HTMLLinkElement|HTMLIFrameElement} btnDelete
 * @property {Ajax} btnDelete
 */
export default class DeleteColorSizeProduct {
    constructor() {
        this.btnDelete = document.querySelectorAll('#delete-colorSize')
        this.ajax = new Ajax()
        this.notification = new NotificationElement()
    }

    async delete(e)
    {
        e.preventDefault()

        const target = e.target
        const attribute =  target.getAttribute('data-colorSize-id')
        const colorSizeID = attribute !== null ? attribute : target.parentNode.getAttribute('data-colorSize-id')
        const userID = attribute !== null ? target.getAttribute('data-user-auth') : target.parentNode.getAttribute('data-user-auth')

        if (colorSizeID){
            const formData = new FormData()
            formData.append('colorSizeID', colorSizeID)
            formData.append('userID', userID)

            const request = await this.ajax.ajaxURL('/api/colorSize/delete', 'POST', formData)
            if (request.status === 200 || request.status === 302) {
                const response = await request.json()
                const message = response.message
                if (response.success) {
                    let trProduct = document.querySelector('#colorSize-' + colorSizeID)
                    trProduct.remove()
                }
                this.notification.notification(message)
            }
        }
    }

    init()
    {
        if (this.btnDelete.length !== 0) {
            this.btnDelete.forEach((btn) => {
                btn.addEventListener('click', this.delete.bind(this))
            })
        }
    }
}

const Delete = new DeleteColorSizeProduct()
Delete.init()
