import Ajax from "../../app/src/Ajax.js";
import NotificationElement from "../../component/element/NotificationElement.js";
/**
 * @property {HTMLLinkElement|HTMLIFrameElement} btnDelete
 * @property {Ajax} btnDelete
 */
export default class DeleteProduct {
    constructor() {
        this.btnDelete = document.querySelectorAll('#delete-product')
        this.ajax = new Ajax()
        this.notification = new NotificationElement()
    }

    async delete(e)
    {
        e.preventDefault()

        const target = e.target
        const attribute =  target.getAttribute('data-product-id')
        const productID = attribute !== null ? attribute : target.parentNode.getAttribute('data-product-id')
        const userID = attribute !== null ? target.getAttribute('data-user-auth') : target.parentNode.getAttribute('data-user-auth')

        if (productID){
            const formData = new FormData()
            formData.append('productID', productID)
            formData.append('userID', userID)

            const request = await this.ajax.ajaxURL('/api/product/delete', 'POST', formData)
            if (request.status === 200 || request.status === 302) {
                const response = await request.json()
                const message = response.message
                if (response.success) {
                    let trProduct = document.querySelector('#product-' + productID)
                    trProduct.remove()
                }
                this.notification.notification(message)
            }
        }
    }

    init()
    {
        if (this.btnDelete.length !== 0) {
            this.btnDelete.forEach((btn) => {
                btn.addEventListener('click', this.delete.bind(this))
            })
        }
    }
}

const Delete = new DeleteProduct()
Delete.init()
