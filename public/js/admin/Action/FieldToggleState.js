/**
 * @property {HTMLInputElement} toggleField
 * @property {HTMLInputElement} field
 */
export default class FieldToggleState
{

    constructor() {
        this.toggleField = document.querySelector('.field-toggle-disabled')
        this.field = document.querySelector('#field-disabled')
    }

    /**
     * @param {Event} e
     */
    toggleDisabled(e)
    {
        const target = e.target
        const dataPromoActive = target.getAttribute('data-promo-active')
        if (dataPromoActive === 'true') {
            target.setAttribute('data-promo-active', 'false')
            this.field.disabled = true
        } else {
            target.setAttribute('data-promo-active', 'true')
            this.field.disabled = false
        }
    }

    init()
    {
        this.toggleField.addEventListener('click', this.toggleDisabled.bind(this))
    }

}

const field = new FieldToggleState()
field.init()
