import NotificationElement from "../../component/element/NotificationElement.js";

/**
 * @property  {HTMLElement} product
 * @property {NotificationElement} notif
 */
export default class Image {
    constructor() {
        this.product = document.querySelector('#product-value') ?? null
        this.notif = new NotificationElement()
    }

    /**
     * @param {Array} options
     * @return {FormData}
     */
    setFormData(options)
    {
       const formData = new FormData()
       let productID = this.product !== null ? this.product.value : null;

       formData.append('imagesID', JSON.stringify(options))
       formData.append('productID', JSON.stringify(productID))

       return formData
    }

    /**
     * @param {Array} options
     */
    async deleteFile(options)
    {
        const formData = this.setFormData(options)
        const request = await fetch('/api/image-product/delete/files', {
            method: 'POST',
            body: formData,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
        })
        if (request.status === 200 || request.status === 302) {
            const response = await request.json()
            if (response.success) {
                const message = response.flash

                options.forEach((id) => {
                    const elementImageDelete = document.querySelectorAll('#image-list-' + id)
                    elementImageDelete.forEach((element) => {
                        element.remove()
                    })
                })
                this.notif.notification(message)
            }
        }
    }
}
