import Image from "./../model/Image.js";

/**
 * @property {HTMLFormElement} submitFORM
 * @property {Array} valueOptions
 */
export default class ImageDelete {
    constructor() {
        this.submitFORM = document.querySelector('#delete-files')
        this.valueOptions = []
    }

    /**
     * @param {HTMLFormControlsCollection} elementsOptions
     */
    arrayToPush(elementsOptions)
    {
        const arrayToElements = Array.from(elementsOptions)
        arrayToElements.forEach((element) => {
            if (element.type === 'checkbox') {
                if (element.checked) {
                    this.valueOptions.push(element.value)
                }
            }
        })
    }

    /**
     * @param {Event} e
     */
    submit(e)
    {
        e.preventDefault()
        const elementsOptions = e.target.elements;
        this.arrayToPush(elementsOptions)

        const image = new Image()
        console.log(this.valueOptions)
        return image.deleteFile(this.valueOptions)
    }

    init() {
        if (this.submitFORM) {
            this.submitFORM.addEventListener('submit', this.submit.bind(this))
        }
    }
}

const imageDelete = new ImageDelete()
imageDelete.init()
