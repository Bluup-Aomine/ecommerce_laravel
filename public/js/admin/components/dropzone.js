/**
 * @property {HTMLDivElement} messageJSon
 */
export default class MyDropzone {
    constructor() {
        this.messageJSon = document.querySelector('#message-json')
    }

    addMessageJson()
    {
        this.messageJSon.innerHTML = "<div class=\"alert alert-success alert-dismissible fade show\">\n" +
            "        <i class=\"fa fa-check mr-2\"></i>\n" +
            "        Vous avez ajouter votre(vos) image(s) à votre produit avec succès !!!\n" +
            "        <button class=\"close\" type=\"button\" aria-label=\"close\" data-dismiss=\"alert\">\n" +
            "            <span aria-hidden=\"true\">×</span>\n" +
            "        </button>\n" +
            "    </div>"
    }

    /**
     * @param {string} myDropzone
     */
    initDropzone(myDropzone)
    {
        const toThis = this;
        new Dropzone(myDropzone, {
            'method': 'POST',
            'withCredentials': true,
            'maxFilesize': 10,
            'acceptedFiles': '.png, .jpg, .jpeg',
            'addRemoveLinks': true,
            'success': function () {
                return toThis.addMessageJson()
            },
            'removedfile': function (file) {
                let productID = $('#productID').val()

                const formData = new FormData()
                formData.append('filename', file.name)
                formData.append('productID', productID)

                fetch('/api/image-product/delete/json', {
                    method: 'POST',
                    body: formData,
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest'
                    },
                }).then((response) => {
                    if (response && (response.status === 200 || response.status === 302)) {
                        file.previewElement.remove();
                    }
                })
            }
        })
        this.clickBtnSubmitDropzone()
    }

    clickBtnSubmitDropzone()
    {
        document.querySelector('#dropzone-submit').addEventListener('click', function (e) {
            e.preventDefault()
            window.location.replace(window.location.origin + '/admin/products')
        })
    }

    init()
    {
        Dropzone.autoDiscover = false;
        const toThis = this
        $(document).ready(function () {
            const myDropzone = '.dropzone'
            if ($('.dropzone').length) {
                return toThis.initDropzone(myDropzone)
            }
        })
    }
}

const dropzone = new MyDropzone()
dropzone.init()
