$(document).ready(function () {
    function slickCarousel()
    {
        $('.carousel-news-product').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button"></button>',
            nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button"></button>'
        });
    }

    if ($(window).width() > 650) {
        slickCarousel()
    }

    $(window).on('resize', function (e) {
        const width = e.currentTarget.innerWidth
        if (width <= 650) {
            $('.carousel-news-product').slick('unslick')
        } else {
            slickCarousel()
        }
    })

})
