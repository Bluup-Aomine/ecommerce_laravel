/**
 * @property {HTMLDivElement} search
 */
export default class SearchWidget {
    constructor() {
        this.search = document.querySelector('.element-search-form-toggle')
        this.init()
    }

    init() {
        this.search.addEventListener('click', function (e) {
            e.preventDefault()

            const elementSearch = document.querySelector('.element-search-container')
            elementSearch.classList.add('element-search-full-screen')
        })

        document.querySelector('#close-search')
            .addEventListener('click', function (e) {
                e.preventDefault()

                const elementSearch = document.querySelector('.element-search-container')
                elementSearch.classList.remove('element-search-full-screen')
            })
    }
}

const searchWidget = new SearchWidget()
searchWidget.init()
