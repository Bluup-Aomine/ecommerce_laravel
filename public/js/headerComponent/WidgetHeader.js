/**
 * @property {HTMLDivElement} search
 */
export default class WidgetHeader {
    constructor() {
        this.widgets = document.querySelectorAll('.element-d-none')
        this.init()
    }

    init() {
        setTimeout(() => this.removeDisplayNone(), 200)
    }

    removeDisplayNone()
    {
        this.widgets.forEach(function (item) {
            item.classList.remove('d-none')
        })
    }
}

const widget = new WidgetHeader()
widget.init()
