/**
 * @property {HTMLSpanElement} buttonMenu
 * @property {HTMLSpanElement} buttonText
 * @property {HTMLElement} aside
 */
export default class HeaderComponent {
    constructor() {
        this.buttonMenu = document.querySelector('.button-menu')
        this.buttonMenuMobile = document.querySelector('.button-menu-mobile')
        this.buttonText = document.querySelector('.button-text')
        this.aside = document.querySelector('.canvas')
        this.init()
    }

    init() {
        addEventListener('mousemove', function (e) {
            e.preventDefault()

            const aside = document.querySelector('.canvas')
            aside.classList.remove('d-none')
        })
        this.buttonMenu.addEventListener('click', this.clickButtonMenu.bind(this))
        this.buttonMenuMobile.addEventListener('click', this.clickButtonMenu.bind(this))
        this.buttonText.addEventListener('click', this.clickRemoveMenu.bind(this))
    }

    /**
     * @param e
     */
    clickButtonMenu(e) {
        e.preventDefault()

        const canvasBg = document.querySelector('.canvas-bg')
        this.openMenu(this.aside, 'is-closed', 'is-open')
        this.openMenu(canvasBg, 'is-closed', 'is-open')
    }

    /**
     * @param e
     */
    clickRemoveMenu(e) {
        e.preventDefault()

        const canvasBg = document.querySelector('.canvas-bg')
        this.closedMenu(canvasBg, 'is-closed', 'is-open')
        this.closedMenu(this.aside, 'is-closed', 'is-open')
    }

    /**
     *
     * @param {Element} element
     * @param {string} closedClass
     * @param {string} openClass
     */
    openMenu(element, closedClass, openClass) {
        element.classList.remove(closedClass)
        element.classList.add(openClass)
    }

    /**
     *
     * @param {Element} element
     * @param {string} closedClass
     * @param {string} openClass
     */
    closedMenu(element, closedClass, openClass) {
        element.classList.remove(openClass)
        element.classList.add(closedClass)
    }
}

let header = new HeaderComponent()
header.init()
