/**
 *  @property {HTMLDivElement} urlLoad
 *  @property {HTMLUListElement} listProductsCart
 *  @property {HTMLParagraphElement} totalProductsCart
 *  @property {HTMLSpanElement} countQuantity
 */
export default class LoadProductsBasket {
    constructor() {
        this.urlLoad = document.querySelector("#url-load-products")
        this.listProductsCart = document.querySelector('.products-list-cart')
        this.totalProductsCart = document.querySelector('#total-products-cart')
        this.countQuantity = document.querySelector('#count-quantity-products-header')
    }

    ajaxURl()
    {
        const url = this.urlLoad.getAttribute('value')
        if (url.indexOf('product/basket/load') === -1) {
            return false
        }
        return fetch(url, {
            method: 'GET',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
    }

    async init()
    {
        const request = await this.ajaxURl()
        if (!request) {
            return null
        }
        if (request.status === 200 || request.status === 302) {
            const response = await request.json()
            if (response.success) {
                response.data.products.forEach((item) => {
                    this.listProductsCart.innerHTML += item
                })
                this.totalProductsCart.innerHTML = response.data.totalPrice
                this.countQuantity.innerHTML = response.data.count
            }
        }
    }
}

const loadProducts = new LoadProductsBasket()
loadProducts.init()
