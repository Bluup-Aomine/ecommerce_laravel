import NotificationElement from "./element/NotificationElement.js";

/**
 * @property {HTMLFormElement|HTMLLinkElement} addCart
 * @property {HTMLInputElement} addCartProductID
 * @property {HTMLInputElement} quantity
 * @property {HTMLDivElement} totalProductsCart
 * @property {NotificationElement} notification
 * @property {HTMLUListElement} listProductsCart
 * @property {HTMLLIElement} features
 */
export default class AddCartProduct {
    constructor() {
        this.addCart = document.querySelector('#add-to-cart')
        this.addCartProductID = document.querySelector('#add-card-product-id')
        this.quantity = document.querySelector('#quantity-product')
        this.totalProductsCart = document.querySelector('#total-products-cart')
        this.notification = new NotificationElement()
        this.listProductsCart = document.querySelector('.products-list-cart')
        this.features = document.querySelectorAll('.variable-item')
    }

    /**
     * @param {string} productID
     * @param {string} quantity
     * @param {boolean} wishlist
     * @param {Array} features
     * @return FormData
     */
    setFormData(productID, quantity, wishlist = false, features)
    {
        const formData = new FormData()
        formData.append('productID', JSON.stringify(productID))
        formData.append('quantity', JSON.stringify(quantity))
        formData.append('features', JSON.stringify(features))
        if (wishlist) {
            formData.append('wishlist', 'true')
        }
        return formData
    }

    /**
     * @param {Event} e
     * @param {Boolean} wishlist
     */
    async addProduct(e, wishlist = false)
    {
        e.preventDefault()

        const productID = this.addCartProductID.getAttribute('data-product-id')
        const quantity = wishlist ? 1 : this.quantity.value

        const getFeatures = this.getFeatures()
        const formData = this.setFormData(productID, quantity, false, getFeatures)
        const url = this.addCart.getAttribute(wishlist ? 'href' : 'action')
        const response = await this.ajaxURL(url, formData, 'POST')

        if (response.status === 200 || response.status === 302) {
            const json = await response.json()
            if (json.success) {
                this.responseAction(json, productID)
                this.notification.notification(json.data.block)
            } else {
                this.notification.notification(json.render)
            }
        }
    }

    /**
     * @param json
     * @param {string} productID
     */
    responseAction(json, productID)
    {
        let countElement = document.querySelector('#count-quantity-products-header')
        const productTotalPrice = json.data.priceTotal.toString()
        const spanProduct = json.data.spanProduct

        countElement.innerHTML = json.data.count

        const quantityElement = document.querySelector('#quantity-product-span-' + productID)
        if (quantityElement !== null) {
            quantityElement.innerHTML = spanProduct
        } else {
            this.listProductsCart.insertAdjacentHTML('beforeend', json.data.product)
        }
        this.totalProductsCart.innerHTML = '<strong>Total:</strong><span class="price-amount"><span class="price-symbol"> $</span>' + productTotalPrice + '</span>'
    }

    /**
     * @param {string} url
     * @param {FormData} formData
     * @param {string} method
     * @return {Promise<Response>}
     */
    async ajaxURL(url, formData, method = 'POST')
    {
        return fetch(url, {
            method: method,
            body: formData,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        })
    }

    /**
     * @param {Boolean} wishlist
     */
    init(wishlist = false)
    {
        if (this.addCart) {
            this.addCart.addEventListener(wishlist ? 'click' : 'submit', this.addProduct.bind(this))
        }
    }

    /**
     * @return {[]}
     */
    getFeatures() {
        let data = []

        this.features.forEach((feature) => {
            if ($(feature).hasClass('selected')) {
                data.push(feature.getAttribute('data-title'))
            }
        })
        return data
    }
}

const addCartProduct = new AddCartProduct()
addCartProduct.init()
