import FilterComponent from "./FilterComponent.js";

/**
 * @property {HTMLDivElement} sliderPrice
 * @property {HTMLFormElement} formPrice
 * @property {HTMLSpanElement} fromPrice
 * @property {HTMLSpanElement} toPrice
 * @property {FilterComponent} filterComponent
 */
export default class Slider {
    constructor() {
        this.sliderPrice = document.getElementById('ui-slider')
        this.formPrice = document.getElementById('price-form-filter')
        this.fromPrice = document.querySelector('.from-price')
        this.toPrice = document.querySelector('.to-price')
        this.filterComponent = new FilterComponent()
    }

    /**
     * @param {Array} handle
     */
    async slide(handle)
    {
        const loading = this.filterComponent.initLoading()

        const min = parseInt(handle[0])
        const max = parseInt(handle[1])

        const action = this.formPrice.getAttribute('action')
        const url = action.split('/10')
        const newUrl = (url[0] + '/' + min + '/' + max)

        this.replaceValues(min, max)
        const response = await this.ajaxUrl(newUrl, 'GET')
        return this.filterComponent.responseJson(response, loading)
    }

    /**
     * @param {string} url
     * @param {string} method
     */
    async ajaxUrl(url, method= 'GET')
    {
        return fetch(url, {
            method: method,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
        })
    }

    init()
    {
        const $this = this
        $('document').ready(function () {
            if ($this.sliderPrice) {
                const slider = noUiSlider.create($this.sliderPrice, {
                    start: [50, 10000],
                    range: {
                        'min': 10,
                        'max': 15000
                    },
                    connect: true
                })
                slider.on('slide', $this.slide.bind($this))
            }
        })
    }

    replaceValues(min, max) {
        this.fromPrice.innerHTML = '$' + min
        this.toPrice.innerHTML = '$' + max
    }
}

let slider = new Slider()
slider.init()
