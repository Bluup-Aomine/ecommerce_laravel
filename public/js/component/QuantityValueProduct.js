/**
 * @property {HTMLInputElement} quantity
 * @property {HTMLInputElement} decrement
 * @property {HTMLInputElement} increment
 */
export default class QuantityValueProduct {
    constructor() {
        this.quantity = document.querySelector('#quantity-product')
        this.decrement = document.querySelector('.min-quantity-products')
        this.increment = document.querySelector('.plus-quantity-products')
    }

    actionDecrement(e)
    {
        e.preventDefault()

        const valueQuantity = parseInt(this.quantity.getAttribute('value'))
        this.quantity.setAttribute('value', valueQuantity !== 0 ? (valueQuantity - 1) : 0)
    }

    actionIncrement(e)
    {
        e.preventDefault()

        const valueQuantity = parseInt(this.quantity.getAttribute('value'))
        this.quantity.setAttribute('value', (valueQuantity + 1))
    }

    init()
    {
        this.decrement.addEventListener('click', this.actionDecrement.bind(this))
        this.increment.addEventListener('click', this.actionIncrement.bind(this))
    }
}
const actionQuantity = new QuantityValueProduct()
actionQuantity.init()
