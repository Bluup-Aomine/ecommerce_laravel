/**
 * @property {HTMLDivElement} items
 */
export default class HoverElement {
    constructor() {
        this.items = document.querySelectorAll('.products-collection-visual')
    }

    /**
     * @param {HTMLDivElement} item
     * @param {Event} e
     */
    addElementHover(item, e)
    {
        e.preventDefault()

        const element = item.querySelector('.products-collection-details')
        element.classList.add('products-collection-details-active')
    }

    /**
     * @param {HTMLDivElement} item
     * @param {Event} e
     */
    removeElementHover(item, e)
    {
        e.preventDefault()

        const element = item.querySelector('.products-collection-details')
        element.classList.remove('products-collection-details-active')
    }

    init()
    {
        this.items.forEach((item) => {
            item.addEventListener('mouseover', this.addElementHover.bind(this, item))
            item.addEventListener('mouseout', this.removeElementHover.bind(this, item))
        })
    }
}

const element = new HoverElement()
element.init()


