import AddCartProduct from "../AddCartProduct.js";
import ModalVisualProduct from "./ModalVisualProduct.js";

export default class AddCartProductCollection extends AddCartProduct
{
    constructor() {
        super();

        this.btns = document.querySelectorAll('#add-to-cart')
    }

    /**
     * @return {[]}
     */
    getFeaturesCollection()
    {
        let data = []

        const linkTab = document.querySelectorAll('.colors-list-products-item')
        linkTab.forEach((feature) => {
            if ($(feature).hasClass('active')) {
                data.push(feature.getAttribute('data-title'))
            }
        })
        return data
    }

    /**
     * @param {Event} e
     */
    async action(e)
    {
        e.preventDefault()

        const productID = this.addCartProductID.getAttribute('data-product-id')
        const quantity = '1'

        const getFeatures = this.getFeaturesCollection()
        const formData = this.setFormData(productID, quantity, false, getFeatures)
        const url = this.addCart.getAttribute('href')
        const response = await this.ajaxURL(url, formData, 'POST')
        if (response.status === 200 || response.status === 302) {
            const elementModal = document.querySelector('#visual-product-' + productID)
            const json = await response.json()
            if (json.success === false) {
                const message = json.render
                elementModal.classList.add('d-none')
                return this.notification.notification(message)
            }

            if (json.success) {
                (new ModalVisualProduct()).close(productID)
                this.responseAction(json, productID)
                this.notification.notification(json.data.block)
            }
        }
    }

    init(wishlist = false) {
        if (this.btns.length !== 0) {
            this.btns.forEach((btn) => {
                btn.addEventListener('click', this.action.bind(this))
            })
        }
    }
}

const addCart = new AddCartProductCollection()
addCart.init(false)
