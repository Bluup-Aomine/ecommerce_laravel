import Ajax from "../../app/src/Ajax.js";
/**
 * @property {HTMLLinkElement} btnsModal
 * @property {HTMLUListElement} sizesList
 * @property {HTMLUListElement} colorsList
 * @property {HTMLDivElement} modal
 * @property {Ajax} ajax
 */
export default class ModalVisualProduct {
    constructor() {
        this.btnsModal = document.querySelectorAll('#btn-visual-modal')
        this.sizesList = document.querySelector('#sizes-list-collections')
        this.colorsList = document.querySelector('#colors-list-collections')
        this.modals = document.querySelectorAll('#modal-back-product')
        this.ajax = new Ajax()
    }

    /**
     * @param {string} productID
     */
    close(productID)
    {
        const elementModal = document.querySelector('#visual-product-' + productID)
        elementModal.classList.add('d-none')
    }

    /**
     * @param e
     */
    closeModal(e)
    {
        e.preventDefault()
        const parent = e.target.parentNode
        if (!$(parent).hasClass('d-none')) {
            $(parent).addClass('d-none')
        }
    }

    /**
     * @param {Event} e
     */
    async openModal(e)
    {
        e.preventDefault()

        const productID = e.target.getAttribute('data-product-id')
        const elementModal = document.querySelector('#visual-product-' + productID)
        elementModal.classList.remove('d-none')

        const formData = new FormData()
        formData.append('productID', productID)
        const request = await this.ajax.ajaxURL('/api/collections/modal', 'POST', formData)
        if (request.status === 200 || request.status === 302) {
            const response = await request.json()
            if (response.success) {
                this.colorsList.innerHTML = ''
                this.sizesList.innerHTML = ''

                response.colors.forEach((li) => {
                    this.colorsList.innerHTML += li
                })
                response.sizes.forEach((li) => {
                    this.sizesList.innerHTML += li
                })
                this.chooseLink()
            }
        }
    }

    chooseLink()
    {
        const colorsLink = document.querySelectorAll('.colors-list-products-links')
        colorsLink.forEach((link) => {
            link.addEventListener('click' , function (e) {
                e.preventDefault()

                const parent = e.target.parentNode
                $(parent).addClass('active')
                $(parent).siblings().removeClass('active')
            })
        })
    }

    init()
    {
        if (this.btnsModal.length !== 0) {
            this.modals.forEach((element) => {
                element.addEventListener('click', this.closeModal.bind(this))
            })
            this.btnsModal.forEach((element) => {
                element.addEventListener('click', this.openModal.bind(this))
            })
        }
    }
}

const modal = new ModalVisualProduct()
modal.init()
