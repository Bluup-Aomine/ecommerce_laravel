import createElement from "./element/createElement.js";

/**
 * @property {HTMLLinkElement} filters
 * @property {HTMLLinkElement} filtersPage
 * @property {HTMLLinkElement} filterClear
 * @property {HTMLSelectElement} filterClear
 * @property {HTMLUListElement} blockProducts
 * @property {HTMLUListElement} filtersList
 * @property {HTMLBodyElement} body
 */
export default class FilterComponent {
    constructor() {
        this.filters = document.querySelectorAll('.url-filter')
        this.filtersPage = document.querySelectorAll('.filter-page')
        this.filterClear = document.querySelector('#clear-filter')
        this.filterOrderBy = document.querySelector('.orderby')
        this.blockProducts = document.querySelector('#block-products')
        this.filtersList = document.querySelector('#filters-list')
        this.body = document.querySelector('body')
    }

    /**
     * @return {HTMLDivElement}
     */
    initLoading()
    {
        const CreateElement = new createElement()
        const loading = CreateElement.loadingCreate()
        this.body.append(loading)
        return loading
    }

    /**
     * @param {Event} e
     */
    async filterElement(e) {
        e.preventDefault()

        const loading = this.initLoading()
        const href = e.target.getAttribute('href')

        let url = href === null ? e.target.parentNode : href;
        if (url instanceof HTMLFormElement) {
            const action = url.getAttribute('action')
            const children = this.filterOrderBy.childNodes
            url = this.valueFormSelected(action, children, action)
        }
        const response = await this.ajaxUrl(url, 'GET')

        return this.responseJson(response, loading)
    }

    /**
     * @param {string} action
     * @param {ChildNode} childs
     * @param {string} urlDefault
     * @return {string}
     */
    valueFormSelected(action, childs, urlDefault)
    {
        let value = ''
        childs.forEach((element) => {
            if (element.selected && element.selected === true) {
                if (element.value === 'clear') {
                    value += 'clear'
                } else {
                    value += element.value
                }}
        })
        return value !== 'clear' ? (action.split('/clear')[0] + '/selected/' + value) : urlDefault
    }

    /**
     * @param response
     * @param {HTMLDivElement} loading
     * @return {Promise<void>}
     */
    async responseJson(response, loading)
    {
        if (response.status === 200 || response.status === 302) {
            const json = await response.json()
            if (json.success) {
                this.blockProducts.innerHTML = ''
                this.InnerHtmlProductAndFilter(json)
                loading.remove()
            }
        }
    }

    /**
     * @param json
     * @constructor
     */
    InnerHtmlProductAndFilter(json)
    {
        const products = json.data.products
        const itemsFiltersPrice = json.data.itemsFiltersPrice

        const itemsFilters = json.data.itemsFilters
        if (itemsFilters !== undefined) {
            this.filtersList.innerHTML += itemsFilters
        }
        this.HtmlRender(products, this.blockProducts)
        this.HtmlRender(itemsFiltersPrice, this.filtersList, true)
    }

    /**
     * @param {Array} items
     * @param {HTMLElement} elementNeedle
     * @param {Boolean} reinitialized
     * @constructor
     */
    HtmlRender(items, elementNeedle, reinitialized= false)
    {
        if (items === undefined) {
            return null
        }
        if (reinitialized) {
            elementNeedle.innerText = ''
        }
        return items.forEach((item) => {
            elementNeedle.innerHTML += item
        })
    }

    /**
     * @param {string} url
     * @param {string} method
     */
    async ajaxUrl(url, method)
    {
        return fetch(url, {
            method: method,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
        })
    }

    init()
    {
        if (this.filters && this.filters.length !== 0) {
            this.filterOrderBy.addEventListener('click', this.filterElement.bind(this))
            this.filterClear.addEventListener('click', this.filterElement.bind(this))
            this.filtersPage.forEach((filter) => {
                filter.addEventListener('click', this.filterElement.bind(this))
            })
            this.filters.forEach((filter) => {
                filter.addEventListener('click', this.filterElement.bind(this))
            })
        }
    }
}

const filterComponent = new FilterComponent()
filterComponent.init()
