export default class CreateElement {
    constructor() {
    }

    /**
     * @return {HTMLDivElement}
     */
    loadingCreate()
    {
        const loading = document.createElement('div')
        loading.setAttribute('id', 'loading-page')
        return loading
    }
}
