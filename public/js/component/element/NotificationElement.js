export default class NotificationElement {
    constructor() {
        this.messageJson = document.querySelector('#message-json')
    }

    /**
     * @param {string} html
     * @return {*}
     */
    notification(html)
    {
        return this.messageJson.innerHTML = html
    }
}
