import Ajax from "../../app/src/Ajax.js";

/**
 * @property {HTMLLinkElement} removeBasketProduct
 * @property {HTMLDivElement} divMessage
 * @property {Ajax} ajax
 */
export default class DeleteBasketProduct {
    constructor() {
        this.removeBasketProduct = document.querySelectorAll('#remove-basket-product')
        this.divMessage = document.querySelector('#message-json')
        this.ajax = new Ajax()
    }

    /**
     * @param {Event} e
     */
    async remove(e)
    {
        e.preventDefault()

        const url = e.target.href
        const basketProductID = url.split('delete/')[1]
        const response = await this.ajaxURL(url, 'DELETE')
        if (response.status === 200 || response.status === 302) {
            const json = await response.json()
            if (json.success) {
                const element = document.querySelector('#basket-product-' + basketProductID)
                if (element) {
                    element.remove()
                    return this.divMessage.innerHTML = json.data.block
                }
            }
        }
    }

    /**
     * @param {string} url
     * @param {string} method
     * @return {Promise<Response>}
     */
    async ajaxURL(url, method)
    {
        return this.ajax.ajaxURL(url, method)
    }

    init()
    {
        if (this.removeBasketProduct) {
            this.removeBasketProduct.forEach((item) => {
                item.addEventListener('click', this.remove.bind(this))
            })
        }
    }
}

const deleteBasketProduct = new DeleteBasketProduct()
deleteBasketProduct.init()
