import Ajax from "../../app/src/Ajax.js";

/**
 * @property {HTMLDivElement} hiddenForm
 * @property {HTMLLinkElement} hiddenForm
 * @property {HTMLLinkElement} btnEditWishlist
 * @property {HTMLLinkElement} btnRemove
 * @property {Ajax} ajax
 * @property {HTMLDivElement} messageJSON
 */
export default class Wishlist
{
    constructor() {
        this.hiddenForm = document.querySelector('.hidden-form-title')
        this.btnHidden = document.querySelector('#click-hidden-form')
        this.btnEditWishlist = document.querySelector('#edit-form-wishlist')
        this.btnRemove = document.querySelectorAll('#remove-wishlist-product')
        this.ajax = new Ajax()
        this.messageJSON = document.querySelector('#message-json')
    }

    /**
     * @param {Event} e
     */
    hidden(e)
    {
        e.preventDefault()

        this.hiddenForm.style.display = 'none'
    }

    /**
     * @param {Event} e
     */
    show(e)
    {
        e.preventDefault()

        this.hiddenForm.style.display = 'block'
    }

    /**
     * @param {Event} e
     * @returns {Promise<void>}
     */
    async delete(e)
    {
        e.preventDefault()

        const url = e.target.href
        const wishlistID = url.split('delete/')[1] ? parseInt(url.split('delete/')[1]) : null
        const request = await this.ajax.ajaxURL(url, 'DELETE')
        if (request.status === 200 || request.status === 302) {
            const response = await request.json()
            if (response.success) {
                const trWishlist = document.querySelector('#tr-wishlist-' + wishlistID)
                if (trWishlist) {
                    trWishlist.remove()
                    this.messageJSON.innerHTML = response.data.messageJSON
                }
            }
        }
    }

    init()
    {
        this.btnHidden ? this.btnHidden.addEventListener('click', this.hidden.bind(this)) : null
        this.btnEditWishlist ? this.btnEditWishlist.addEventListener('click', this.show.bind(this)) : null
        if (this.btnRemove) {
            this.btnRemove.forEach((item) => {
                item.addEventListener('click', this.delete.bind(this))
            })
        }
    }sss
}

const wishlist = new Wishlist()
wishlist.init()
