import AddCartProduct from "../AddCartProduct.js";

export default class AddCartProductWishlist extends AddCartProduct
{
    constructor() {
        super();
    }

    init(wishlist = true) {
        super.init(wishlist);
    }

    async addProduct(e, wishlist = true) {
        await super.addProduct(e, wishlist);
    }

    setFormData(productID, quantity, wishlist = true, features) {
        return super.setFormData(productID, quantity, wishlist, features);
    }

    async ajaxURL(url, formData, method = 'POST') {
        return super.ajaxURL(url, formData, method);
    }
}
if (window.location.pathname === '/products/wishlist') {
    const addCart = new AddCartProductWishlist()
    addCart.init()
}
