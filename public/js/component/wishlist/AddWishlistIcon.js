import AddWishlist from "./AddWishlist.js";
import NotificationElement from "../element/NotificationElement.js";

export default class AddWishlistIcon extends AddWishlist{
    constructor() {
        super();
        this.btnsAddIcon = document.querySelectorAll('#btn-add-wishlist')
        this.notification = new NotificationElement()
    }

    /**
     * @param {Boolean} pageWishlist
     * @return {HTMLDivElement}
     */
    initLoading(pageWishlist) {
        return super.initLoading(pageWishlist);
    }

    /**
     * @param {HTMLLinkElement} btn
     * @param {Response} json
     * @return NotificationElement
     */
    successHTMLIcon(btn, json)
    {
        const wishlistByProduct = document.querySelector('#wishlist-index-' + json.data.productID)
        btn.classList.add('d-none')
        wishlistByProduct.classList.remove('d-none')

        return this.notification.notification(json.data.messageJSON)
    }

    /**
     * @param {HTMLLinkElement} btn
     * @param {Event} e
     * @return {Promise<void>}
     */
    async addIcon(btn, e) {
        e.preventDefault()
        const loading = this.initLoading(false)

        const url = btn.getAttribute('href')
        const formData = this.setFormData(false, btn)
        const response = await this.ajaxUrl(url, formData)
        if (response.status === 200 || response.status === 302) {
            loading.remove()
            const json = await response.json()
            if (json.success) {
                return this.successHTMLIcon(btn, json)
            }
        }
    }

    /**
     * @param {string} url
     * @param {FormData} FormData
     * @param {string} method
     * @return {Promise<Response>}
     */
    async ajaxUrl(url, FormData, method = 'POST') {
        return super.ajaxUrl(url, FormData, method);
    }

    init() {
        if (this.btnsAddIcon.length !== 0) {
            this.btnsAddIcon.forEach((btn) => {
                btn.addEventListener('click', this.addIcon.bind(this, btn))
            })
        }
    }
}

const addWishlistIcon = new AddWishlistIcon()
addWishlistIcon.init()
