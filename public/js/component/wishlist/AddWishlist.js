import createElement from "../element/createElement.js";

/**
 * @property {HTMLLinkElement} wishlist
 * @property {HTMLLinkElement} wishlistBrowse
 * @property {HTMLBodyElement} body
 * @property {HTMLDivElement} messageJSON
 */
export default class AddWishlist {
    constructor() {
        this.wishlist = document.querySelector('.add-wishlist-action')
        this.wishlistBrowse = document.querySelector('.add-wishlist-browse')
        this.body = document.querySelector('body')
        this.messageJSON = document.querySelector('#message-json')
    }

    /**
     * @param {Boolean} pageWishlist
     * @return {HTMLDivElement}
     */
    initLoading(pageWishlist = true) {
        const CreateElement = new createElement()
        const loading = CreateElement.loadingCreate();
        if (pageWishlist) {
            this.wishlist.append(loading)
        } else {
            this.body.append(loading)
        }
        return loading
    }

    /**
     * @param {Boolean} pageWishlist
     * @param {HTMLLinkElement|null} btn
     * @return FormData
     */
    setFormData(pageWishlist = true, btn = null) {
        const productID = pageWishlist ? this.wishlist.getAttribute('data-product-id') : btn.getAttribute('data-product-id')
        let formData = new FormData()
        formData.append('productID', JSON.stringify(productID))
        return formData
    }

    successHtml() {
        let span = document.createElement('span')
        span.innerText = 'Product Added '

        this.wishlist.before(span)
        this.wishlistBrowse.classList.remove('d-none')
        this.wishlist.classList.add('d-none')
    }

    /**
     * @param {Event} e
     * @return {Promise<void>}
     */
    async add(e) {
        e.preventDefault()
        const loading = this.initLoading()

        const url = this.wishlist.parentNode.getAttribute('action')
        const formData = this.setFormData()
        const response = await this.ajaxUrl(url, formData)
        if (response.status === 200 || response.status === 302) {
            loading.remove()
            const json = await response.json()
            if (json.success) {
                return this.successHtml()
            } else {
                this.messageJSON.innerHTML = json.message
            }
        }
    }

    /**
     * @param {string} url
     * @param {FormData} FormData
     * @param {string} method
     * @return {Promise<Response>}
     */
    async ajaxUrl(url, FormData, method = 'POST') {
        return fetch(url, {
            method: method,
            body: FormData,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        })
    }

    init() {
        if (this.wishlist) {
            this.wishlist.addEventListener('click', this.add.bind(this))
        }
    }
}

const addWishlist = new AddWishlist()
addWishlist.init()
