import Ajax from "../../app/src/Ajax.js";
import NotificationElement from "../element/NotificationElement.js";
import MessageError from "./MessageError.js";
import CreateElement from "../element/createElement.js";

/**
 * @property {NotificationElement} notif
 * @property {Ajax} ajax
 * @property {MessageError} error
 * @property {CreateElement} $createElement
 * @property {HTMLFormElement} formCart
 * @property {HTMLFormElement} formInformations
 * @property {HTMLDivElement} loadingElement
 * @property {HTMLElement|null} loading
 */
export default class Card {
    constructor() {
        this.notif = new NotificationElement()
        this.ajax = new Ajax()
        this.error = new MessageError()
        this.$createElement = new CreateElement()

        this.formCart = document.getElementById('payment-form-cart')
        this.formInformations = document.getElementById('payment-form-customer')
        this.loadingElement = document.getElementById('loading-element')
        this.loading = null
    }

    /**
     * @param payment
     * @return {FormData}
     */
    setFormDataCheckout(payment = null)
    {
        const form = this.formInformations
        const formData = new FormData()

        formData.append('name', form.querySelector('#name').value)
        formData.append('email', form.querySelector('#email').value)
        formData.append('phone', form.querySelector('#phone').value)
        formData.append('address', form.querySelector('#address').value)
        formData.append('country', form.querySelector('#country').value)
        formData.append('order_notes', form.querySelector('#order_notes').value)
        if (payment) {
            formData.append('paymentIntent', JSON.stringify(payment))
        }

        return formData
    }

    /**
     * @param {string} payment_id
     * @param {object} payment_method
     * @return {Promise<void>}
     * @constructor
     */
    async UpdateAmountPayment(payment_id, payment_method)
    {
        let formData = new FormData()
        formData.append('payment_id', payment_id)
        formData.append('payment_method', JSON.stringify(payment_method))
        formData.append('country', this.formInformations.querySelector('#country').value)
        const request = await this.ajax.ajaxURL('/api/orders/payment/update', 'POST', formData)
        if (request.status === 302 || request.status === 200) {
            let response = await request.json()
            return response.success
        }
    }

    /**
     * @return {string} payment_id
     * @return {Promise<boolean>}
     */
    async responseCustomerSuccess(payment_id)
    {
        const form = this.formInformations
        const action = form.action
        const formData = this.setFormDataCheckout()
        formData.append('payment_id', payment_id)

        const request = await this.ajax.ajaxURL(action, 'POST', formData)
        if (request.status === 200 || request.status === 302) {
            const response = await request.json()
            if (!response.success) {
                return this.messageError(response)
            }
            this.loading.remove()
            this.notif.notification(response.message)
            return true
        }
    }

    /**
     * @param result
     * @return object
     */
    async responseCardSuccess(result)
    {
        const payment = result.paymentIntent
        const form = this.formCart
        const action = form.action
        const formData = this.setFormDataCheckout(payment)

        const request = await this.ajax.ajaxURL(action, 'POST', formData)
        if (request.status === 200 || request.status === 302) {
            const response = await request.json()
            if (!response.success) {
                return this.messageError(response)
            }
            return response
        }
    }


    sendLoading()
    {
        this.loading = this.$createElement.loadingCreate()
        this.loadingElement.append(this.loading)
    }

    /**
     * @param {Response} response
     * @return {boolean}
     */
    messageError(response) {
        this.error.renderMessage(response.messages)
        return false
    }

    /**
     * @return {boolean}
     */
    errorMessage() {
        this.error.renderMessageError('Une erreur est survenu lors de la transaction, veuillez réessayer ?!')
        return false
    }
}
