/**
 * @property {HTMLInputElement} cartRatio
 * @property {HTMLInputElement} paypalRatio
 * @property {HTMLDivElement} cartContent
 * @property {HTMLDivElement} paypalContent
 */
export default class ChangeMethodPayment {
    constructor() {
        this.cartRatio = document.querySelector('#input-cart-ratio')
        this.paypalRatio = document.querySelector('#input-paypal-ratio')

        this.cartContent = document.querySelector('#cart-content')
        this.paypalContent = document.querySelector('#paypal-content')
    }

    /**
     * @param {Event} e
     */
    change(e)
    {
        const target = e.target
        const id = target.id
        if (id === 'input-paypal-ratio') {
            this.cartContent.classList.add('d-none')
            this.paypalContent.classList.remove('d-none')
        } else {
            this.cartContent.classList.remove('d-none')
            this.paypalContent.classList.add('d-none')
        }
    }

    init()
    {
        this.cartRatio.addEventListener('click', this.change.bind(this))
        this.paypalRatio.addEventListener('click', this.change.bind(this))
    }
}

const changeMP = new ChangeMethodPayment()
changeMP.init()
