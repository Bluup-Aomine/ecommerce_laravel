/**
 * @property {HTMLLinkElement} links
 */
export default class tabs {
    constructor() {
        this.links = document.querySelectorAll('.checkout-link')
    }

    /**
     * @param {Event} e
     */
    showPanel(e) {
        e.preventDefault()

        const target = e.target
        const parent = target.parentNode
        const id = target.getAttribute('id') ? target.getAttribute('id') : parent.getAttribute('id')

        const element = document.querySelector('#' + id)
        const elementTab = document.querySelector('#tab-checkout-' + id)
        if (element) {
            $(element).addClass('active')
            $(element).siblings().removeClass('active')
            $(elementTab).addClass('show active')
            $(elementTab).siblings().removeClass('show active')
        }
    }

    init() {
        if (this.links.length !== 0) {
            this.links.forEach((item) => {
                item.addEventListener('click', this.showPanel.bind(this))
            })
        }
    }
}

const tab = new tabs()
tab.init()
