export default class MessageError {
    constructor() {
        this.messageListError = document.querySelector('#message-danger-json-list')
        this.messageError = document.querySelector('#message-danger-json')
    }

    /**
     * @param {string} message
     */
    renderMessageError(message)
    {
        let messageError = document.querySelector('#message-error')
        this.messageError.classList.remove('d-none')
        messageError.innerHTML = message
    }

    /**
     * @param {Array} messages
     */
    renderMessage(messages)
    {
        const element = this.messageListError.querySelector('#list-errors-forms')
        this.messageListError.classList.remove('d-none')
        element.innerHTML = '';

        const data = Object.values(messages)
        data.forEach((items) => {
            items.forEach((value) => {
                element.innerHTML += '<li>'+ value +'</li>'
            })
        })
    }
}
