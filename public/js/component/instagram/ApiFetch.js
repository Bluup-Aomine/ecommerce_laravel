import Ajax from "../../app/src/Ajax.js";

export default class ApiFetch {

    constructor() {
        this.ajax = new Ajax()
        this.elementsInstagram = document.querySelector('#instagram-elements')
    }

    async init() {
        const request = await this.ajax.ajaxURL('https://v1.nocodeapi.com/bluupa0mine/instagram/EcNtmUJWUUoCVcQC', 'GET')
        if (request.status === 200 || request.status === 302) {
            const response = await request.json()
            let countItems = response.data.length
            const data = countItems > 6  ? response.data.slice(countItems - 7, countItems - 1) : response.data

            data.forEach((item) => {
                let imgInstagram = this.imgInstagram(item.media_url, item.permalink)
                this.elementsInstagram.innerHTML += imgInstagram
            })
        }
    }

    /**
     * @param {string} image
     * @param {string} link
     * @returns {string}
     */
    imgInstagram(image, link) {
        return `<div class="instagram lg-size-18 lg-size-25 lg-size-33">
                   <a href="${link}" target="_blank" class="ratio-img-instagram" style="background-image: url('${image}')"></a>
                </div>`
    }
}

const apiFetch = new ApiFetch()
apiFetch.init()

