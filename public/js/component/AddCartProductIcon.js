import AddCartProduct from "./AddCartProduct.js";

/**
 * @property {HTMLLinkElement} btnsAddCartIcon
 */
export default class AddCartProductIcon extends AddCartProduct {
    constructor() {
        super();
        this.btnsAddCartIcon = document.querySelectorAll('#btnAddCartIcon')
    }

    /**
     * @param {HTMLLinkElement} btn
     * @param {Event} e
     * @return {Promise<void>}
     */
    async addProductIcon(btn, e) {
        e.preventDefault()

        const productID = btn.getAttribute('data-product-id')
        const quantity = '1'

        const formData = this.setFormData(productID, quantity, false , [])
        const url = btn.getAttribute('href')
        const response = await this.ajaxURL(url, formData, 'POST')

        if (response.status === 200 || response.status === 302) {
            const json = await response.json()
            if (json.success) {
                this.responseAction(json, productID)
                this.notification.notification(json.data.block)
            } else {
                this.notification.notification(json.render)
            }
        }
    }

    /**
     * @param {Boolean} wishlist
     */
    init(wishlist = false) {
        if (this.btnsAddCartIcon.length !== 0) {
            this.btnsAddCartIcon.forEach((btn) => {
                btn.addEventListener('click', this.addProductIcon.bind(this, btn))
            })
        }
    }
}

const addCartProductIcon = new AddCartProductIcon()
addCartProductIcon.init()
