/**
 * @property {HTMLDivElement} sliderImage
 */
export default class SwiperComponent {
    constructor() {
        this.sliderImage = document.querySelector('#slider-image')
    }

    init() {
        const $this = this
        $(document).ready(function () {
            if ($this.sliderImage) {
                new Swiper('#slider-image', {
                    slidesPerView: 1,
                    spaceBetween: 30,
                    loop: true,

                    // If we need pagination
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true
                    },

                    // Navigation arrows
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    }
                })
            }
        })
    }
}

const swipper = new SwiperComponent()
swipper.init()
