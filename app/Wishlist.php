<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class Wishlist extends Model
{
    protected $fillable = ['title', 'product_id', 'user_id'];

    public $timestamps = true;

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param int $userID
     * @return Wishlist
     */
    public function add(int $userID): Wishlist
    {
        $wishlist = Wishlist::findByUser((int)Auth::id())->first();
        if (!$wishlist) {
            $this->title = 'Your Wishlist title !!!';
            $this->user_id = (integer)$userID;
            $this->save();
            return $this;
        }
        return $wishlist;
    }

    /**
     * @param $query
     * @param int $userID
     * @return Builder|null
     */
    public function scopeFindByUser($query, int $userID): ?Builder
    {
        return $query->where('user_id', $userID);
    }
}
