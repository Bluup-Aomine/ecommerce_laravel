<?php

namespace App;

use App\src\helpers\Paypal\ApiPaypal;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;
use PayPal\Api\Payment;

class OrderPaypal extends Model
{
    public $timestamps = true;
    protected $guarded = [];
    protected $table = 'orders_paypal';

    /**
     * @return array
     */
    public static function countProducts(): array
    {
        $newData = [];
        foreach (OrderPaypal::all() as $order) {
            $products = $order['products'] ?: [];
            foreach ($products as $key => $product) {
                $newData[] = $product;
            }
        }
        return $newData;
    }

    /**
     * @param OrderPaypal $orderPaypal
     * @return string|null
     */
    public static function getName(OrderPaypal $orderPaypal): ?string
    {
        $apiContext = (new ApiPaypal())->api();
        $paymentID = $orderPaypal->getPaymentId();
        $payment = Payment::get($paymentID, $apiContext);

        return $payment->getPayer()->getPayerInfo()->getFirstName() ?: null;
    }

    /**
     * @return string|null
     */
    public function getPaymentId(): ?string
    {
        return $this->payment_id;
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param Payment $payment
     * @param CustomerPaypal $customer
     * @param string $country
     * @param string|null $other_notes
     * @param Basket|null $basket
     * @return OrderPaypal
     */
    public function setOrder(Payment $payment, CustomerPaypal $customer, string $country, ?string $other_notes = null, ?Basket $basket = null): OrderPaypal
    {
        $basket = Basket::FindByUser((integer)Auth::id())->first();
        $productsBasket = BasketsProduct::getProduct($basket) ?: [];

        // Decrement Quantity to product
        (new ColorSizeProduct())->decrementQuantity($basket, $productsBasket);
        return OrderPaypal::create([
            'payment_id' => $payment->getId(),
            'amount' => (float)$payment->getTransactions()[0]->amount->total,
            'products' => serialize($this->getProducts($productsBasket)),
            'user_id' => (int)Auth::id(),
            'other_notes' => $other_notes,
            'customer_id' => $customer->id,
            'tva' => $country === 'FR',
            'tracking_order' => 'en cours de traitement'
        ]);
    }

    /**
     * @param array $items
     * @return array
     */
    private function getProducts(array $items): array
    {
        $data = [];
        foreach ($items as $key => $value) {
            $colorSizeProduct = ColorSizeProduct::FindByFeatures([$value['size'], $value['color']], $value['product_id'])->first()->id;
            $data[$key]['name'] = $value['product'];
            $data[$key]['total'] = (integer)$value['total'];
            $data[$key]['price'] = (int)$value['price'];
            $data[$key]['quantity'] = $value['quantity'];
            $data[$key]['color_size_product'] = $colorSizeProduct;
            $data[$key]['image'] = $value['image'];
        }
        return $data;
    }

    /**
     * @param Payment $payment
     * @param string $phone
     * @return CustomerPaypal
     */
    public function setCustomer(Payment $payment, string $phone): CustomerPaypal
    {
        $payer = $payment->getPayer();

        return CustomerPaypal::create([
            'firstname' => $payer->getPayerInfo()->getFirstName(),
            'lastname' => $payer->getPayerInfo()->getLastName(),
            'email' => $payer->getPayerInfo()->getEmail(),
            'phone' => $phone,
            'address' => $payer->getPayerInfo()->getShippingAddress()->getLine1(),
            'total_purchase' =>  (int)$payment->getTransactions()[0]->amount->total,
            'user_id' => (int)Auth::id()
        ]);
    }

    /**
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value): string
    {
        return Carbon::parse($value)->format('d M, Y');
    }

    /**
     * @param $value
     * @return array
     */
    public function getProductsAttribute($value): array
    {
        return unserialize($value);
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return Carbon::make($this->created_at)->format('F j, Y');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLast($query)
    {
        return $query->orderBy('created_at', 'desc');
    }
}
