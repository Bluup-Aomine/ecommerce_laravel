<?php

namespace App;

use App\src\traits\WishlistsProductTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class WishlistsProduct extends Model
{
    protected $guarded = [];

    public $timestamps = true;

    use WishlistsProductTrait;

    /**
     * @return BelongsTo
     */
    public function wishlist(): BelongsTo
    {
        return $this->belongsTo(Wishlist::class);
    }

    /**
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @param Product $product
     * @param Wishlist $wishlist
     * @return bool|string
     */
    public function add(Product $product, Wishlist $wishlist)
    {
        if ($wishlist) {
            if (!is_null(WishlistsProduct::FindByProductAndWishlist((int)$product->id, (int)$wishlist->id)->first())) {
                return 'Vous avez déja ajouter ce produit à votre wishlist !!!';
            }
            $this->product_id = (int)$product->id;
            $this->wishlist_id = (int)$wishlist->id;
            return $this->save();
        }
        return 'Vous n\'avez acune wishlist crée !!!';
    }

    /**
     * Delete The product in the wishlist to User connected !!!
     *
     * @return void
     */
    public function remove(): void
    {
        $wishlist = self::getWishlistByUser();
        $wishlistProduct = WishlistsProduct::FindProductsByWishlist((int)$wishlist->id);
        if ($wishlistProduct) {
            $wishlistProduct->delete();
        }
    }

    /**
     * @param Product $product
     * @return WishlistsProduct|null
     */
    public static function getByProduct(Product $product): ?WishlistsProduct
    {
        $productID = (integer)$product->id ?: null;
        $wishlist = self::getWishlistByUser();
        if (is_null($wishlist)) {
            return null;
        }

        return WishlistsProduct::findByProductAndWishlist($productID, $wishlist ? (int)$wishlist->id : null)->first();
    }

    /**
     * @param $query
     * @param int $productID
     * @param int|null $wishListID
     * @return Builder|null
     */
    public function scopeFindByProductAndWishlist($query, int $productID, ?int $wishListID = null): ?Builder
    {
        if (is_null($wishListID)) {
            return null;
        }
        return $query->select('id')->where('product_id', $productID)->where('wishlist_id', $wishListID);
    }

    /**
     * @param $query
     * @param int $wishlistID
     * @return Builder|null
     */
    public function scopeFindProductsByWishlist($query, int $wishlistID): ?Builder
    {
        return $query->with('product')->where('wishlist_id', $wishlistID);
    }

    /**
     * @return Wishlist|null
     */
    private static function getWishlistByUser(): ?Wishlist
    {
        return Wishlist::findByUser((int)Auth::id())->first();
    }
}
