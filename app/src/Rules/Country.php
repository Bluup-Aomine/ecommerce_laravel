<?php

namespace App\src\Rules;

use LVR\CountryCode\Two;

class Country extends Two
{
    /**
     * @return string
     */
    public function message(): string
    {
        return 'Le champs country doit comporter un code ISO3166-A2';
    }
}
