<?php

namespace App\src\traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

trait PasswordAction
{

    /**
     * @param Request $request
     * @return bool
     */
    public function setPasswords(Request $request): bool
    {
        $passwordNew = $request->get('password_new') ?: null;
        $passwordNewConfirmation = $request->get('password_new_confirmation') ?: null;

        if ($passwordNew !== $passwordNewConfirmation) {
            return false;
        }
        return $this->updatePassword($passwordNew);
    }

    /**
     * @param string $password
     * @return bool
     */
    public function goodPassword(string $password): bool
    {
        return password_verify($password, $this->password);
    }

    /**
     * @param string $passwordNew
     * @return bool
     */
    private function updatePassword(string $passwordNew): bool
    {
        $this->password = Hash::make($passwordNew);
        return $this->save();
    }

}
