<?php

namespace App\src\traits\Controller;

use App\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

trait SearchProduct
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function search(Request $request): RedirectResponse
    {
        $search = $request->get('search') ?: '';
        if (empty($search)) {
            return redirect()->route('home')->with('error', 'Le champs search ne doit pas être vide si vous voulez trouver un produit !!!');
        }
        return redirect()->route('search.products', [$search]);
    }

    /**
     * @param string $search
     * @return Application|Factory|RedirectResponse|View
     */
    public function searchGET(string $search)
    {
        $productsHead = Product::with('images')->SearchProduct($search)->get();
        $productsBody = Product::with('images')->SearchProduct($search, 2, 3)->get();
        $productsFooter = Product::with('images')->SearchProduct($search, 5, 2)->get();

        return view('products.search', compact('productsHead', 'productsBody', 'productsFooter', 'search'));
    }
}
