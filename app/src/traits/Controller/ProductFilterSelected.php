<?php

namespace App\src\traits\Controller;

use App\Product;
use App\src\HTML\Products;
use Illuminate\Http\JsonResponse;

trait ProductFilterSelected
{
    public function popularity()
    {
        //
    }

    /**
     * @return JsonResponse
     */
    public function date(): JsonResponse
    {
        $products = Product::GetDateLast()->paginate(12);
        $itemsProducts = (new Products())->renderItem($products);

        return response()->json([
            'success' => true,
            'data' => [
                'products' => $itemsProducts,
            ]
        ], 302);
    }

    /**
     * @return JsonResponse
     */
    public function priceAsc(): JsonResponse
    {
        $products = Product::OrderByAsc('price')->paginate(12);
        $itemsProducts = (new Products())->renderItem($products);

        return response()->json([
            'success' => true,
            'data' => [
                'products' => $itemsProducts,
            ]
        ], 302);
    }

    /**
     * @return JsonResponse
     */
    public function priceDesc(): JsonResponse
    {
        $products = Product::OrderByDesc('price')->paginate(12);
        $itemsProducts = (new Products())->renderItem($products);

        return response()->json([
            'success' => true,
            'data' => [
                'products' => $itemsProducts,
            ]
        ], 302);
    }
}
