<?php

namespace App\src\traits\Controller;

use App\ColorSizeProduct;
use App\Product;
use App\src\HTML\Collections;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

trait ColorSizeApi
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function apiColorSize(Request $request): JsonResponse
    {
        $productID = $request->get('productID') ?: null;

        $product = Product::find((int)$productID);
        $colors = (new ColorSizeProduct())->getColors($product->colors);
        $sizes = (new ColorSizeProduct())->getSizes($product->sizes);

        return response()->json([
            'success' => true,
            'colors' => (new Collections())->data($colors, 'colors'),
            'sizes' => (new Collections())->data($sizes, 'sizes')
        ]);
    }
}
