<?php

namespace App\src\traits\Controller;

trait ErrorProductsController
{
    /**
     * @var string
     */
    private string $error = '';
    /**
     * @var bool
     */
    private bool $validate = true;

    /**
     * @param int $price
     * @param int|null $pricePromo
     */
    public function promoPriceTooBig(int $price, ?int $pricePromo = null)
    {
        if (!is_null($pricePromo)) {
            if ($pricePromo > $price) {
                $this->error = 'Le prix de la promo est trop grand par rapport au prix basique !';
                $this->validate = false;
            }
        }
    }

    /**
     * @return bool
     */
    public function isValidate(): bool
    {
        return $this->validate;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }
}
