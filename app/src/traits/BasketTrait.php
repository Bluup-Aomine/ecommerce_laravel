<?php

namespace App\src\traits;

use App\Basket;
use App\BasketsProduct;
use App\ColorSizeProduct;
use App\ImageProduct;

trait BasketTrait
{
    /**
     * @param array $baskets
     * @param string $search
     * @return int
     */
    public static function countElement(array $baskets, string $search = 'quantity'): int
    {
        $count = 0;
        foreach ($baskets as $basket) {
            $count += $basket[$search] ?: 0;
        }
        return $count;
    }

    /**
     * @param Basket $basket
     * @return int
     */
    public static function getQuantity(Basket $basket): int
    {
        $quantity = 0;
        foreach (BasketsProduct::getProduct($basket) as $product) {
            $quantity = ($quantity + $product['quantity']);
        }
        return $quantity;
    }

    /**
     * @param BasketsProduct $basketProduct
     * @param string $key
     * @param array $newBasket
     * @return array
     */
    private static function basketToArray(BasketsProduct $basketProduct, string $key, array $newBasket): array
    {
        $priceProduct = $basketProduct->product->promo ? (integer)$basketProduct->product->price_promo : (integer)$basketProduct->product->price;
        $quantity = $basketProduct->quantity;
        $images = ImageProduct::findByProduct((integer)$basketProduct->product->id);
        $colorSizeProduct = ColorSizeProduct::find((int)$basketProduct->color_size_product_id);

        $newBasket[$key]['id'] = $basketProduct->id ?: '';
        $newBasket[$key]['product_id'] = $basketProduct->product->id ?: '';
        $newBasket[$key]['product'] = $basketProduct->product->name ?: '';
        $newBasket[$key]['price'] = $priceProduct ?: 0;
        $newBasket[$key]['color'] = $colorSizeProduct->color ?: '';
        $newBasket[$key]['size'] = $colorSizeProduct->size ?: '';
        $newBasket[$key]['total'] = ($quantity * $priceProduct);
        $newBasket[$key]['quantity'] = $quantity;
        $newBasket[$key]['image'] = !empty($images) && isset($images[0]) ? $images[0]->img_medium : '';

        return $newBasket;
    }
}
