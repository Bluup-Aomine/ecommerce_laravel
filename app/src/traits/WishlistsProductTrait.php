<?php

namespace App\src\traits;

use App\ImageProduct;
use App\WishlistsProduct;

trait WishlistsProductTrait
{
    /**
     * @param int|null $wishlistID
     * @return array
     */
    public static function getProductsWithImages(?int $wishlistID = null): array
    {
        $newData = [];
        if (is_null($wishlistID)) {
            return $newData;
        }

        $wishlistProducts = WishlistsProduct::findProductsByWishlist($wishlistID)->get();
        foreach ($wishlistProducts as $key => $wishlistProduct) {
            $images = ImageProduct::findByProduct((integer)$wishlistProduct->product_id);

            $newData[$key]['id'] = $wishlistProduct->id ?: '';
            $newData[$key]['productID'] = $wishlistProduct->product_id ?: '';
            $newData[$key]['product'] = $wishlistProduct->product->name ?: '';
            $newData[$key]['price'] = $wishlistProduct->product->price ?: '';
            $newData[$key]['image'] = isset($images[0]) ? $images[0]->img_thumb : '';
        }
        return $newData;
    }

    public function removed()
    {

    }
}
