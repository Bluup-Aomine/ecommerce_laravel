<?php

namespace App\src\traits;

use App\ImageProduct;
use App\src\helpers\FilesStructure;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;

trait ImageProductAction
{

    /**
     * @var string[]
     */
    private $typeImage = ['img_original', 'img_medium', 'img_thumb'];

    /**
     * @param ImageProduct $image
     * @return bool|null
     * @throws Exception
     */
    public function deleteImage(ImageProduct $image)
    {
        return $image->delete();
    }

    /**
     * @param ImageProduct $image
     * @return void
     * @throws BindingResolutionException
     */
    public function unlinkImage(ImageProduct $image): void
    {
        foreach ($this->getTypeImage() as $typeImage) {
            $fileStructure = new FilesStructure($image->{$typeImage});
            $filename = $fileStructure->fileAbsolute();
            unlink($filename);
        }
    }

    /**
     * @return string[]
     */
    private function getTypeImage(): array
    {
        return $this->typeImage;
    }
}
