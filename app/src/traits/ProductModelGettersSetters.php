<?php
namespace App\src\traits;

use App\Product;

trait ProductModelGettersSetters {

    /**
     * @param $value
     * @return string
     */
    public function getPriceAttribute($value): string
    {
        return $value . '.00';
    }

    /**
     * @return int
     */
    public function getPriceOriginal(): int
    {
        return $this->price ?: 0;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return substr($this->content, 0, 85) . '...';
    }

    /**
     * @param int $id
     * @param string $property
     * @return array|null
     */
    public static function getProperty(int $id, string $property): ?array
    {
        $itemsToArray = [];
        $attributes = self::getAttributesEloquent($id, $property);
        $items = explode(', ', $attributes);

        foreach ($items as $item) {
            $itemsToArray[$item] = ucfirst($item);
        }
        return $itemsToArray;
    }

    /**
     * @param int $id
     * @param string $property
     * @return string|null
     */
    private static function getAttributesEloquent(int $id, string $property): ?string
    {
        $product = Product::GetPropertyEloquent($id, $property);
        return $product->first()->{$property};
    }
}
