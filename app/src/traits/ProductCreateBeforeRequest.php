<?php
namespace App\src\traits;

use App\Product;
use Carbon\Carbon;

trait ProductCreateBeforeRequest
{
    /**
     * @var array
     */
    protected $fakeData = [];
    /**
     * @var null|Product
     */
    protected $product = null;

    public function create()
    {
        $this->fakeData();
        $this->product = Product::create($this->fakeData);
    }

    public function getProductID()
    {
        return $this->product->id;
    }

    private function fakeData()
    {
        $this->fakeData['name'] = '';
        $this->fakeData['content'] = '';
        $this->fakeData['price'] = 0;
        $this->fakeData['promo'] = 0;
        $this->fakeData['created_at'] = Carbon::now();
        $this->fakeData['updated_at'] = Carbon::now();
    }
}
