<?php
namespace App\src\helpers;

use App\Product;

class Price
{
    /**
     * @param array $items
     * @return int|mixed
     */
    public static function addTotal(array $items): int
    {
        $total = 0;
        foreach ($items as $item) {
            $total += ($item['total']);
        }
        return $total;
    }

    /**
     * @param array $items
     * @return int
     */
    public static function totalAllProducts(array $items): int
    {
        $products = [];
        foreach ($items as $key => $item) {
            $productID = $item['product_id'];
            $product = Product::find((int)$productID);
            // Calcul total product price
            $productTotal = ($item['quantity'] * $product['price']);
            $products[$key]['total'] = ($productTotal);
        }
        return self::addTotal($products);
    }

    /**
     * @param array $items
     * @return float
     */
    public static function shipping(array $items): float
    {
        $priceTotal = self::addTotal($items) ?: 0;
        return ($priceTotal * 0.1);
    }

    /**
     * @param int $priceTotal
     * @return float
     */
    public static function shippingPrice(int $priceTotal): float
    {
        return ($priceTotal * 0.1);
    }
}
