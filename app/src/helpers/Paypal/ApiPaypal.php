<?php

namespace App\src\helpers\Paypal;

use Illuminate\Support\Facades\Config;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class ApiPaypal
{
    /**
     * @return ApiContext
     */
    public function api(): ApiContext
    {
        $credential = new OAuthTokenCredential($this->config()['client_id'], $this->config()['secret']);
        $apiContext = new ApiContext($credential);
        if (app()->environment('production')) {
            $apiContext->setConfig(['mode' => 'live']);
        }
        return $apiContext;
    }

    /**
     * @return array
     */
    public function config(): array
    {
        return Config::get('paypal');
    }
}
