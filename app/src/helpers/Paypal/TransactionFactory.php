<?php

namespace App\src\helpers\Paypal;

use App\Basket;
use App\BasketsProduct;
use App\src\helpers\Price;
use Illuminate\Support\Facades\Auth;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Transaction;

class TransactionFactory
{
    /**
     * @param Basket $basket
     * @param float|int $vatRate
     * @param float $shipping
     * @return Transaction
     */
    public static function fromBasket(Basket $basket, float $vatRate = 0, float $shipping = 0): Transaction
    {
        $list = new ItemList();
        $products = BasketsProduct::getProduct($basket) ?: [];
        foreach ($products as $product) {
            $item = (new Item())
                ->setName($product['product'])
                ->setPrice($product['price'])
                ->setCurrency('EUR')
                ->setQuantity($product['quantity']);
            $list->addItem($item);
        }

        $total = ((int)Price::addTotal($products));
        $shippingPrice = Price::shippingPrice($total);
        $details = (new Details())
            ->setShipping($shipping !== 0 ? $shippingPrice : 0)
            ->setTax(Basket::getVatPrice($vatRate))
            ->setSubtotal($total);
        $totalShippingAndTax = $shipping !== 0 ? (($total + Basket::getVatPrice($vatRate)) + $shippingPrice) : ($total + Basket::getVatPrice($vatRate));
        $amount = (new Amount())
            ->setTotal($totalShippingAndTax)
            ->setCurrency('EUR')
            ->setDetails($details);

        return (new Transaction())
            ->setItemList($list)
            ->setDescription(Auth::user()->username . ' a acheté sur le site poeandtik.com !!!')
            ->setAmount($amount)
            ->setCustom('demo-1');
    }
}
