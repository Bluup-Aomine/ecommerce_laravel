<?php

namespace App\src\helpers;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;

class ImageMove
{
    /**
     * @var string
     */
    private string $directory = 'productImages/';
    /**
     * @var UploadedFile
     */
    private UploadedFile $UploadedFile;
    /**
     * @var array
     */
    private array $allDatas = [];
    /**
     * @var string
     */
    private string $directoryBase;
    /**
     * @var string
     */
    private string $publicPath;

    /**
     * ImageMove constructor.
     * @param UploadedFile $uploadedFile
     * @throws BindingResolutionException
     */
    public function __construct(UploadedFile $uploadedFile)
    {
        $this->UploadedFile = $uploadedFile;
        $this->directoryBase = 'img/' . $this->directory;
        $this->publicPath = app()->environment('production') ? FilesStructure::makeDirectory('img', '/app') : public_path('img');
        $this->directory = $this->publicPath . DIRECTORY_SEPARATOR . $this->directory;
    }

    /**
     */
    public function moveImage()
    {
        if (!is_dir($this->directory)) {
            mkdir($this->directory, 0777, true);
        }
        $this->doResize();
        $this->doResize(450, 250);
        $this->doResize(200, 200);
    }

    /**
     * @return array
     */
    public function allDatas(): array
    {
        $allDatas = array_unique($this->allDatas);
        return $allDatas;
    }

    /**
     * @param int|null $width
     * @param int|null $height
     * @return string
     */
    private function setFileName(int $width = null, int $height = null): string
    {
        $path = pathinfo($this->UploadedFile->getClientOriginalName());
        $filename = Str::slug($path['filename']) ?: '';
        $extension = $path['extension'] ?: '';

        $file = $this->directory . $filename . '.' . $extension;
        $fileBase = $this->directoryBase . $filename . '.' . $extension;

        $this->allDatas[] = $path['basename'] ?: null;
        if (!is_null($width) && !is_null($height)) {
            $file = $filename . '-' . ($width . 'x' . $height) . '.' . $extension;
            $fileResize = $this->directory . $file;
            $fileResizeBase = $this->directoryBase . $file;

            $this->allDatas[] = $fileResizeBase;
            return $fileResize;
        }
        $this->allDatas[] = $fileBase;
        return $file;
    }

    /**
     * @param string $filename
     * @param int|null $width
     * @param int|null $height
     * @return \Intervention\Image\Image
     */
    private function saveImage(string $filename, int $width = null, int $height = null): \Intervention\Image\Image
    {
        $img = Image::make($this->UploadedFile);
        if (!is_null($width) && !is_null($height)) {
            return $img->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            })->save($filename);
        }
        return $img->save($filename);
    }

    /**
     * @param int|null $width
     * @param int|null $height
     * @return \Intervention\Image\Image
     */
    private function doResize(int $width = null, int $height = null): \Intervention\Image\Image
    {
        $fileName = $this->setFileName($width, $height);
        return $this->saveImage($fileName, $width, $height);
    }
}
