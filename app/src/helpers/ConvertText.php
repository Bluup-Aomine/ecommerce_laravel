<?php

namespace App\src\helpers;

use Illuminate\Http\Request;

class ConvertText
{
    /**
     * @param Request $request
     * @param string $link
     * @return string
     */
    public static function linkImage(Request $request, string $link): string
    {
        $httpOrigin = $request->server->get('HTTP_ORIGIN') ?: '';
        $part = explode($httpOrigin, $link);
        return public_path($part[1]);
    }
}
