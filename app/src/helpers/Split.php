<?php

namespace App\src\helpers;

class Split
{
    /**
     * @param string $delimiter
     * @param string $needle
     * @param int $index
     * @return false|string[]
     */
    public static function cut(string $delimiter = '', string $needle, int $index = 0)
    {
        return explode($delimiter, $needle)[$index];
    }
}
