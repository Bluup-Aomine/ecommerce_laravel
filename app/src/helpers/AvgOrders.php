<?php

namespace App\src\helpers;

use App\Order;
use App\OrderPaypal;

class AvgOrders
{
    /**
     * @return float|int
     */
    public function avg()
    {
        $sumOrders = Order::sum('amount') / 100;
        $sumOrdersPaypal = (integer)OrderPaypal::sum('amount');
        $sumTotal = ($sumOrders + $sumOrdersPaypal);

        $countOrders = Order::count();
        $countOrdersPaypal = OrderPaypal::count();
        $countTotal = ($countOrders + $countOrdersPaypal);

        return $sumTotal !== 0 || $countTotal !== 0 ? ($sumTotal / $countTotal) : 0;
    }
}
