<?php

namespace App\src\helpers;

use Illuminate\Contracts\Container\BindingResolutionException;

class FilesStructure
{
    /**
     * @var string
     */
    const DIR = 'productImages/';
    /**
     * @var string
     */
    private string $fileHttp = '';
    /**
     * @var string
     */
    private string $publicPath;

    /**
     * FilesStructure constructor.
     * @param string $fileHttp
     * @throws BindingResolutionException
     */
    public function __construct(string $fileHttp)
    {
        $this->fileHttp = $fileHttp;
        $this->publicPath = app()->environment('production') ? self::makeDirectory('img', '/app') : public_path('img');
    }

    /**
     * @return string|null
     */
    public function fileAbsolute(): ?string
    {
        $part = explode(self::DIR, $this->fileHttp);
        if (isset($part[1])) {
            return $this->publicPath . DIRECTORY_SEPARATOR . self::DIR . $part[1];
        }
        return null;
    }

    /**
     * @param string $path
     * @param string $delimiter
     * @return string
     * @throws BindingResolutionException
     */
    public static function makeDirectory(string $path, string $delimiter): string
    {
        $directory = app()->make('path');
        $newDirectory = explode($delimiter, $directory)[0];
        return $newDirectory . ($path ? DIRECTORY_SEPARATOR.ltrim($path, DIRECTORY_SEPARATOR) : $path);
    }
}
