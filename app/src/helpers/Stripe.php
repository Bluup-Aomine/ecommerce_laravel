<?php

namespace App\src\helpers;

use stdClass;
use Stripe\Customer;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\PaymentMethod;
use Stripe\StripeClient;
use Stripe\StripeObject;

class Stripe
{

    /**
     * @var StripeClient
     */
    private StripeClient $stripeClient;

    /**
     * Stripe constructor.
     */
    public function __construct()
    {
        $stripeSecret = app()->environment('production') ? 'prod' : 'test';
        $this->stripeClient = new StripeClient($stripeSecret);
    }

    /**
     * @param array $data
     * @return Customer
     * @throws ApiErrorException
     */
    public function customer(array $data): Customer
    {
        return $this->stripeClient->customers->create($data);
    }

    /**
     * @param array $data
     * @return PaymentIntent
     * @throws ApiErrorException
     */
    public function paymentIntent(array $data): PaymentIntent
    {
        return $this->stripeClient->paymentIntents->create($data);
    }

    /**
     * @param object $card
     * @return PaymentMethod
     * @throws ApiErrorException
     */
    public function setCard(object $card)
    {
        return $this->stripeClient->paymentMethods->create(['type' => 'card', 'card' => $card]);
    }

    /**
     * @param string|null $id
     * @return Customer
     * @throws ApiErrorException
     */
    public function getCustomer(?string $id = null): Customer
    {
        return $this->stripeClient->customers->retrieve($id);
    }

    /**
     * @param string $id
     * @return PaymentMethod
     * @throws ApiErrorException
     */
    public function getPaymentMethod(string $id): PaymentMethod
    {
        return $this->stripeClient->paymentMethods->retrieve($id);
    }

    /**
     * @param string $id
     * @return PaymentIntent
     * @throws ApiErrorException
     */
    public function getPaymentIntent(string $id): PaymentIntent
    {
        return $this->stripeClient->paymentIntents->retrieve($id);
    }

    /**
     * @param string $paymentID
     * @param Customer|null $customer
     * @param StripeObject|stdClass|null $card
     * @param int|null $amount
     * @param string $country
     * @return void
     * @throws ApiErrorException
     */
    public function updatePayment(
        string $paymentID,
        ?Customer $customer = null,
        $card = null,
        ?int $amount =  null,
        string $country = 'FR'
    ): void
    {
        $data = [];
        if (!is_null($customer)) {
            $data['customer'] = $customer->id;
        }
        if (!is_null($card)) {
            if ($card->country === 'FR' || $country === 'FR') {
                $data['amount'] = ($amount + ($amount * 0.20));
            }
        }
        $this->stripeClient->paymentIntents->update($paymentID, $data);
    }
}
