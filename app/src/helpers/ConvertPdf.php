<?php

namespace App\src\helpers;

use App\CustomerPaypal;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Html2Pdf;
use Stripe\Customer;
use Stripe\PaymentMethod;

class ConvertPdf
{
    /**
     * @var Html2Pdf
     */
    private Html2Pdf $html2Pdf;

    /**
     * ConvertPdf constructor.
     * @param Html2Pdf $html2Pdf
     */
    public function __construct(Html2Pdf $html2Pdf)
    {
        $this->html2Pdf = $html2Pdf;
        $this->stripe = new Stripe();
    }

    /**
     * @param $collection
     * @param string|null $file
     * @param Customer|CustomerPaypal $customer
     * @param object|null $paymentMethod
     * @param int|null $priceTotal
     * @return string|void
     */
    public function pdf($collection, ?string $file, $customer, ?object $paymentMethod = null, ?int $priceTotal = null)
    {
        try {
            ob_start();

            include dirname(__FILE__) . '/../Vue/Invoice.php';

            $content = ob_get_clean();

            $this->html2Pdf->writeHTML($content);
            $this->html2Pdf->output('invoice.pdf');
        } catch (Html2PdfException $exception) {
            $this->html2Pdf->clean();

            return $exception->getMessage();
        }
    }
}
