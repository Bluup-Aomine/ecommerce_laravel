<?php

namespace App\src\helpers;

use App\Basket;
use App\BasketsProduct;
use App\ColorSizeProduct;
use App\Product;
use App\src\Exception\ColorSizeProductException;
use App\src\HTML\Cart;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SessionBasket
{
    /**
     * @var array
     */
    private array $basket = [];
    /**
     * @var Request
     */
    private Request $request;
    /**
     * @var Cart
     */
    private Cart $cart;
    /**
     * @var bool
     */
    private bool $errorStock = false;

    /**
     * SessionBasket constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->cart = new Cart();
    }

    /**
     * @return bool
     */
    public function getErrorStock(): bool
    {
        return $this->errorStock;
    }

    /**
     * @param int $productID
     * @param int $quantity
     * @param array $features
     * @return null|void
     */
    public function setBasket(int $productID, int $quantity, array $features)
    {
        $colorSizeProduct  = ColorSizeProduct::FindByFeatures($features, $productID)->first();
        if ($colorSizeProduct) {
            if ($this->checkProductBasketExist($productID)) {
                $keyCurrentBasket = $this->getCurrentKeyBasket($productID);
                $currentBasket = $this->getBasketSession()[$keyCurrentBasket] ?: null;
                $newQuantity = ($quantity + $currentBasket['quantity']);
                if (((int)$colorSizeProduct->quantity - (int)$newQuantity) < 0) {
                    $this->errorStock = true;
                    return;
                }
                $this->updateBasket($newQuantity, $keyCurrentBasket);
            } else {
                $this->setBasketSession($productID, $quantity, $colorSizeProduct);
            }
        }
    }

    /**
     * @throws ColorSizeProductException
     * @return void
     */
    public function setSessionToBasketUser(): void
    {
        $baskets = $this->getBasketSession();
        foreach ($baskets as $basket) {
            $carac = ColorSizeProduct::find($basket['color_size_product_id']) ?: null;
            $features = (new ColorSizeProduct())->toFeatures($carac);
            $b = (new Basket())->add();
            (new BasketsProduct())->add($basket['product_id'], $b->id, $basket['quantity'], $features);
        }
    }

    /**
     * @param int $quantity
     * @param int $key
     */
    public function updateBasket(int $quantity, int $key)
    {
        $basket = $this->getItem($key);
        if ($basket) {
            $basket['quantity'] = $quantity;
            $this->request->session()->put('basket.' . $key, $basket);
        }
    }

    /**
     * @param int $basketsProductID
     */
    public function deleteProductBasket(int $basketsProductID)
    {
        $currentKeyBasket = $this->getCurrentKeyBasket($basketsProductID);
        $this->request->session()->forget('basket.' . $currentKeyBasket);
    }

    /**
     * @param Cart $cart
     * @return JsonResponse
     */
    public function responseAddBasket(Cart $cart): JsonResponse
    {
        $basket = $this->getBasketSession();
        $lastBasket = $basket[count($basket) - 1];
        $lastProductBasket = Product::with('images')->find($basket[0]['product_id']);

        $spanCartProduct = $this->cart->spanElementHTML($lastProductBasket, $lastBasket['quantity']);
        return response()->json([
            'success' => true,
            'data' => [
                'message' => 'Vous avez ajouté ce produit à votre panier !!!',
                'block' => $cart->addCartSuccess('Vous avez ajouté ce produit à votre panier !!!'),
                'spanProduct' => $spanCartProduct,
                'product' => $this->cart->listProduct($lastProductBasket),
                'quantityLastProduct' => $lastBasket['quantity'] ?: 0,
                'priceTotal' => Price::totalAllProducts($basket),
                'count' => $this->countItems()
            ]
        ], 302);
    }

    /**
     * @param Cart $cart
     * @return JsonResponse
     */
    public function responseLoadBasket(Cart $cart): JsonResponse
    {
        $total = $this->getTotal() ?: 0;

        $products = $this->getProducts();
        $htmlBasketProducts = $cart->cart($products);
        $htmlTotalProducts = $cart->totalCart($total);
        return response()->json([
            'success' => true,
            'data' => [
                'products' => $htmlBasketProducts,
                'totalPrice' => $htmlTotalProducts,
                'count' => ($this->countQuantity($products))
            ]
        ]);
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        $products = [];
        foreach ($this->getBasketSession() as $key => $item) {
            $product = Product::with('images')->find((int)$item['product_id']);
            $colorSize = ColorSizeProduct::find((int)$item['color_size_product_id']);

            $products[$key]['id'] = $product->id;
            $products[$key]['name'] = $product->name;
            $products[$key]['size'] = $colorSize->size;
            $products[$key]['color'] = $colorSize->color;
            $products[$key]['price'] = $product->price;
            $products[$key]['quantity'] = $item['quantity'];
            $products[$key]['total'] = ($item['quantity'] * $product->price);
            $products[$key]['image'] = $product->images[0]->img_medium;
        }
        return $products;
    }

    /**
     * @return int|mixed
     */
    public function getTotal(): int
    {
        $total = 0;
        foreach ($this->getBasketSession() as $key => $item) {
            $product = Product::with('images')->find((int)$item['product_id']);
            $total += ($item['quantity'] * $product['price']);
        }
        return $total;
    }

    /**
     * @param int $productID
     * @param int $quantity
     * @param ColorSizeProduct $colorSizeProduct
     */
    private function setBasketSession(int $productID, int $quantity, ColorSizeProduct $colorSizeProduct)
    {
        $this->basket['product_id'] = $productID;
        $this->basket['quantity'] = $quantity;
        $this->basket['color_size_product_id'] = $colorSizeProduct->id;
        $this->request->session()->push('basket', $this->basket);
    }

    /**
     * @param int $productID
     * @return int|string|null
     */
    private function getCurrentKeyBasket(int $productID)
    {
        $keyBasket = null;
        $basketProducts = $this->getBasketSession();
        foreach ($basketProducts as $key => $basket) {
            if ((int)$basket['product_id'] === $productID) {
                $keyBasket = $key;
            }
        }
        return $keyBasket;
    }

    /**
     * @param int $productID
     * @return bool
     */
    private function checkProductBasketExist(int $productID): bool
    {
        $success = false;
        $basketProducts = $this->getBasketSession();
        foreach ($basketProducts as $basket) {
            if ($basket['product_id'] === $productID) {
                $success = true;
            }
        }
        return $success;
    }

    /**
     * @return int
     */
    private function countItems(): int
    {
        $items = $this->getBasketSession() ?: [];
        return count($items);
    }

    /**
     * @param array $products
     * @return int|mixed
     */
    private function countQuantity(array $products): int
    {
        $countQuantity = 0;
        foreach ($products as $product) {
            $countQuantity += (int)$product['quantity'];
        }
        return $countQuantity;
    }

    /**
     * @return mixed
     */
    private function getBasketSession()
    {
        return $this->request->session()->get('basket') ?: [];
    }

    /**
     * @param int $index
     * @return mixed
     */
    private function getItem(int $index)
    {
        return $this->getBasketSession()[$index];
    }
}
