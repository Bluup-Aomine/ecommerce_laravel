<?php


namespace App\src\HTML;


class HtmlMessage
{
    /**
     * @param string|null $url
     * @param string|null $nameBtn
     * @param string $message
     * @return string
     */
    public function messagePrimary(?string $url, ?string $nameBtn, string $message): string
    {
        $btn = !is_null($nameBtn) ? '<a href="' . $url . '" class="button" tabindex="1">' . $nameBtn . '</a>' : null;
        return <<<HTML
            <div class="notices-wrapper">
                <div class="notices-message" role="alert">
                    $btn
                    $message
                </div>
            </div>
        HTML;
    }

    /**
     * @param string $message
     * @return string
     */
    public function jsonMessageError(string $message): string
    {
        return <<<HTML
            <div class="notices-wrapper">
                <div class="notices-message notice-danger" role="alert">
                    {$message}
                </div>
            </div>
        HTML;

    }

    /**
     * @param string $message
     * @param bool $success
     * @return string
     */
    public function messageJSON(string $message = '', $success = true): string
    {
        $class = $success ? 'alert-success' : 'alert-danger';
        return <<<HTML
            <div class="alert $class alert-dismissible fade show">
                <i class="fa fa-check mr-2"></i>
                $message
                <button class="close" type="button" aria-label="close" data-dismiss="alert">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
        HTML;

    }
}
