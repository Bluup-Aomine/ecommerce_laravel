<?php

namespace App\src\HTML;

use App\Basket;

class Cart
{
    /**
     * @var HtmlMessage
     */
    private HtmlMessage $message;

    public function __construct()
    {
        $this->message = new HtmlMessage();
    }

    /**
     * @param string $message
     * @param Basket|null $basket
     * @return string
     */
    public function addCartSuccess(string $message, ?Basket $basket = null): string
    {
        $route = $basket ? route('product.basket.edit', [$basket]) : route('product.basket.session');
        return $this->message->messagePrimary($route, 'Voir Panier', $message);
    }

    /**
     * @param string $message
     * @return string
     */
    public function deleteCartSuccess(string $message): string
    {
        return $this->message->messagePrimary(null, null, $message);
    }

    /**
     * @param array|object $products
     * @return string|array
     */
    public function cart($products)
    {
        $html = [];
        if (is_object($products)) {
            return $this->listProduct($products);
        }
        foreach ($products as $product) {
            $html[] = $this->listProduct($product);
        }

        return $html;
    }

    /**
     * @param int $total
     * @return string
     */
    public function totalCart(int $total): string
    {
        return $this->totalPrice($total);
    }

    /**
     * @param array|object $lastProduct
     * @param int $quantity
     * @return string
     */
    public function spanElementHTML($lastProduct, int $quantity)
    {
        $price = $lastProduct['price'];
        return <<<HTML
            $quantity x
            <span class="price-amount" id="price-total-products">
                  <span class="price-symbol">€</span>$price
            </span>
        HTML;
    }

    /**
     * @param $product
     * @return string
     */
    public function listProduct($product): string
    {
        $id = $product['id'] ?: '';
        $productID = isset($product['product_id']) ? $product['product_id'] : $id;
        $name = isset($product['product']) ? $product['product'] : $product['name'];
        $price = $product['price'] ?: 0;
        $quantity = $product['quantity'] ?: 1;
        $image = $product['image'] ?: $product->images[0]->img_medium;

        return <<<HTML
                        <li class="product-cart-item">
                             <a href="#" class="product-item-image">
                                 <img src="$image" alt="" width="420" height="510" class="image-product-thumbnail img-responsive">
                                 $name
                             </a>
                             <span class="quantity" id="quantity-product-span-$productID">
                                $quantity x
                                    <span class="price-amount" id="price-total-products">
                                          <span class="price-symbol">€</span>$price
                                    </span>
                             </span>
                        </li>
        HTML;
    }

    /**
     * @param int $total
     * @return string
     */
    private function totalPrice(int $total): string
    {
        return <<<HTML
            <strong>Total:</strong>
            <span class="price-amount" id="price-total-products">
                  <span class="price-symbol">€</span>$total
            </span>
        HTML;
    }
}
