<?php

namespace App\src\HTML;

use App\ColorSizeProduct;
use App\Product;
use Illuminate\Pagination\LengthAwarePaginator;
use phpDocumentor\Reflection\DocBlock\Tags\Deprecated;

class Products
{
    /**
     * @param LengthAwarePaginator $products
     * @return array
     */
    public function renderItem(LengthAwarePaginator $products): array
    {
        $items = [];
        foreach ($products as $key => $product) {
            $items[$key] = $this->itemProduct($product);
        }
        return $items;
    }

    /**
     * @param int $min
     * @param int $max
     * @return array
     */
    public function renderItemFilterPrice(int $min, int $max): array
    {
        $items = [];
        $index = $min !== 0 ? 2 : 1;
        for ($i = 0; $i < $index; $i++) {
            $items[$i] = $this->itemFilterPrice($min, $max, $i);
        }
        return $items;
    }

    /**
     * @param $item
     * @return string
     */
    public function renderItemFilter($item): string
    {
        return $this->itemFilter($item);
    }

    /**
     * @param Product|ColorSizeProduct $product
     * @return string
     */
    private function itemProduct($product): string
    {
        $route_show = route('product.show', [$product]);
        $price = $product->price . '.00';
        if (isset($product->images) && !empty($product->images)) {
            $image = $product->images[0]->img_medium ?? null;
        } else {
            $image = null;
        }

        return "<li class=\"product\">
                   <div class=\"product-contain\">
                         <div class=\"product-content-top\">
                                 <div class=\"product-image\">
                                          <a href=\"#\">
                                               <img
                                               src=\"{$image}\"
                                               class=\"product-img\" width=\"420\" height=\"510\" alt=\"\">
                                          </a>
                                           <div class=\"product-active-buttons\">
                                                 <div class=\"button-cart top-btn\" aria-label=\"Add To Cart\">
                                                       <a href=\"#\" class=\"btn btn-add-cart\">
                                                             <i class=\"fa fa-shopping-cart icon icon-bag-cart\"></i>
                                                       </a>
                                                 </div>
                                                 <div class=\"button-cart top-btn\" aria-label=\"Quick View\">
                                                      <a href=\"{$route_show}\" class=\"btn btn-add-cart\">
                                                            <i class=\"fa fa-shopping-bag icon icon-bag-cart\"></i>
                                                      </a>
                                                 </div>
                                                 <div class=\"button-cart top-btn\" aria-label=\"Quick View\">
                                                      <a href=\"#\" class=\"btn btn-add-cart\">
                                                           <i class=\"fa fa-heart-o icon icon-bag-cart\"></i>
                                                      </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style=\"padding-left: 25px;\">
                                                <h3 class=\"product-title\">
                                                    <a href=\"{$route_show}\">$product->name</a>
                                                </h3>
                                                <div class=\"star-rating\">
                                                        <span style=\"width: 100%\">
                                                            Rated
                                                            <strong class=\"rating\">5.00</strong>
                                                             out of 5
                                                        </span>
                                                </div>
                                                <span class=\"price price-filter-products\">
                                                        <span class=\"price-amount\">
                                                            <span class=\"price-symbol\">$</span>{$price}
                                                        </span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>";
    }

    /**
     * @param $filter
     * @return string
     */
    private function itemFilter($filter): string
    {
        $route = route('product-filter.clear.filter', $filter);
        return <<<HTML
                 <li class="chosen">
                    <a href="$route" aria-label="Remove filterComponent" class="remove-filter">$filter</a>
                 </li>
            HTML;
    }

    /**
     * @param int $min
     * @param int $max
     * @param int $index
     * @return string
     */
    private function itemFilterPrice(int $min, int $max, int $index): string
    {
        $title = $index === 0 && $min !== 0 ? 'Min ' : 'Max ';
        $value = $index === 0 && $min !== 0 ? $min : $max;

        $route = route('product-filter.clear.filter', 'price');
        return <<<HTML
                <li class="chosen">
                    <a href="$route" aria-label="Remove filterComponent" class="remove-filter">
                        $title
                        <span class="price-amount">
                             <span class="price-symbol">$</span>$value
                        </span>
                    </a>
                 </li>
            HTML;
    }
}
