<?php

namespace App\src\HTML;

class Collections
{

    /**
     * @param array $items
     * @param string $callable
     * @return array
     */
    public function data(array $items = [], string $callable): array
    {
        $data = [];
        foreach ($items as $key => $item) {
            $data[] = $this->{$callable}($item, $key);
        }
        return $data;
    }

    /**
     * @param array $element
     * @param int $key
     * @return string
     */
    public function colors(array $element = [], int $key = 0): string
    {
        $name = $element['name'] ?: '';
        $classname = $key === 0 ? 'active' : null;

        return <<<HTML
                <li class="colors-list-products-item {$classname}" data-title="{$name}">
                     <a href="#" class="colors-list-products-links color-{$name}"></a>
                </li>
        HTML;
    }

    /**
     * @param array $element
     * @param int $key
     * @return string
     */
    public function sizes(array $element = [], int $key = 0): string
    {
        $name = $element['name'] ?: '';
        $item = mb_strtoupper($name);
        $classname = $key === 0 ? 'active' : null;

        return <<<HTML
                <li class="colors-list-products-item {$classname}" data-title="{$name}">
                     <a href="#" class="colors-list-products-links">{$item}</a>
                </li>
        HTML;
    }
}
