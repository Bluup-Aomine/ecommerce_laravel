<?php

namespace App\src\Exception;

use Exception;

class ColorSizeProductException extends Exception
{
    protected $message = 'Aucunes de ces caractéristiques sont présentes sur ce produit !!!';
}
