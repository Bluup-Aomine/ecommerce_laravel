<?php

namespace App\src\Exception;

use Exception;

class BasketException extends Exception
{
    protected $message = 'Impossible de modifié votre panier, veuillez recommencer cette action !!!';
}
