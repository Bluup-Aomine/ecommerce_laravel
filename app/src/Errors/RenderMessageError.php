<?php

namespace App\src\Errors;

use App\src\HTML\HtmlMessage;
use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class RenderMessageError
{
    /**
     * @param Exception $exception
     * @return JsonResponse
     */
    public static function MessageErrorJson(Exception $exception): JsonResponse
    {
        if ($exception instanceof ModelNotFoundException) {
            return self::MessageResponseError('Aucun résultat trouvé sur cette image !');
        }
        return self::MessageResponseError('Une erreur est survenue lors de votre requête !');
    }

    /**
     * @param string $message
     * @param string|null $jsonMessage
     * @return JsonResponse
     */
    public static function MessageResponseError(string $message, ?string $jsonMessage = null): JsonResponse
    {
        $data['success'] = false;
        $data['message'] = (new HtmlMessage())->jsonMessageError($message);
        if (!is_null($jsonMessage)) {
            $data['render'] = $jsonMessage;
        }
        return response()->json($data, 302);
    }

    /**
     * @param string $message
     * @param Validator $validator
     * @param string $route
     * @return RedirectResponse
     */
    public static function MessageErrorWithSession(string $message, Validator $validator, string $route): RedirectResponse
    {
        return redirect()->route($route)->withErrors($validator)->with('messageError', $message);
    }
}
