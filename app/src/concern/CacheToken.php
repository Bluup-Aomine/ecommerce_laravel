<?php

namespace App\src\concern;

class CacheToken
{
    /**
     * @var string
     */
    private static string $tokenStripeTest = '';
    /**
     * @var string
     */
    private static string $tokenStripeProd = '';

    /**
     * @return string
     */
    static function getTokenStripe(): string
    {
        return app()->environment('production') ? self::$tokenStripeProd : self::$tokenStripeTest;
    }

}
