<page backtop="10mm" backbottom="10mm" backleft="20mm" backright="20mm">
    <page_header>
        <div class="invoice-title">
            <h4 style="float: right!important; font-size: 16px!important;">Order #<?= $collection->id ?></h4>
            <div style="margin-bottom: 1.5rem!important;">
                <img src="<?= $file ?>" alt="logo" height="20">
            </div>
        </div>
        <hr>
    </page_header>
    <div style="padding-top: 75px">
        <div style="display: flex; flex-wrap: wrap; margin-right: -12px; margin-left: -12px">
            <div
                style="flex: 0 0 50%; max-width: 50%; position: relative; width: 100%; padding-right: 12px; padding-left: 12px">
                <div style="margin-bottom: 1rem; font-style: normal; line-height: 18px">
                    <strong>Billed To:</strong><br>
                    Simon Tomi<br>
                </div>
            </div>
            <div
                style="flex: 0 0 50%; max-width: 50%; position: relative; width: 100%; padding-right: 12px; padding-left: 12px">
                <div style="margin-bottom: 1rem; font-style: normal; line-height: 18px; margin-top: 0!important;">
                    <strong>Shipped To:</strong>
                    <br>
                    <span><?= $customer->name ?: $customer->firstname . ' ' . $customer->lastname ?></span>
                    <br>
                    <span><?= is_object($customer->address) ? $customer->address->line1 : $customer->address ?></span>
                    <br>
                    <span><?= $customer->phone ?></span>
                </div>
            </div>
        </div>
        <div style="display: flex; flex-wrap: wrap; margin-right: -12px; margin-left: -12px">
            <div
                style="flex: 0 0 50%; max-width: 50%; position: relative; width: 100%; padding-right: 12px; padding-left: 12px;">
                <div style="margin-bottom: 1rem; font-style: normal; line-height: 18px; margin-top: 0!important;">
                    <strong>Payment Method:</strong>
                    <br>
                    <span>
                        <?= $paymentMethod && $paymentMethod->brand ? ucfirst($paymentMethod->brand) . ' ending **** ' : 'Paypal' ?>
                        <?= $paymentMethod && $paymentMethod->last4 ? $paymentMethod->last4 : null ?>
                    </span>
                    <br>
                    <span><?= $customer->email ?></span>
                </div>
            </div>
            <div
                style="flex: 0 0 50%; max-width: 50%; position: relative; width: 100%; padding-right: 12px; padding-left: 12px; text-align: right!important;">
                <div style="margin-bottom: 1rem; font-style: normal; line-height: 18px; margin-top: 0!important;">
                    <strong>Order Date:</strong>
                    <br>
                    <span><?= $collection->getCreatedAt() ?></span>
                </div>
            </div>
        </div>
        <div style="padding-bottom: .5rem!important; padding-top: .5rem!important; margin-top: 1rem!important;">
            <h3 style="font-size: 15px!important; font-weight: 600!important;">Order summary</h3>
        </div>
        <div style="display: block; width: 100%; overflow-x: auto">
            <table style="border-collapse: collapse; width: 100%; margin-bottom: 1rem; color: #495057">
                <thead>
                <tr>
                    <th style="width: 70px; vertical-align: bottom; border-bottom: 2px solid #eff2f7; white-space: nowrap; font-weight: 600; padding: 10px; border-top: 1px solid #eff2f7">
                        No.
                    </th>
                    <th style="width: 70px; vertical-align: bottom; border-bottom: 2px solid #eff2f7; white-space: nowrap; font-weight: 600; padding: 10px; border-top: 1px solid #eff2f7">
                        Item
                    </th>
                    <th style="width: 70px; vertical-align: bottom; border-bottom: 2px solid #eff2f7; white-space: nowrap; font-weight: 600; padding: 10px; border-top: 1px solid #eff2f7; text-align: right!important;">
                        Price
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($collection['products'] as $key => $product): ?>
                    <tr>
                        <td style="white-space: nowrap; padding: 10px; vertical-align: top; border-top: 1px solid #eff2f7">
                            <?= $key + 1 ?>
                        </td>
                        <td style="white-space: nowrap; padding: 10px; vertical-align: top; border-top: 1px solid #eff2f7">
                            <?= $product['name'] ?>
                        </td>
                        <td style="white-space: nowrap; padding: 10px; vertical-align: top; border-top: 1px solid #eff2f7">
                            € <?= $product['price'] ?>.00
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td></td>
                    <td style="white-space: nowrap; padding: 10px; vertical-align: top; border-top: 1px solid #eff2f7; text-align: right!important;">
                        Sub Total
                    </td>
                    <td style="white-space: nowrap; padding: 10px; vertical-align: top; border-top: 1px solid #eff2f7; text-align: right!important;">
                        € <?= $priceTotal ?>.00
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="white-space: nowrap; padding: 10px; vertical-align: top; border-top: 1px solid #eff2f7; text-align: right!important;">
                        <strong>Shipping</strong>
                    </td>
                    <td style="white-space: nowrap; padding: 10px; vertical-align: top; border-top: 1px solid #eff2f7; text-align: right!important;">
                        € <?= \App\src\helpers\Price::shipping($collection['products']) ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="white-space: nowrap; padding: 10px; vertical-align: top; border-top: 1px solid #eff2f7; text-align: right!important;">
                        <strong>TVA</strong>
                    </td>
                    <td style="white-space: nowrap; padding: 10px; vertical-align: top; border-top: 1px solid #eff2f7; text-align: right!important;">
                        <?= $collection['tva'] ? 'WITH' : 'WITHOUT' ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="white-space: nowrap; padding: 10px; vertical-align: top; border-top: 1px solid #eff2f7; text-align: right!important;">
                        <strong>Total</strong>
                    </td>
                    <td style="white-space: nowrap; padding: 10px; vertical-align: top; border-top: 1px solid #eff2f7; text-align: right!important;">
                        € <?= is_int($collection['amount']) ? $collection['amount'] . '.00' : $collection['amount'] ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <page_footer>
        <table style="width: 100%; border: solid 1px black;">
            <tr>
                <td style="text-align: left;width: 33%">PDF</td>
                <td style="text-align: center;width: 34%">Facture</td>
                <td style="text-align: right;width: 33%"><?php echo date('d/m/Y'); ?></td>
            </tr>
        </table>
    </page_footer>
</page>
