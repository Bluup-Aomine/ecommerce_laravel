<?php

namespace App\src\Validator;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class CategoryValidator extends ValidatorRequest {
    /**
     * @param array|Request $results
     * @return Validator
     */
    public static function make($results): Validator
    {
        self::setRules();
        self::setMessages();
        return parent::make($results);
    }

    protected static function setRules()
    {
        self::$rules = [
            'name' => 'required|min:3|max:50',
            'content' => 'required|min:5|max:500'
        ];
    }

    protected static function setMessages()
    {
        self::$messages = [
            'name.min' => 'Le champs name doit contenir au minimum 3 caractères',
            'name.max' => 'Le champs name doit contenir au maximum 100 caractères',
            'name.required' => 'Le champs name doit être requis',

            'content.min' => 'Le champs content doit contenir au minimum 5 caractères',
            'content.max' => 'Le champs content doit contenir au maximum 500 caractères',
            'content.required' => 'Le champs content doit être requis'
        ];
    }
}
