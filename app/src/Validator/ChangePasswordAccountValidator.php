<?php

namespace App\src\Validator;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class ChangePasswordAccountValidator extends ValidatorRequest
{
    /**
     * @param array|Request $results
     * @return Validator
     */
    public static function make($results): Validator
    {
        self::setRules();
        self::setMessages();
        return parent::make($results);
    }

    protected static function setRules()
    {
        self::$rules = [
            'password' => 'required|min:5',
            'password_new' => 'required|min:5',
            'password_new_confirmation' => 'required|min:5'
        ];
    }

    protected static function setMessages()
    {
        self::$messages = [
            'password.required' => 'Le champs mot de passe doit être requis',
            'password.min' => 'Le champs mot de passe doit contenir au minimum 5 caractères',

            'password_new.required' => 'Le champs Nouveau mot de passe doit être requis',
            'password_new.min' => 'Le champs Nouveau mot de passe doit contenir au minimum 5 caractères',

            'password_new_confirmation.required' => 'Le champs Confirmer votre nouveau mot de passe doit être requis',
            'password_new_confirmation.min' => 'Le champs Confirmer votre nouveau mot de passe doit contenir au minimum 5 caractères'
        ];
    }
}
