<?php

namespace App\src\Validator;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class ColorSizeProductValidator extends ValidatorRequest
{
    /**
     * @param array|Request $results
     * @return Validator
     */
    public static function make($results): Validator
    {
        self::setRules();
        self::setMessages();
        return parent::make($results);
    }

    protected static function setRules()
    {
        self::$rules = [
            'colors' => 'required',
            'sizes' => 'required',
            'quantity' => 'required|integer'
        ];
    }

    protected static function setMessages()
    {
        self::$messages = [
            'colors.required' => 'Le champs couleur doit être requis',
            'sizes.required' => 'Le champs taille doit être requis',
            'quantity.required' => 'Le champs quantité doit être requis',

            'quantity.integer' => 'Le champs quantité doit être un nombre'
        ];
    }
}
