<?php

namespace App\src\Validator;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class ProductValidator extends ValidatorRequest
{
     /**
     * @param array|Request $results
     * @return Validator
     */
    public static function make($results): Validator
    {
        self::setRules();
        self::setMessages();
        return parent::make($results);
    }

    protected static function setRules()
    {
        self::$rules = [
            'name' => 'required|min:5|max:100',
            'content' => 'required|min:10',
            'price' => 'required|integer|between:20,1000',
            'colors' => 'required',
            'sizes' => 'required',
            'category_id' => 'required'
        ];
    }

    protected static function setMessages()
    {
        self::$messages = [
            'name.min' => 'Le nom du produit doit contenir au minimum 5 caractères',
            'name.max' => 'Le nom du produit doit contenir au maximum 100 caractères',
            'name.required' => 'Le champs name doit être requis',

            'content.required' => 'Le champs contenu doit être requis',
            'content.min' => 'Le champs contenu doit contenir  au minimum 10 caractères',

            'price.required' => 'Le champs prix doit être requis',
            'price.between' => 'Le prix doit être entre 20 € et 1000 €',

            'colors' => 'Le champs couleurs doit être requis pour votre produit',
            'sizes' => 'Le champs tailles doit être requis pour votre produit',
            'category_id' => 'Le champs category doit être requis pour votre produit'
        ];
    }
}
