<?php

namespace App\src\Validator;

use App\src\Rules\Country;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class CheckoutValidator extends ValidatorRequest
{
    /**
     * @param array|Request $results
     * @return Validator
     */
    public static function make($results): Validator
    {
        self::setRules();
        self::setMessages();
        return parent::make($results);
    }

    protected static function setRules()
    {
        self::$rules = [
            'name' => 'required|min:5|max:60',
            'email' => 'required|min:5|email',
            'phone' => 'required|regex:/(0)[0-9]{9}/',
            'address' => 'required|min:5',
            'country' => ['required', new Country()]
        ];
    }

    protected static function setMessages()
    {
        self::$messages = [
            'name.required' => 'Le champs name doit être requis',
            'name.min' => 'Le champs name doit contenir au minimum 5 caractères',
            'name.max' => 'Le champs name doit contenir au maximum 60 caractères',

            'email.required' => 'Le champs email doit être requis',
            'email.min' => 'Le champs email doit contenir au minimum 5 caractères',
            'email.email' => 'Le champs email doit être une addresse email',

            'phone.required' => 'Le champs phone doit être requis',
            'phone.regex' => 'Le champs phone doit être un numéro de téléphonique',

            'address.required' => 'Le champs address doit être requis',
            'address.min' => 'Le champs address doit contenir au minimun 5 caractères',

            'country.required' => 'Le champs country doit être requis'
        ];
    }
}
