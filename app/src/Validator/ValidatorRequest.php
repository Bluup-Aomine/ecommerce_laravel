<?php

namespace App\src\Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ValidatorRequest
{
    /**
     * @var array
     */
    protected static $rules = [];
    /**
     * @var array
     */
    protected static $messages = [];

    /**
     * @param Request|array $results
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($results): \Illuminate\Contracts\Validation\Validator
    {
        if (is_object($results) && ($results instanceof Request)) {
            $results = $results->all();
        }
        return Validator::make($results, self::$rules, self::$messages);
    }

    protected static function setRules()
    {
        self::$rules = [];
    }

    protected static function setMessages()
    {
        self::$messages = [];
    }
}
