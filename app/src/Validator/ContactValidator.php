<?php

namespace App\src\Validator;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class ContactValidator extends ValidatorRequest
{
    /**
     * @param array|Request $results
     * @return Validator
     */
    public static function make($results): Validator
    {
        self::setRules();
        self::setMessages();
        return parent::make($results);
    }

    protected static function setRules()
    {
        self::$rules = [
            'name' => 'required|min:3|max:50',
            'email' => 'required|min:5|max:100',
            'content' => 'required'
        ];
    }

    protected static function setMessages()
    {
        self::$messages = [
            'name.min' => 'Le champs name doit contenir au minimum 3 caractères',
            'name.max' => 'Le champs name doit contenir au maximum 50 caractères',
            'name.required' => 'Le champs name doit être requis',

            'email.min' => 'Le champs email doit contenir au minimum 5 caractères',
            'email.max' => 'Le champs email doit contenir au maximum 50 caractères',
            'email.required' => 'Le champs email doit être requis',

            'content.required' => 'Le champs contenu doit être requis'
        ];
    }
}
