<?php

namespace App\src\Validator;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class AccountDetailsValidator extends ValidatorRequest
{
    /**
     * @param array|Request $results
     * @return Validator
     */
    public static function make($results): Validator
    {
        self::setRules();
        self::setMessages();
        return parent::make($results);
    }

    protected static function setRules()
    {
        self::$rules = [
            'username' => 'required|min:3|max:50',
            'email' => 'required'
        ];
    }

    protected static function setMessages()
    {
        self::$messages = [
            'username.required' => 'Le champs username doit être requis',
            'username.min' => 'Le champs username doit contenir au minimum 3 caractères',
            'username.max' => 'Le champs username doit contenur au maximum 50 caractères',

            'email.required' => 'Le champs email doit être requis'
        ];
    }
}
