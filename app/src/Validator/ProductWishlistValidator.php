<?php

namespace App\src\Validator;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class ProductWishlistValidator extends ValidatorRequest
{
    /**
     * @param array|Request $results
     * @return Validator
     */
    public static function make($results): Validator
    {
        self::setRules();
        self::setMessages();
        return parent::make($results);
    }

    protected static function setRules()
    {
        self::$rules = [
            'title' => 'required|min:5|max:100'
        ];
    }

    protected static function setMessages()
    {
        self::$messages = [
            'title.required' => 'Le champs couleur doit être requis',
            'title.min' => 'Le champs taille doit être requis',
            'title.max' => 'Le champs quantité doit être requis'
        ];
    }
}
