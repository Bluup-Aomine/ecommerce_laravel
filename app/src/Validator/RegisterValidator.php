<?php

namespace App\src\Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterValidator extends ValidatorRequest {
     /**
     * @param array|Request $results
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function make($results): \Illuminate\Contracts\Validation\Validator
    {
        self::setRules();
        self::setMessages();
        return parent::make($results);
    }

    protected static function setRules()
    {
        self::$rules = [
            'name' => 'required|min:3|max:50',
            'email' => 'required',
            'password' => 'required|min:5',
            'password_confirm' => 'required|min:5'
        ];
    }

    protected static function setMessages()
    {
        self::$messages = [
            'name.min' => 'Le champs username doit contenir au minimum 3 caractères',
            'name.max' => 'Le champs username doit contenir au maximum 100 caractères',
            'name.required' => 'Le champs username doit être requis',

            'email.required' => 'Le champs email doit être requis',

            'password.required' => 'Le champs password doit être requis',
            'password.min' => 'Le champs password doit contenir au minimum 5 caractères',
            'password_confirm.required' => 'Le champs password confirmation doit être requis',
            'password_confirm.min' => 'Le champs password confirmation doit contenir au minimum 5 caractères',
        ];
    }
}
