<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class Category extends Model
{
    protected $fillable = ['name', 'content', 'slug'];

    public $timestamps = true;

    /**
     * @return HasMany
     */
    public function product(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return substr($this->content, 0, 100) . '...';
    }

    /**
     * @param $query
     * @param string|null $slug
     * @return Builder
     */
    public function scopeFindBySlug($query, ?string $slug): Builder
    {
        return $query->where('slug', $slug);
    }

    /**
     * @return array
     */
    public static function allWithCountable(): array
    {
        $categoryToArray = [];
        $categories = Category::all();
        foreach ($categories as $key => $category) {
            $categoryToArray[$key]['id'] = $category->id;
            $categoryToArray[$key]['name'] = $category->name;
            $categoryToArray[$key]['slug'] = $category->slug;
            $categoryToArray[$key]['count'] = Product::findByCategory((int)$category->id)->count();
        }
        return $categoryToArray;
    }
}
