<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use PayPal\Api\Payer;

class CustomerPaypal extends Model
{
    public $timestamps = true;

    protected $guarded = [];

    protected $table = 'customers_paypal';

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $value
     * @return string
     */
    public function getTotalPurchaseAttribute($value): string
    {
        return $value . ' €';
    }

    /**
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value): string
    {
        return Carbon::parse($value)->format('d M, Y');
    }
}
