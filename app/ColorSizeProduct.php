<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;

class ColorSizeProduct extends Model
{
    protected $fillable = ['color', 'size', 'quantity', 'product_id'];

    public $timestamps = true;
    /**
     * @var array
     */
    private array $colors = [
        'blue' => '#3336FF',
        'red' => '#FF0000',
        'black' => '#000000',
        'white' => '#ffffff',
        'green' => '#00FF00',
        'belgian' => '#e7c194',
        'orange ' => '#FFA500',
        'yellow' => '#FFFF00'
    ];
    /**
     * @var array|string[]
     */
    private array $sizes = ['xs', 's', 'm', 'l', 'xl'];

    /**
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return HasMany
     */
    public function basketProduct(): HasMany
    {
        return $this->hasMany(BasketsProduct::class);
    }

    /**
     * @param Request $request
     * @param Product $product
     * @return bool
     */
    public function add(Request $request, Product $product): bool
    {
        $colors = $request->get('colors') ?: '';
        $sizes = $request->get('sizes') ?: '';

        if (strpos($product->colors, $colors) !== false && strpos($product->sizes, $sizes) !== false) {
            $this->color = $request->get('colors');
            $this->size = $request->get('sizes');
            $this->quantity = (int)$request->get('quantity');
            $this->product_id = (int)$product->id;
            return $this->save();
        }
        return false;
    }

    /**
     * @param Request $request
     * @param Product $product
     * @param int $colorSizeProductID
     * @return bool|void
     */
    public function updateData(Request $request, Product $product, int $colorSizeProductID)
    {
        $colors = $request->get('colors') ?: '';
        $sizes = $request->get('sizes') ?: '';

        $colorSizeProduct = ColorSizeProduct::find($colorSizeProductID);
        if (strpos($product->colors, $colors) !== false && strpos($product->sizes, $sizes) !== false) {
            $colorSizeProduct->color = $request->get('colors');
            $colorSizeProduct->size = $request->get('sizes');
            $colorSizeProduct->quantity = (int)$request->get('quantity');
            return $colorSizeProduct->save();
        }
        return false;
    }

    /**
     * @param $collection
     * @return array
     */
    public function toFeatures($collection): array
    {
        $data = [];
        if (!is_array($collection)) {
            $data[] = $collection->size;
            $data[] = $collection->color;
        } else {
            $data[] = $collection['size'];
            $data[] = $collection['color'];
        }
        return $data;
    }

    /**
     * @return array
     */
    public function arrayAllToSizes(): array
    {
        return $this->arrayToCount($this->sizes, 'size');
    }

    /**
     * @return array
     */
    public function arrayAllToColors(): array
    {
        return $this->arrayToCount($this->colors, 'color');
    }

    /**
     * @param string $colors
     * @return array
     */
    public function getColors(string $colors): array
    {
        $data = [];

        $colors = explode(',', $colors);
        foreach ($colors as $key => $color) {
            $color = trim($color);
            $data[$key]['color'] = $this->colors[$color] ?: '#fff';
            $data[$key]['name'] = $color;
        }
        return $data;
    }

    /**
     * @param string $sizes
     * @return array
     */
    public function getSizes(string $sizes): array
    {
        $data = [];

        $sizes = explode(',', $sizes);
        foreach ($sizes as $key => $size) {
            $size = trim($size);
            $data[$key]['size'] = $size ?: '';
            $data[$key]['name'] = $size;
        }
        return $data;
    }

    /**
     * @param Basket $basket
     * @param array $productsBasket
     */
    public function decrementQuantity(Basket $basket, array $productsBasket)
    {
        $quantity = BasketsProduct::getQuantity($basket);
        foreach ($productsBasket as $product) {
            $features = $this->toFeatures($product);
            $colorSizeProduct = ColorSizeProduct::FindByFeatures($features, (int)$product['product_id'])->first();
            $colorSizeProduct->quantity = ($colorSizeProduct->quantity - $quantity);
            $colorSizeProduct->save();
        }
    }

    /**
     * @param array $items
     * @param string $column
     * @return array
     */
    private function arrayToCount(array $items, string $column): array
    {
        $data = [];
        foreach ($items as $key => $item) {
            $needle = $this->where($column, $item)->get();
            $count = count($needle);
            $data[$key]['item'] = $item;
            $data[$key]['count'] = $count;
            if ($column === 'color') {
                $data[$key]['color'] = $this->colors[$key];
            }
        }
        return  $data;
    }

    /**
     * @param $query
     * @param string $value
     * @param string $needle
     * @return Builder;
     */
    public function scopeJoinProducts($query, string $value, string $needle): Builder
    {
        return $query
            ->join('products', 'color_size_products.product_id', '=', 'products.id')
            ->select('products.*')
            ->where($needle, $value);
    }

    /**
     * @param $query
     * @param int $productID
     * @return Builder
     */
    public function scopeFindByProduct($query, int $productID): Builder
    {
        return $query->where('product_id', $productID);
    }

    /**
     * @param $query
     * @param array $features
     * @param int $productID
     * @return Builder
     */
    public function scopeFindByFeatures($query, array $features, int $productID): Builder
    {
        $size = trim($features[0]) ?: '';
        $color = trim($features[1]) ?: '';

        return $query->where('color', $color)->where('size', $size)->where('product_id', $productID);
    }
}
