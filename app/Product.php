<?php

namespace App;

use App\src\helpers\ConvertText;
use App\src\traits\ProductModelGettersSetters;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Product extends Model
{
    CONST TYPES_IMAGES = ['img_original', 'img_thumb', 'img_medium'];

    protected $fillable = ['name', 'slug', 'content', 'price', 'price_promo', 'promo', 'category_id', 'colors', 'sizes', 'user_id'];

    public $timestamps = true;

    use ProductModelGettersSetters;

    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(ImageProduct::class);
    }

    /**
     * @return HasMany
     */
    public function ColorSize(): HasMany
    {
        return $this->hasMany(ColorSizeProduct::class);
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return HasMany
     */
    public function wishlistProducts(): HasMany
    {
        return $this->hasMany(WishlistsProduct::class);
    }

    /**
     * @param string $slug
     * @return string
     */
    public static function setSlug(string $slug): string
    {
        return Str::slug($slug, '-');
    }

    /**
     * @return bool
     */
    public function existCategory(): bool
    {
        return Category::count() !== 0;
    }

    /**
     * @param array $data
     * @param User $user
     * @return mixed
     */
    public function canCreate(array $data, User $user)
    {
        [$message, $success] = $user->RenderResultsJson();
        if ($user->isAdmin()) {
            $data['colors'] = $this->editArgument($data['colors']);
            $data['sizes'] = $this->editArgument($data['sizes']);
            $this->create($data);
        }
        return [$message, $success];
    }

    /**
     * @param User $user
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function canDelete(User $user, Request $request)
    {
        if ($user->isAdmin()) {
            foreach ($this->images()->get() as $image) {
                unlink(ConvertText::linkImage($request, $image->{self::TYPES_IMAGES[0]}));
                unlink(ConvertText::linkImage($request, $image->{self::TYPES_IMAGES[1]}));
                unlink(ConvertText::linkImage($request, $image->{self::TYPES_IMAGES[2]}));
            }
            $this->delete();
        }
        return $user->isAdmin() ? ['Le produit a était supprimé avec succès !!!', true] : $user->RenderResultsJson();
    }

    /**
     * @param array $data
     * @param User $user
     * @return array
     */
    public function canEdit(array $data, User $user)
    {
        [$message, $success] = $user->RenderResultsJson(true);
        if ($user->isAdmin()) {
            $data['colors'] = $this->editArgument($data['colors']);
            $data['sizes'] = $this->editArgument($data['sizes']);
            $this->update($data);
        }
        return [$message, $success];
    }

    /**
     * @param array $arguments
     * @return string
     */
    private function editArgument(array $arguments = []): string
    {
        $end = '';
        foreach ($arguments as $key => $argument) {
            if ($key === count($arguments) - 1) {
                $end .= $argument;
            } else {
                $end .= $argument . ', ';
            }
        }
        return $end;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeGetLast($query)
    {
        $id = (int)Auth::id();
        return $query->where('user_id', $id)->orderBy('id', 'desc')->first();
    }

    /**
     * @param $query
     * @param int $id
     * @param string $property
     * @return Builder
     */
    public function scopeGetPropertyEloquent($query, int $id, string $property): Builder
    {
        return $query->select($property)->where('id', $id);
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeFindByUser($query): Builder
    {
        $userID = (integer)Auth::id();
        return $query->where('user_id', $userID);
    }

    /**
     * @param $query
     * @param int $categoryID
     * @return Builder
     */
    public function scopeFindByCategory($query, int $categoryID): Builder
    {
        return $query->where('category_id', $categoryID);
    }

    /**
     * @param $query
     * @param int $min
     * @param int $max
     * @return Builder
     */
    public function scopeGetBetween($query, int $min, int $max): Builder
    {
        return $query->where('price', '>=', $min)->where('price', '<=', $max);
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeGetDateLast($query): Builder
    {
        return $query->orderBy('created_at', 'DESC');
    }

    /**
     * @param $query
     * @param string $column
     * @return Builder
     */
    public function scopeOrderByAsc($query, string $column): Builder
    {
        return $query->orderBy($column, 'ASC');
    }

    /**
     * @param $query
     * @param string $column
     * @return Builder
     */
    public function scopeOrderByDesc($query, string $column): Builder
    {
        return $query->orderBy($column, 'DESC');
    }

    /**
     * @param $query
     * @param int $categoryID
     * @param Product $product
     * @return mixed
     */
    public function scopeRelated($query, int $categoryID, Product $product): Builder
    {
        return $query
            ->with('images')
            ->where('id', '!=', $product->id)
            ->where('category_id', (int)$categoryID)
            ->orderBy('created_at', 'DESC')
            ->limit(4);
    }

    /**
     * @param $query
     * @param Category $category
     * @param int|null $offset
     * @param int|null $limit
     * @return Builder
     */
    public function scopeFindItemsListing($query, Category $category, ?int $offset = null, ?int $limit = null): Builder
    {
        return $query
            ->with('images')
            ->where('category_id', (int)$category->id)
            ->offset($offset === null ? 0 : $offset)
            ->limit($limit === null ? 2 : $limit);
    }

    /**
     * @param $query
     * @param string $value
     * @param int|null $offset
     * @param int|null $limit
     * @return Builder
     */
    public function scopeSearchProduct($query, string $value, ?int $offset = null, ?int $limit = null): Builder
    {
        return $query
            ->with('images')
            ->where('name', 'like', '%' . $value . '%')
            ->offset($offset === null ? 0 : $offset)
            ->limit($limit === null ? 2 : $limit);
    }
}
