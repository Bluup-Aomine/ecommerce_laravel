<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    private string $email;
    /**
     * @var string
     */
    private string $content;
    /**
     * @var string
     */
    private string $name;

    /**
     * Create a new message instance.
     * @param string $name
     * @param string $email
     * @param string $content
     */
    public function __construct(string $name, string $email, string $content)
    {
        $this->email = $email;
        $this->content = $content;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): ContactMail
    {
        $fromEmail = app()->environment('production') ? '' : $this->email;
        $fromName = app()->environment('production') ? '' : $this->name;
        $toEmail = app()->environment('production') ? 'prod' : 'dev';
        $toName = app()->environment('production') ? 'prod' : 'dev';
        return $this
            ->from($fromEmail, $fromName)
            ->replyTo($this->email, $this->name)
            ->to($toEmail, $toName)
            ->subject('Contact')
            ->view('mail.contact')
            ->with([
                'content' => $this->content,
                'name' => $this->name
            ]);
    }
}
