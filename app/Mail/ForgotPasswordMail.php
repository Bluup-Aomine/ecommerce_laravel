<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User|null
     */
    private ?User $user;

    /**
     * Create a new message instance.
     * @param User|null $user
     */
    public function __construct(?User $user = null)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): ForgotPasswordMail
    {
        $fromEmail = app()->environment('production') ? 'prod' : 'dev';
        return $this
            ->from($fromEmail)
            ->to($this->user->email, $this->user->name)
            ->subject('Mot de passe oublié ?!')
            ->view('mail.forgot_password')
            ->with([
                'username' => $this->user->name,
                'token' => $this->user->confirmation_token
            ]);
    }
}
