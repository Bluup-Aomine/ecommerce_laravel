<?php

namespace App\Mail;

use App\Order;
use App\OrderPaypal;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class PurshaseProductMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Order|OrderPaypal
     */
    private $order;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $name;

    /**
     * Create a new message instance.
     * @param $order
     */
    public function __construct($order)
    {
        $this->order = $order;
        $this->email = Auth::user()->email;
        $this->name = Auth::user()->name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fromEmail = app()->environment('production') ? 'prod' : 'dev';
        return $this
            ->from($fromEmail)
            ->to($this->email, $this->name)
            ->subject('Achat de Produit')
            ->view('mail.purshase-product')
            ->with(['order' => $this->order]);
    }
}
