<?php

namespace App;

use App\src\traits\PasswordAction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, PasswordAction;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this && $this->role === 'admin';
    }

    /**
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    /**
     * @return HasMany
     */
    public function orders_paypal(): HasMany
    {
        return $this->hasMany(OrderPaypal::class);
    }

    /**
     * @return HasMany
     */
    public function customer(): HasMany
    {
        return $this->hasMany(Customer::class);
    }

    /**
     * @return HasMany
     */
    public function customer_paypal(): HasMany
    {
        return $this->hasMany(CustomerPaypal::class);
    }

    /**
     * @return HasMany
     */
    public function basket(): HasMany
    {
        return $this->hasMany(User::class);
    }

    /**
     * @return HasMany
     */
    public function account(): HasMany
    {
        return $this->hasMany(Account::class);
    }

    /**
     * @param bool $updated
     * @return array
     */
    public function RenderResultsJson(bool $updated = false): array
    {
        if (!$updated) {
            $message = $this->isAdmin() ? 'Ajout du produit effectué, vous pouvez maintenant ajouter des images à votre produit !!!' : 'Vous n\'avez pas le droit d\'effectuer cette action';
        } else {
            $message = $this->isAdmin() ? 'Modification du produit effectué, vous pouvez maintenant ajouter des images à votre produit !!!' : 'Vous n\'avez pas le droit d\'effectuer cette action';
        }
        $success = $this->isAdmin();
        return [$message, $success];
    }

    /**
     * @param array $dataUser
     * @return bool
     */
    public function updateDetails (array $dataUser): bool
    {
        return $this->update($dataUser);
    }

    /**
     * @param $query
     * @param string $email
     * @return Builder
     */
    public function scopeFindMail($query, string $email): Builder
    {
        return $query->where('email', $email);
    }

    /**
     * @param $query
     * @param string|null $token
     * @return Builder
     */
    public function scopeFindToken($query, ?string $token = null): Builder
    {
        return $query->where('confirmation_token', $token);
    }
}
