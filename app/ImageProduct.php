<?php

namespace App;

use App\src\helpers\ImageMove;
use App\src\traits\ImageProductAction;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ImageProduct extends Model
{
    protected $guarded = [];

    public $timestamps = true;

    use ImageProductAction;

    /**
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @param UploadedFile $uploadedFile
     * @return array
     * @throws BindingResolutionException
     */
    public function initImage(UploadedFile $uploadedFile): array
    {
        // Move Image in the directory choose !!!
        $imageMove = new ImageMove($uploadedFile);
        $imageMove->moveImage();
        return $imageMove->allDatas();
    }

    /**
     * @param int $productID
     * @param array $data
     * @return bool
     */
    public function createImage(int $productID, array $data = []): bool
    {
        [$name, $img_original, $img_medium, $img_thumb] = array_values($data);

        $this->name = $name ?: '';
        $this->img_original = asset($img_original) ?: '';
        $this->img_thumb = asset($img_thumb) ?: '';
        $this->img_medium = asset($img_medium) ?: '';
        $this->product_id = $productID ?: null;
        return $this->save();
    }

    /**
     * @param array $imagesID
     * @throws Exception
     */
    public function deleteImages(array $imagesID)
    {
        foreach ($imagesID as $id) {
            $image = ImageProduct::findOrFail((int)$id);

            $this->deleteImage($image);
            $this->unlinkImage($image);
        }
    }

    /**
     * @param $query
     * @param int|null $productID
     * @return mixed|array
     */
    public function scopeFindByProduct($query, ?int $productID)
    {
        if (!is_null($productID)) {
            return $query->where('product_id', (int)$productID)->get();
        }
        return [];
    }
}
