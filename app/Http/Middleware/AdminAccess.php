<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->role !== 'admin') {
            return redirect()->route('home')->with('messageError', 'Vous n\' avez pas le droit d\'accéder à cette page');
        }
        return $next($request);
    }
}
