<?php

namespace App\Http\Controllers;

use App\Category;
use App\Forms\CategoryForm;
use App\src\Validator\CategoryValidator;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Kris\LaravelFormBuilder\FormBuilder;

class CategoryController extends Controller
{

    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['create', 'edit', 'store', 'update']]);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $categories = Category::all();
        return view('categories.index', compact('categories'));
    }

    /**
     * @param Category $category
     */
    public function show(Category $category)
    {
        $category = Category::findOrfail((int)$category->id);
    }

    /**
     * @return Application|Factory|View
     */
    public function listening()
    {
        $categories = Category::all();
        return \view('categories.listening', compact('categories'));
    }

    /**
     * @param FormBuilder $formBuilder
     * @return Application|Factory|View
     */
    public function create(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(CategoryForm::class, [
            'method' => 'POST',
            'url' => route('category.store')
        ]);
        return view('categories.create', compact('form'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $validator = CategoryValidator::make($request);
        if ($validator->fails()) {
            return redirect()->route('category.create')
                ->with('message', 'Certaines informations sont incorrects !!!')
                ->withErrors($validator);
        }

        Category::create([
            'name' => $request->get('name'),
            'slug' => Str::slug($request->get('name'), '-'),
            'content' => $request->get('content')
        ]);

        return redirect()->route('category.index')->with('message', 'Vous avez ajouté une nouvelle catégorie avec succès !!!');
    }

    /**
     * @param FormBuilder $formBuilder
     * @param Category $category
     * @return Application|Factory|View
     */
    public function edit(FormBuilder $formBuilder, Category $category)
    {
        $form = $formBuilder->create(CategoryForm::class, [
            'method' => 'PUT',
            'url' => route('category.update', [$category])
        ]);
        return view('categories.edit', compact('form', 'category'));
    }

    /**
     * @param Request $request
     * @param Category $category
     * @return RedirectResponse
     */
    public function update(Request $request, Category $category)
    {
        $validator = CategoryValidator::make($request);
        if ($validator->fails()) {
            return redirect()->route('category.edit', [$category])
                ->with('message', 'Certaines informations sont incorrects !!!')
                ->with('errors', $validator->errors());
        }

        $category->name = $request->get('name');
        $category->slug = Str::slug($request->get('name'));
        $category->content = $request->get('content');
        $category->save();

        return redirect()->route('category.index')->with('message', 'Vous avez edité votre catégorie avec succès !!!');
    }
}
