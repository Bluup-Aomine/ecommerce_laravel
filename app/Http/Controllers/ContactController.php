<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\src\Validator\ContactValidator;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function contact()
    {
        return view('other.contact');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function post(Request $request): RedirectResponse
    {
        $validator = ContactValidator::make($request->all());
        if ($validator->fails()) {
            return redirect()
                ->route('contact')
                ->withErrors($validator)
                ->with('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        }
        $data = [
            'name' => $request->get('name') ?: '',
            'email' => $request->get('email') ?: '',
            'content' => $request->get('content') ?: ''
        ];

        Mail::send(new ContactMail($data['name'], $data['email'], $data['content']));
        return redirect()->route('contact')->with('message', 'Votre message à bien était envoyé !!!');
    }
}
