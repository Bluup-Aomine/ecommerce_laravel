<?php

namespace App\Http\Controllers;

use App\Basket;
use App\BasketsProduct;
use App\src\helpers\Price;
use App\src\helpers\Stripe;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;

class BasketController extends Controller
{
    /**
     * @var Stripe
     */
    private Stripe $stripe;

    /**
     * BasketController constructor.
     * @param Stripe $stripe
     */
    public function __construct(Stripe $stripe)
    {
        $this->stripe = $stripe;
    }

    /**
     * @return Application|Factory|RedirectResponse|View
     * @throws ApiErrorException
     */
    public function checkout()
    {
        if (Auth::guest()) {
            return redirect()
                ->route('my-account')
                ->with('messageError', 'Vous devez tout d\'abord vous connectez pour acheter vos différents produits !!!');
        }
        $total = Basket::total();
        if ($total <= 0) {
            return redirect()->route('home');
        }

        $amount = $total < 50 ? ($total + Price::shippingPrice($total)) : $total;
        $data = [
            'amount' => ($amount * 100),
            'currency' => 'eur'
        ];
        $intent = $this->stripe->paymentIntent($data);
        $payment_intent_id = $intent->id;

        $clientSecret = Arr::get($intent, 'client_secret');
        $products = $this->getBasketProducts();

        return view('baskets.checkout.checkout', compact('clientSecret', 'payment_intent_id', 'products', 'total'));
    }

    /**
     * @return array
     */
    private function getBasketProducts(): array
    {
        $basket = Basket::findByUser((int)Auth::id())->first();
        return BasketsProduct::getProduct($basket);
    }
}
