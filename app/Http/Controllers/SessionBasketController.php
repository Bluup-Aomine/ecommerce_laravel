<?php

namespace App\Http\Controllers;

use App\src\helpers\SessionBasket;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SessionBasketController
{
    /**
     * @var SessionBasket
     */
    private SessionBasket $sessionBasket;

    /**
     * SessionBasketController constructor.
     * @param SessionBasket $sessionBasket
     */
    public function __construct(SessionBasket $sessionBasket)
    {
        $this->sessionBasket = $sessionBasket;
    }

    /**
     * @return Application|Factory|View
     */
    public function basket()
    {
        $basketProducts = $this->sessionBasket->getProducts();
        $total = $this->sessionBasket->getTotal();
        $basket = null;
        return view('baskets.edit', compact('basket', 'basketProducts', 'total'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        $quantity = $request->get('quantity')[0] ?: 1;
        $basket = $request->get('session-basket') ?: 0;

        $this->sessionBasket->updateBasket((int)$quantity, (int)$basket);
        return redirect()
            ->route('product.basket.session')
            ->with('message', 'Votre panier à était modifié avec succés !!!');
    }
}
