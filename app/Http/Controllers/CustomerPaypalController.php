<?php

namespace App\Http\Controllers;

use App\CustomerPaypal;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CustomerPaypalController extends Controller
{
    /**
     * @var CustomerPaypal
     */
    private CustomerPaypal $customerPaypal;

    /**
     * CustomerPaypalController constructor.
     * @param CustomerPaypal $customerPaypal
     */
    public function __construct(CustomerPaypal $customerPaypal)
    {
        $this->customerPaypal = $customerPaypal;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $customers = CustomerPaypal::paginate(10);
        return view('template.admin.customers.index', compact('customers'));
    }
}
