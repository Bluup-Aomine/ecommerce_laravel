<?php

namespace App\Http\Controllers;

use App\Product;
use App\src\Errors\RenderMessageError;
use App\src\HTML\HtmlMessage;
use App\src\Validator\ProductWishlistValidator;
use App\Wishlist;
use App\WishlistsProduct;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ProductWishlistController extends Controller
{
    /**
     * @var HtmlMessage
     */
    private HtmlMessage $messageHTML;

    /**
     * ProductWishlistController constructor.
     * @param HtmlMessage $htmlMessage
     */
    public function __construct(HtmlMessage $htmlMessage)
    {
        $this->messageHTML = $htmlMessage;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $userID = (int)Auth::id();
        $wishlist = Wishlist::findByUser($userID)->first();
        $products = WishlistsProduct::getProductsWithImages($wishlist ? (int)$wishlist->id : null);

        return view('wishlists.index', compact('wishlist', 'products'));
    }

    /**
     * @param Request $request
     * @param Wishlist $wishlist
     * @return RedirectResponse
     */
    public function update(Request $request, Wishlist $wishlist): RedirectResponse
    {
        $validator = ProductWishlistValidator::make($request);
        if ($validator->fails()) {
            return redirect()
                ->route('product.create')
                ->withErrors($validator)
                ->with('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        }
        $wishlist = Wishlist::findOrFail((int)$wishlist->id);
        $wishlist->title = $request->get('title') ?: '';
        $wishlist->save();

        return redirect()->route('product.wishlist.index')->with('message', 'Vous avez modifié le titre de votre wishlist avec succés');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function add(Request $request): JsonResponse
    {
        $productID = $request->get('productID') ? \GuzzleHttp\json_decode($request->get('productID')) : null;
        $userID = Auth::id() ?: null;

        if (is_null($userID)) {
            return RenderMessageError::MessageResponseError('Vous devez vous connecté pour ajouter un produit à votre wishlist !!!');
        }
        $product = Product::findOrFail((integer)$productID);
        $wishlist = (new Wishlist())->add($userID);
        if ($wishlist) {
            $action = (new WishlistsProduct())->add($product, $wishlist);
            if (is_string($action)) {
                return RenderMessageError::MessageResponseError($action);
            }
        }

        $message = 'Vous avez ajouté le produit ' . $product->title . 'dans votre wishlist !!!';
        return response()->json([
            'success' => true,
            'data' => [
                'message' => $message,
                'messageJSON' => $this->messageHTML->messagePrimary(null, null, $message),
                'productID' => $productID
            ]
        ], 302);
    }

    /**
     * @param int $wishlistProductID
     * @return string
     */
    public function delete(int $wishlistProductID)
    {
        $wishlistProduct = WishlistsProduct::find($wishlistProductID);
        if ($wishlistProduct) {
            $product = Product::find((int)$wishlistProduct->product_id);

            $wishlistProduct->delete();
            $message = 'Vous avez supprimer le produit "' . $product->name . '" de votre wishlist !!!';
            return response()->json([
               'success' => true,
               'data' => [
                   'message' => $message,
                   'messageJSON' => $this->messageHTML->messagePrimary(null, null, $message)
               ]
            ], 302);
        }
        return RenderMessageError::MessageResponseError('Ce produit n\'est pas existant dans votre wishlist !!!');
    }
}
