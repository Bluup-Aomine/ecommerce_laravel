<?php

namespace App\Http\Controllers;

use App\Product;
use App\Wishlist;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $products = Product::with('wishlistProducts')->with('images')->limit(12)->get();
        $wishlist = Wishlist::find(Auth::id());
        return view('home', compact('products', 'wishlist'));
    }
}
