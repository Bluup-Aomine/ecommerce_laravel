<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PageController extends Controller
{

    /**
     * @return Application|Factory|View
     */
    public function errorCode404()
    {
        return view('pages.404');
    }

    /**
     * @return Application|Factory|View
     */
    public function CGV()
    {
        return view('pages.CGV');
    }

}
