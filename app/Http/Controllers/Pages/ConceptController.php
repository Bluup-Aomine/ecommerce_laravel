<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;

class ConceptController extends Controller
{
    public function index()
    {
        return view('pages.concept');
    }
}
