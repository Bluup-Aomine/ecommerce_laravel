<?php

namespace App\Http\Controllers;

use App\Forms\EditTrackingOrderForm;
use App\Order;
use App\src\helpers\ConvertPdf;
use App\src\helpers\Price;
use App\src\helpers\Stripe;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Kris\LaravelFormBuilder\FormBuilder;
use Spipu\Html2Pdf\Html2Pdf;
use Stripe\Exception\ApiErrorException;

class OrderController extends Controller
{
    /**
     * @var Stripe
     */
    private Stripe $stripe;
    /**
     * @var FormBuilder
     */
    private FormBuilder $formBuilder;

    /**
     * OrderController constructor.
     * @param FormBuilder $formBuilder
     */
    public function __construct(FormBuilder $formBuilder)
    {
        $this->stripe = new Stripe();
        $this->formBuilder = $formBuilder;
    }

    /**
     * Display a listing of the resource.
     * @return Application|Factory|View
     */
    public function index()
    {
        $orders = Order::paginate(10);
        $page = 'admin/orders';
        return view('orders.index', compact('orders', 'page'));
    }

    /**
     * @param Request $request
     * @param Order $order
     * @return void
     * @throws ApiErrorException
     */
    public function invoice(Request $request, Order $order)
    {
        $html2Pdf = new Html2Pdf();
        $convert = new ConvertPdf($html2Pdf);

        $file = $request->server->get('DOCUMENT_ROOT') . '/img/icons/logo-text.jpg';

        $payment = $this->stripe->getPaymentIntent($order->payment_indent_id);
        $customer = $this->stripe->getCustomer($payment->customer);
        $paymentMethod = $this->stripe->getPaymentMethod($payment->payment_method)->card;
        $priceTotal = Price::addTotal($order['products']);
        return $convert->pdf($order, $file, $customer, $paymentMethod, $priceTotal);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Order $order
     * @return Application|Factory|View|void
     */
    public function edit(Order $order)
    {
        $trackingOrder = $order->tracking_order ?: null;
        $form = $this->formBuilder->create(EditTrackingOrderForm::class, [
            'method' => 'PUT',
            'url' => route('orders.update', [$order]),
            'data' => ['tracking_order' => $trackingOrder]
        ]);
        return view('orders.edit', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Order $order
     * @return RedirectResponse
     */
    public function update(Request $request, Order $order): RedirectResponse
    {
        $updated = $order->update($request->all());
        if (!$updated) {
            return redirect()
                ->route('orders.index')
                ->with('messageError', 'An error has occurred !');
        }
        return redirect()
            ->route('orders.index')
            ->with('message', 'You are updated your tracking order with success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
