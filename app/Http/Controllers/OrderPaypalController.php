<?php

namespace App\Http\Controllers;

use App\CustomerPaypal;
use App\Forms\EditTrackingOrderForm;
use App\OrderPaypal;
use App\src\helpers\ConvertPdf;
use App\src\helpers\Price;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Kris\LaravelFormBuilder\FormBuilder;
use PayPal\Api\Payment;
use Spipu\Html2Pdf\Html2Pdf;

class OrderPaypalController extends Controller
{
    /**
     * @var Payment
     */
    private Payment $payment;
    /**
     * @var FormBuilder
     */
    private FormBuilder $formBuilder;

    /**
     * OrderPaypalController constructor.
     * @param Payment $payment
     * @param FormBuilder $formBuilder
     */
    public function __construct(Payment $payment, FormBuilder $formBuilder)
    {
        $this->payment = $payment;
        $this->formBuilder = $formBuilder;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $ordersPaypal = OrderPaypal::paginate(5);
        $page = 'admin/orders-paypal';
        return view('ordersPaypal.paypal-index', compact('ordersPaypal', 'page'));
    }

    /**
     * @param Request $request
     * @param OrderPaypal $order
     * @return void
     */
    public function invoice(Request $request, OrderPaypal $order)
    {
        $html2Pdf = new Html2Pdf();
        $convert = new ConvertPdf($html2Pdf);

        $file = $request->server->get('DOCUMENT_ROOT') . '/img/icons/logo-text.jpg';

        $customer = CustomerPaypal::find($order->customer_id)->first();
        $priceTotal = Price::addTotal($order['products']);
        return $convert->pdf($order, $file, $customer, null, $priceTotal);
    }

    /**
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit(int $id)
    {
        $orderPaypal = OrderPaypal::findOrFail($id);
        $trackingOrder = $orderPaypal->tracking_order ?: null;
        $form = $this->formBuilder->create(EditTrackingOrderForm::class, [
            'method' => 'PUT',
            'url' => route('orders.paypal.update', [$orderPaypal->id]),
            'data' => ['tracking_order' => $trackingOrder]
        ]);
        return view('ordersPaypal.edit', compact('form'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $orderPaypal = OrderPaypal::findOrFail($id);
        $updated = $orderPaypal->update($request->all());
        if (!$updated) {
            return redirect()->route('orders.paypal.index')->with('messageError', 'An error has occurred !');
        }
        return redirect()->route('orders.paypal.index')->with('message', 'You are updated your tracking order with success !');
    }
}
