<?php

namespace App\Http\Controllers;

use App\Basket;
use App\BasketsProduct;
use App\ColorSizeProduct;
use App\src\Errors\RenderMessageError;
use App\src\Exception\BasketException;
use App\src\Exception\ColorSizeProductException;
use App\src\helpers\Price;
use App\src\helpers\SessionBasket;
use App\src\HTML\Cart;
use App\src\HTML\HtmlMessage;
use App\WishlistsProduct;
use ErrorException;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class BasketProductController extends Controller
{
    /**
     * @var Basket
     */
    private Basket $basket;
    /**
     * @var BasketsProduct
     */
    private BasketsProduct $basketsProduct;
    /**
     * @var Cart
     */
    private Cart $cart;
    /**
     * @var SessionBasket
     */
    private SessionBasket $sessionBasket;
    /**
     * @var HtmlMessage
     */
    private HtmlMessage $htmlMessage;

    /**
     * BasketProductController constructor.
     * @param Basket $basket
     * @param BasketsProduct $basketsProduct
     * @param SessionBasket $sessionBasket
     */
    public function __construct(Basket $basket, BasketsProduct $basketsProduct, SessionBasket $sessionBasket)
    {
        $this->basket = $basket;
        $this->basketsProduct = $basketsProduct;
        $this->cart = new Cart();
        $this->htmlMessage = new HtmlMessage();
        $this->sessionBasket = $sessionBasket;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        //
    }

    /**
     * @param Basket $basket
     * @return Application|Factory|RedirectResponse|View
     */
    public function edit(Basket $basket)
    {
        $basket = Basket::find((integer)$basket->id);
        if (!Auth::user() || ($basket && (int)$basket->user_id !== (int)Auth::id())) {
            return redirect()->route('home')->with('message', 'Vous ne pouvez pas accèder à cette page !');
        }
        $basketProducts = BasketsProduct::getProduct($basket);
        $total = Price::addTotal($basketProducts);
        return view('baskets.edit', compact('basket', 'basketProducts', 'total'));
    }

    /**
     * @return JsonResponse
     */
    public function loadProductsBasket(): JsonResponse
    {
        if (!Auth::user()) {
            return $this->sessionBasket->responseLoadBasket($this->cart);
        }
        $basket = Basket::findByUser((integer)Auth::id())->first();
        $basketProducts = BasketsProduct::getProduct($basket);

        $total = Price::addTotal($basketProducts);
        $htmlBasketProducts = $this->cart->cart($basketProducts);
        $htmlTotalProducts = $this->cart->totalCart($total);

        return response()->json([
            'success' => true,
            'data' => [
                'products' => $htmlBasketProducts,
                'totalPrice' => $htmlTotalProducts,
                'count' => BasketsProduct::countElement($basketProducts)
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $productID = $request->get('productID') ? \GuzzleHttp\json_decode($request->get('productID')) : null;
        $quantity = $request->get('quantity') ? \GuzzleHttp\json_decode($request->get('quantity')) : 1;
        $features = $request->get('features') ? \GuzzleHttp\json_decode($request->get('features')) : [];
        $wishlist = $request->get('wishlist') ? true : false;

        if (empty($features)) {
            $colorSize = ColorSizeProduct::FindByProduct($productID)->first();
            if (is_null($colorSize)) {
                return $this->renderMessageErrorColorSize();
            }
            $features = (new ColorSizeProduct())->toFeatures($colorSize);
        }
        if (count($features) < 2) {
            return $this->renderMessageErrorColorSize();
        }

        $basket = $this->basket->add();
        try {
            // Check if the product isn't sold out
            $colorSizeProduct  = ColorSizeProduct::FindByFeatures($features, $productID)->first();
            if (($colorSizeProduct->quantity - (int)$quantity) < 0) {
                return $this->renderMessageRuptureStockQuantity();
            }
            // Method for add a product in the basket
            if (!is_null($basket)) {
                $basketProduct = BasketsProduct::findForBasketProduct((integer)$basket->id, (integer)$productID, (integer)$colorSizeProduct->id)->first();
                if ($this->basketsProduct->checkStockOut($basketProduct, $quantity, $colorSizeProduct->quantity)) {
                    return $this->renderMessageRuptureStockQuantity();
                }
                $this->basketsProduct->add((integer)$productID, (integer)$basket->id, (integer)$quantity, $features);
            } else {
                $this->sessionBasket->setBasket($productID, $quantity, $features);
                if ($this->sessionBasket->getErrorStock()) {
                    return $this->renderMessageRuptureStockQuantity();
                }
                return $this->sessionBasket->responseAddBasket($this->cart);
            }
        } catch (Exception $e) {
            if ($e instanceof ColorSizeProductException || $e instanceof ErrorException)  {
                $message = 'Aucunes de ces caractéristiques sont présentes sur ce produit !!!';
                $render = (new HtmlMessage())->jsonMessageError($e instanceof ColorSizeProduct ? $e->getMessage() : $message);
                return RenderMessageError::MessageResponseError($e instanceof ColorSizeProduct ? $e->getMessage() : $message, $render);
            }
        }
        if ($wishlist) {
            (new WishlistsProduct())->remove();
        }

        $lastProductBasket = BasketsProduct::getLastProduct($basket)[0];
        $basketProducts = BasketsProduct::getProduct($basket);

        $spanCartProduct = $this->cart->spanElementHTML($lastProductBasket, $lastProductBasket['quantity']);
        return response()->json([
            'success' => true,
            'data' => [
                'message' => 'Vous avez ajouté ce produit à votre panier !!!',
                'block' => $this->cart->addCartSuccess('Vous avez ajouté ce produit à votre panier !!!', $basket),
                'spanProduct' => $spanCartProduct,
                'product' => $this->cart->listProduct($lastProductBasket),
                'priceTotal' => Price::addTotal($basketProducts),
                'count' => BasketsProduct::countElement($basketProducts)
            ]
        ], 302);
    }

    /**
     * @param Request $request
     * @param Basket $basket
     * @return RedirectResponse
     */
    public function update(Request $request, Basket $basket): RedirectResponse
    {
        try {
            $quantities = $request->get('quantity') ?: [];

            $basket = Basket::findOrFail((integer)$basket->id);
            $basket->updateBasketProduct($quantities);
            return redirect()
                ->route('product.basket.edit', [$basket])
                ->with('message', 'Votre panier à était modifié avec succés !!!');
        } catch (Exception $exception) {
            if ($exception instanceof BasketException) {
                return redirect()
                    ->route('product.basket.edit', [$basket])
                    ->with('messageError', $exception->getMessage());
            }
        }
    }

    /**
     * @param int $basketsProductID
     * @return JsonResponse
     */
    public function delete(int $basketsProductID): JsonResponse
    {
        $basketProduct = BasketsProduct::find((integer)$basketsProductID);
        $basket = Basket::findByUser((integer)Auth::id())->first();

        if (!Auth::user()) {
            $this->sessionBasket->deleteProductBasket($basketsProductID);
            return response()->json([
                'success' => true,
                'data' => [
                    'message' => 'Vous avez supprimé ce produit à votre panier !!!',
                    'block' => $this->cart->deleteCartSuccess('Vous avez suprimé ce produit à votre panier !!!')
                ]
            ], 302);
        }
        if (is_null($basket) || is_null($basketProduct) || $basket->id !== (int)$basketProduct->basket_id) {
            return RenderMessageError::MessageResponseError('Ce produit n\'est pas existant dans votre panier !!!');
        }

        $basketProduct->delete();
        return response()->json([
            'success' => true,
            'data' => [
                'message' => 'Vous avez supprimé ce produit à votre panier !!!',
                'block' => $this->cart->deleteCartSuccess('Vous avez suprimé ce produit à votre panier !!!')
            ]
        ], 302);
    }

    /**
     * @return JsonResponse
     */
    private function renderMessageErrorColorSize(): JsonResponse
    {
        $message = 'La taille et la couleur du produit doit être selectionné pour être ajouté dans votre panier !!!';
        return RenderMessageError::MessageResponseError($message, $this->htmlMessage->jsonMessageError($message));
    }

    /**
     * @return JsonResponse
     */
    private function renderMessageRuptureStockQuantity(): JsonResponse
    {
        $message = 'Ce produit est en rupture de stock !';
        return RenderMessageError::MessageResponseError($message, $this->htmlMessage->jsonMessageError($message));
    }
}
