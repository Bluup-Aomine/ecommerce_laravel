<?php

namespace App\Http\Controllers\Admin;

use App\ColorSizeProduct;
use App\Forms\ColorSizeProductForm;
use App\Http\Controllers\Controller;
use App\Product;
use App\src\HTML\HtmlMessage;
use App\src\traits\Controller\ColorSizeApi;
use App\src\Validator\ColorSizeProductValidator;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Kris\LaravelFormBuilder\FormBuilder;

class ColorSizeProductController extends Controller
{
    use ColorSizeApi;

    /**
     * @var FormBuilder
     */
    private $formBuilder;
    /**
     * @var HtmlMessage
     */
    private HtmlMessage $htmlMessage;

    /**
     * ColorSizeProductController constructor.
     * @param FormBuilder $formBuilder
     */
    public function __construct( FormBuilder $formBuilder)
    {
        $this->formBuilder = $formBuilder;
        $this->htmlMessage = new HtmlMessage();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * @param Product $product
     * @return Application|Factory|View
     */
    public function create(Product $product)
    {
        $sizes = Product::getProperty($product->id, 'sizes');
        $colors = Product::getProperty($product->id, 'colors');
        $form = $this->formBuilder->create(ColorSizeProductForm::class, [
            'data' => [
                'sizes' => $sizes,
                'colors' => $colors
            ],
            'method' => 'POST',
            'url' => route('colors-sizes.store', [$product])
        ]);
        return view('template.admin.colorsSizeProducts.create', compact('form', 'product'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @param Product $product
     * @return RedirectResponse
     */
    public function store(Request $request, Product $product): RedirectResponse
    {
        $validator = ColorSizeProductValidator::make($request);
        if ($validator->fails()) {
            return redirect()
                ->route('colors-sizes.create', [$product])
                ->withErrors($validator)
                ->with('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        }
       $colorSize = new ColorSizeProduct();
       $added = $colorSize->add($request, $product);
       if (!$added) {
           return redirect()
               ->route('colors-sizes.create', [$product])
               ->with('messageError', 'Impossible de continuer la requête puisque vos caractéristiques ne correspond pas à ceux choisis dans votre produit !!!');
       }

       return redirect()->route('listening.products')->with('message', 'Vous avez ajouté une nouvelle caractéristique sur votre produit avec succès !!!');
    }

    /**
     * Display the specified resource.
     * @param ColorSizeProduct $colorSize
     * @return Response
     */
    public function show(ColorSizeProduct $colorSize)
    {

    }

    /**
     * Show the form for editing the specified resource.
     * @param Product $product
     * @param int $colorSizeProductID
     * @return Application|Factory|View|void
     */
    public function edit(Product $product, int $colorSizeProductID)
    {
        $colorSizeProduct = ColorSizeProduct::findOrFail($colorSizeProductID);
        $sizes = Product::getProperty($product->id, 'sizes');
        $colors = Product::getProperty($product->id, 'colors');
        $form = $this->formBuilder->create(ColorSizeProductForm::class, [
            'data' => [
                'sizes' => $sizes,
                'colors' => $colors,
                'colorSizeProduct' => $colorSizeProduct
            ],
            'method' => 'PUT',
            'url' => route('colors-sizes.update', [$product, $colorSizeProductID])
        ]);
        return view('template.admin.colorsSizeProducts.edit', compact('form', 'product'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Product $product
     * @param int $colorSizeProductID
     * @return RedirectResponse
     */
    public function update(Request $request, Product $product, int $colorSizeProductID)
    {
        $validator = ColorSizeProductValidator::make($request);
        if ($validator->fails()) {
            return redirect()
                ->route('colors-sizes.edit', [$product, $colorSizeProductID])
                ->withErrors($validator)
                ->with('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        }
        $colorSize = new ColorSizeProduct();
        $added = $colorSize->updateData($request, $product, $colorSizeProductID);
        if (!$added) {
            return redirect()
                ->route('colors-sizes.edit', [$product])
                ->with('messageError', 'Impossible de continuer la requête puisque vos caractéristiques ne correspond pas à ceux choisis dans votre produit !!!');
        }

        return redirect()->route('listening.products')->with('message', 'Vous avez bien modifié les caractéristiques sur votre produit avec succès !!!');
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteJSON(Request $request)
    {
        $colorSize = ColorSizeProduct::find((integer)$request->get('colorSizeID'));
        $product = Product::find($colorSize->product_id);
        if (is_null($colorSize)) {
            return response()->json([
                'success' => false,
                'message' => $this->htmlMessage->jsonMessageError('Ces caractéristiques pour le produit "' . $product->name . '" ne sont pas éxistant !!!')
            ]);
        }
        $colorSize->delete();
        return response()->json([
            'success' => true,
            'message' => $this->htmlMessage->messageJSON( 'Vous avez supprimé vos caractéristiques du produit "' . $product->name . '" !!!')
        ]);
    }
}
