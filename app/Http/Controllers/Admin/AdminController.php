<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderPaypal;
use App\Product;
use App\src\helpers\AvgOrders;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Stripe\Exception\ApiErrorException;

class AdminController extends Controller
{
    /**
     * @var AvgOrders
     */
    private AvgOrders $avgOrders;

    /**
     * AdminController constructor.
     * @param AvgOrders $avgOrders
     */
    public function __construct(AvgOrders $avgOrders)
    {
        $this->middleware('auth');
        $this->middleware('admin');
        $this->avgOrders = $avgOrders;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $products = Product::findByUser()->limit(10)->get();
        $orders = Order::last()->limit(10)->get();
        $ordersPaypal = OrderPaypal::last()->limit(10)->get();

        $countProducts = Product::count();
        $countCategories = Category::count();
        $countOrders = (Order::count() + OrderPaypal::count());

        $sumOrders = (((Order::sum('amount') / 100) + OrderPaypal::sum('amount')));
        $averageOrders = (float)number_format(($this->avgOrders->avg()), 2);
        $totalProducts = Order::countProducts(Order::all()) ?: 0;
        $user = Auth::user();

        $page = 'admin';
        return view('template/admin/index', compact('products', 'countProducts', 'countCategories', 'orders', 'ordersPaypal', 'page', 'countOrders', 'sumOrders', 'averageOrders', 'totalProducts', 'user'));
    }
}
