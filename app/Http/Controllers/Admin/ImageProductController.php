<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ImageProduct;
use App\Product;
use App\src\Errors\RenderMessageError;
use App\src\HTML\HtmlMessage;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ImageProductController extends Controller
{
    /**
     * @var HtmlMessage
     */
    private HtmlMessage $htmlMessage;

    /**
     * ImageProductController constructor.
     * @param HtmlMessage $htmlMessage
     */
    public function __construct(HtmlMessage $htmlMessage)
    {
        $this->htmlMessage = $htmlMessage;
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws BindingResolutionException
     */
    public function store(Request $request): RedirectResponse
    {
        // We created an fake product for recuperate after ID to product !!!
        // $this->create();
        // $productID = $this->getProductID();

        if ($request->has('file')) {
            $imageProduct = new ImageProduct();
            $data = $imageProduct->initImage($request->file('file'));

            // Create Image to Product in the Database
            $productIDJSON = $request->input('product_id');
            $productID = (int)$productIDJSON;
            $imageProduct->createImage($productID, $data);

            return redirect()->route('admin')->with('Vous avez ajouter vos images à votre produit avec succès !!!');
        }
        return redirect()->route('product.create')->with('error', 'Une erreur est survenue lors de la requête !!!');
    }

    /**
     * @param ImageProduct $imageProduct
     */
    public function destroy(ImageProduct $imageProduct)
    {
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteFiles(Request $request): JsonResponse
    {
        $options = $request->get('imagesID') ?: [];
        $productID = $request->get('productID') ?: null;

        $imagesID = \GuzzleHttp\json_decode($options);
        $productID = \GuzzleHttp\json_decode($productID);

        if (is_array($imagesID)) {
            $images = new ImageProduct();
            try {
                $images->deleteImages($imagesID);

                $message = 'Vous avez supprimer les ou l\'image(s) avec succès !!!';
                return response()->json([
                    'success' => true,
                    'message' => $message,
                    'flash' => $this->htmlMessage->messageJSON($message),
                    'images' => ImageProduct::FindByProduct((integer)$productID)
                ]);
            } catch (Exception $e) {
                return RenderMessageError::MessageErrorJson($e);
            }
        }
    }

    /**
     * @param Request $request
     */
    public function destroyJSON(Request $request)
    {
        $productID = $request->get('productID') ?: null;
        $imageProduct = ImageProduct::where('product_id', (int)$productID)->first();
        $imageProduct->delete();
    }
}
