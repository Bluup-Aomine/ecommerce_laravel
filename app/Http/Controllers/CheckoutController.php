<?php

namespace App\Http\Controllers;

use App\Basket;
use App\BasketsProduct;
use App\ColorSizeProduct;
use App\Customer;
use App\Mail\PurshaseProductMail;
use App\Order;
use App\src\helpers\Stripe;
use App\src\HTML\HtmlMessage;
use App\src\Validator\CheckoutValidator;
use DateTime;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentMethod;
use Stripe\StripeClient;
use Stripe\StripeObject;
use Stripe\Token;

class CheckoutController extends Controller
{
    /**
     * @var Stripe
     */
    private Stripe $stripe;

    /**
     * CheckoutController constructor.
     * @param Stripe $stripe
     */
    public function __construct(Stripe $stripe)
    {
        $this->stripe = $stripe;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ApiErrorException
     */
    public function updateAmountPaymentJSON(Request $request)
    {
        $country = $request->get('country') ?: 'FR';
        $paymentID = $request->get('payment_id') ?: null;
        $paymentMethod = $request->get('payment_method') ? \GuzzleHttp\json_decode($request->get('payment_method')) : null;

        $payment = $this->stripe->getPaymentIntent($paymentID);
        try {
            $this->stripe->updatePayment($payment->id, null, $paymentMethod->paymentMethod->card, $payment->amount, $country);
            return response()->json([
                'success' => true
            ], 302);
        } catch (Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], 302);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ApiErrorException
     */
    public function store (Request $request): JsonResponse
    {
        $validator = CheckoutValidator::make($request->all());
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validator->getMessageBag()
            ]);
        }
        $country = $request->get('country') ?: 'FR';
        $payment = $request->get('paymentIntent') ?: null;
        $data = $payment !== null ? \GuzzleHttp\json_decode($payment) : null;
        // Get Payment Method
        $paymentIntent = $this->stripe->getPaymentIntent($data->id);
        $card = $this->getCard($paymentIntent->payment_method);

        $order = new Order();
        $order->payment_indent_id = $paymentIntent->id ?: '';
        $order->amount = $paymentIntent->amount ?: 0;
        $order->payment_method = $card->brand ?: 'visa';

        $order->payment_indent_created = (new DateTime())
            ->setTimestamp($data->created)
            ->format('Y-m-d H:i:s');
        $order->order_notes = '';
        $order->tva = $country === 'FR' || $card->payment_method->country === 'FR';
        $order->tracking_order = 'en cours de traitement';

        $products = [];
        $userID = (integer)Auth::id();
        $basket = Basket::FindByUser($userID)->first();

        $productsBasket = BasketsProduct::getProduct($basket) ?: [];
        foreach ($productsBasket as $key => $product) {
            $products[$key]['name'] = $product['product'];
            $products[$key]['total'] = (integer)$product['total'];
            $products[$key]['price'] = (integer)$product['price'];
            $products[$key]['quantity'] = $product['quantity'];
            $products[$key]['image'] = $product['image'];
        }
        $order->products = serialize($products);
        $order->user_id  = $userID;
        $order->save();
        (new ColorSizeProduct())->decrementQuantity($basket, $productsBasket);

        $status = $data->status;
        if ($status === 'succeeded') {
            BasketsProduct::remove($basket);
        }

        if ($status !== 'succeeded') {
            return response()->json([
                'success' => false,
                'message' => (new HtmlMessage())->messagePrimary(null, null, 'Un problème a était survenue lors de votre achat !!!'),
                'payment_id' => $paymentIntent->id
            ], 302);
        }
        return response()->json(['success' => true], 302);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ApiErrorException
     */
    public function createCustomer(Request $request): JsonResponse
    {
        $data = array_slice($request->all(), 0, 3);
        $data['address'] = ['line1' => $request->get('address') ?: null];

        $customer = $this->stripe->customer($data);
        $order = Order::FindUserLast((integer)Auth::id())->first();
        // Add a new customer for stripe
        $model = new Customer();
        $model->add($customer, $order->amount);
        // Update payment order with stripe
        $this->stripe->updatePayment($request->get('payment_id'), $customer);
        $order->order_notes = $request->get('order_notes') ?: '';
        $order->save();

        $this->setEmail();
        return response()->json([
            'success' => true,
            'message' => (new HtmlMessage())->messagePrimary(null, null, 'Votre commande a bien était effectué !!!')
        ], 302);
    }

    /**
     * @param string $id
     * @return StripeObject
     * @throws ApiErrorException
     */
    private function getCard(string $id): StripeObject
    {
        return $this->stripe->getPaymentMethod($id)->card;
    }

    /**
     * @return void
     */
    protected function setEmail(): void
    {
        $order = Order::findUserLast((int)Auth::id())->first();
        Mail::send(new PurshaseProductMail($order));
    }
}
