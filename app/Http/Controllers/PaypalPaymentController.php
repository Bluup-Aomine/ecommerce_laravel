<?php

namespace App\Http\Controllers;

use App\Basket;
use App\BasketsProduct;
use App\OrderPaypal;
use App\src\helpers\Paypal\ApiPaypal;
use App\src\helpers\Paypal\TransactionFactory;
use App\src\helpers\Price;
use App\src\helpers\Split;
use App\src\HTML\HtmlMessage;
use App\src\Validator\CheckoutValidator;
use Config;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;

class PaypalPaymentController extends Controller
{
    /**
     * @var ApiContext
     */
    private ApiContext $apiContext;
    /**
     * @var OrderPaypal
     */
    private OrderPaypal $orderPaypal;

    /**
     * PaypalPaymentController constructor.
     * @param OrderPaypal $orderPaypal
     */
    public function __construct(OrderPaypal $orderPaypal)
    {
        $this->apiContext = (new ApiPaypal())->api();
        $this->orderPaypal = $orderPaypal;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $validator = CheckoutValidator::make($request->all());
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validator->getMessageBag()
            ]);
        }
        $shipping = Price::addTotal(BasketsProduct::getProduct($this->basket())) > 50 ? 0.1 : 0;
        $transaction = TransactionFactory::fromBasket($this->basket(), 0, $shipping);

        $payment = new Payment();
        $payment->addTransaction($transaction);
        $payment->setIntent('sale');
        $redirectUrls = (new RedirectUrls())
            ->setReturnUrl(route('paypal.payment.sale'))
            ->setCancelUrl(route('checkout'));
        $payment->setRedirectUrls($redirectUrls);
        $payment->setPayer((new Payer())->setPaymentMethod('paypal'));

        try {
            $payment->create($this->apiContext);
            return response()->json([
                'id' => $payment->getId(),
                'success' => true
            ]);
        } catch (PayPalConnectionException $exception) {
            $dataError = \GuzzleHttp\json_decode($exception->getData());
            if ($dataError->name === 'VALIDATION_ERROR') {
                return response()->json([
                    'success' => false,
                    'message' => (new HtmlMessage())->messageJSON('Une erreur est survenue lors de la transaction de votre commande !', false)
                ], 302);
            }
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function payment(Request $request)
    {
        $paymentID = $request->json('paymentID') ?: null;
        $payerID = $request->json('payerID') ?: null;
        $other_notes = $request->json('other_notes') ?: null;

        $payment = Payment::get($paymentID, $this->apiContext);
        // Update Payer With credentials request
        $payment->getPayer()->getPayerInfo()->setEmail($request->get('email'));
        $payment->getPayer()->getPayerInfo()->setFirstName(Split::cut(' ', $request->get('name'), 0));
        $payment->getPayer()->getPayerInfo()->setLastName(Split::cut(' ', $request->get('name'), 1));
        $payment->getPayer()->getPayerInfo()->getShippingAddress()->setLine1($request->get('address'));
        $payment->getPayer()->getPayerInfo()->getShippingAddress()->setCountryCode($request->get('country'));

        $shipping = Price::addTotal(BasketsProduct::getProduct($this->basket())) > 50 ? 0.1 : 0;
        $country = $payment->getPayer()->getPayerInfo()->getCountryCode();
        $execution = (new PaymentExecution())
            ->setPayerId($payerID)
            ->addTransaction(TransactionFactory::fromBasket($this->basket(), $country === 'FR' ? 0.2 : 0, $shipping));
        $customer = $this->orderPaypal->setCustomer($payment, $request->get('phone'));

        try {
            $payment->execute($execution, $this->apiContext);
            $this->orderPaypal->setOrder($payment, $customer, $country, $other_notes, $this->basket());

            BasketsProduct::remove($this->basket());
            return response()->json([
                'id' => $payment->getId(),
                'success' => true,
                'message' => (new HtmlMessage())->messagePrimary(null, null, 'Votre achat a était effectué avec succès !!!')
            ], 302);
        } catch (PayPalConnectionException $exception) {;
            if ($exception->getData()->name === 'VALIDATION_ERROR') {
                return response()->json([
                    'success' => false,
                    'message' => (new HtmlMessage())->messageJSON('Une erreur est survenue lors de la transaction de votre commande !', false)
                ], 302);
            }
        }
    }

    /**
     * @return Basket|null
     */
    private function basket(): ?Basket
    {
       return Basket::FindByUser((int)Auth::id())->first();
    }
}
