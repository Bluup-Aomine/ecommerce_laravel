<?php

namespace App\Http\Controllers;

use App\Account;
use App\Order;
use App\OrderPaypal;
use App\src\Errors\RenderMessageError;
use App\src\Validator\AccountDetailsValidator;
use App\src\Validator\ChangePasswordAccountValidator;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AccountController extends Controller
{

    /**
     * @var Account
     */
    private Account $account;

    /**
     * AccountController constructor.
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * Display a listing of the resource.
     * @return Application|Factory|View
     */
    public function index()
    {
        $user = Auth::user();
        $account = $user ? $user->account()->first() : null;

        $orders = Order::all();
        $ordersPaypal = OrderPaypal::all();

        return view('auth.account.account', compact('user', 'account', 'orders', 'ordersPaypal'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function postAccount(Request $request): RedirectResponse
    {
        $validator = AccountDetailsValidator::make($request->all());
        if ($validator->fails()) {
            return redirect()
                ->route('my-account')
                ->withErrors($validator)
                ->with('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        }
        $data = $this->setDataAccount($request);
        $action = $this->account->updateData($data);
        if (!$action) {
            return redirect()->route('my-account')->with('message', 'Une erreur est brutalement survenue, veuillez réessayer !!!');
        }
        return redirect()->route('my-account')->with('message', 'Vos information ont bien était modifié !!!');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function changePassword(Request $request): RedirectResponse
    {
        $validator = ChangePasswordAccountValidator::make($request->all());
        if ($validator->fails()) {
            return RenderMessageError::MessageErrorWithSession('Impossible de continuer la requête suite à certaines informations invalides !!!', $validator, 'my-account');
        }
        $password = $request->get('password') ?: null;
        $verifyPassword = Auth::user()->goodPassword($password);
        if (!$verifyPassword) {
            return RenderMessageError::MessageErrorWithSession('Votre mot de passe inscrit dans le formulaire n\'est pas identique à celui de notre base de donnée !!!', $validator, 'my-account');
        }
        $action = Auth::user()->setPasswords($request);
        if (!$action) {
            return RenderMessageError::MessageErrorWithSession('Votre nouveau mot de passe doit être égale au mot de passe confirmation !!!', $validator, 'my-account');
        }
        return redirect()->route('my-account')->with('message', 'Votre mot de passe à bien était modifié !!!');
    }

    /**
     * @param Request $request
     * @return array
     */
    private function setDataAccount(Request $request): array
    {
        return [
            'firstname' => $request->get('firstname') ?: '',
            'lastname' => $request->get('lastname') ?: '',
            'name' => $request->get('username') ?: '',
            'email' => $request->get('email') ?: ''
        ];
    }
}
