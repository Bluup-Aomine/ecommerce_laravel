<?php

namespace App\Http\Controllers;

use App\Customer;
use App\src\helpers\Stripe;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class CustomerController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $customers = Customer::paginate(10);
        $stripe = new Stripe();
        return view('template.admin.customers.index', compact('customers', 'stripe'));
    }
}
