<?php

namespace App\Http\Controllers;

use App\Category;
use App\ColorSizeProduct;
use App\Forms\ProductsForm;
use App\Forms\ProductsImagesForm;
use App\ImageProduct;
use App\Product;
use App\src\HTML\HtmlMessage;
use App\src\traits\Controller\ErrorProductsController;
use App\src\traits\Controller\SearchProduct;
use App\src\Validator\ProductValidator;
use App\User;
use App\WishlistsProduct;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Kris\LaravelFormBuilder\FormBuilder;

class ProductController extends Controller
{

    use SearchProduct, ErrorProductsController;

    /**
     * @var FormBuilder
     */
    private FormBuilder $formBuilder;
    /**
     * @var ColorSizeProduct
     */
    private ColorSizeProduct $colorSizeProduct;
    /**
     * @var HtmlMessage
     */
    private HtmlMessage $htmlMessage;

    /**
     * ProductController constructor.
     * @param FormBuilder $formBuilder
     * @param ColorSizeProduct $colorSizeProduct
     * @param HtmlMessage $htmlMessage
     */
    public function __construct(FormBuilder $formBuilder, ColorSizeProduct $colorSizeProduct, HtmlMessage $htmlMessage)
    {
        $this->middleware('auth', ['only' => ['create', 'store', 'listening', 'edit', 'update']]);
        $this->formBuilder = $formBuilder;
        $this->colorSizeProduct = $colorSizeProduct;
        $this->htmlMessage = $htmlMessage;
    }

    public function index()
    {
        return null;
    }

    /**
     * Display a listing of the resource.
     * @param Category $category
     * @return Application|Factory|View
     */
    public function collections(Category $category)
    {
        $productsHead = Product::FindItemsListing($category)->get();
        $productsBody = Product::FindItemsListing($category, 2, 3)->get();
        $productsFooter = Product::FindItemsListing($category, 5, 2)->get();

        return view('products.index', compact('productsHead', 'productsBody', 'productsFooter', 'category'));
    }

    /**
     * @return Application|Factory|View
     */
    public function listening()
    {
        $products = Product::with('category')->with('ColorSize')->get();
        $page = 'admin/products';
        return view('products.listening', compact('products', 'page'));
    }

    /**
     * @param Category $category
     * @return Application|Factory|View
     */
    public function listeningCategories(Category $category)
    {
        $products = Product::where('category_id', (int)$category->id)
            ->with('category')
            ->get();
        $colorSizeProducts = ColorSizeProduct::all();
        return view('products.listening-categories', compact('products', 'category', 'colorSizeProducts'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Application|Factory|View
     */
    public function create()
    {
        $form = $this->formBuilder->create(ProductsForm::class, [
            'method' => 'POST',
            'url' => route('product.store'),
            'data' => ['updated' => false]
        ]);
        $formDropzone = $this->formBuilder->create(ProductsImagesForm::class, [
            'method' => 'POST',
            'url' => route('image-product.store'),
            'class' => 'dropzone dz-clickable',
            'enctype' => 'multipart/form-data',
            'data' => ['product' => null]
        ]);
        return view('products.create', compact('form', 'formDropzone'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $product = new Product();
        $existing = $product->existCategory();
        if (!$existing) {
            return redirect()
                ->route('product.create')
                ->with('messageError', 'Vous devez tout d\'abord créer une catégorie afin de l\'associer à votre produit !!!');
        }

        $validator = ProductValidator::make($request);
        if ($validator->fails()) {
            return redirect()
                ->route('product.create')
                ->withErrors($validator)
                ->with('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        }

        $user = User::findOrFail((int)Auth::id());
        $data = [
            'name' => $request->get('name'),
            'slug' => Product::setSlug($request->get('name')),
            'content' => $request->get('content'),
            'price' => (int)$request->get('price'),
            'promo' => (int)$request->get('promo') === 1,
            'price_promo' => (int)$request->get('promo') === 1 ? (int)$request->get('price_promo') : null,
            'colors' => $request->get('colors'),
            'sizes' => $request->get('sizes'),
            'category_id' => (int)$request->get('category_id'),
            'user_id' => $user->id
        ];
        $this->promoPriceTooBig($data['price'], $data['price_promo']);
        if (!$this->isValidate()) {
            return redirect()->route('product.create')->with('messageError', $this->getError());
        }
        [$message, $success] = $product->canCreate($data, $user);

        return redirect()
            ->route('product.create')
            ->with('filed', $success ? true : false)
            ->with('productID', $success ? (int)Product::getLast()->id : null)
            ->with($success ? 'message' : 'messageError', $message);
    }

    /**
     * Display the specified resource.
     * @param  Product $product
     * @return Application|Factory|View
     */
    public function show(Product $product)
    {
        $product = Product::with('category')->findOrFail((integer)$product->id);
        $productsRelated = Product::Related($product->category->id, $product)->get();

        $wishlist = WishlistsProduct::getByProduct($product);
        $sizes = explode(',', $product->sizes);
        $colors = $this->colorSizeProduct->getColors($product->colors);
        return view('products.show', compact('product', 'productsRelated', 'wishlist', 'colors', 'sizes'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param  Product  $product
     * @return Application|Factory|View
     */
    public function edit(Product $product)
    {
        $form = $this->formBuilder->create(ProductsForm::class, [
            'method' => 'PUT',
            'url' => route('product.update', [$product]),
            'data' => ['updated' => true, 'product' => $product]
        ]);
        $formDropzone = $this->formBuilder->create(ProductsImagesForm::class, [
            'method' => 'POST',
            'url' => route('image-product.store'),
            'class' => 'dropzone dz-clickable',
            'enctype' => 'multipart/form-data',
            'data' => ['product' => $product]
        ]);
        $images = ImageProduct::where('product_id', (int)$product->id)->get();
        return view('products.edit', compact('form', 'formDropzone', 'images', 'product'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param  Product  $product
     * @return RedirectResponse
     */
    public function update(Request $request, Product $product): RedirectResponse
    {
        $validator = ProductValidator::make($request);
        if ($validator->fails()) {
            return redirect()
                ->route('product.edit', [$product])
                ->withErrors($validator)
                ->with('messageError', 'Impossible de continuer la requête suite à certaines informations invalides !!!');
        }

        $user = User::find((int)$product->user_id);
        $data = [
            'name' => $request->get('name'),
            'slug' => Product::setSlug($request->get('name')),
            'content' => $request->get('content'),
            'price' => (int)$request->get('price'),
            'promo' => (int)$request->get('promo') === 1,
            'colors' => $request->get('colors'),
            'sizes' => $request->get('sizes'),
            'category_id' => (int)$request->get('category_id'),
        ];

        [$message, $success] = $product->canEdit($data, $user);
        return redirect()
            ->route('product.edit', [$product])
            ->with('productID', $success ? (int)$product->id : null)
            ->with($success ? 'message' : 'messageError', $message);
    }

    /**
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function deleteJSON(Request $request)
    {
        $product = Product::find((integer)$request->get('productID'));
        if (is_null($product)) {
            return response()->json([
                'success' => false,
                'message' => $this->htmlMessage->jsonMessageError('Ce produit "' . $product->name . '" n\'est pas éxistant !!!')
            ]);
        }
        return $this->destroy($request, $product, true);
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param Product|null $product
     * @param bool $toJSON
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function destroy(Request $request, Product $product, bool $toJSON = false)
    {
        $user = User::findOrFail((integer)$request->get('userID'));
        [$message, $success] = $product->canDelete($user, $request);
        if ($toJSON) {
            return response()->json([
                'success' => true,
                'message' => $this->htmlMessage->messageJSON( 'Vous avez supprimé votre produit "' . $product->name . '" !!!')
            ]);
        }

        return redirect()
            ->route('product.index')
            ->with($success ? 'message' : 'messageError', $message);
    }
}
