<?php

namespace App\Http\Controllers;

use App\Category;
use App\ColorSizeProduct;
use App\Product;
use App\src\Errors\RenderMessageError;
use App\src\HTML\Products;
use App\src\traits\Controller\ProductFilterSelected;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductFilterController extends Controller
{

    use ProductFilterSelected;

    /**
     * @param string $slug
     * @return JsonResponse
     */
    public function category(string $slug): JsonResponse
    {
        $category = Category::findBySlug($slug)->first();
        if ($category) {
            $products = Product::findByCategory($category->id)->with('images')->paginate(12);
            $itemsProducts = (new Products())->renderItem($products);
            $itemsFilters = (new Products())->renderItemFilter($category->name);

            return response()->json([
                'success' => true,
                'data' => [
                    'products' => $itemsProducts,
                    'itemsFilters' => $itemsFilters,
                ]
            ], 302);
        }

        return RenderMessageError::MessageResponseError('Cette catégorie n\'est pas existante dans notre base de donnée !!!');
    }

    /**
     * @return JsonResponse
     */
    public function clear(): JsonResponse
    {
        $products = Product::paginate(12);
        $itemsProducts = (new Products())->renderItem($products);
        return response()->json([
            'success' => true,
            'data' => [
                'products' => $itemsProducts
            ]
        ], 302);
    }

    /**
     * @param int $index
     * @return JsonResponse
     */
    public function page(int $index)
    {
        $products = Product::with('images')->paginate($index);
        $itemsProducts = (new Products())->renderItem($products);
        return response()->json([
            'success' => true,
            'data' => [
                'products' => $itemsProducts
            ]
        ], 302);
    }

    /**
     * @param string $size
     * @return JsonResponse
     */
    public function size(string $size): JsonResponse
    {
        $products = ColorSizeProduct::joinProducts($size, 'size')->paginate(12);
        $itemsProducts = (new Products())->renderItem($products);
        return response()->json([
            'success' => true,
            'data' => [
                'products' => $itemsProducts
            ]
        ], 302);
    }

    /**
     * @param string $color
     * @return JsonResponse
     */
    public function color(string $color): JsonResponse
    {
        $products = ColorSizeProduct::joinProducts($color, 'color')->paginate(12);
        $itemsProducts = (new Products())->renderItem($products);
        return response()->json([
            'success' => true,
            'data' => [
                'products' => $itemsProducts
            ]
        ], 302);
    }

    /**
     * @param int $min
     * @param int $max
     * @return JsonResponse
     */
    public function price(int $min, int $max): JsonResponse
    {
        $products = Product::GetBetween($min, $max)->paginate(12);
        $itemsProducts = (new Products())->renderItem($products);
        $itemsFiltersPrice = (new Products())->renderItemFilterPrice($min, $max);

        return response()->json([
            'success' => true,
            'data' => [
                'products' => $itemsProducts,
                'itemsFiltersPrice' => $itemsFiltersPrice,
            ]
        ], 302);
    }
}
