<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class EditTrackingOrderForm extends Form
{
    public function buildForm()
    {
        $trackingOrder = $this->getData('tracking_order') ?: null;

        $this->add('tracking_order', 'select', [
            'attr' => [
                'class' => 'form-control form-input-light'
            ],
            'choices' => ['en cours de traitement' => 'En cours de traitement', 'envoyer' => 'Envoyer', 'reçu' => 'Reçu'],
            'empty_value' => 'Choose tracking order',
            'value' => $trackingOrder
        ]);

        $this->add('submit', 'submit', [
            'attr' => [
                'class' => 'btn btn-primary btn-submit-primary'
            ],
            'label' => 'Send'
        ]);
    }
}
