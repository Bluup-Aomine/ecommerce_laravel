<?php

namespace App\Forms;

use App\Category;
use App\Product;
use Kris\LaravelFormBuilder\Form;

class ProductsForm extends Form
{
    /**
     * @var array
     */
    private array $choicesColors = [];
    /**
     * @var array
     */
    private array $choicesSizes = [];
    /**
     * @var array
     */
    private array $choicesCategories = [];
    /**
     * @var Product|null
     */
    private ?Product $product;

    /**
     * ProductsForm constructor.
     */
    public function __construct()
    {
        $this->setChoicesColors();
        $this->setChoicesSizes();
        $this->setChoiceCategories();
    }

    public function buildForm()
    {
        // Add fields here...
        if ($this->getData('updated')) {
            $this->product = $this->getData('product');
        } else {
            $this->product = null;
        }
        $this->add('name', 'text', [
            'attr' => [
                'class' => 'form-control form-input-light'
            ],
            'label' => 'Product Name',
            'value' => $this->product ? $this->product->name : ''
        ]);

        $this->add('price', 'number', [
            'attr' => [
                'class' => 'form-control form-input-light'
            ],
            'label' => 'Price',
            'value' => $this->product ? $this->product->getPriceOriginal() : 0
        ]);

       $this->add('category_id', 'select', [
            'attr' => [
                'class' => 'form-control form-input-light'
            ],
            'label' => 'Categories',
            'choices' => $this->choicesCategories,
            'selected' => $this->product ? $this->product->category_id : null
       ]);

        $this->add('colors', 'choice', [
            'attr' => [
                'class' => 'form-control form-input-light'
            ],
            'multiple' => true,
            'label' => 'Couleurs',
            'choices' => $this->choicesColors,
            'selected' => $this->product ? explode(', ', $this->product->colors) : null
        ]);

        $this->add('sizes', 'choice', [
            'attr' => [
                'class' => 'form-control form-input-light'
            ],
            'multiple' => true,
            'label' => 'Tailles',
            'choices' => $this->choicesSizes,
            'selected' => $this->product ? explode(', ', $this->product->sizes) : null
        ]);

        $this->add('content', 'textarea', [
            'attr' => [
                'class' => 'form-control form-input-light',
                'rows' => 15,
                'cols' => 5
            ],
            'label' => 'Product Description',
            'value' => $this->product ? $this->product->content : ''
        ]);

        $this->add('promo', 'checkbox', [
            'wrapper' => ['class' => 'custom-control custom-checkbox custom-control-top'],
            'attr' => [
                'class' => 'custom-control-input field-toggle-disabled',
                'data-promo-active' => is_null($this->product) ? 'false' : (string)$this->product->promo,
                'checked' => is_null($this->product) ? false : (boolean)$this->product->promo
            ],
            'label' => 'Promo ?',
            'label_attr' => ['class' => 'custom-control-label']
        ]);

        $attrPricePromo = [
            'class' => 'form-control form-input-light',
            'id' => 'field-disabled',
        ];
        if (is_null($this->product)) {
            $attrPricePromo['disabled'] = true;
        } else {
            $attrPricePromo['disabled'] = $this->product->promo ? false : true;
        }
        $this->add('price_promo', 'number', [
            'attr' => $attrPricePromo,
            'label' => 'Price Promo',
            'value' => $this->product ? $this->product->price_promo : 0
        ]);

        $this->add('submit', 'submit', [
            'attr' => [
                'class' => 'btn btn-primary btn-submit-primary'
            ],
            'label' => 'Envoyer'
        ]);
    }

    private function setChoicesColors()
    {
        $this->choicesColors['blue'] = 'Bleu';
        $this->choicesColors['red'] = 'Rouge';
        $this->choicesColors['black'] = 'Noir';
        $this->choicesColors['white'] = 'Blanc';
        $this->choicesColors['green'] = 'Vert';
        $this->choicesColors['belgian'] = 'Belge';
        $this->choicesColors['orange'] = 'Orange';
        $this->choicesColors['yellow'] = 'Jaune';
    }

    private function setChoicesSizes()
    {
        $this->choicesSizes['xs'] = 'XS';
        $this->choicesSizes['s'] = 'S';
        $this->choicesSizes['m'] = 'M';
        $this->choicesSizes['l'] = 'L';
        $this->choicesSizes['xl'] = 'XL';
    }

    private function setChoiceCategories()
    {
        $categories = Category::all();
        foreach ($categories as $category) {
            $this->choicesCategories[(int)$category->id] = $category->name ?: null;
        }
    }
}
