<?php

namespace App\Forms;

use App\Product;
use Kris\LaravelFormBuilder\Form;

class ProductsImagesForm extends Form
{
    public function buildForm()
    {
        // Add fields here...
        $product = $this->getData('product');
        $this->add('product_id', 'hidden', [
            'attr' => ['id' => 'productID'],
            'value' => !is_null($product) ? $product->id : Product::GetLast()->id
        ]);
    }
}
