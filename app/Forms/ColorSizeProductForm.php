<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ColorSizeProductForm extends Form
{
    public function buildForm()
    {
        $colorSizeProduct = $this->getData('colorSizeProduct') ?: null;

        // Add fields here...
        $this->add('colors', 'choice', [
            'attr' => [
                'class' => 'form-control form-input-light'
            ],
            'selected' => $colorSizeProduct ? [$colorSizeProduct->color] : null,
            'label' => 'Couleur',
            'choices' => $this->getData('colors')
        ]);
        $this->add('sizes', 'choice', [
            'attr' => [
                'class' => 'form-control form-input-light'
            ],
            'selected' => $colorSizeProduct ? [$colorSizeProduct->size]: null,
            'label' => 'Taille',
            'choices' => $this->getData('sizes')
        ]);
        $this->add('quantity', 'number', [
            'attr' => [
                'class' => 'form-control form-input-light'
            ],
            'value' => $colorSizeProduct ? $colorSizeProduct->quantity : null,
            'label' => 'Quantité'
        ]);
        $this->add('submit', 'submit', [
            'attr' => [
                'class' => 'btn btn-submit-primary btn-primary'
            ]
        ]);
    }
}
