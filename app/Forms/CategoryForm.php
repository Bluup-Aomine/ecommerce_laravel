<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CategoryForm extends Form
{
    public function buildForm()
    {
        // Add fields here...
        $this->add('name', 'text', [
            'placeholder' => 'Name...',
            'attr' => [
                'class' => 'form-control form-input-light'
            ]
        ]);
        $this->add('content', 'textarea', [
            'placeholder' => 'Votre contenu...',
            'attr' => [
                'class' => 'form-control form-input-light'
            ]
        ]);

        $this->add('submit', 'submit', [
            'attr' => [
                'class' => 'btn btn-primary btn-submit-primary'
            ]
        ]);
    }
}
