<?php

namespace App;

use App\src\Exception\BasketException;
use App\src\helpers\Price;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class Basket extends Model
{
    protected $fillable = ['user_id'];

    public $timestamps = true;

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Recuperate the total to price in the basket
     */
    public static function total()
    {
        $basket = self::getBasket();
        $total = 0;
        if ($basket) {
            $items = BasketsProduct::getProduct($basket);
            foreach ($items as $item) {
                $total += $item['total'];
            }
        }
        return $total;
    }

    /**
     * @param float|int $vatRate
     * @return false|float
     */
    public static function getVatPrice(float $vatRate = 0)
    {
        $basket = self::getBasket();
        $products = BasketsProduct::getProduct($basket);
        return round(Price::addTotal($products) * $vatRate * 100) / 100;
    }

    /**
     * @return Basket|null
     */
    public function add(): ?Basket
    {
        if (Auth::user()) {
            $basket = self::getBasket();
            if ($basket) {
                return $basket;
            }
            $this->user_id = (integer)Auth::id();
            $this->save();
            return $this;
        }
        return null;
    }

    /**
     * @param string[] $quantities
     * @return bool
     * @throws BasketException
     */
    public function updateBasketProduct($quantities): bool
    {
        if (!is_array($quantities)) {
            throw new BasketException();
        }
        $basketProducts = BasketsProduct::findBasket((integer)$this->id)->get();
        foreach ($quantities as $key => $quantity) {
            $basketProducts[$key]->quantity = $quantity;
            $basketProducts[$key]->save();
        }
        return true;
    }

    /**
     * @param $query
     * @param int|null $userID
     * @return Builder
     */
    public function scopeFindByUser($query, ?int $userID): Builder
    {
        return $query->where('user_id', $userID);
    }

    /**
     * @return Basket|null
     */
    private static function getBasket(): ?Basket
    {
        $userID = (integer)Auth::id();
        return Basket::findByUser($userID)->first();
    }
}
