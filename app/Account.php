<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class Account extends Model
{
    protected $guarded = [];

    public $timestamps = true;

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function updateData(array $data = []): bool
    {
        $dataUser = array_slice($data, 2, 2);
        $user = Auth::user();
        if (!$user->account()->first()) {
            $this->actionAccount($data, $user);
        } else {
            $this->actionAccount($data, $user,true);
        }
        return $user->updateDetails($dataUser);
    }

    /**
     * @param array $data
     * @param Authenticatable|null $user
     * @param bool $update
     * @return void
     */
    private function actionAccount(array $data, ?Authenticatable $user, bool $update = false): void
    {
        $dataAccount = array_slice($data, 0, 2);
        if ($update) {
            $user->account()->first()->update($dataAccount);
        } else {
            $dataAccount['user_id'] = (integer)$user->id;
            $this->create($dataAccount);
        }
    }
}
