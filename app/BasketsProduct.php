<?php

namespace App;

use App\src\Exception\ColorSizeProductException;
use App\src\traits\BasketTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class BasketsProduct extends Model
{
    protected $guarded = [];

    public $timestamps = true;

    use BasketTrait;

    /**
     * @return BelongsTo
     */
    public function basket(): BelongsTo
    {
        return $this->belongsTo(Basket::class);
    }

    /**
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return BelongsTo
     */
    public function colorSize(): BelongsTo
    {
        return $this->belongsTo(ColorSizeProduct::class);
    }

    /**
     * @param int|null $productID
     * @param int|null $basketID
     * @param int $quantity
     * @param array $features
     * @return bool
     * @throws ColorSizeProductException
     */
    public function add(?int $productID, ?int $basketID, int $quantity = 1, array $features = []): bool
    {
        $colorSizeProduct  = ColorSizeProduct::FindByFeatures($features, $productID)->first();
        $basketProduct = BasketsProduct::findForBasketProduct((integer)$basketID, (integer)$productID, (integer)$colorSizeProduct->id)->first();
        if (is_null($colorSizeProduct)) {
            throw new ColorSizeProductException();
        }

        if (!$basketProduct) {
            $this->basket_id = $basketID;
            $this->product_id = $productID;
            $this->color_size_product_id = (int)$colorSizeProduct->id;
            $this->quantity = $quantity;
            return $this->save();
        }
        $basketProduct->quantity = ($basketProduct->quantity + $quantity);
        return $basketProduct->save();
    }

    /**
     * @param BasketsProduct|null $basketsProduct
     * @param int $quantity
     * @param int $colorSizeProductQuantity
     * @return bool
     */
    public function checkStockOut(?BasketsProduct $basketsProduct = null, int $quantity, int $colorSizeProductQuantity): bool
    {
        $qty = $quantity;
        if (!is_null($basketsProduct)) {
            $qty = ($basketsProduct->quantity + $quantity);
        }
        return ($colorSizeProductQuantity - $qty) < 0;
    }

    /**
     * @param Basket $basket
     * @return void
     */
    public static function remove(Basket $basket): void
    {
        $items = BasketsProduct::FindBasket((int)$basket->id)->get();
        foreach ($items as $item) {
            $item->delete();
        }
    }

    /**
     * @param Basket|null $basket
     * @return array
     */
    public static function getProduct(?Basket $basket): array
    {
        $newBasket = [];
        if (is_null($basket)) {
            return $newBasket;
        }
        $basketProducts = BasketsProduct::findByBasket((integer)$basket->id)->get();
        foreach ($basketProducts as $key => $basketProduct) {
            $newBasket = self::basketToArray($basketProduct, $key, $newBasket);
        }
        return $newBasket;
    }

    /**
     * @param Basket $basket
     * @return array
     */
    public static function getLastProduct(Basket $basket)
    {
        $newBasket = [];
        $basketProduct = BasketsProduct::findByBasket((integer)$basket->id, true)->first();
        $newBasket = self::basketToArray($basketProduct, 0, $newBasket);
        return $newBasket;
    }

    /**
     * @param $query
     * @param int $basketID
     * @param bool $lastBasket
     * @return Builder
     */
    public function scopeFindByBasket($query, int $basketID, bool $lastBasket = false): Builder
    {
        if ($lastBasket) {
            return $query->with('product', 'colorSize')->where('basket_id', $basketID)->orderBy('id', 'DESC');
        }
        return $query->with('product', 'colorSize')->where('basket_id', $basketID);
    }

    /**
     * @param $query
     * @param int $basketID
     * @return Builder
     */
    public function scopeFindBasket($query, int $basketID): Builder
    {
        return $query->where('basket_id', $basketID);
    }

    /**
     * @param $query
     * @param int $basketID
     * @param int $productID
     * @return Builder
     */
    public function scopeFindByBasketAndProduct($query, int $basketID, int $productID): Builder
    {
        return $query->where('basket_id', $basketID)->where('product_id', $productID);
    }

    /**
     * @param $query
     * @param int $basketID
     * @param int $productID
     * @param int $colorSizeProductID
     * @return Builder
     */
    public function scopeFindForBasketProduct($query, int $basketID, int $productID, int $colorSizeProductID): Builder
    {
        return $query->where('basket_id', $basketID)->where('product_id', $productID)->where('color_size_product_id', $colorSizeProductID);
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeLastByUser($query): Builder
    {
        $userID = (integer)Auth::id();
    }
}
