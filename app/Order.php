<?php

namespace App;

use App\src\helpers\Stripe;
use Carbon\Carbon;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Stripe\Exception\ApiErrorException;

class Order extends Model
{

    public $timestamps = true;

    protected $guarded = [];
    /**
     * @var Stripe
     */
    private static Stripe $stripe;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        self::$stripe = new Stripe();
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param Collection $orders
     * @return int
     */
    public static function countProducts(Collection $orders): int
    {
        $newData = [];
        foreach ($orders as $order) {
            $products = $order['products'] ?: [];
            foreach ($products as $key => $product) {
                $newData[] = $product;
            }
        }
        return count($newData + OrderPaypal::countProducts());
    }

    /**
     * @param $query
     * @param int $userID
     * @return Builder
     */
    public function scopeFindUserLast($query, int $userID): Builder
    {
        return $query->where('user_id', $userID)->orderBy('created_at', 'DESC');
    }

    /**
     * @param $value
     * @return array
     */
    public function getProductsAttribute($value): array
    {
        return unserialize($value);
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return Carbon::make($this->created_at)->format('F j, Y');
    }

    /**
     * @param $value
     * @return string
     */
    public function getPaymentIndentCreatedAttribute($value): string
    {
        return Carbon::parse($value)->format('d M, Y');
    }

    /**
     * @param $value
     * @return float|int
     */
    public function getAmountAttribute($value)
    {
        return ($value / 100);
    }

    /**
     * @param Order $order
     * @return string|null
     * @throws ApiErrorException
     */
    public static function getName(Order $order): ?string
    {
        $paymentIntent = self::$stripe->getPaymentIntent($order->payment_indent_id);
        $customer = self::$stripe->getCustomer($paymentIntent->customer);

        return $customer->name;
    }

    /**
     * @return Stripe
     */
    private static function getStripe(): Stripe
    {
        return self::$stripe;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLast($query)
    {
        return $query->orderBy('created_at', 'desc');
    }
}
