<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class Customer extends Model
{
    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $value
     * @return string
     */
    public function getCreatedATAttribute($value): string
    {
        return Carbon::parse($value)->format('d M, Y');
    }

    /**
     * @param \Stripe\Customer $customer
     * @param string $amount
     */
    public function add(\Stripe\Customer $customer, string $amount)
    {
        $this->user_id = (integer)Auth::id();
        $this->customer_id = $customer->id;
        $this->created_at = Carbon::now()->format('Y-m-d H:i:s');
        $this->total_purchase = (int)$amount;
        $this->save();
    }

    /**
     * @param $query
     * @param string $customer
     * @return Builder
     */
    public function scopeFindByCustomer($query, string $customer): Builder
    {
        return $query->where('customer_id', $customer);
    }
}
